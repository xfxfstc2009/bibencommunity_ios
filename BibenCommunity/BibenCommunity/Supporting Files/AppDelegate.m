//
//  AppDelegate.m
//  BY
//
//  Created by 黄亮 on 2018/7/25.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "AppDelegate.h"
#import "BYTabbarViewController.h"
#import "AppDelegate+Config.h"
#import "ZYTabBar.h"
#import "PDWelcomeViewController.h"

@interface AppDelegate ()<UITabBarControllerDelegate,ZYTabBarDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    if (@available(iOS 11.0, *)){
        [[UIScrollView appearance] setContentInsetAdjustmentBehavior:UIScrollViewContentInsetAdjustmentNever];
    }
    
    // 1. 创建视图
    [self mainSetup];
    [self createStartAnimation];
    // 2. 绑定三方授权
    [ShareSDKManager registeShareSDK];
    // 3. 互动直播注册
    [self configILiveSDK];
    // 4. 添加阿里百川IM
    [[AliChatManager sharedInstance] callThisInDidFinishLaunching];
    // 5.
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {

}


- (void)applicationDidEnterBackground:(UIApplication *)application {

}


- (void)applicationWillEnterForeground:(UIApplication *)application {

}


- (void)applicationDidBecomeActive:(UIApplication *)application {

}


- (void)applicationWillTerminate:(UIApplication *)application {
    
}


- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
    if ([viewController.tabBarItem.title isEqualToString:@"消息"]){
        if (![[AccountModel sharedAccountModel]hasLoggedIn]){
            [[AccountModel sharedAccountModel] authorizeWithCompletionHandler:^(BOOL successed) {
                if (successed){
                    [[BYTabbarViewController sharedController] showMessage];
                }
            }];
            return NO;
        } else {
            return YES;
        }
    } else if ([viewController.tabBarItem.title isEqualToString:@"我的"]){
        if (![[AccountModel sharedAccountModel]hasLoggedIn]){
            [[AccountModel sharedAccountModel] authorizeWithCompletionHandler:^(BOOL successed) {
                if (successed){
                    [[BYTabbarViewController sharedController] showCenter];
                }
            }];
            return NO;
        } else {
            return YES;
        }
    }
    return YES;
}

#pragma mark - ZYTabBarDelegate
// 发布按钮跳转
- (void)centerItemBtnClick{
    [BYCommonViewController centerItemBtnClickAction];
}

#pragma mark - 是否是第一次使用
-(void)createStartAnimation{
    // 1. 判断是否第一次使用此版本
    NSString *versionKey = @"CFBundleShortVersionString";
    NSString *lastVersionCode = [[NSUserDefaults standardUserDefaults] objectForKey:versionKey];
    NSString *currentVersionCode = [[NSBundle mainBundle].infoDictionary objectForKey:versionKey];
    if ([lastVersionCode isEqualToString:currentVersionCode]) {                 // 非第一次使用软件
        [UIApplication sharedApplication].statusBarHidden = NO ;
        [self viewInfoManager];
    } else {                                                                    // 第一次使用软件
        [[NSUserDefaults standardUserDefaults] setObject:currentVersionCode forKey:versionKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        // 跳转开场动画
        
        PDWelcomeViewController *welcomeViewController = [[PDWelcomeViewController alloc] init];
        welcomeViewController.startBlock = ^(){
            [self viewInfoManager];
        };
        
        self.window.rootViewController = welcomeViewController;
    }
}

#pragma mark - createMainView
-(void)mainSetup{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor clearColor];
    [self.window makeKeyAndVisible];
}


-(void)viewInfoManager{
    BYTabbarViewController *tabBarController = [BYTabbarViewController sharedController];
    tabBarController.delegate = self;
    ((ZYTabBar *)tabBarController.tabBar).tabbarDelegate = self;
    self.window.rootViewController = tabBarController;
}


@end

