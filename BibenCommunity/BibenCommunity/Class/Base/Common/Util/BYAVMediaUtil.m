//
//  BYAVMediaUtil.m
//  BY
//
//  Created by 黄亮 on 2018/8/13.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYAVMediaUtil.h"
#import <AVFoundation/AVFoundation.h>

@implementation BYAVMediaUtil

+ (BOOL)verifyAVAuthorization:(BYAVMedioType)avMedioType{
    AVAuthorizationStatus videoStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if ((videoStatus == AVAuthorizationStatusRestricted || videoStatus == AVAuthorizationStatusDenied) && avMedioType == BYAVMedioTypeVideo) {
        [BYAlertView showAlertAnimationWithTitle:@"" message:@"您没有相机使用权限,请到 设置->隐私->相机 中开启权限" inView:kCommonWindow];
        return NO;
    }
    AVAuthorizationStatus audioStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio];
    if ((audioStatus == AVAuthorizationStatusRestricted || videoStatus == AVAuthorizationStatusDenied) && avMedioType == BYAVMedioTypeAudio) {
        [BYAlertView showAlertAnimationWithTitle:@"" message:@"您没有麦克风使用权限,请到 设置->隐私->麦克风 中开启权限" inView:kCommonWindow];
        return NO;
    }
    return YES;
}

@end
