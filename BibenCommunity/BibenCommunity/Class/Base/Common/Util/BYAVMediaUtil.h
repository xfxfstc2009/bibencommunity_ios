//
//  BYAVMediaUtil.h
//  BY
//
//  Created by 黄亮 on 2018/8/13.
//  Copyright © 2018年 BY. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger ,BYAVMedioType) {
    BYAVMedioTypeVideo, // 相册
    BYAVMedioTypeAudio  // 麦克风
};

@interface BYAVMediaUtil : NSObject

/**
 校验使用权限

 @param avMedioType 校验类型
 @return bool
 */
+ (BOOL)verifyAVAuthorization:(BYAVMedioType)avMedioType;
@end
