//
//  BYRewardView.m
//  BY
//
//  Created by 黄亮 on 2018/8/9.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYRewardView.h"
#import "UILabel+BYExtension.h"

@interface BYRewardView()<UITextFieldDelegate>

// ** 所加载的界面
@property (nonatomic ,weak) UIView *superView;

// ** 显示内容界面
@property (nonatomic ,strong) UIView *contentView;

// ** 打赏用户名
@property (nonatomic ,strong) NSString *rewardUserName;

// ** 剩余余额
@property (nonatomic ,assign) CGFloat surplusBBT;

// ** 金额输入
@property (nonatomic ,strong) UITextField *textField;

// ** 打赏用户
@property (nonatomic ,strong) UILabel *rewardUserLab;

// ** 当前余额
@property (nonatomic ,strong) UILabel *balanceBBTLab;

// ** 展示输入金额
@property (nonatomic ,strong) UILabel *bbtNumLab;

@end

@implementation BYRewardView

+ (id)initWithShowInView:(UIView *)view
               userName:(NSString *)userName
             surplusBBT:(CGFloat)surplusBBT
{
    BYRewardView *rewardView = [[BYRewardView alloc] initWithFrame:CGRectMake(0, 0, view.bounds.size.width, view.bounds.size.height)];
    rewardView.superView = view;
    rewardView.rewardUserName = userName;
    rewardView.surplusBBT = surplusBBT;
    return  rewardView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setRewardUserName:(NSString *)rewardUserName{
    _rewardUserName = rewardUserName;
    _rewardUserLab.text = rewardUserName;
    
}

- (void)setSurplusBBT:(CGFloat)surplusBBT{
    _surplusBBT = surplusBBT;
    _balanceBBTLab.text = [NSString stringWithFormat:@"当前余额 : %.2f BBT",surplusBBT];
    NSString *string = [NSString stringWithFormat:@"%.2f BBT",surplusBBT];
    NSAttributedString *tagAttributedString = [[NSAttributedString alloc] initWithString:string attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12]}];
    [_balanceBBTLab setTagAttributedString:tagAttributedString];
}

- (void)showAnimation{
    [self.superView addSubview:self];
    self.contentView.layer.transform = CATransform3DMakeScale(1, 1, 1);
    [UIView animateWithDuration:0.2 animations:^{
        self.contentView.layer.transform = CATransform3DMakeScale(1.1, 1.1, 1);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.2 animations:^{
            self.contentView.layer.transform = CATransform3DMakeScale(1, 1, 1);
        }];
    }];
}

- (void)hiddenAnimation{
    [UIView animateWithDuration:0.2 animations:^{
        self.layer.opaque = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)exitBtnAction{
    [self hiddenAnimation];
}

- (void)confirmBtnAction{
    if ([_bbtNumLab.text floatValue] > _surplusBBT) {
        NSLog(@"超出最大金额");
        return;
    }
    _didConfirmHandle([_bbtNumLab.text floatValue]);
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self endEditing:YES];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *rangString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (rangString.length > 7) return NO;
    rangString = rangString.length ? rangString : @"0";

    _bbtNumLab.text = [NSString stringWithFormat:@"%liBBT",(long)[rangString intValue]];
    NSAttributedString *tagAttributedString1 = [[NSAttributedString alloc] initWithString:rangString attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:30],NSForegroundColorAttributeName:[UIColor blackColor]}];
    [_bbtNumLab setTagAttributedString:tagAttributedString1];

    return YES;
}


#pragma mark - configSubView Mehtod
- (void)setContentView{
    // 添加毛玻璃效果
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    effectView.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
    effectView.layer.opacity = 0.7;
    [self addSubview:effectView];
    
    CGFloat rate = 283/375.0;
    CGFloat rateWH = 283/361.0;
    CGFloat width = kCommonScreenWidth*rate;
    UIView *contentView = [[UIView alloc] init];
    [contentView setBackgroundColor:[UIColor whiteColor]];
    [contentView layerCornerRadius:5 size:CGSizeMake(width, width/rateWH)];
    [self addSubview:contentView];
    _contentView = contentView;
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(width/rateWH);
        make.centerX.centerY.mas_equalTo(0);
    }];
    
    // 打赏文案
    UILabel *title = [[UILabel alloc] init];
    title.text = @"打赏";
    title.font = [UIFont systemFontOfSize:17];
    [contentView addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(35);
        make.height.mas_equalTo(18);
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(25);
    }];
    
    // 退出按钮
    UIButton *exitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [exitBtn addTarget:self action:@selector(exitBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [exitBtn setImage:[UIImage imageNamed:@"liveRoom_exit"] forState:UIControlStateNormal];
    [contentView addSubview:exitBtn];
    [exitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(30);
        make.centerY.equalTo(title.mas_centerY).with.offset(0);
        make.right.mas_equalTo(-20);
    }];
    
    for (int i = 0; i < 3; i ++) {
        UIView *lineView = [[UIView alloc] init];
        lineView.backgroundColor = kColorRGBValue(0xF2F2F2);
        [contentView addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(28);
            make.right.mas_equalTo(-28);
            make.height.mas_equalTo(0.5);
            make.top.mas_equalTo(i == 0 ? 65 : (i == 1 ? 119 : 174));
        }];
    }
    
    // 打赏给
    UILabel *rewardLab = [[UILabel alloc] init];
    rewardLab.text = @"打赏给";
    rewardLab.font = [UIFont systemFontOfSize:13];
    rewardLab.textColor = kColorRGBValue(0x3a3a3a);
    [contentView addSubview:rewardLab];
    [rewardLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(32);
        make.top.mas_equalTo(87);
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(14);
    }];
    
    // 打赏给的用户
    UILabel *rewardUserLab = [[UILabel alloc] init];
    rewardUserLab.font = [UIFont systemFontOfSize:13];
    rewardUserLab.textColor = kColorRGBValue(0x3a3a3a);
    rewardUserLab.textAlignment = NSTextAlignmentRight;
    rewardUserLab.text = _rewardUserName;
    [contentView addSubview:rewardUserLab];
    _rewardUserLab = rewardUserLab;
    [rewardUserLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(rewardLab.mas_centerY).with.offset(0);
        make.left.equalTo(rewardLab.mas_right).with.offset(35);
        make.right.mas_equalTo(-50);
        make.height.mas_equalTo(14);
    }];
    
    // 下标图片
    UIImageView *dowmImgView = [[UIImageView alloc] init];
    [dowmImgView setImage:[UIImage imageNamed:@"liveRoom_down"]];
    [contentView addSubview:dowmImgView];
    [dowmImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(dowmImgView.image.size.width);
        make.height.mas_equalTo(dowmImgView.image.size.height);
        make.centerY.equalTo(rewardLab.mas_centerY).with.offset(0);
        make.right.mas_equalTo(-34);
    }];
    
    // 总金额
    UILabel *amountLab = [[UILabel alloc] init];
    amountLab.text = @"总金额";
    amountLab.font = [UIFont systemFontOfSize:13];
    amountLab.textColor = kColorRGBValue(0x3a3a3a);
    [contentView addSubview:amountLab];
    [amountLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(32);
        make.top.mas_equalTo(140);
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(14);
    }];
    
    // 单位（bbt）
    UILabel *unitLab = [[UILabel alloc] init];
    unitLab.text = @"BBT";
    unitLab.font = [UIFont systemFontOfSize:15];
    unitLab.textColor = kColorRGBValue(0x3a3a3a);
    [contentView addSubview:unitLab];
    [unitLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-34);
        make.centerY.equalTo(amountLab.mas_centerY).with.offset(0);
        make.width.mas_equalTo(stringGetWidth(unitLab.text, unitLab.font.pointSize));
        make.height.mas_equalTo(stringGetHeight(unitLab.text, unitLab.font.pointSize));
    }];
    
    // 金额输入
    UITextField *textField = [[UITextField alloc] init];
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"最低打赏金额:1" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12],NSForegroundColorAttributeName:kColorRGBValue(0xD5D5D5)}];
    textField.textAlignment = NSTextAlignmentRight;
    textField.font = [UIFont systemFontOfSize:13];
    textField.keyboardType = UIKeyboardTypeNumberPad;
    textField.delegate = self;
    [contentView addSubview:textField];
    _textField = textField;
    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(unitLab.mas_left).with.offset(-10);
        make.centerY.equalTo(unitLab.mas_centerY).with.offset(0);
        make.left.equalTo(amountLab.mas_right).with.offset(35);
        make.height.mas_equalTo(44);
    }];
    
    // 当前余额
    UILabel *balanceBBTLab = [[UILabel alloc] init];
    balanceBBTLab.text = @"当前余额 : 00.00 BBT";
    balanceBBTLab.font = [UIFont systemFontOfSize:10];
    balanceBBTLab.textColor = kColorRGBValue(0x6E6E6E);
    balanceBBTLab.textAlignment = NSTextAlignmentRight;
    NSAttributedString *tagAttributedString1 = [[NSAttributedString alloc] initWithString:@"00.00 BBT" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12]}];
    [balanceBBTLab setTagAttributedString:tagAttributedString1];
    [contentView addSubview:balanceBBTLab];
    _balanceBBTLab = balanceBBTLab;
    [balanceBBTLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-34);
        make.top.mas_equalTo(187);
        make.left.mas_equalTo(35);
        make.height.mas_equalTo(13);
    }];
    
    // 确定按钮
    UIButton *confirmBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [confirmBtn setBackgroundColor:kColorRGBValue(0xEE4944)];
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:@"确定" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16],NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [confirmBtn setAttributedTitle:attributedString forState:UIControlStateNormal];
    [confirmBtn addTarget:self action:@selector(confirmBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [contentView addSubview:confirmBtn];
    [confirmBtn layerCornerRadius:23 size:CGSizeMake(227, 46)];
    [confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(227);
        make.height.mas_equalTo(46);
        make.bottom.mas_equalTo(-30);
        make.centerX.mas_equalTo(0);
    }];
    
    // 展示输入金额
    UILabel *bbtNumLab = [[UILabel alloc] init];
    bbtNumLab.text = @"0BBT";
    bbtNumLab.textColor = kColorRGBValue(0x3a3a3a);
    bbtNumLab.font = [UIFont systemFontOfSize:15];
    bbtNumLab.textAlignment = NSTextAlignmentCenter;
    NSAttributedString *tagAttributedString = [[NSAttributedString alloc] initWithString:@"0" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:30],NSForegroundColorAttributeName:[UIColor blackColor]}];
    [bbtNumLab setTagAttributedString:tagAttributedString];
    [contentView addSubview:bbtNumLab];
    _bbtNumLab = bbtNumLab;
    [bbtNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.bottom.equalTo(confirmBtn.mas_top).with.offset(-35);
        make.left.mas_equalTo(35);
        make.right.mas_equalTo(-35);
    }];
    
}


@end
