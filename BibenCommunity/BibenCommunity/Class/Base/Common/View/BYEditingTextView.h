//
//  BYEditingTextView.h
//  BY
//
//  Created by 黄亮 on 2018/9/11.
//  Copyright © 2018年 BY. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger ,BYEditingTextViewType) {
    BYEditingTextViewTypeNormal = 0,
    BYEditingTextViewTypeIntro // 简介编辑
};

@interface BYEditingTextView : UIView

/** 输入字符最大限制 (可不设置，带默认normal:15  intro:500)*/
@property (nonatomic ,assign) NSInteger maxlength;

@property (nonatomic ,copy) NSString *placeholder;

@property (nonatomic ,strong) UITextView *textView;

/** 提交信息时回调参数 */
@property (nonatomic ,copy) void (^didConfirmHandle)(NSString *text);

/** 初始化 */
- (instancetype)initWithFathureView:(UIView *)fathureView editingType:(BYEditingTextViewType)editingType;

/**
 显示动画
 */
- (void)showAnimation;

@end
