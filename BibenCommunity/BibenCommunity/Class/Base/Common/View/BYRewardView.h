//
//  BYRewardView.h
//  BY
//
//  Created by 黄亮 on 2018/8/9.
//  Copyright © 2018年 Belief. All rights reserved.
//  打赏

#import <UIKit/UIKit.h>

@interface BYRewardView : UIView

// ** 确定打赏回调bbt金额 */
@property (nonatomic ,copy) void (^didConfirmHandle)(CGFloat bbt);

/**
 初始化创建

 @param view 加载到的view
 @param userName 打赏的用户名
 @param surplusBBT 剩余BBT
 @return return value description
 */
+ (id)initWithShowInView:(UIView *)view
                userName:(NSString *)userName
              surplusBBT:(CGFloat)surplusBBT;

/**
 开始动画显示
 */
- (void)showAnimation;
@end
