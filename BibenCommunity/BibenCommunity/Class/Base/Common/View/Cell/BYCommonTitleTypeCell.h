//
//  BYCommonTitleTypeCell.h
//  BY
//
//  Created by 黄亮 on 2018/9/7.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCommonTableViewCell.h"

@interface BYCommonTitleTypeCell : BYCommonTableViewCell

/** 左侧标题 */
@property (nonatomic ,strong) UILabel *titleLab;

/** 输入 */
@property (nonatomic ,strong) UITextField *textField;

/** 右侧图标 (例箭头指标)*/
@property (nonatomic ,strong) UIImageView *rightImageView;

/** 右侧标题 */
@property (nonatomic ,strong) UILabel *rightTitleLab;

/** 右侧图片左侧的图片(例：用户头像) */
@property (nonatomic ,strong) PDImageView *rightLogoImageView;


/**
 设置标题

 @param title title description
 */
- (void)setTitle:(NSString *)title;

- (void)setTextFieldText:(NSString *)text;

- (void)setPlaceholder:(NSString *)placeholder;

- (void)setRightImage:(UIImage *)image;

- (void)setRightTitle:(NSString *)title;

- (void)setRightLogoImage:(UIImage *)image;
- (void)serRightLogoImageUrl:(NSString *)url;

- (void)showBottomLine:(BOOL)show;
@end
