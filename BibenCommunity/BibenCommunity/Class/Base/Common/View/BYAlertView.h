//
//  BYAlertView.h
//  BY
//
//  Created by 黄亮 on 2018/8/10.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger ,BYAlertViewStyle) {
    BYAlertViewStyleAlert = 0,
    BYAlertViewStyleActionSheet  // 待后面完善
};

@interface BYAlertView : UIView

@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSString *message;
@property (nonatomic, readonly) BYAlertViewStyle style;

/**
 初始化创建
 
 @param title 标题
 @param message 内容
 @param view 父视图
 @param alertViewStyle 类型
 @return return value description
 */
+ (id)initWithTitle:(NSString *)title message:(NSString *)message inView:(UIView *)view alertViewStyle:(BYAlertViewStyle)alertViewStyle;


/**
 快速创建显示alert类型弹窗
 
 @param title 标题
 @param message 内容
 @param view 父视图
 @return return value description
 */
+ (id)showAlertAnimationWithTitle:(NSString *)title message:(NSString *)message inView:(UIView *)view;

/**
 添加取消按钮标题（可不设置采用默认）
 
 @param title 标题
 */
- (void)addActionCancleTitle:(NSString *)title handle:(void(^)())handle;

/**
 添加自定义按钮标题
 
 @param title 标题
 */
- (void)addActionCustomTitle:(NSString *)title handle:(void(^)())handle;

/**
 新增按钮并可自定义字体
 
 @param attributedString attributedString description
 */
- (void)addActionCancleAttributedString:(NSAttributedString *)attributedString handle:(void(^)())handle;
- (void)addActionCustomAttributedString:(NSAttributedString *)attributedString handle:(void(^)())handle;


- (void)showAnimation;





#pragma mark - mark ares
+(instancetype)sharedAlertView;                                                               /**< 单例*/

-(void)showAlertSignWithTitle:(NSInteger )bbt block:(void(^)())block;


@end
NS_ASSUME_NONNULL_END
