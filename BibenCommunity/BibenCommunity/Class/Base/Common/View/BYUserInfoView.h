//
//  BYUserInfoView.h
//  BY
//
//  Created by 黄亮 on 2018/8/10.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BYCommonUserModel;
@interface BYUserInfoView : UIView

// ** 举报事件回调 */
@property (nonatomic ,copy) void(^reportActionHandle)(NSInteger userId);

// ** 主页事件回调 */
@property (nonatomic ,copy) void(^homePageActionHandle)(NSInteger userId);

// ** 关注事件回调 */
@property (nonatomic ,copy) void(^attentionActionHandle)(NSInteger userId);

// ** 用户信息(再次赋值用于修改数据) */
@property (nonatomic ,strong) BYCommonUserModel *userModel;


/**
 初始化创建
 
 @param view 加载到的view
 @param userModel 用户模型
 @return return value description
 */
+ (id)initWithShowInView:(UIView *)view
               userModel:(BYCommonUserModel *)userModel;

/**
 开始动画显示
 */
- (void)showAnimation;


/**
 动画消失
 */
- (void)hiddenAnimation;

@end
