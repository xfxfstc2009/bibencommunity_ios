//
//  BYCommonModel.h
//  BY
//
//  Created by 黄亮 on 2018/7/26.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BYCommonModel : NSObject

/** 所加载的cell ClassName */
@property (nonatomic ,strong) NSString *cellString;

/** cell高度 */
@property (nonatomic ,assign) CGFloat cellHeight;

/** 标题 */
@property (nonatomic ,strong) NSString *title;

// ** 设置moke数据方法
- (void)getMokeData;

@end
