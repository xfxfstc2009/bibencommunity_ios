//
//  BYCommonTableViewCell.h
//  BY
//
//  Created by 黄亮 on 2018/7/31.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableViewCell+BYExtension.h"

@interface BYCommonTableViewCell : UITableViewCell

/** indexPath,自己赋值 */
@property (nonatomic ,strong) NSIndexPath *indexPath;


/**
 用于向tableView发送内部子控件的事件
 在cell子控件事件调用处发起此方法即可
 (或使用代理的方式，代理的方式通过重写setCellDelegate降cell的delegate传给delegate)

 @param actionName 事件名
 @param param 参数
 @param indexPath indexPath
 */
- (void)sendActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath;

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath;
- (void)setCellDelegate:(id)delegate;
@end
