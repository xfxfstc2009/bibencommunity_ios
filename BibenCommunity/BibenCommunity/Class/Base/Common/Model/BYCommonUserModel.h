//
//  BYCommonUserModel.h
//  BY
//
//  Created by 黄亮 on 2018/8/10.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import <Foundation/Foundation.h>

#define K_C_U_M [BYCommonUserModel shareObject]

@interface BYCommonUserModel : NSObject<NSCopying>

// ** 用户头像地址 */
@property (nonatomic ,strong) NSString *user_headImgURL;

// ** 用户名 */
@property (nonatomic ,strong) NSString *user_name;

// ** 用户id */
@property (nonatomic ,assign) NSInteger user_id;

// ** 用户签名 */
@property (nonatomic ,strong) NSString *user_sign;

// ** 关注数 */
@property (nonatomic ,assign) NSInteger user_attentionNum;

// ** 粉丝数 */
@property (nonatomic ,assign) NSInteger user_fansNum;

// ** 直播数 */
@property (nonatomic ,assign) NSInteger user_liveNum;

// ** 学堂数 */
@property (nonatomic ,assign) NSInteger user_schoolNum;

// ** 直播房间号 */
@property (nonatomic ,strong) NSString *user_roomId;
+ (instancetype)shareObject;

@end
