//
//  NSDate+BYExtension.m
//  BY
//
//  Created by 黄亮 on 2018/8/11.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "NSDate+BYExtension.h"

@implementation NSDate (BYExtension)

+ (id)shareManager{
    static NSDate *data;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        data = [[NSDate alloc] init];
    });
    return data;
}

/**
 *  获获取当前客户端的逻辑日历
 *
 *  @return 当前客户端的逻辑日历
 */
+ (NSCalendar *) currentCalendar {
    static NSCalendar *sharedCalendar = nil;
    if (!sharedCalendar) {
        sharedCalendar = [NSCalendar autoupdatingCurrentCalendar];
    }
    return sharedCalendar;
}

- (NSDateFormatter *)by_getNSDateFormatter{
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
    });
    return formatter;
}

+ (NSDate *)by_date{
    NSDate *date = [NSDate date];
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate: date];
    NSDate *localeDate = [date  dateByAddingTimeInterval: interval];
    return localeDate;
}

+ (NSDate *)by_dateFromString:(NSString *)string dateformatter:(NSString *)dateformatter{
    NSDateFormatter *formatter = [[self shareManager] by_getNSDateFormatter];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [formatter setDateFormat:dateformatter];
    return [formatter dateFromString:string];
}

+ (NSString *)by_stringFromDate:(NSDate *)date dateformatter:(NSString *)dateformatter{
    NSDateFormatter *formatter = [[self shareManager] by_getNSDateFormatter];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [formatter setDateFormat:dateformatter];
    return [formatter stringFromDate:date];
}

+ (NSInteger )by_startTime:(NSDate *)startTime endTime:(NSDate *)endTime{
    // 2.创建日历
    NSInteger timeDifference = 0;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yy-MM-dd HH:mm:ss"];
   
    NSTimeInterval oldTime = [startTime timeIntervalSince1970];
    NSTimeInterval newTime = [endTime timeIntervalSince1970];
    timeDifference = newTime - oldTime;
    return timeDifference;
}

+ (NSDateComponents *)getCurrentDateComponents{
    NSCalendar *calendar = [NSDate currentCalendar];
    NSDate *date = [NSDate date];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:date];
    return components;
}

+ (NSInteger)getCurrentMonth{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *date = [NSDate date];
    NSDateComponents *components = [calendar components:NSCalendarUnitMonth fromDate:date];
    return components.month;
}

+ (NSInteger)getDayNumInMonth:(NSInteger)month{
    NSCalendar *calender = [NSCalendar currentCalendar];
    NSDateComponents *components = [calender components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
    NSDate *date = [NSDate by_dateFromString:[NSString stringWithFormat:@"%d-%d-01",(int)components.year,(int)month] dateformatter:@"yyyy-MM-dd"];
    NSRange range = [calender rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:date];
    return range.length;
}

+ (NSArray *)getSurplusMonth{
    NSInteger currentMonth = [NSDate getCurrentMonth];
    NSMutableArray *allMonth = [NSMutableArray array];
    for (int i = 1; i < 13; i++) {
        [allMonth addObject:@(i)];
    }
    NSRange range = NSMakeRange(0, currentMonth - 1);
    [allMonth removeObjectsInRange:range];
    return allMonth;
}

@end
