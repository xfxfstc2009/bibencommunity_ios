//
//  UITableViewCell+BYExtension.m
//  BY
//
//  Created by 黄亮 on 2018/8/14.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "UITableViewCell+BYExtension.h"

static const char tableViewKey;
@implementation UITableViewCell (BYExtension)

- (void)setTableView:(UITableView *)tableView{
    if (objc_getAssociatedObject(self, &tableViewKey)) return;
    objc_setAssociatedObject(self, &tableViewKey, tableView, OBJC_ASSOCIATION_ASSIGN);
}

- (UITableView *)tableView{
    return objc_getAssociatedObject(self, &tableViewKey);
}
@end
