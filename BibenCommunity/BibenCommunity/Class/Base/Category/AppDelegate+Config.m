//
//  AppDelegate+Config.m
//  BY
//
//  Created by 黄亮 on 2018/8/23.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "AppDelegate+Config.h"
#import <ILiveSDK/ILiveCoreHeader.h>
#import "BYIMManager.h"
#import "BYALIMManager.h"

@interface AppDelegate ()<QAVLogger>

@end

@implementation AppDelegate (Config)


- (void)configTIM{
    [[BYIMManager sharedInstance] initSdk];
}

- (void)configILiveSDK{
    // 设置非云上环境(必须在初始化sdk之前设置)
    [[ILiveSDK getInstance] setChannelMode:E_ChannelIMSDK withHost:@""];
    [[ILiveSDK getInstance] initSdk:kTXLiveAppid accountType:kTXLiveAccountType];
    [[ILiveSDK getInstance] setConsoleLogPrint:kIsOpenLog];
//    TIMManager *immanager = [[ILiveSDK getInstance] getTIMManager];
//    [immanager disableIM];
    [QAVAppChannelMgr setExternalLogger:self];
    PDLog(@"互动直播SDK :%@",[[ILiveSDK getInstance] getVersion]);
    PDLog(@"腾讯云IMSDK :%@",[[TIMManager sharedInstance] GetVersion]);

}

- (void)configALIMSDK{
    [[BYALIMManager shareInstance] initSDK];
}

#pragma mark - avsdk日志代理
- (BOOL)isLogPrint
{
    return NO;
}

- (NSString *)getLogPath {
    return [[TIMManager sharedInstance] getLogPath];
}

@end
