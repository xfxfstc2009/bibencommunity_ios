//
//  NSString+BYExtension.h
//  BY
//
//  Created by 黄亮 on 2018/7/26.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (BYExtension)

/**
 计算string展示的Size
 
 @param font 字体大小
 @param maxWidth 展示的最大宽度
 @return 计算的Size
 */
- (CGSize )getStringSizeWithFont:(UIFont *)font maxWidth:(CGFloat)maxWidth;

/**
 格式化integer返回相对于的x万、x千、x百

 @param num 输入
 @return 格式化格式
 */
- (NSString *)formatIntegerNum:(NSInteger)num;

@end
