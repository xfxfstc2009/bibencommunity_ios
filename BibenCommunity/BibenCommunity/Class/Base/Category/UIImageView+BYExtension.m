//
//  UIImageView+BYExtension.m
//  BY
//
//  Created by 黄亮 on 2018/7/27.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "UIImageView+BYExtension.h"
#import "UIImage+BYExtension.h"

@implementation UIImageView (BYExtension)




- (void)setImageColor:(UIColor *)color{
    self.image = [UIImage imageWithColor:color];
}

- (void)drawRect:(CGRect)rect{
    [super drawRect:rect];
}
@end
