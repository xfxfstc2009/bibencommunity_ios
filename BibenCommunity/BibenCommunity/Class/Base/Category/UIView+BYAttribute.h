//
//  UIView+BYAttribute.h
//  BY
//
//  Created by 黄亮 on 2018/8/17.
//  Copyright © 2018年 BY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (BYAttribute)

// **  创建 [[self alloc] init]
+ (instancetype)by_init;

@end

@interface UILabel (BYAttribute)

- (void)setBy_font:(CGFloat)fontSize;

/**
 简易设置attributedText
 title设置在字典中key为title
 @param attributes key:title ... value :...
 */
- (void)setBy_attributedText:(NSDictionary *)attributes;

@end

@interface UIButton (BYAttribute)

+ (instancetype)by_buttonWithCustomType;
- (void)setBy_imageName:(NSString *)imageName forState:(UIControlState)state;

/**
 简易设置AttributedTitle
 title设置在字典中key为title
 @param attributes key:title ... value :...
 */
- (void)setBy_attributedTitle:(NSDictionary *)attributes forState:(UIControlState)state;
@end

@interface UIImageView (BYAttribute)

- (void)by_setImageName:(NSString *)imageName;

@end

