//
//  UITextView+BYExtension.m
//  BY
//
//  Created by 黄亮 on 2018/9/11.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "UITextView+BYExtension.h"

@interface UITextView()

@property (nonatomic ,strong) UILabel *placeholderLab;

@end
static const char placeholderLabkey;
static const char placeholderkey;
@implementation UITextView (BYExtension)

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)registerNSNotification{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textViewTextDidChage_notic:) name:UITextViewTextDidChangeNotification object:nil];
}

- (void)setPlaceholderLab:(UILabel *)placeholderLab{
    objc_setAssociatedObject(self, &placeholderLabkey, placeholderLab, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UILabel *)placeholderLab{
    UILabel *lab = objc_getAssociatedObject(self, &placeholderLabkey);
    if (!lab) {
        lab = [[UILabel alloc] init];
        [lab setBy_font:13];
        lab.textColor = kColorRGBValue(0x969696);
        objc_setAssociatedObject(self, &placeholderLabkey, lab, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return lab;
}

//- (void)setPlaceholder:(NSString *)placeholder{
//    objc_setAssociatedObject(self, &placeholderkey, placeholder, OBJC_ASSOCIATION_COPY_NONATOMIC);
//    self.placeholderLab.text = placeholder;
//    [self addSubview:self.placeholderLab];
//}

- (void)setBy_placeholder:(NSString *)by_placeholder{
    objc_setAssociatedObject(self, &placeholderkey, by_placeholder, OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.placeholderLab.text = by_placeholder;
    [self addSubview:self.placeholderLab];
}

- (NSString *)by_placeholder{
    return objc_getAssociatedObject(self, &placeholderkey);
}

- (void)textViewTextDidChage_notic:(NSNotification *)notic{
    [self setNeedsLayout];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.placeholderLab.alpha = self.text.length ? NO : YES;
    if (self.placeholderLab.text.length) {
        self.placeholderLab.frame = CGRectMake(3, 7, stringGetWidth(self.by_placeholder, self.placeholderLab.font.pointSize), stringGetHeight(self.by_placeholder, self.placeholderLab.font.pointSize));
    }
}

@end
