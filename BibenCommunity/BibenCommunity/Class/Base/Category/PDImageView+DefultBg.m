//
//  PDImageView+DefultBg.m
//  BibenCommunity
//
//  Created by 随风 on 2018/9/21.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "PDImageView+DefultBg.h"

@implementation PDImageView (DefultBg)

- (BOOL)verifyIsLocalDefultBg:(NSString *)urlString{
    for (int i = 0; i < 6; i ++) {
        NSString *liveRoomBgImgName = [NSString stringWithFormat:@"defult_liveroom_bg_%02d",i];
        NSString *liveBgImgName = [NSString stringWithFormat:@"defult_live_bg_%02d",i];
        if ([urlString isEqualToString:liveRoomBgImgName] ||
            [urlString isEqualToString:liveBgImgName]) {
            return YES;
        }
    }
    return NO;
}

@end
