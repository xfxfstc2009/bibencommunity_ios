//
//  UIBarButtonItem+BYBarButtonItem.h
//  BY
//
//  Created by 黄亮 on 2018/8/3.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,BYBarButtonItemType){
    BYBarButtonItemTypeLeft,
    BYBarButtonItemTypeRight
} ;

@interface UIBarButtonItem (BYBarButtonItem)


/**
 初始化创建只有image的baritem

 @param imageName imageName
 @param target target
 @param action action descripion
 @param type 类型
 @return return value
 */
+ (instancetype)initWithImageName:(NSString *)imageName
                           target:(id)target
                           action:(SEL)action
                             type:(BYBarButtonItemType)type;

/**
 初始化创建只有title的barItem

 @param title title
 @param target target
 @param action action
 @param type 类型
 @return return value
 */
+ (instancetype)initWithTitle:(NSString *)title
                       target:(id)target
                       action:(SEL)action
                         type:(BYBarButtonItemType)type;
;
@end
