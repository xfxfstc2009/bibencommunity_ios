//
//  NSString+BYExtension.m
//  BY
//
//  Created by 黄亮 on 2018/7/26.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "NSString+BYExtension.h"

@implementation NSString (BYExtension)

- (CGSize )getStringSizeWithFont:(UIFont *)font maxWidth:(CGFloat)maxWidth{
    CGSize size;
    if (kCommonGetSystemVersion() >= 7) {
        size = [self boundingRectWithSize:CGSizeMake  (maxWidth, MAXFLOAT) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil].size;
    }
    else{
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        size = [self sizeWithFont:font constrainedToSize:CGSizeMake(maxWidth, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    }
    return size;
}

- (NSString *)formatIntegerNum:(NSInteger)num{
    if (num > 9999) {
        CGFloat numF = num/1000.0;
        return [NSString stringWithFormat:@"%.f万",numF];
    }
    return [NSString stringWithFormat:@"%li",num];
}

@end
