//
//  UILabel+BYExtension.m
//  BY
//
//  Created by 黄亮 on 2018/8/9.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "UILabel+BYExtension.h"

@implementation UILabel (BYExtension)

- (void)setTagAttributedString:(NSAttributedString *)attributedString{
    NSString *string = self.text;
    NSRange rang = [string rangeOfString:attributedString.string];
    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:self.text attributes:@{NSFontAttributeName:self.font,NSForegroundColorAttributeName:self.textColor}];
    NSRange tagRang = NSMakeRange(0, attributedString.string.length);
    NSDictionary *tagAttributedStringKey = [attributedString attributesAtIndex:0 effectiveRange:&tagRang];
    [mutableAttributedString addAttributes:tagAttributedStringKey range:rang];
    self.attributedText = mutableAttributedString;
}

@end
