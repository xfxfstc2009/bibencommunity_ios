//
//  UIView+BYExtension.m
//  BY
//
//  Created by 黄亮 on 2018/8/9.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "UIView+BYExtension.h"

@implementation UIView (BYExtension)

- (void)layerCornerRadius:(CGFloat)radius size:(CGSize)size{
//    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, size.width, size.height) byRoundingCorners:UIRectCornerAllCorners cornerRadii:CGSizeMake(radius,radius)];
//    CAShapeLayer *maskLayer = [[CAShapeLayer alloc]init];
//    //设置大小
//    maskLayer.frame = CGRectMake(0, 0, size.width, size.height);
//    //设置图形样子
//    maskLayer.path = maskPath.CGPath;
//    self.layer.mask = maskLayer;
//    [self layoutIfNeeded];
    [self layerCornerRadius:radius borderWidth:1 fillColor:nil borderColor:nil size:size];
}

- (void)layerCornerRadius:(CGFloat)radius borderWidth:(CGFloat)borderWidth borderColor:(UIColor *)borderColor size:(CGSize)size{
    [self layerCornerRadius:radius borderWidth:borderWidth fillColor:self.backgroundColor borderColor:borderColor size:size];
}

- (void)layerCornerRadius:(CGFloat)radius borderWidth:(CGFloat)borderWidth fillColor:(UIColor *)fillColor borderColor:(UIColor *)borderColor size:(CGSize)size{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, size.width, size.height) byRoundingCorners:UIRectCornerAllCorners cornerRadii:CGSizeMake(radius,radius)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc]init];
    //设置大小
    maskLayer.frame = CGRectMake(0, 0, size.width, size.height);
    if (!borderColor && !fillColor) {
        maskLayer.fillColor = self.backgroundColor.CGColor;
        maskLayer.path = maskPath.CGPath;
        [self setBackgroundColor:[UIColor clearColor]];
        [self.layer addSublayer:maskLayer];
        [self.layer insertSublayer:maskLayer atIndex:0];
//        self.layer.mask = maskLayer;
        [self layoutIfNeeded];
        return;
    }
    //设置图形样子
    maskLayer.path = maskPath.CGPath;
    maskLayer.fillColor = fillColor ? fillColor.CGColor : self.backgroundColor.CGColor;
    maskLayer.strokeColor = borderColor.CGColor;
    maskLayer.lineWidth = borderWidth;
    [self setBackgroundColor:[UIColor clearColor]];
    [self.layer addSublayer:maskLayer];
    [self.layer insertSublayer:maskLayer atIndex:0];
    [self layoutIfNeeded];
}

- (void)layerCornerRadius:(CGFloat)radius byRoundingCorners:(UIRectCorner)corners size:(CGSize)size{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, size.width, size.height) byRoundingCorners:corners cornerRadii:CGSizeMake(radius,radius)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc]init];
    //设置大小
    maskLayer.frame = CGRectMake(0, 0, size.width, size.height);
    maskLayer.fillColor = self.backgroundColor.CGColor;
    //设置图形样子
    maskLayer.path = maskPath.CGPath;
    [self setBackgroundColor:[UIColor clearColor]];
    [self.layer insertSublayer:maskLayer atIndex:0];
    [self.layer addSublayer:maskLayer];
}


@end
