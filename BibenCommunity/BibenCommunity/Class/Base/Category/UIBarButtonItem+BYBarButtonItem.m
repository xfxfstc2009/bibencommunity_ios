//
//  UIBarButtonItem+BYBarButtonItem.m
//  BY
//
//  Created by 黄亮 on 2018/8/3.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "UIBarButtonItem+BYBarButtonItem.h"

@implementation UIBarButtonItem (BYBarButtonItem)

+ (instancetype)initWithImageName:(NSString *)imageName
                           target:(id)target
                           action:(SEL)action
                             type:(BYBarButtonItemType)type  {
    UIButton * barbutton = [[UIButton alloc]init];
    if (@available(iOS 11.0,*)) {
        [barbutton setContentMode:UIViewContentModeScaleToFill];
        if (type == BYBarButtonItemTypeLeft) {
            [barbutton setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 70)];
        }
        else
        {
            [barbutton setContentEdgeInsets:UIEdgeInsetsMake(0, 70, 0, 0)];
        }
    }
    [barbutton setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [barbutton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    [barbutton sizeToFit];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc]initWithCustomView:barbutton];
    return barButtonItem;

}

+ (instancetype)initWithTitle:(NSString *)title
                       target:(id)target
                       action:(SEL)action
                         type:(BYBarButtonItemType)type{
    UIButton * barbutton = [[UIButton alloc]init];
    if (@available(iOS 11.0,*)) {
        [barbutton setContentMode:UIViewContentModeScaleToFill];
        [barbutton setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 5, 70)];
        if (type == BYBarButtonItemTypeLeft) {
            [barbutton setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 5, 70)];
            barbutton.titleEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 0);
        }
        else
        {
            [barbutton setContentEdgeInsets:UIEdgeInsetsMake(0, 70, 5, 0)];
//            barbutton.titleEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 0);

        }
    }
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:kColorRGBValue(0xed4a45)};
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:title attributes:attributes];
    [barbutton setAttributedTitle:attributedString forState:UIControlStateNormal];
    [barbutton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    [barbutton sizeToFit];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc]initWithCustomView:barbutton];
    return barButtonItem;

}
@end
