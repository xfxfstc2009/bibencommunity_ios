//
//  AppDelegate+Config.h
//  BY
//
//  Created by 黄亮 on 2018/8/23.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (Config)


/**
 配置云通讯
 */
- (void)configTIM;

/**
 配置互动直播
 */
- (void)configILiveSDK;

/**
 配置阿里百川IM
 */
- (void)configALIMSDK;
@end
