//
//  BYSchoolController.m
//  BY
//
//  Created by 黄亮 on 2018/7/25.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYSchoolController.h"
#import "BYSchoolViewModel.h"

@interface BYSchoolController ()


@end

@implementation BYSchoolController

- (Class)getViewModelClass{
    return [BYSchoolViewModel class];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.hidden = NO;
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"学堂";
}



@end
