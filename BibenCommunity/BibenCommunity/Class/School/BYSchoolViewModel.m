//
//  BYSchoolViewModel.m
//  BY
//
//  Created by 黄亮 on 2018/8/9.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYSchoolViewModel.h"
#import "BYVideoPlayController.h"
#import "BYUploadVodController.h"
#import "BYLivePlayController.h"
#import "BYCreatLiveController.h"
#import "BYILiveRoomController.h"
#import "BYPPTRoomController.h"
#import "BYNewLiveController.h"
#import "BYLiveRoomSetController.h"
#import "BYLiveSetController.h"
#import "BYLiveRoomHomeController.h"
#import "BYLiveHostController.h"

#import <ILiveSDK/ILiveCoreHeader.h>

#import "BYRewardView.h"
#import "BYUserInfoView.h"
#import "BYAlertView.h"

#import "BYCommonLiveModel.h"
#import "BYIMManager.h"
#import "BYALIMManager.h"
#import "BYCommonUserModel.h"
#import "BYLiveHomeVideoModel.h"
#import "BYLiveConfig.h"
#import "BYLiveHomeRequest.h"
#import "NSDate+BYExtension.h"

@interface BYSchoolViewModel()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic ,strong) NSArray *titles;

@property (nonatomic ,strong) BYRewardView *rewardView;

@property (nonatomic ,strong) BYUserInfoView *userInfoView;

@property (nonatomic ,strong) BYCommonUserModel *userModel;

@end

@implementation BYSchoolViewModel

- (void)setContentView{
    
    
    _titles = @[@"视频播放",@"视频上传",@"直播播放",@"打赏",@"用户信息弹窗",@"alert弹窗",@"进入直播间",@"互动直播",@"聊天室",@"新建直播",@"直播间设置",@"直播设置",@"直播间",@"直播操作间"];
    
    UITableView *tableView = [[UITableView alloc] init];
    tableView.delegate = self;
    tableView.dataSource = self;
    [S_V_VIEW addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
}

// 加入房间
- (void)joinRoom{
//    BYLiveHomeVideoModel *model = [[BYLiveHomeVideoModel alloc] getSingleMokeModel];
//    @weakify(self);
//    [[BYIMManager sharedInstance] login:^{
//        @strongify(self);
//        @weakify(self);
//        [[BYIMManager sharedInstance] joinAVChatRoomGroupId:@"6666" succ:^{
//            @strongify(self);
//            NSLog(@"进入聊天室成功");
//            BYPPTRoomController *pPTRoomController = [[BYPPTRoomController alloc] initWithModel:model];
//            pPTRoomController.title = stringFormatInteger(K_C_U_M.user_id);
//            [S_V_NC pushViewController:pPTRoomController animated:YES];
//        } fail:^{
//            
//        }];
//    }];
}


#pragma mark -kkkk
- (void)createRoom{
    //    K_C_U_M.user_id = 6666;
    //    K_C_U_M.user_roomId = @"6666";
//    BYLiveHomeVideoModel *model = [[BYLiveHomeVideoModel alloc] getSingleMokeModel];
//    //    @weakify(self);
//    //    [[BYIMManager sharedInstance] login:^{
//    //        @strongify(self);
//    //        @weakify(self);
//    //        [[BYIMManager sharedInstance] createAVChatRoomGroupId:@"6666" succ:^{
//    //            @strongify(self);
//    BYPPTRoomController *pPTRoomController = [[BYPPTRoomController alloc] initWithModel:model];
//    pPTRoomController.title = [NSString stringWithFormat:@"房主:%@",K_C_U_M.user_roomId];
//    [S_V_NC pushViewController:pPTRoomController animated:YES];
    //        } fail:nil];
    //    }];
}

#pragma mark - UITableViewDelegate/DataScoure
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _titles.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.textLabel.text = _titles[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
        {
            BYVideoPlayController *videoPlayController = [[BYVideoPlayController alloc] init];
            [S_V_NC pushViewController:videoPlayController animated:YES];
        }
            break;
        case 1:
        {
            BYUploadVodController *uploadVodController = [[BYUploadVodController alloc] init];
            [S_V_NC pushViewController:uploadVodController animated:YES];
        }
            break;
        case 2:
        {
            BYLivePlayController *livePlayController = [[BYLivePlayController alloc] init];
            [S_V_NC pushViewController:livePlayController animated:YES];
        }
            break;
        case 3:
        {
            BYRewardView *rewardView = [BYRewardView initWithShowInView:S_V_VIEW userName:@"测试用户" surplusBBT:330.00];
            [rewardView showAnimation];
        }
            break;
        case 4:
        {
            [self.userInfoView showAnimation];
        }
            break;
        case 5:
        {
            BYAlertView *alertView = [BYAlertView initWithTitle:@"温馨提示" message:@"是否现在就开始直播？" inView:S_V_VIEW alertViewStyle:BYAlertViewStyleAlert];
            [alertView addActionCancleTitle:@"取消" handle:^{
                NSLog(@"点击取消");
            }];
            [alertView addActionCustomTitle:@"确定" handle:^{
                NSLog(@"点击确定");
            }];
            [alertView showAnimation];
        }
        case 6: // 进入直播间
        {
            
            // 新建成功后暂跳互动直播
            [BYCommonLiveModel shareManager].begin_time = [NSDate by_stringFromDate:[NSDate by_date] dateformatter:K_D_F];
            BYLiveConfig *config = [[BYLiveConfig alloc] init];
            config.isHost = NO;
            config.roomId = @"11111";
            BYILiveRoomController *ilivewRoomController = [[BYILiveRoomController alloc] initWithConfig:config];
            [S_V_NC pushViewController:ilivewRoomController animated:YES];
            //            K_C_U_M.user_id = 6666;
            //            if ([[ILiveLoginManager getInstance] getSig].length) {
            //                BYLiveConfig *liveConfig = [[BYLiveConfig alloc] init];
            //                liveConfig.isHost = NO;
            //                liveConfig.roomId = @"7001";
            //                BYILiveRoomController *iliveRoomController = [[BYILiveRoomController alloc] initWithConfig:liveConfig];
            //                [S_V_NC pushViewController:iliveRoomController animated:YES];
            //                return;
            //            }
            //            @weakify(self);
            //            [[BYLiveHomeRequest alloc] loadRequestGetILiveLoginSig:K_C_U_M.user_id successBlock:^(id object) {
            //                @strongify(self);
            //                @weakify(self);
            //                [[ILiveLoginManager getInstance] iLiveLogin:stringFormatInteger(K_C_U_M.user_id) sig:object succ:^{
            //                    @strongify(self);
            //                    BYLiveConfig *liveConfig = [[BYLiveConfig alloc] init];
            //                    liveConfig.isHost = NO;
            //                    liveConfig.roomId = @"7001";
            //                    BYILiveRoomController *iliveRoomController = [[BYILiveRoomController alloc] initWithConfig:liveConfig];
            //                    [S_V_NC pushViewController:iliveRoomController animated:YES];
            //                    NSLog(@"登录成功了！！！！！！");
            //                } failed:^(NSString *module, int errId, NSString *errMsg) {
            //                    showToastView([NSString stringWithFormat:@"登录失败了！:%@",errMsg], S_V_VIEW);
            //                }];
            //            } faileBlock:^(NSError *error) {
            //                showToastView(@"获取直播登录sig失败！", S_V_VIEW);
            //            }];
            
        }
            break;
        case 7: // 发布直播
        {
            //            K_C_U_M.user_id = 7001;
            //            if ([[ILiveLoginManager getInstance] getSig].length) {
            //                BYCreatLiveController *releaseLiveController = [[BYCreatLiveController alloc] init];
            //                BYNavigationController *navigationController = [[BYNavigationController alloc] initWithRootViewController:releaseLiveController];
            //                [self.viewController presentViewController:navigationController animated:YES completion:nil];
            //                return;
            //            }
            //            @weakify(self);
            //            [[BYLiveHomeRequest alloc] loadRequestGetILiveLoginSig:K_C_U_M.user_id successBlock:^(id object) {
            //                @strongify(self);
            //                @weakify(self);
            //                [[ILiveLoginManager getInstance] iLiveLogin:@"7001" sig:object succ:^{
            //                    @strongify(self);
            //                    BYCreatLiveController *releaseLiveController = [[BYCreatLiveController alloc] init];
            //                    BYNavigationController *navigationController = [[BYNavigationController alloc] initWithRootViewController:releaseLiveController];
            //                    [self.viewController presentViewController:navigationController animated:YES completion:nil];
            //                    NSLog(@"登录成功了！！！！！！");
            //                } failed:^(NSString *module, int errId, NSString *errMsg) {
            //                    showToastView([NSString stringWithFormat:@"登录失败了！:%@",errMsg], S_V_VIEW);
            //                }];
            //            } faileBlock:^(NSError *error) {
            //                showToastView(@"获取直播登录sig失败！", S_V_VIEW);
            //            }];
            
            //            NSString *sig = @"eJxlz1FPgzAQB-B3PgXps9G2gDCTPeCcEzdYELdEXhpCC1azrrYdTI3ffRsusYl3j7--3eW*Hdd1wfOiuKzqersThphPyYB74wIILv5QSk5JZYin6D9ke8kVI1VjmBoQBUGAIbQznDJheMPPiRBCZKmm72Q48TvuHxXD68heoHk7YDrNJ8lM4Xjna1Vu5vORRHmWd9mySNhVW37UpkCTUZQ*rO5fHp-KmN-CZpG0r6vlXZX1nfiK9TTskUHleu1rATsuUzQL6Vsj*3g8tk4avmHnf7xjR9j3Le2Y0nwrhgCGKEDYg6cCzo9zAMpHW70_";
            //            @weakify(self);
            //            [[ILiveLoginManager getInstance] iLiveLogin:@"7001" sig:sig succ:^{
            //                @strongify(self);
            //                BYCreatLiveController *releaseLiveController = [[BYCreatLiveController alloc] init];
            //                BYNavigationController *navigationController = [[BYNavigationController alloc] initWithRootViewController:releaseLiveController];
            //                [self.viewController presentViewController:navigationController animated:YES completion:nil];
            //                NSLog(@"登录成功了！！！！！！");
            //            } failed:^(NSString *module, int errId, NSString *errMsg) {
            //                showToastView([NSString stringWithFormat:@"登录失败了！:%@",errMsg], S_V_VIEW);
            //            }];
            ////
        }
            break;
        case 8:
        {
            [self createRoom];
            //            [self joinRoom];
        }
            break;
        case 9:
        {
            BYNewLiveController *newLiveController = [[BYNewLiveController alloc] init];
            [S_V_NC pushViewController:newLiveController animated:YES];
        }
            break;
        case 10:
        {
            BYLiveRoomSetController *liveRoomSetController = [[BYLiveRoomSetController alloc] init];
            [S_V_NC pushViewController:liveRoomSetController animated:YES];
        }
            break;
        case 11:
        {
            BYLiveSetController *liveSetController = [[BYLiveSetController alloc] init];
            [S_V_NC pushViewController:liveSetController animated:YES];
        }
            break;
        case 12:
        {
            BYLiveRoomHomeController *liveRoomHomeController = [[BYLiveRoomHomeController alloc] init];
            [S_V_NC pushViewController:liveRoomHomeController animated:YES];
        }
            break;
        case 13:
        {
            BYLiveHostController *liveHostController = [[BYLiveHostController alloc] init];
            [S_V_NC pushViewController:liveHostController animated:YES];
        }
            break;
        default:
            break;
    }
}

- (BYUserInfoView *)userInfoView{
    if (!_userInfoView) {
        _userInfoView = [BYUserInfoView initWithShowInView:S_V_VIEW userModel:self.userModel];
    }
    return _userInfoView;
}

- (BYCommonUserModel *)userModel{
    if (!_userModel) {
        _userModel = [[BYCommonUserModel alloc] init];
        _userModel.user_headImgURL = @"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1532606738980&di=c5bef9564963b43f6a2394fc822e810a&imgtype=0&src=http%3A%2F%2Fimgtu.5011.net%2Fuploads%2Fcontent%2F20170209%2F4934501486627131.jpg";
        _userModel.user_name = @"用户Test";
        _userModel.user_id = 41244;
        _userModel.user_sign = @"这是签名~这是签名~这是签名~这是签名~这是签名~这是签名~这是签名~这是签名~这是签名~这是签名~";
        _userModel.user_attentionNum = 413;
        _userModel.user_fansNum = 32;
        _userModel.user_liveNum = 421;
        _userModel.user_schoolNum = 977;
    }
    return _userModel;
}
@end
