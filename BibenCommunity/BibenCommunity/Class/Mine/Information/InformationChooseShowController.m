//
//  InformationChooseShowController.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/9/11.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "InformationChooseShowController.h"
#import "AreaAnalysis.h"

static char actionCheckToChooseCity;
static char actionCheckToChooseDateKey;
@interface InformationChooseShowController ()<UIPickerViewDelegate,UIPickerViewDataSource>
{
    NSString *chooseProvice;
    NSString *chooseCity;
    NSString *chooseTime;
}
@property (nonatomic,weak)UIViewController *showViewController;
@property (nonatomic,strong)UIView *backgrondView;
@property (nonatomic,strong)UIImageView *shareView;
@property (nonatomic,strong) UIImage *backgroundImage;                      // 背景图片
@property (nonatomic,strong)NSArray *shareButtonArray;

// titleBar
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIButton *cancelButton;
@property (nonatomic,strong)UIButton *checkButton;

// 省市
@property (nonatomic,strong)NSMutableArray *proviceArr;             /**< 省数组*/
@property (nonatomic,strong)NSArray *proviceTempArr;                /**< 省临时调用数组*/
@property (nonatomic,strong)NSArray *cityArr;
@property (nonatomic,strong)UIPickerView *pickerView;

// 时间
@property (nonatomic,strong)UIDatePicker *birthdayPicker;
@end

@implementation InformationChooseShowController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self arrayWithInit];
    [self createSheetView];                      // 加载Sheetview
    if (self.isHasGesture){
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sheetViewDismiss)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [self.backgrondView addGestureRecognizer:tapGestureRecognizer];
    }

}

#pragma mark - 创建sheetView
-(void)createSheetView{
    if (IS_IOS7_LATER){
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = YES;
    }
    
    // 创建背景色
    self.backgrondView = [[UIView alloc]initWithFrame:self.view.bounds];
    self.backgrondView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.65f];
    [self.view addSubview:self.backgrondView];
    [self backgroundColorFadeInOrOutFromValue:.0f toValue:1.0f];
    
    [self createSharetView];
    
}

-(void)arrayWithInit{
    if (self.transferPageType == InfomationPageTypeCity){
        self.proviceArr = [NSMutableArray array];
        self.proviceTempArr = [AreaAnalysis getAllProvice];
        for (int i = 0 ; i < self.proviceTempArr.count;i++){
            NSString *provice = [self.proviceTempArr objectAtIndex:i];
            NSArray *proviceArr = [provice componentsSeparatedByString:@"]"];
            [self.proviceArr addObject:[proviceArr firstObject]];
        }
        chooseProvice = [self.proviceArr firstObject];
    }
}

#pragma mark - 创建view
-(void)createSharetView{
    CGFloat viewHeight = LCFloat(305) + LCFloat(24);
    CGRect screenRect = [[UIScreen mainScreen]bounds];
    
    // ShareViewShow
    _shareView = [[UIImageView alloc]initWithFrame:CGRectMake(0, screenRect.size.height, screenRect.size.width, viewHeight)];
    _shareView.image = [Tool stretchImageWithName:@"bg_center_top"];
    _shareView.userInteractionEnabled = YES;
    _shareView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_shareView];
    
    // 创建标题
    self.titleLabel = [GWViewTool createLabelFont:@"15" textColor:@"232323"];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.frame = CGRectMake(0, LCFloat(24), kScreenBounds.size.width, LCFloat(55));
    if (self.transferPageType == InfomationPageTypeCity){
        self.titleLabel.text = @"选择城市";
    } else if (self.transferPageType == InfomationPageTypeDate){
        self.titleLabel.text = @"选择日期";
    }
    
    [_shareView addSubview:self.titleLabel];
    
    self.cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.cancelButton.backgroundColor = [UIColor clearColor];
    [self.cancelButton setTitle:@"取消" forState:UIControlStateNormal];
    [self.cancelButton setTitleColor:[UIColor colorWithCustomerName:@"A5A5A5"] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [self.cancelButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sheetViewDismiss];
    }];
    self.cancelButton.frame = CGRectMake(LCFloat(20), LCFloat(24), LCFloat(80), LCFloat(54));
    self.cancelButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"15"];
    [_shareView addSubview:self.cancelButton];
    
    // check
    self.checkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.checkButton.backgroundColor = [UIColor clearColor];
    [self.checkButton setTitleColor:[UIColor colorWithCustomerName:@"ED4A45"] forState:UIControlStateNormal];
    self.checkButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(20) - LCFloat(80), LCFloat(24), LCFloat(80), LCFloat(54));
    self.checkButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"15"];
    [self.checkButton setTitle:@"确定" forState:UIControlStateNormal];
    [_shareView addSubview:self.checkButton];
    [self.checkButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.transferPageType == InfomationPageTypeCity){
            void(^block)(NSString *provice,NSString *city) = objc_getAssociatedObject(strongSelf, &actionCheckToChooseCity);
            if (block){
                block(strongSelf->chooseProvice,strongSelf->chooseCity);
            }
        } else if (strongSelf.transferPageType == InfomationPageTypeDate){
            void(^block)(NSString *date) = objc_getAssociatedObject(strongSelf, &actionCheckToChooseDateKey);
            if (block){
                block(strongSelf->chooseTime);
            }
        }

        [strongSelf sheetViewDismiss];
    }];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.frame = CGRectMake(LCFloat(20), CGRectGetMaxY(self.titleLabel.frame), kScreenBounds.size.width - 2 * LCFloat(20), .5f);
    [_shareView addSubview:lineView];
    lineView.backgroundColor = [UIColor hexChangeFloat:@"EDEDED"];
    
    if(self.transferPageType == InfomationPageTypeCity){
        [self createPickerView];
    } else if (self.transferPageType == InfomationPageTypeDate){
        [self createDatePicker];
    }
}



#pragma makr - 创建pickerView
-(void)createPickerView{
    self.pickerView = [[UIPickerView alloc]init];
    self.pickerView.frame = CGRectMake(LCFloat(20), LCFloat(24) + self.titleLabel.size_height, kScreenBounds.size.width - 2 * LCFloat(22), LCFloat(300) - (LCFloat(24) + self.titleLabel.size_height));
    self.pickerView.dataSource = self;
    self.pickerView.backgroundColor = [UIColor clearColor];
    self.pickerView.delegate = self;
    [_shareView addSubview:self.pickerView];
}

#pragma mark - UIPickerViewDataSource
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (component == 0){
        return self.proviceArr.count;
    } else {
        return self.cityArr.count;
    }
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (component == 0){
        return [self.proviceArr objectAtIndex:row];
    } else if (component == 1){
        return [self.cityArr objectAtIndex:row];
    }
    return @"";
}

#pragma mark - UIPickerViewDelegte
// 执行选中方法
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (component == 0){
        // 1. 选中当前的省
        NSString *provice = [self.proviceArr objectAtIndex:row];
        // 2. 找出当前省对应的市
        NSString *proviceTemp = @"";
        for (int i = 0 ; i < self.proviceTempArr.count;i++){
            NSString *info = [self.proviceTempArr objectAtIndex:i];
            if ([info hasPrefix:provice]){
                proviceTemp = info;
                break;
            }
        }
        
        self.cityArr = [AreaAnalysis getAllCityWithProvice:proviceTemp];
        [self.pickerView reloadComponent:1];
        // 选中的省
        chooseProvice = provice;
        // 选中的市
        chooseCity = [self.cityArr objectAtIndex:[self.pickerView selectedRowInComponent:1]];
    } else if (component == 1){
        chooseCity = [self.cityArr objectAtIndex:[self.pickerView selectedRowInComponent:1]];
    }
}


#pragma mark - 创建datepicker
-(void)createDatePicker{
    self.birthdayPicker = [[UIDatePicker alloc]init];
    self.birthdayPicker.datePickerMode = UIDatePickerModeDate;
    self.birthdayPicker.backgroundColor = [UIColor clearColor];
    NSDate *date = [NSDate date];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];//设置为中文
    self.birthdayPicker.locale = locale;
    [self.birthdayPicker setMaximumDate:date];
    self.birthdayPicker.frame = CGRectMake(LCFloat(20), LCFloat(24) + self.titleLabel.size_height, kScreenBounds.size.width - 2 * LCFloat(22), LCFloat(300) - (LCFloat(24) + self.titleLabel.size_height));
    [self.birthdayPicker addTarget: self action: @selector(onDatePickerChanged:) forControlEvents:UIControlEventValueChanged];
    [_shareView addSubview:self.birthdayPicker];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateAndTime = [dateFormatter stringFromDate:date];
    chooseTime = dateAndTime;
}

#pragma mark - UIDatePicker
-(void)onDatePickerChanged:(UIDatePicker *)sender{
    UIDatePicker *datepicker = (UIDatePicker *)sender;
    if(datepicker == self.birthdayPicker){
        NSDate *select  = [self.birthdayPicker date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSString *dateAndTime = [dateFormatter stringFromDate:select];
        chooseTime = dateAndTime;
    }
}




#pragma mark - actionClick
-(void)sheetViewDismiss{
    [self dismissFromView:_showViewController];
}


#pragma mark 背景色渐入效果
- (void)backgroundColorFadeInOrOutFromValue:(NSInteger)fromValue toValue:(NSInteger)toValue{
    CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.duration = 1.1;
    theAnimation.fromValue = [NSNumber numberWithFloat:fromValue];
    theAnimation.toValue = [NSNumber numberWithFloat:toValue];
    [self.backgrondView.layer addAnimation:theAnimation forKey:@"anumateOpacity"];
}

// 隐藏view
- (void)dismissFromView:(UIViewController *)viewController{
    __weak InformationChooseShowController *weakVC = self;
    
    NSInteger screenHeight = [[UIScreen mainScreen] bounds].size.height;
    [UIView animateWithDuration:0.2f animations:^{
        weakVC.shareView.frame = CGRectMake(0, screenHeight, weakVC.shareView.bounds.size.width, weakVC.shareView.bounds.size.height);
    } completion:^(BOOL finished) {
        [weakVC willMoveToParentViewController:nil];
        [weakVC.view removeFromSuperview];
        [weakVC removeFromParentViewController];
    }];
    //背景色渐出
    [self backgroundColorFadeInOrOutFromValue:1.f toValue:0.f];
}

// 显示view
- (void)showInView:(UIViewController *)viewController{
    __weak InformationChooseShowController *weakVC = self;
    _showViewController = viewController;
    [viewController.view.window addSubview:self.view];
    [viewController addChildViewController:self];
    [self didMoveToParentViewController:viewController];
    
    NSInteger screenHeight = [[UIScreen mainScreen] bounds].size.height;
    [UIView animateWithDuration:0.3f animations:^{
        weakVC.shareView.frame = CGRectMake(weakVC.shareView.frame.origin.x, screenHeight-_shareView.bounds.size.height-(IS_IOS7_LATER ? 0 : 20), weakVC.shareView.bounds.size.width, weakVC.shareView.bounds.size.height);
    }];
}


- (void)hideParentViewControllerTabbar:(UIViewController *)viewController{              // 毛玻璃效果
    self.backgroundImage = [[UIImage screenShoot:viewController.view] applyExtraLightEffect];
}

-(void)actionCheckToChooseCity:(void(^)(NSString *provice ,NSString *city))block{
    objc_setAssociatedObject(self, &actionCheckToChooseCity, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionCheckToChooseDate:(void(^)(NSString *date))block{
    objc_setAssociatedObject(self, &actionCheckToChooseDateKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
