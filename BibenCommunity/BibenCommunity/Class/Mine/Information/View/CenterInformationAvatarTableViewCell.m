//
//  CenterInformationAvatarTableViewCell.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/28.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterInformationAvatarTableViewCell.h"

@interface CenterInformationAvatarTableViewCell()
@property (nonatomic,strong)PDImageView *avatarBgImgView;
@property (nonatomic,strong)PDImageView *avararImgView;
@property (nonatomic,strong)UIButton *avatarButton;
@property (nonatomic,strong)UILabel *titleLabel;

@end
static char avatarButtonClickWithBlockKey;
@implementation CenterInformationAvatarTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.avatarBgImgView = [[PDImageView alloc]init];
    self.avatarBgImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarBgImgView];
    
    self.avararImgView = [[PDImageView alloc]init];
    self.avararImgView.backgroundColor = [UIColor clearColor];
     self.avararImgView.clipsToBounds = YES;
    [self addSubview:self.avararImgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"15" textColor:@"696969"];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.titleLabel];
    
    self.avatarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.avatarButton.backgroundColor = [UIColor clearColor];
    __weak typeof(self)weakSelf = self;
    [self.avatarButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &avatarButtonClickWithBlockKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.avatarButton];
}

-(void)setTransferAccountModel:(AccountModel *)transferAccountModel{
    _transferAccountModel = transferAccountModel;

    self.avatarBgImgView.image = [UIImage imageNamed:@"bg_center_avatar_alpha"];
    self.avatarBgImgView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(105)) / 2., 0, LCFloat(105), LCFloat(105));
    
    self.avararImgView.frame = CGRectMake(0, 0, LCFloat(70), LCFloat(70));
    self.avararImgView.layer.cornerRadius = self.avararImgView.size_width / 2.;
    self.avararImgView.center = self.avatarBgImgView.center;
    [self.avararImgView uploadImageWithURL:transferAccountModel.loginServerModel.account.head_img placeholder:nil callback:NULL];
    
    self.titleLabel.text = [NSString stringWithFormat:@"ID:%@",transferAccountModel.loginServerModel.account._id];
    self.titleLabel.frame = CGRectMake(0, CGRectGetMaxY(self.avatarBgImgView.frame) + LCFloat(11), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    
    self.avatarButton.frame = self.avatarBgImgView.frame;
    
}



+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(101);
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"正文"]];
    cellHeight += LCFloat(11);
    return cellHeight;
}

-(void)avatarButtonClickWithBlock:(void(^)())block{
    objc_setAssociatedObject(self, &avatarButtonClickWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
