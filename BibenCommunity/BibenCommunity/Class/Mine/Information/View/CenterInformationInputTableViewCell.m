//
//  CenterInformationInputTableViewCell.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/28.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterInformationInputTableViewCell.h"

static char textFieldKey;
static char textFieldDidEndBlockKey;
@interface CenterInformationInputTableViewCell()<UITextFieldDelegate>
@property (nonatomic,strong)PDImageView *bgImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)PDImageView *arrowImgView;
@end

@implementation CenterInformationInputTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.backgroundColor = [UIColor clearColor];
    self.backgroundView = self.bgImgView;
    
    self.titleLabel = [GWViewTool createLabelFont:@"14" textColor:@"727272"];
    NSString *titleStr = @"昵称";
    CGSize titleSize = [titleStr sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.titleLabel.font])];
    self.titleLabel.frame = CGRectMake(LCFloat(33), 0, titleSize.width, titleSize.height);
    self.titleLabel.center_y = [CenterInformationInputTableViewCell calculationCellHeight] / 2.;
    [self addSubview:self.titleLabel];
    
    // arrow
    self.arrowImgView = [[PDImageView alloc]init];
    self.arrowImgView.backgroundColor = [UIColor clearColor];
    self.arrowImgView.image = [UIImage imageNamed:@"icon_base_arrow"];
    self.arrowImgView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(28) - LCFloat(5), 0, LCFloat(5), LCFloat(9));
    self.arrowImgView.center_y = self.titleLabel.center_y;
    [self addSubview:self.arrowImgView];
    
    self.inputTextField =[[UITextField alloc]init];
    self.inputTextField.placeholder = @"";
    self.inputTextField.frame = CGRectMake(LCFloat(80), 0, self.arrowImgView.orgin_x - LCFloat(11) - LCFloat(80), 44);
    self.inputTextField.center_y = self.arrowImgView.center_y;
    [self addSubview:self.inputTextField];
    [self.inputTextField addTarget:self action:@selector(textFieldDidChanged) forControlEvents:UIControlEventEditingChanged];
    self.inputTextField.delegate = self;
    
    __weak typeof(self)weakSelf = self;
    [self.inputTextField showBarCallback:^(UITextField *textField, NSInteger buttonIndex, UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.inputTextField.isFirstResponder){
            [strongSelf.inputTextField resignFirstResponder];
        }
    }];
}

-(void)setTransferDymic:(NSString *)transferDymic{
    _transferDymic = transferDymic;
    self.inputTextField.text = transferDymic;
}

-(void)setTransferPlaceholder:(NSString *)transferPlaceholder{
    _transferPlaceholder = transferPlaceholder;
    self.inputTextField.placeholder = transferPlaceholder;
}

-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
    self.titleLabel.text = transferTitle;
}

-(void)setHasArrow:(BOOL)hasArrow{
    _hasArrow = hasArrow;
    if (hasArrow){
        self.arrowImgView.hidden = NO;
    } else {
        self.arrowImgView.hidden = YES;
    }
}

-(void)setTransferBgImgName:(NSString *)transferBgImgName{
    _transferBgImgName = transferBgImgName;
    self.bgImgView.image = [Tool stretchImageWithName:transferBgImgName];
}

-(void)textFieldDidChanged{
    void(^block)(NSString *info) = objc_getAssociatedObject(self, &textFieldKey);
    if (block){
        block(self.inputTextField.text);
    }
}

-(void)textFieldDidChangeBlock:(void (^)(NSString *))block{
    objc_setAssociatedObject(self, &textFieldKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)textFieldDidEndBlock:(void(^)(NSString *info))block{
    objc_setAssociatedObject(self, &textFieldDidEndBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    void(^block)(NSString *info) = objc_getAssociatedObject(self, &textFieldDidEndBlockKey);
    if (block){
        block(self.inputTextField.text);
    }
}

-(void)setTransferType:(InformationInputType)transferType{
    _transferType = transferType;
    if (transferType == InformationInputTypeNick){
        self.titleLabel.center_y = [CenterInformationInputTableViewCell calculationCellHeightWithType:InformationInputTypeNick] / 2.;
        self.arrowImgView.center_y = self.titleLabel.center_y;
        self.inputTextField.center_y = self.arrowImgView.center_y;
    } else if (transferType == InformationInputTypeGender){
        self.titleLabel.center_y = ([CenterInformationInputTableViewCell calculationCellHeightWithType:InformationInputTypeGender] - LCFloat(24))/ 2. + LCFloat(24);
        self.arrowImgView.center_y = self.titleLabel.center_y;
        self.inputTextField.center_y = self.arrowImgView.center_y;
    } else if (transferType == InformationInputTypeNormal){
        self.titleLabel.center_y = [CenterInformationInputTableViewCell calculationCellHeightWithType:InformationInputTypeNormal] / 2.;
        self.arrowImgView.center_y = self.titleLabel.center_y;
        self.inputTextField.center_y = self.arrowImgView.center_y;
    } else if (transferType == InformationInputTypeSign){
        self.titleLabel.center_y = ([CenterInformationInputTableViewCell calculationCellHeightWithType:InformationInputTypeSign] - LCFloat(24)) / 2.;
        self.arrowImgView.center_y = self.titleLabel.center_y;
        self.inputTextField.center_y = self.arrowImgView.center_y;
    }
}

+(CGFloat)calculationCellHeightWithType:(InformationInputType)type{
    if (type == InformationInputTypeNick){
        return LCFloat(101);
    } else if (type == InformationInputTypeGender){
        return LCFloat(84);
    } else if (type == InformationInputTypeNormal){
        return LCFloat(58);
    } else if (type == InformationInputTypeGender){
        return LCFloat(84);
    }
    return LCFloat(70);
}
@end
