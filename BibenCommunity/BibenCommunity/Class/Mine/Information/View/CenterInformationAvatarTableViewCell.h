//
//  CenterInformationAvatarTableViewCell.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/28.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

@interface CenterInformationAvatarTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)AccountModel *transferAccountModel;

-(void)avatarButtonClickWithBlock:(void(^)())block;

@end
