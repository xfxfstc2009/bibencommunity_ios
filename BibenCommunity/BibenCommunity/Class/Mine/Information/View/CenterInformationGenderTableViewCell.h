//
//  CenterInformationGenderTableViewCell.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/28.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

@interface CenterInformationGenderTableViewCell : PDBaseTableViewCell

@property (nonatomic,copy)NSString *transferChooseGender;

-(void)actionSelectedGenderWithBlock:(void(^)(NSString *gender))block;

@end

@interface CenterInformationGenderSingleView:UIView
@property (nonatomic,strong)UILabel *titleLabel;
-(instancetype)initWithFrame:(CGRect)frame gender:(NSString *)gender block:(void(^)())block;

-(void)hasSelected:(BOOL)selected;

@end
