//
//  CenterInformationGenderTableViewCell.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/28.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterInformationGenderTableViewCell.h"
#import "CenterInformationInputTableViewCell.h"

static char actionSelectedGenderWithBlockKey;
@interface CenterInformationGenderTableViewCell()

@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)PDImageView *bgImgView;

@property (nonatomic,strong)CenterInformationGenderSingleView *boyView;
@property (nonatomic,strong)CenterInformationGenderSingleView *girlView;


@end
@implementation CenterInformationGenderTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    PDImageView *bgImageView = [[PDImageView alloc]init];
    bgImageView.backgroundColor = [UIColor clearColor];
    bgImageView.image = [Tool stretchImageWithName:@"bg_center_top"];
    self.backgroundView = bgImageView;
    
    self.titleLabel = [GWViewTool createLabelFont:@"14" textColor:@"727272"];
    NSString *titleStr = @"性别";
    CGSize titleSize = [titleStr sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.titleLabel.font])];
    self.titleLabel.frame = CGRectMake(LCFloat(33), 0, titleSize.width, titleSize.height);
    self.titleLabel.text = titleStr;
    self.titleLabel.center_y = LCFloat(24) + ([CenterInformationInputTableViewCell calculationCellHeightWithType:InformationInputTypeGender] - LCFloat(24)) / 2.;
    [self addSubview:self.titleLabel];
    
    __weak typeof(self)weakSelf = self;
    self.boyView = [[CenterInformationGenderSingleView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.titleLabel.frame) + LCFloat(11), 0, LCFloat(40) + LCFloat(47), 40) gender:@"男" block:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(NSString *gender) = objc_getAssociatedObject(strongSelf, &actionSelectedGenderWithBlockKey);
        if (block){
            block(@"男");
        }
    }];
    
    [self addSubview:self.boyView];
    
    self.girlView = [[CenterInformationGenderSingleView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.boyView.frame) + LCFloat(11), 0, LCFloat(40) + LCFloat(47), 40) gender:@"女" block:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(NSString *gender) = objc_getAssociatedObject(strongSelf, &actionSelectedGenderWithBlockKey);
        if (block){
            block(@"女");
        }
    }];
    
    [self addSubview:self.girlView];
    
    self.boyView.center_y = self.titleLabel.center_y;
    self.girlView.center_y = self.titleLabel.center_y;
}


-(void)setTransferChooseGender:(NSString *)transferChooseGender{
    _transferChooseGender = transferChooseGender;
    if ([transferChooseGender isEqualToString:@"女"]){
        [self.girlView hasSelected:YES];
        [self.boyView hasSelected:NO];
    } else {
        [self.girlView hasSelected:NO];
        [self.boyView hasSelected:YES];
    }
}

-(void)actionSelectedGenderWithBlock:(void(^)(NSString *gender))block{
    objc_setAssociatedObject(self, &actionSelectedGenderWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end



@interface CenterInformationGenderSingleView()
@property (nonatomic,strong)PDImageView *checkImgView;
@property (nonatomic,strong)UILabel *checkTitle;
@property (nonatomic,strong)UIButton *checkButton;
@end
@implementation CenterInformationGenderSingleView

-(instancetype)initWithFrame:(CGRect)frame gender:(NSString *)gender block:(void(^)())block{
    self = [super initWithFrame:frame];
    if (self){
        [self createViewGender:gender block:block];
    }
    return self;
}

#pragma mark - createView
-(void)createViewGender:(NSString *)gender block:(void(^)())block{
    self.checkImgView = [[PDImageView alloc]init];
    self.checkImgView.backgroundColor = [UIColor clearColor];
    self.checkImgView.image = [UIImage imageNamed:@"icon_center_check_nor"];
    self.checkImgView.frame = CGRectMake(0, 0, LCFloat(31), LCFloat(31));
    self.checkImgView.backgroundColor = [UIColor clearColor];
    CGFloat imgHeight = (LCFloat(31) - (LCFloat(7) + LCFloat(24)) / 2.);
    CGFloat margin = (self.size_height - imgHeight) / 2.;
    self.checkImgView.orgin_y = margin - LCFloat(3.5);
    [self addSubview:self.checkImgView];
    
    self.checkTitle = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    self.checkTitle.text = gender;
    [self addSubview:self.checkTitle];
    CGSize checkSize = [Tool makeSizeWithLabel:self.checkTitle];
    self.checkTitle.frame = CGRectMake(CGRectGetMaxX(self.checkImgView.frame), 0, checkSize.width, checkSize.height);
    self.checkTitle.center_y = self.center_y;
    
    self.checkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.checkButton.backgroundColor = [UIColor clearColor];
    self.checkButton.frame = self.bounds;
    [self addSubview:self.checkButton];
    __weak typeof(self)weakSelf = self;
    [self.checkButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        
        if (block){
            block();
        }
    }];
}

-(void)hasSelected:(BOOL)selected{
    if (selected){
        self.checkImgView.image = [UIImage imageNamed:@"icon_center_check_hlt"];
    } else {
        self.checkImgView.image = [UIImage imageNamed:@"icon_center_check_nor"];
    }
}

@end


