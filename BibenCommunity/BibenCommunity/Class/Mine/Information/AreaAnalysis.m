//
//  AreaAnalysis.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/9/12.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "AreaAnalysis.h"

@implementation AreaAnalysis

#pragma mark - 读取所有的省
+(NSArray *)getAllProvice{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"area" ofType:@"plist"];
    NSMutableDictionary *country = [[NSMutableDictionary alloc] initWithContentsOfFile:path];
    NSMutableDictionary *proviceDic = [country objectForKey:@"country"];
    NSArray *proviceArr = proviceDic.allKeys;
    return proviceArr;
}

+(NSArray *)getAllCityWithProvice:(NSString *)provice{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"area" ofType:@"plist"];
    NSMutableDictionary *country = [[NSMutableDictionary alloc] initWithContentsOfFile:path];
    NSMutableDictionary *proviceDic = [country objectForKey:@"country"];

    NSArray *cityArr;
    if ([proviceDic.allKeys containsObject:provice]){
        cityArr = [proviceDic objectForKey:provice];
    }
    // 获取市
    if ([provice hasPrefix:@"北京"]){
        return @[@"北京市"];
    } else if ([provice hasPrefix:@"上海"]){
        return @[@"上海市"];
    } else if ([provice hasPrefix:@"天津"]){
        return @[@"天津市"];
    } else if ([provice hasPrefix:@"重庆市"]){
        return @[@"重庆市"];
    }
    
    NSMutableArray *cityTempArr = [NSMutableArray array];
    for (int i = 0 ; i < cityArr.count;i++){
        NSArray *singleCityArr = [cityArr objectAtIndex:i];
        NSString *cityStr = [singleCityArr firstObject];
        NSArray *cityStrTempArr = [cityStr componentsSeparatedByString:@"]"];
        NSString *mainCityName = [cityStrTempArr firstObject];
        [cityTempArr addObject:mainCityName];
    }
    
    return cityTempArr;
}

@end
