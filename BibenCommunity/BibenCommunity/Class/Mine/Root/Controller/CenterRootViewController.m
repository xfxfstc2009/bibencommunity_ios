//
//  CenterRootViewController.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/22.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterRootViewController.h"
#import "CenterMessageRootViewController.h"         // message
#import "NetworkAdapter+Login.h"
#import "NetworkAdapter+Center.h"
#import "KMScrollingHeaderView.h"
#import "CenterHeaderViewInfo.h"                    // 头部薪资
#import "CenterNormalTableViewCell.h"
#import "CenterInformationSettingViewController.h"
#import "CenterInvitationViewController.h"
#import "CenterHeaderSegmentItemsTableViewCell.h"
#import "CenterFollowViewController.h"
#import "CenterUserInformationViewController.h"
#import "CenterBBTViewController.h"
#import "CenterAboutViewController.h"
#import "BYLiveRoomHomeController.h"
#import "BYCreatLiveRoomController.h"
#import "CenterSignView.h"


#import "LivePPTRootViewController.h"

@interface CenterRootViewController ()<UITableViewDelegate,UITableViewDataSource,KMScrollingHeaderViewDelegate>
@property (nonatomic,strong)NSArray *centerArr;
@property (nonatomic,strong)UIView *navBarView;
@property (nonatomic,strong)KMScrollingHeaderView *scrollingHeaderView;
@property (nonatomic,strong)CenterHeaderViewInfo *headerViewInfo;

@property (nonatomic,strong)UIView *rightNavView;
@property (nonatomic,strong)LOTAnimationView *settingAnimation;
@property (nonatomic,strong)UIButton *rightSettingButton;

@end

@implementation CenterRootViewController

+(instancetype)sharedController{
    static CenterRootViewController *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[CenterRootViewController alloc] init];
    });
    return _sharedAccountModel;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.headerViewInfo.transferAccountModel = [AccountModel sharedAccountModel];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self setupDetailsPageView];
    
    // 测试自动登录
    [self sendRequestToGetInfo];
}

#pragma mark - createNavBar
-(void)createNavBar{
    // 1. 创建本地的View
    self.rightNavView = [[UIView alloc]init];
    self.rightNavView.backgroundColor = [UIColor clearColor];
    self.rightNavView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - 44, [BYTabbarViewController sharedController].statusHeight, 44, 44);
    [self.view addSubview:self.rightNavView];
    
    self.navBarView = [[UIView alloc]init];
    self.navBarView.backgroundColor = [UIColor redColor];
    self.navBarView.frame = CGRectMake(0, 0, kScreenBounds.size.width, [BYTabbarViewController sharedController].navBarHeight);
    [self.view addSubview:self.navBarView];
    
    UIButton *settingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    settingButton.backgroundColor = [UIColor clearColor];
    [settingButton setImage:[UIImage imageNamed:@"icon_center_setting"] forState:UIControlStateNormal];
    settingButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(7) - LCFloat(44), 20, LCFloat(44), LCFloat(44));
    [self.navBarView addSubview:settingButton];
    __weak typeof(self)weakSelf = self;
    [settingButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfo];
    }];

    self.scrollingHeaderView.navbarView = self.navBarView;
}

#pragma mark - CreateBottomScrollingView
- (void)setupDetailsPageView {
    self.scrollingHeaderView = [[KMScrollingHeaderView alloc]initWithFrame:self.view.bounds];
    self.scrollingHeaderView.backgroundColor = [UIColor whiteColor];
    self.scrollingHeaderView.tableView.dataSource = self;
    self.scrollingHeaderView.tableView.delegate = self;
    self.scrollingHeaderView.delegate = self;
    
    self.scrollingHeaderView.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.scrollingHeaderView.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.scrollingHeaderView.tableView.showsVerticalScrollIndicator = YES;
    self.scrollingHeaderView.tableView.backgroundColor = [UIColor whiteColor];
    
    self.scrollingHeaderView.tableView.separatorColor = [UIColor whiteColor];
    self.scrollingHeaderView.headerImageViewContentMode = UIViewContentModeScaleToFill;
    [self.view addSubview:self.scrollingHeaderView];
    
    self.scrollingHeaderView.navbarViewFadingOffset = 30;
    self.scrollingHeaderView.headerImageViewHeight = LCFloat(300);              // 底部的图片的底
    self.scrollingHeaderView.headerImageViewScalingFactor = 500;

    [self.scrollingHeaderView reloadScrollingHeader];
    
    [self createNavBarInfo];
}

-(void)createNavBarInfo{
    CGFloat origin_y = (self.scrollingHeaderView.headerImageViewHeight - [CenterHeaderViewInfo calculationHeight]) / 2. - 0;
    self.headerViewInfo = [[CenterHeaderViewInfo alloc]initWithFrame:CGRectMake(LCFloat(7), origin_y, kScreenBounds.size.width - 2 * LCFloat(7), [CenterHeaderViewInfo calculationHeight])];
    __weak typeof(self)weakSelf = self;
    [self.headerViewInfo actionClickWithHeaderAvatarBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        CenterInformationSettingViewController *informationViewController = [[CenterInformationSettingViewController alloc]init];
        [informationViewController hidesTabBarWhenPushed];
        [strongSelf.navigationController pushViewController:informationViewController animated:YES];
    }];
    self.headerViewInfo.transferAccountModel = [AccountModel sharedAccountModel];
    
    [self.scrollingHeaderView addSubview:self.headerViewInfo];
    
    // 设置按钮
    self.rightSettingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightSettingButton.backgroundColor = [UIColor clearColor];
    [self.rightSettingButton setImage:[UIImage imageNamed:@"icon_center_setting"] forState:UIControlStateNormal];
    CGFloat margin = (self.headerViewInfo.orgin_y - LCFloat(44)) / 2.;
    
    self.rightSettingButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(7) - LCFloat(44), margin, LCFloat(44), LCFloat(44));
    [self.scrollingHeaderView addSubview:self.rightSettingButton];
    [self.rightSettingButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        CenterInformationSettingViewController *informationViewController = [[CenterInformationSettingViewController alloc]init];
        [informationViewController hidesTabBarWhenPushed];
        [strongSelf.navigationController pushViewController:informationViewController animated:YES];
    }];
}



#pragma mark - KMScrollingHeaderViewDelegate
- (void)detailsPage:(KMScrollingHeaderView *)detailsPageView headerImageView:(PDImageView *)imageView{
    imageView.image = [UIImage imageNamed:@"bg_center_alpha"];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"个人中心";
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:@"设置" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfo];
    }];
    
    [self leftBarButtonWithTitle:@"消息" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        CenterMessageRootViewController *centerMessageViewController = [[CenterMessageRootViewController alloc]init];
        [centerMessageViewController hidesTabBarWhenPushed];
        [strongSelf.navigationController pushViewController:centerMessageViewController animated:YES];
    }];
}

-(void)arrayWithInit{
    self.centerArr = @[@[@"信息"],@[@"我的钱包",@"每日签到",@"邀请好友",@"关于币本"],@[@"退出登录"]];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.centerArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.centerArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"信息" sourceArr:self.centerArr]){
        static NSString *cellIdentifyWithRowZero = @"cellIdentifyWithRowZero";
        CenterHeaderSegmentItemsTableViewCell *cellWithRowZero = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowZero];
        if (!cellWithRowZero){
            cellWithRowZero = [[CenterHeaderSegmentItemsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowZero];
        }
        __weak typeof(self)weakSelf = self;
        [cellWithRowZero segmentListItemClickWithBlock:^(NSInteger index) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (index == 0){ // BBT
                CenterBBTViewController *bbtViewController = [[CenterBBTViewController alloc]init];
                [bbtViewController hidesTabBarWhenPushed];
                [strongSelf.navigationController pushViewController:bbtViewController animated:YES];
            } else if (index == 1){         // 关注
                CenterFollowViewController *followViewController = [[CenterFollowViewController alloc]init];
                followViewController.transferType = CenterFansViewControllerTypeLink;
                [followViewController hidesTabBarWhenPushed];
                __weak typeof(self)weakSelf = self;
                [followViewController actionReoloadFollow:^{
                    if (!weakSelf){
                        return ;
                    }
                    __strong typeof(weakSelf)strongSelf = weakSelf;
                    [strongSelf.scrollingHeaderView.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                }];
                [strongSelf.navigationController pushViewController:followViewController animated:YES];
            } else if (index == 2){         // 粉丝
                CenterFollowViewController *followViewController = [[CenterFollowViewController alloc]init];
                followViewController.transferType = CenterFansViewControllerTypeFans;
                [followViewController hidesTabBarWhenPushed];
                __weak typeof(self)weakSelf = self;
                [followViewController actionReoloadFollow:^{
                    if (!weakSelf){
                        return ;
                    }
                    __strong typeof(weakSelf)strongSelf = weakSelf;
                    [strongSelf.scrollingHeaderView.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                }];
                [strongSelf.navigationController pushViewController:followViewController animated:YES];
            } else if (index == 55){         // 文章
                LivePPTRootViewController *pptViewController = [[LivePPTRootViewController alloc]init];
                [pptViewController hidesTabBarWhenPushed];
                [self.navigationController pushViewController:pptViewController animated:YES];
//                return;
//                CenterUserInformationViewController *userInfoViewController = [[CenterUserInformationViewController alloc]init];
//                [strongSelf.navigationController pushViewController:userInfoViewController animated:YES];
            } else if (index == 3){         // 直播
                if ([[[NSUserDefaults standardUserDefaults] objectForKey:kiSHaveLiveRoomKey] isEqual:@YES]) {
                    BYLiveRoomHomeController *livRoomeHomeController = [[BYLiveRoomHomeController alloc] init];
                    [livRoomeHomeController hidesTabBarWhenPushed];
                    [strongSelf.navigationController pushViewController:livRoomeHomeController animated:YES];
                } else{
                    BYCreatLiveRoomController *creatLiveRoomController = [[BYCreatLiveRoomController alloc] init];
                    [creatLiveRoomController hidesTabBarWhenPushed];
                    [strongSelf.navigationController pushViewController:creatLiveRoomController animated:YES];
                }
            }
        }];
        
        [cellWithRowZero reloadItems];
        return cellWithRowZero;
    }  else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"我的钱包" sourceArr:self.centerArr]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        CenterNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[CenterNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        if (indexPath.row == 0){
            cellWithRowOne.transferTitle = @"我的钱包";
            cellWithRowOne.transferIconImg = [UIImage imageNamed:@"icon_center_money"];
            cellWithRowOne.transferBgImgName = @"bg_center_top";
        } else if (indexPath.row == 1){
            cellWithRowOne.transferTitle = @"每日签到";
            cellWithRowOne.transferDymic = @"签到领取BBT";
            cellWithRowOne.transferIconImg = [UIImage imageNamed:@"icon_center_sign"];
            cellWithRowOne.transferBgImgName = @"bg_center_normal";
        } else if (indexPath.row == 2){
            cellWithRowOne.transferTitle = @"邀请好友";
            cellWithRowOne.transferDymic = @"邀请送BBT";
            cellWithRowOne.transferIconImg = [UIImage imageNamed:@"icon_center_inv"];
            cellWithRowOne.transferBgImgName = @"bg_center_normal";
        } else if (indexPath.row == 3){
            cellWithRowOne.transferTitle = @"关于币本";
            cellWithRowOne.transferIconImg = [UIImage imageNamed:@"icon_center_about"];
            cellWithRowOne.transferBgImgName = @"bg_center_bottom";
        }
        cellWithRowOne.hasArrow = YES;
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWButtonTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        cellWithRowTwo.transferTitle = @"退出登录";
        
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf logoutManager];
            return;
        }];
        return cellWithRowTwo;
    }
}




#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"我的钱包" sourceArr:self.centerArr]){
            CenterBBTViewController *bbtViewController = [[CenterBBTViewController alloc]init];
            [bbtViewController hidesTabBarWhenPushed];
            [self.navigationController pushViewController:bbtViewController animated:YES];
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"每日签到" sourceArr:self.centerArr]){
            [self sendRequestToSign];
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"邀请好友" sourceArr:self.centerArr]){
            CenterInvitationViewController *invitationViewController = [[CenterInvitationViewController alloc]init];
            [invitationViewController hidesTabBarWhenPushed];
            [self.navigationController pushViewController:invitationViewController animated:YES];
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"关于币本" sourceArr:self.centerArr]){
            CenterAboutViewController *aboutViewController = [[CenterAboutViewController alloc]init];
            [aboutViewController hidesTabBarWhenPushed];
            [self.navigationController pushViewController:aboutViewController animated:YES];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return LCFloat(0);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"信息" sourceArr:self.centerArr]){
        return [CenterHeaderSegmentItemsTableViewCell calculationCellHeight];
    }  else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"我的钱包" sourceArr:self.centerArr]){
        if (indexPath.row == 0){
            return [CenterNormalTableViewCell calculationCellHeightWithLocation:cellLocationTop];
        } else if ([indexPath row] == [[self.centerArr objectAtIndex:indexPath.section] count] - 1){
            return [CenterNormalTableViewCell calculationCellHeightWithLocation:cellLocationBottom];
        } else {
            return [CenterNormalTableViewCell calculationCellHeightWithLocation:cellLocationNormal];
        }

    } else {
        return [GWButtonTableViewCell calculationCellHeight];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.scrollingHeaderView.tableView) {
        if (!(indexPath.section == [self cellIndexPathSectionWithcellData:@"信息" sourceArr:self.centerArr] || indexPath.section == [self cellIndexPathSectionWithcellData:@"退出登录" sourceArr:self.centerArr])){
            SeparatorType separatorType = SeparatorTypeMiddle;
            if ( [indexPath row] == 0) {
                separatorType  = SeparatorTypeHead;
                [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"center"];
            } else if ([indexPath row] == [[self.centerArr objectAtIndex:indexPath.section] count] - 1) {
                separatorType  = SeparatorTypeBottom;
            } else {
                separatorType  = SeparatorTypeMiddle;
                [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"center"];
            }
        }
    }
}


#pragma mark - Interface
#pragma mark 签到
-(void)sendRequestToSign{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] centerSignGetBBTManagerBlock:^(BOOL isSuccessed, NSInteger reward) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            [[BYAlertView sharedAlertView] showAlertSignWithTitle:reward block:^{
                // 刷新
                [AccountModel sharedAccountModel].loginServerModel.finance.coin += reward;
                NSInteger section = [strongSelf cellIndexPathSectionWithcellData:@"信息" sourceArr:strongSelf.centerArr];
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:section];
                [strongSelf.scrollingHeaderView.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            }];
        } else {
            [StatusBarManager statusBarHidenWithText:@"签到失败"];
        }
    }];
}

-(void)sendRequestToGetInfo{
    [[NetworkAdapter sharedAdapter]centerGetUserCountInfoManagerBlock:^(BOOL isSuccessed) {
        self.headerViewInfo.transferAccountModel = [AccountModel sharedAccountModel];
        [self.scrollingHeaderView.tableView reloadData];
    }];
}


#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat point = scrollView.contentOffset.y;
    self.headerViewInfo.orgin_y = (self.scrollingHeaderView.headerImageViewHeight - self.headerViewInfo.size_height) / 2.- point;
    CGFloat margin = (self.headerViewInfo.orgin_y - LCFloat(44)) / 2.;
    self.rightSettingButton.orgin_y = self.headerViewInfo.orgin_y - margin - LCFloat(44);
}

-(void)logoutManager{
    __weak typeof(self)weakSelf = self;
    [[AccountModel sharedAccountModel] logoutManagerBlock:^{
        if (!weakSelf){
            return ;
        }
        // 进行UI退出登录
        [[CenterRootViewController sharedController] logoutUIManager];
    }];
}

-(void)logoutUIManager{
    __weak typeof(self)weakSelf = self;
    [self authorizeWithCompletionHandler:^(BOOL successed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfo];
    }];
}
@end


