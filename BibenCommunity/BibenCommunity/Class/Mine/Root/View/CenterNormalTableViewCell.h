//
//  CenterNormalTableViewCell.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/24.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

typedef NS_ENUM(NSInteger, cellLocation) {
    cellLocationTop,
    cellLocationNormal,
    cellLocationBottom,
};

@interface CenterNormalTableViewCell : PDBaseTableViewCell

@property (nonatomic,copy)NSString *transferBgImgName;
@property (nonatomic,copy)NSString *transferTitle;
@property (nonatomic,strong)UIImage *transferIconImg;
@property (nonatomic,copy)NSString *transferDymic;
@property (nonatomic,assign)BOOL hasArrow;

+(CGFloat)calculationCellHeightWithLocation:(cellLocation)location;

@end
