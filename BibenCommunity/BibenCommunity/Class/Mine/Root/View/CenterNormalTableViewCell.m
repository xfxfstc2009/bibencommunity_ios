//
//  CenterNormalTableViewCell.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/24.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterNormalTableViewCell.h"

@interface CenterNormalTableViewCell()
@property (nonatomic,strong)PDImageView *bgImgView;
@property (nonatomic,strong)PDImageView *iconImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *dymicLabel;
@property (nonatomic,strong)PDImageView *arrowImgView;

@end

@implementation CenterNormalTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // bgImgView
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.backgroundColor = [UIColor clearColor];
    self.backgroundView = self.bgImgView;
    
    // 2. iconImg
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.iconImgView];
    
    // 3. title
    self.titleLabel = [GWViewTool createLabelFont:@"14" textColor:@"373737"];
    [self addSubview:self.titleLabel];
    
    // 4. dymic
    self.dymicLabel = [GWViewTool createLabelFont:@"15" textColor:@"B1B1B1"];
    self.dymicLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.dymicLabel];
    
    self.arrowImgView = [[PDImageView alloc]init];
    self.arrowImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.arrowImgView];
}

-(void)setTransferBgImgName:(NSString *)transferBgImgName{
    _transferBgImgName = transferBgImgName;
    self.bgImgView.image = [Tool stretchImageWithName:transferBgImgName];
}

-(void)setTransferDymic:(NSString *)transferDymic {
    _transferDymic = transferDymic;
    self.dymicLabel.text = transferDymic;
}

-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
    self.titleLabel.text = transferTitle;
}

-(void)setTransferIconImg:(UIImage *)transferIconImg{
    _transferIconImg = transferIconImg;
    self.iconImgView.image = transferIconImg;
}

-(void)setHasArrow:(BOOL)hasArrow{
    _hasArrow = hasArrow;
    [self frameInfo];
}


-(void)frameInfo{
    self.iconImgView.frame = CGRectMake(LCFloat(33), 0, LCFloat(21), LCFloat(21));
    self.iconImgView.center_y = [CenterNormalTableViewCell calculationCellHeight] / 2.;
    
    
    CGSize titleSize = [Tool makeSizeWithLabel:self.titleLabel];
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.iconImgView.frame) + LCFloat(11), 0, titleSize.width, titleSize.height);
    
    self.arrowImgView.image = [UIImage imageNamed:@"icon_base_arrow"];
    self.arrowImgView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(65) / 2. - LCFloat(5), 0, LCFloat(5), LCFloat(9));
    
    if (self.hasArrow){
        self.arrowImgView.hidden = NO;
    } else {
        self.arrowImgView.hidden = YES;
    }
    
    if ([self.transferBgImgName isEqualToString:@"bg_center_top"]){ // 第一个
        self.iconImgView.center_y = ([CenterNormalTableViewCell calculationCellHeightWithLocation:cellLocationTop] - LCFloat(24)) / 2. + LCFloat(24);
    } else if([self.transferBgImgName isEqualToString:@"bg_center_bottom"]){    //最后一个
        self.iconImgView.center_y = ([CenterNormalTableViewCell calculationCellHeightWithLocation:cellLocationBottom] - LCFloat(24)) / 2. ;
    } else {
        self.iconImgView.center_y = [CenterNormalTableViewCell calculationCellHeightWithLocation:cellLocationNormal] / 2.;
    }
    self.titleLabel.center_y = self.iconImgView.center_y;
    self.arrowImgView.center_y = self.titleLabel.center_y;

    // dymic
    if (self.transferDymic.length){
        CGSize dymicSize = [Tool makeSizeWithLabel:self.dymicLabel];
        self.dymicLabel.frame = CGRectMake(self.arrowImgView.orgin_x - LCFloat(11) - dymicSize.width, 0, dymicSize.width, dymicSize.height);
        self.dymicLabel.center_y = self.titleLabel.center_y;
    }
}

+(CGFloat)calculationCellHeightWithLocation:(cellLocation)location{
    if (location == cellLocationTop){
        return LCFloat(84);
    } else if (location == cellLocationNormal){
        return LCFloat(60);
    } else if (location == cellLocationBottom){
        return LCFloat(84);
    }
    return LCFloat(60);
}

@end
