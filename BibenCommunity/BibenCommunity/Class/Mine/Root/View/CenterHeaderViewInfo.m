//
//  CenterHeaderViewInfo.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/24.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterHeaderViewInfo.h"
#import "CenterHeaderLevelIconView.h"

static char actionClickWithHeaderAvatarBlockKey;
@interface CenterHeaderViewInfo()
@property (nonatomic,strong)PDImageView *avatarBgImgView;
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *nickLabel;
@property (nonatomic,strong)UILabel *IdLabel;
@property (nonatomic,strong)UILabel *descLabel;
@property (nonatomic,strong)UIButton *actionButton;
@property (nonatomic,strong)CenterHeaderLevelIconView *levelIconView;
@end

@implementation CenterHeaderViewInfo

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    self.avatarBgImgView = [[PDImageView alloc]init];
    self.avatarBgImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarBgImgView];
    
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarImgView];
    
    self.nickLabel = [GWViewTool createLabelFont:@"18" textColor:@"白"];
    self.nickLabel.font = [self.nickLabel.font boldFont];
    [self addSubview:self.nickLabel];
    
    self.IdLabel = [GWViewTool createLabelFont:@"13" textColor:@"白"];
    [self addSubview:self.IdLabel];
    
    self.descLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"白"];
    [self addSubview:self.descLabel];
    
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.actionButton];
    __weak typeof(self)weakSelf = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithHeaderAvatarBlockKey);
        if (block){
            block();
        }
    }];
    
    self.levelIconView = [[CenterHeaderLevelIconView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.nickLabel.frame) + LCFloat(12), 0, [CenterHeaderLevelIconView calculationSize].width, [CenterHeaderLevelIconView calculationSize].height) withLevel:1];
    self.levelIconView.hidden = YES;
    self.levelIconView.alpha = 0;
    [self addSubview:self.levelIconView];
}

-(void)setTransferAccountModel:(AccountModel *)transferAccountModel{
    _transferAccountModel = transferAccountModel;
    
    // avatarBg
    self.avatarBgImgView.frame = CGRectMake(0, 0, self.size_height, self.size_height);
    self.avatarBgImgView.image = [UIImage imageNamed:@"bg_center_avatar_alpha"];
    
    // avatar
    self.avatarImgView.frame = CGRectMake(0, 0, LCFloat(70), LCFloat(70));
    self.avatarImgView.layer.cornerRadius = self.avatarImgView.size_height / 2.;
    self.avatarImgView.center = self.avatarBgImgView.center;
    self.avatarImgView.clipsToBounds = YES;
    [self.avatarImgView uploadImageWithURL:transferAccountModel.loginServerModel.account.head_img placeholder:nil callback:NULL];
    
    // 3. btn
    self.actionButton.frame = self.avatarBgImgView.frame;
    
    // 4. nick
    self.nickLabel.text = transferAccountModel.loginServerModel.user.nickname.length?transferAccountModel.loginServerModel.user.nickname:@"没有名字";
    CGSize nickSize = [Tool makeSizeWithLabel:self.nickLabel];
    
    // 5.id
    self.IdLabel.text = [NSString stringWithFormat:@"ID:%@",transferAccountModel.loginServerModel.account._id];
    CGSize IDSize = [Tool makeSizeWithLabel:self.IdLabel];

    
    // 6.desc
    self.descLabel.text = transferAccountModel.loginServerModel.user.self_introduction.length?transferAccountModel.loginServerModel.user.self_introduction:@"这家伙很懒，什么都没有留下";
    CGFloat width = kScreenBounds.size.width - CGRectGetMaxX(self.avatarBgImgView.frame) - 2 * LCFloat(11);
    CGSize descSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    CGFloat mainDescHeight = 0;
    if (descSize.height >= 2 * [NSString contentofHeightWithFont:self.descLabel.font]){
        mainDescHeight = 2 * [NSString contentofHeightWithFont:self.descLabel.font];
    } else {
        mainDescHeight = 1 * [NSString contentofHeightWithFont:self.descLabel.font];
    }

    
    CGFloat margin_h = ([CenterHeaderViewInfo calculationHeight] - [NSString contentofHeightWithFont:self.nickLabel.font] - LCFloat(6) - [NSString contentofHeightWithFont:self.IdLabel.font] - mainDescHeight - LCFloat(11)) / 2.;
    
    self.nickLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarBgImgView.frame) + LCFloat(5), self.avatarBgImgView.orgin_y + margin_h, nickSize.width, nickSize.height);
    self.IdLabel.frame = CGRectMake(self.nickLabel.orgin_x, CGRectGetMaxY(self.nickLabel.frame) +LCFloat(6), kScreenBounds.size.width - CGRectGetMaxX(self.avatarBgImgView.frame) - 2 * LCFloat(11), IDSize.height);
    
    if (descSize.height >= 2 * [NSString contentofHeightWithFont:self.descLabel.font]){
        self.descLabel.numberOfLines = 2;
        self.descLabel.frame = CGRectMake(self.nickLabel.orgin_x, CGRectGetMaxY(self.IdLabel.frame) + LCFloat(11), width, 2 * [NSString contentofHeightWithFont:self.descLabel.font]);
    } else {
        self.descLabel.numberOfLines = 1;
        self.descLabel.frame = CGRectMake(self.nickLabel.orgin_x, CGRectGetMaxY(self.IdLabel.frame) + LCFloat(11), width, 1 * [NSString contentofHeightWithFont:self.descLabel.font]);
    }
    
    self.descLabel.orgin_y = CGRectGetMaxY(self.IdLabel.frame) + LCFloat(11);
    
//    self.levelIconView.orgin_x = CGRectGetMaxX(self.nickLabel.frame) + LCFloat(12);

    CGFloat maxWidth = kScreenBounds.size.width - CGRectGetMaxX(self.avatarBgImgView.frame) - LCFloat(15) - self.levelIconView.size_width - LCFloat(12);
    self.nickLabel.adjustsFontSizeToFitWidth = YES;
    self.nickLabel.size_width = MIN(maxWidth, nickSize.width);
    self.levelIconView.orgin_x = CGRectGetMaxX(self.nickLabel.frame) + LCFloat(12);

    
    if (self.levelIconView && self.levelIconView.hidden == YES){
        self.levelIconView.hidden = NO;
        self.levelIconView.center_y = self.nickLabel.center_y;
        [UIView animateWithDuration:.5f animations:^{
            self.levelIconView.alpha = 1;
        } completion:^(BOOL finished) {
            self.levelIconView.alpha = 1;
            self.levelIconView.hidden = NO;
        }];
    }
    

}

-(void)actionClickWithHeaderAvatarBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithHeaderAvatarBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationHeight{
    
    return LCFloat(105);
}

@end
