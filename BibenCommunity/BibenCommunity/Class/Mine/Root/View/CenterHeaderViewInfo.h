//
//  CenterHeaderViewInfo.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/24.
//  Copyright © 2018年 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CenterHeaderViewInfo : UIView

@property (nonatomic,strong)AccountModel *transferAccountModel;

-(void)actionClickWithHeaderAvatarBlock:(void(^)())block;

+(CGFloat)calculationHeight;

@end
