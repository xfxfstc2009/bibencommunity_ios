//
//  CenterHeaderSegmentItemsTableViewCell.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/29.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

@interface CenterHeaderSegmentSingleItemView:UIView
@property (nonatomic,strong)UILabel *dymicLabel;
@property (nonatomic,strong)UILabel *fixedLabel;

-(void)actionClickWithBlock:(void(^)())block;

+(CGFloat)calculationHeight;

@end

@interface CenterHeaderSegmentItemsTableViewCell : PDBaseTableViewCell

-(void)segmentListItemClickWithBlock:(void(^)(NSInteger index))block;

-(void)reloadItems;

@end
