//
//  CenterUserInfoLivePlayerDetailView.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/25.
//  Copyright © 2018年 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CenterUserInfoLivePlayerDetailView : UIView

-(void)actionClickWithShowUserInfoBlock:(void(^)())block;

@end
