//
//  CenterUserInformationLiveViewController.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/25.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterUserInformationLiveViewController.h"
#import "CenterUserInfoLiveSingleTableViewCell.h"

@interface CenterUserInformationLiveViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *liveTableView;
@property (nonatomic,strong)NSMutableArray *liveMutableArr;
@end

@implementation CenterUserInformationLiveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"直播";
    self.view.backgroundColor = UURandomColor;
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.liveMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.liveTableView){
        self.liveTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.liveTableView.delegate = self;
        self.liveTableView.dataSource = self;
        [self.view addSubview:self.liveTableView];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.liveMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.liveMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    CenterUserInfoLiveSingleTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[CenterUserInfoLiveSingleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [CenterUserInfoLiveSingleTableViewCell calculationCellHeight];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
