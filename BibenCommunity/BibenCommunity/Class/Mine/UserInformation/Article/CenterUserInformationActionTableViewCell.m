//
//  CenterUserInformationActionTableViewCell.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/25.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterUserInformationActionTableViewCell.h"

@interface CenterUserInformationActionTableViewCell()
@property (nonatomic,strong)PDImageView *coinImgView;
@property (nonatomic,strong)UILabel *coinLabel;
@property (nonatomic,strong)UIButton *zanButton;
@property (nonatomic,strong)UIButton *chatButton;
@property (nonatomic,strong)UIButton *shareButton;
@end

@implementation CenterUserInformationActionTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.coinImgView = [[PDImageView alloc]init];
    self.coinImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.coinImgView];
    
    self.coinLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"黑"];
    [self addSubview:self.coinLabel];
    
    self.zanButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.zanButton];
    
    self.chatButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.chatButton];
    
    self.shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.shareButton];
}

-(void)frameInfo{
    
}

@end
