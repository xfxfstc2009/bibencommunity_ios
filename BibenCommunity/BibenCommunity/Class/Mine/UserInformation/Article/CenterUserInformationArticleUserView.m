//
//  CenterUserInformationArticleUserView.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/25.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterUserInformationArticleUserView.h"

static char actionClickWithAvatarBlockKey;
@interface CenterUserInformationArticleUserView()
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *nicklabel;
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)UIButton *actionButton;
@end

@implementation CenterUserInformationArticleUserView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarImgView];
    
    self.nicklabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    [self addSubview:self.nicklabel];
    
    self.timeLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"浅灰"];
    [self addSubview:self.timeLabel];
    
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.actionButton];
    __weak typeof(self)weakSelf = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithAvatarBlockKey);
        if (block){
            block();
        }
    }];
}

-(void)actionClickWithAvatarBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithAvatarBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end
