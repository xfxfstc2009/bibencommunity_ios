//
//  CenterUserInformationEvaluateShareView.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/25.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterUserInformationEvaluateShareView.h"

@interface CenterUserInformationEvaluateShareView()
@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)PDImageView *imgView;

@end

@implementation CenterUserInformationEvaluateShareView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.bgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"正文" textColor:@"灰"];
    [self addSubview:self.titleLabel];
    
    self.imgView = [[PDImageView alloc]init];
    self.imgView.backgroundColor = UURandomColor;
    [self addSubview:self.imgView];
}


@end
