//
//  CenterInvitationNumberTableViewCell.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/30.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "CenterInvitationSingleModel.h"



@interface CenterInvitationNumberSingleView : UIView

@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)UILabel *dymicLabel;

@end



@interface CenterInvitationNumberTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)CenterInvitationSingleModel *transferSingleModel;

@end
