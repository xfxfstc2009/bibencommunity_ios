//
//  ShareRootViewController.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/14.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "AbstractViewController.h"

@interface ShareRootViewController : UIViewController

// 相关属性
@property (nonatomic,assign) BOOL isHasGesture;                             // 判断是否包含手势

- (void)showInView:(UIViewController *)viewController;
- (void)dismissFromView:(UIViewController *)viewController;
- (void)hideParentViewControllerTabbar:(UIViewController *)viewController;              // 毛玻璃效果

-(void)actionClickWithShareBlock:(void(^)(NSString *type))block;

@end
