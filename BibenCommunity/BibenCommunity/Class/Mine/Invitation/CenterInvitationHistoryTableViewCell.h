//
//  CenterInvitationHistoryTableViewCell.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/9/8.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "CenterInvitationSingleModel.h"

@interface CenterInvitationHistoryTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)CenterInvitationHistorySingleModel *transferModel;

@end
