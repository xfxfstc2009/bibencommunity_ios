//
//  CenterBBTHeaderView.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/31.
//  Copyright © 2018年 币本. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CenterBBTSingleModel.h"

@interface CenterBBTHeaderViewItems:UIView

@property (nonatomic,strong)UILabel *dymicLabel;
@property (nonatomic,strong)UILabel *fixedLabel;

@end

@interface CenterBBTHeaderView : UIView

@property (nonatomic,strong)CenterBBTHeaderSingleModel *transferSingleModel;

-(void)actionBackButtonClick:(void(^)())block;

+(CGFloat)calculationHeaderView;

@end

