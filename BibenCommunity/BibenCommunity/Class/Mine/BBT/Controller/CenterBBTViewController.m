//
//  CenterBBTViewController.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/31.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterBBTViewController.h"
#import "CenterBBTSingleCell.h"
#import "CenterBBTHeaderView.h"
#import "NetworkAdapter+Center.h"
#import "CenterBBTHistorySingleModel.h"

@interface CenterBBTViewController ()<UITableViewDataSource,UITableViewDelegate>{
    CenterBBTHeaderSingleModel *singleModel;
}
@property (nonatomic,strong)UITableView *bbtTableView;
@property (nonatomic,strong)CenterBBTHeaderView *headerView;
@property (nonatomic,strong)NSMutableArray *bbtRootMutableArr;
@property (nonatomic,strong)NSMutableArray *orderDetailMutableArr;
@end

@implementation CenterBBTViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetJiangliInfo];
    [self sendRequestTogetHistory];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"BBT资产";
    self.headerView = [[CenterBBTHeaderView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, [CenterBBTHeaderView calculationHeaderView])];
    __weak typeof(self)weakSelf = self;
    [self.headerView actionBackButtonClick:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.navigationController popViewControllerAnimated:YES];
    }];
    [self.view addSubview:self.headerView];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.bbtRootMutableArr = [NSMutableArray array];
    self.orderDetailMutableArr = [NSMutableArray array];
    [self.bbtRootMutableArr addObject:@[@"账单明细"]];
    [self.bbtRootMutableArr addObject:self.orderDetailMutableArr];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.bbtTableView){
        self.bbtTableView = [GWViewTool gwCreateTableViewRect:CGRectMake(0,CGRectGetMaxY(self.headerView.frame) , kScreenBounds.size.width, self.view.size_height - CGRectGetMaxY(self.headerView.frame))];
        self.bbtTableView.dataSource = self;
        self.bbtTableView.delegate = self;
        [self.view addSubview:self.bbtTableView];

    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.bbtRootMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.bbtRootMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"账单明细" sourceArr:self.bbtRootMutableArr]){
        static NSString *cellIdentifyWithRowZero = @"cellIdentifyWithRowZero";
        GWNormalTableViewCell *cellWithRowZero = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowZero];
        if (!cellWithRowZero){
            cellWithRowZero = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowZero];
        }
        cellWithRowZero.transferCellHeight = cellHeight;
        cellWithRowZero.transferTitle = @"账单明细";
        cellWithRowZero.titleLabel.font = [UIFont fontWithCustomerSizeName:@"15"];
        return cellWithRowZero;
    } else {
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        CenterBBTSingleCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[CenterBBTSingleCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferBBTModel = [[self.bbtRootMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        return cellWithRowOne;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.bbtTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == [[self.bbtRootMutableArr objectAtIndex:indexPath.section] count] - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([[self.bbtRootMutableArr objectAtIndex:indexPath.section] count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"center"];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        return LCFloat(58);
    } else {
        return [CenterBBTSingleCell calculationCellHeight];
    }
}

#pragma mark - sendRequest
#pragma mark - 获取历史
-(void)sendRequestTogetHistory{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] centerGetMoneyHistoryListMnagerWithPage:self.bbtTableView.currentPage block:^(CenterBBTHistoryListModel *listModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.bbtTableView.isXiaLa){
            [strongSelf.orderDetailMutableArr removeAllObjects];
        }
        
        if (listModel.content.count){
            [strongSelf.orderDetailMutableArr addObjectsFromArray:listModel.content];\
        }
        
        [strongSelf.bbtTableView reloadData];
        
        [strongSelf.bbtTableView stopPullToRefresh];
        [strongSelf.bbtTableView stopFinishScrollingRefresh];
    }];
}
    
#pragma mark - 获取奖励金额
-(void)sendRequestToGetJiangliInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] centerGetMoneyAndJiangliBlock:^(CenterBBTHeaderSingleModel *singleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.headerView.transferSingleModel = singleModel;
    }];
}

@end
