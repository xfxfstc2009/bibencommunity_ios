//
//  CenterBBTSingleModel.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/31.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "FetchModel.h"

@interface CenterBBTHeaderFinanceSingleModel : FetchModel
@property (nonatomic,assign)CGFloat diamond;
@property (nonatomic,assign)CGFloat coin;

@end


@interface CenterBBTHeaderInvitationSingleModel: FetchModel
@property (nonatomic,copy)NSString *create_time;
@property (nonatomic,copy)NSString *phone;
@property (nonatomic,copy)NSString *invite_account_id;
@property (nonatomic,assign)NSInteger status;
@property (nonatomic,copy)NSString *register_time;
@property (nonatomic,copy)NSString *register_date;
@property (nonatomic,copy)NSString *account_id;
@property (nonatomic,assign)CGFloat total_reward;
@property (nonatomic,assign)CGFloat unclaimed_reward;
@property (nonatomic,assign)CGFloat already_reward;
@property (nonatomic,assign)CGFloat once_reward;
@property (nonatomic,assign)NSInteger count;
@property (nonatomic,assign)NSInteger invite_account_reward;
@end

@interface CenterBBTHeaderSingleModel : FetchModel

@property (nonatomic,strong)CenterBBTHeaderInvitationSingleModel *invitation;
@property (nonatomic,strong)CenterBBTHeaderFinanceSingleModel *finance;

@end




