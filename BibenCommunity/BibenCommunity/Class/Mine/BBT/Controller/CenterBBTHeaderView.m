//
//  CenterBBTHeaderView.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/31.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterBBTHeaderView.h"

static char actionBackButtonClickKey;
@interface CenterBBTHeaderView()
@property (nonatomic,strong)PDImageView *bgImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *yueLabel;
@property (nonatomic,strong)UIView *lineView;
@property (nonatomic,strong)UILabel *fenqilingquLabel;
@property (nonatomic,strong)UIView *lineView2;
@property (nonatomic,strong)CenterBBTHeaderViewItems *dailingquView;
@property (nonatomic,strong)CenterBBTHeaderViewItems *yilingquView;
@property (nonatomic,strong)CenterBBTHeaderViewItems *amountView;
@property (nonatomic,strong)UILabel *naBarLabel;
@property (nonatomic,strong)UIButton *backButton;

@end

@implementation CenterBBTHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark createView
-(void)createView{
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.bgImgView];
    self.bgImgView.frame = self.bounds;
    self.bgImgView.image = [Tool stretchImageWithName:@"bg_center_bbt"];
    
    self.titleLabel = [GWViewTool createLabelFont:@"45" textColor:@"白"];
    self.titleLabel.font = [self.titleLabel.font boldFont];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.titleLabel];

    self.yueLabel = [GWViewTool createLabelFont:@"14" textColor:@"白"];
    [self addSubview:self.yueLabel];
    self.yueLabel.textAlignment = NSTextAlignmentCenter;
    
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor hexChangeFloat:@"E6E6E6"];
    [self addSubview:self.lineView];
    
    self.fenqilingquLabel = [GWViewTool createLabelFont:@"13" textColor:@"白"];
    self.fenqilingquLabel.numberOfLines = 2;
    [self addSubview:self.fenqilingquLabel];
    
    self.lineView2 = [[UIView alloc]init];
    self.lineView2.backgroundColor = [UIColor hexChangeFloat:@"E6E6E6"];
    [self addSubview:self.lineView2];
    
    self.naBarLabel = [GWViewTool createLabelFont:@"16" textColor:@"FFFFFF"];
    self.naBarLabel.text = @"BBT资产";
    self.naBarLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.naBarLabel];
    
    self.backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.backButton.backgroundColor = [UIColor clearColor];
    [self.backButton setImage:[UIImage imageNamed:@"icon_center_bbt_back"] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [self.backButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionBackButtonClickKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.backButton];
    
    self.titleLabel.frame = CGRectMake(0, LCFloat(31), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.titleLabel.font]);

    self.yueLabel.frame = CGRectMake(0, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(14), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.yueLabel.font]);
    
    self.lineView.frame = CGRectMake(0, [CenterBBTHeaderView calculationHeaderView] - LCFloat(60) - 1.f, kScreenBounds.size.width, .5f);
    self.lineView2.frame = CGRectMake(LCFloat(62), CGRectGetMaxY(self.lineView.frame), .5f, [CenterBBTHeaderView calculationHeaderView] - CGRectGetMaxY(self.lineView.frame));
    
    self.fenqilingquLabel.frame = CGRectMake(0, 0, LCFloat(30), 2 * [NSString contentofHeightWithFont:self.fenqilingquLabel.font]);
    self.fenqilingquLabel.center = CGPointMake(LCFloat(62) / 2., self.lineView.orgin_y + self.lineView2.size_height / 2.);
    
    CGFloat item_width = (kScreenBounds.size.width - CGRectGetMaxX(self.lineView2.frame)) / 3.;
    CGFloat item_height = LCFloat(60);
    self.dailingquView = [[CenterBBTHeaderViewItems alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.lineView2.frame), CGRectGetMaxY(self.lineView.frame), item_width, item_height)];
    [self addSubview:self.dailingquView];
    
    self.yilingquView = [[CenterBBTHeaderViewItems alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.dailingquView.frame), CGRectGetMaxY(self.lineView.frame), item_width, item_height)];
    [self addSubview:self.yilingquView];
    
    self.amountView = [[CenterBBTHeaderViewItems alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.yilingquView.frame), CGRectGetMaxY(self.lineView.frame), item_width, item_height)];
    [self addSubview:self.amountView];
    
    self.yueLabel.orgin_y = self.lineView.orgin_y - LCFloat(39) - self.yueLabel.size_height;
    
    self.titleLabel.orgin_y = self.yueLabel.orgin_y - LCFloat(14) - self.titleLabel.size_height;
    
    self.naBarLabel.frame = CGRectMake(0, [BYTabbarViewController sharedController].statusHeight + LCFloat(16), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.naBarLabel.font]);
    
    self.backButton.frame = CGRectMake(0, [BYTabbarViewController sharedController].statusHeight, 44, 44);
}
    

-(void)setTransferSingleModel:(CenterBBTHeaderSingleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    
    // title
    self.titleLabel.text = [NSString stringWithFormat:@"%.2f",transferSingleModel.finance.coin];
    
    // fixedTitle
    self.yueLabel.text = [NSString stringWithFormat:@"余额(BBT)"];

    self.fenqilingquLabel.text = @"分期领取";
    
    self.dailingquView.fixedLabel.text = @"待领取";
    self.dailingquView.dymicLabel.text = [NSString stringWithFormat:@"%.2f",transferSingleModel.invitation.unclaimed_reward];
    
    self.yilingquView.fixedLabel.text = [NSString stringWithFormat:@"已领取（%li次)",transferSingleModel.invitation.count];
    self.yilingquView.dymicLabel.text = [NSString stringWithFormat:@"%.2f",transferSingleModel.invitation.already_reward];
    
    self.amountView.fixedLabel.text = @"总余额";
    self.amountView.dymicLabel.text = [NSString stringWithFormat:@"%.2f",transferSingleModel.invitation.total_reward];
 
}

+(CGFloat)calculationHeaderView{
    CGFloat height = 0;
    if (IS_iPhoneX){
        height += 24;
    }
    height += LCFloat(252);
    return height;
}

-(void)actionBackButtonClick:(void(^)())block{
    objc_setAssociatedObject(self, &actionBackButtonClickKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end






@interface CenterBBTHeaderViewItems()

@end

@implementation CenterBBTHeaderViewItems

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.dymicLabel = [GWViewTool createLabelFont:@"17" textColor:@"白"];
    self.dymicLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.dymicLabel];
    
    self.fixedLabel = [GWViewTool createLabelFont:@"13" textColor:@"白"];
    [self addSubview:self.fixedLabel];
    self.fixedLabel.textAlignment = NSTextAlignmentCenter;
    self.dymicLabel.adjustsFontSizeToFitWidth = YES;
    self.fixedLabel.adjustsFontSizeToFitWidth = YES;
    
    CGFloat margin = (self.size_height - LCFloat(6) - [NSString contentofHeightWithFont:self.fixedLabel.font] - [NSString contentofHeightWithFont:self.dymicLabel.font]) / 2.;
    self.dymicLabel.frame = CGRectMake(0, margin, self.size_width, [NSString contentofHeightWithFont:self.dymicLabel.font]);
    self.fixedLabel.frame = CGRectMake(0, self.size_height - [NSString contentofHeightWithFont:self.fixedLabel.font] - margin, self.size_width, [NSString contentofHeightWithFont:self.fixedLabel.font]);
}



@end
