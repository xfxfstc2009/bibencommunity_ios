//
//  PDWelcomeViewController.h
//  PandaKing
//
//  Created by Cranz on 16/10/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

/// App 第一次启动欢迎页
@interface PDWelcomeViewController : AbstractViewController

@property (nonatomic, copy) void (^startBlock)();         // 是否登录

@end
