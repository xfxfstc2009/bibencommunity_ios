//
//  AboutTopTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/30.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "AboutTopTableViewCell.h"

@interface AboutTopTableViewCell()
@property (nonatomic,strong)PDImageView *iconImgView;
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UILabel *versionLabel;
@end

@implementation AboutTopTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}


-(void)createView{
    // icon
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    self.iconImgView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(64)) / 2., LCFloat(35), LCFloat(64), LCFloat(64));
    self.iconImgView.image = [UIImage imageNamed:@"icon_main_logo"];
    [self addSubview:self.iconImgView];
    
    self.nameLabel = [GWViewTool createLabelFont:@"13" textColor:@"383838"];
    self.nameLabel.font = [self.nameLabel.font boldFont];
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.frame = CGRectMake(0, CGRectGetMaxY(self.iconImgView.frame) + LCFloat(16), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.nameLabel.font]);
    self.nameLabel.text = @"币本社区";
    self.nameLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.nameLabel];
    
    self.versionLabel = [GWViewTool createLabelFont:@"12" textColor:@"929292"];
    self.versionLabel.textAlignment = NSTextAlignmentCenter;
    self.versionLabel.backgroundColor = [UIColor clearColor];
    self.versionLabel.text = [Tool appVersion];
    [self addSubview:self.versionLabel];
    self.versionLabel.frame = CGRectMake(0, CGRectGetMaxY(self.nameLabel.frame), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.versionLabel.font]);
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0 ;
    cellHeight += LCFloat(47);
    cellHeight += LCFloat(64);
    cellHeight += LCFloat(16);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"13"]];
    cellHeight += LCFloat(10);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"12"]];
    cellHeight += LCFloat(48);
    return cellHeight;
}
@end
