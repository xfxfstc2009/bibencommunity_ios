//
//  DelegateOneCell.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/29.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "DelegateOneCell.h"

@interface DelegateOneCell()

@property (nonatomic,strong)UILabel *fixedLabel;

@end

@implementation DelegateOneCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    self.fixedLabel = [GWViewTool createLabelFont:@"13" textColor:@"8F8F8F"];
    self.fixedLabel.numberOfLines = 0;
    [self addSubview:self.fixedLabel];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferInfo:(NSString *)transferInfo{
    if([transferInfo hasPrefix:@"#"]){
        NSArray *tempArr = [transferInfo componentsSeparatedByString:@"#"];
        self.fixedLabel.text = [tempArr lastObject];
    } else if ([transferInfo hasPrefix:@"$"]){
        NSArray *tempArr = [transferInfo componentsSeparatedByString:@"$"];
        self.fixedLabel.text = [tempArr lastObject];
    }

    CGSize contentOfSzie = [self.fixedLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
    
    self.fixedLabel.frame = CGRectMake(LCFloat(11), LCFloat(7), kScreenBounds.size.width - 2 * LCFloat(11), contentOfSzie.height);
}

-(void)setTransferType:(delegateType)transferType{
    _transferType = transferType;
}

+(CGFloat)calculationCellHeightWithInfo:(NSString *)info{
    NSString *newInfo = @"";
    if([info hasPrefix:@"#"]){
        NSArray *tempArr = [info componentsSeparatedByString:@"#"];
        newInfo = [tempArr lastObject];
    } else if ([info hasPrefix:@"$"]){
        NSArray *tempArr = [info componentsSeparatedByString:@"$"];
        newInfo = [tempArr lastObject];
    }
    
    CGSize contentOfSzie = [newInfo sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"13"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
    return LCFloat(7) * 2 + contentOfSzie.height;
}

@end
