//
//  DelegateOneCell.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/29.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,delegateType) {
    delegateTypeHeader,
    delegateTypeTitle,
};

@interface DelegateOneCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,copy)NSString *transferInfo;
@property (nonatomic,assign)delegateType transferType;

+(CGFloat)calculationCellHeightWithInfo:(NSString *)info;
@end
