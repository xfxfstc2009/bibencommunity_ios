//
//  NetworkAdapter+Center.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/27.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "NetworkAdapter.h"
#import "AccountModel.h"
#import "CenterInvitationSingleModel.h"
#import "CenterFollowModel.h"
#import "CenterBBTHistorySingleModel.h"
#import "CenterBBTSingleModel.h"
#import "CenterMessageSingleModel.h"
#import "CenterShareSingleModel.h"


@interface NetworkAdapter (Center)
// 获取账户信息
-(void)centerGetUserCountInfoManagerBlock:(void(^)(BOOL isSuccessed))block;
#pragma mark - 获取资金及注册奖励
-(void)centerGetMoneyAndJiangliBlock:(void(^)(CenterBBTHeaderSingleModel *singleModel))block;
#pragma mark - 获取我邀请的用户数量和奖励总额
-(void)centerGetMoneyget_my_invitationBlock:(void(^)(BOOL isSuccessed,CenterInvitationSingleModel *model))block;
#pragma mark - 获取我的邀请记录
-(void)centerGetInvitationHistoryPage:(NSInteger)page block:(void(^)(CenterInvitationHistoryListModel *singleModel))block;
#pragma mark - 获取关注列表
-(void)sendRequestToGetLinkListManagerWithNumber:(NSInteger)number block:(void(^)(BOOL isSuccessed,CenterFollowModel *singleModel))block;
#pragma mark - 获取粉丝列表
-(void)sendRequestToGetMyFansManagerWithNumber:(NSInteger)number block:(void(^)(BOOL isSuccessed,CenterFollowModel *singleModel))block;
#pragma mark - 关注&取消关注
-(void)centerSendRequestToLinkManagerWithUserId:(NSString *)userId hasLink:(BOOL)hasLink block:(void(^)(BOOL isSuccessed))block;
#pragma mark - 获取分页资金历史记录
-(void)centerGetMoneyHistoryListMnagerWithPage:(NSInteger)page block:(void(^)(CenterBBTHistoryListModel *listModel))block;
#pragma mark - 分页获取我的消息
-(void)fetchMyMessageWithInfoWithPageNum:(NSInteger)pageNum block:(void(^)(CenterMessageListModel *listModel))block;
#pragma mark - 清空我的消息
-(void)centerCleanMyMessagesWithBlock:(void(^)(BOOL isSuccessed))block;
#pragma mark - 消息全部设为已读
-(void)centerMessageReadAllBlock:(void(^)(BOOL isSuccessed))block;
#pragma mark - 获取账户详情信息
-(void)centerGetMainUserInfoManagerBlock:(void(^)())block;
#pragma mark - 修改我的喜欢
-(void)centerChangeMyLike:(NSArray<LoginServerModelAccountTopic> *)transferArr block:(void(^)())block;
#pragma mark - 签到领取BBT
-(void)centerSignGetBBTManagerBlock:(void(^)(BOOL isSuccessed,NSInteger reward))block;
#pragma mark - 获取分享
-(void)centerGetShareInfoWithBlock:(void(^)(BOOL isSuccessed,CenterShareSingleModel *shareModel))block;
@end
