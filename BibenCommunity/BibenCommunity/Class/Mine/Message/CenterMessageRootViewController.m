//
//  CenterMessageRootViewController.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/24.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterMessageRootViewController.h"
#import "CenterMessageSingleModel.h"
#import "CenterMessageTableViewCell.h"
#import "NetworkAdapter+Center.h"

@interface CenterMessageRootViewController ()<UITableViewDelegate,UITableViewDataSource>{
    BOOL hasFirstRead;
}
@property (nonatomic,strong)UITableView *messageTableView;
@property (nonatomic,strong)NSMutableArray *messageMutableArr;
@end

@implementation CenterMessageRootViewController

+(instancetype)sharedController{
    static CenterMessageRootViewController *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[CenterMessageRootViewController alloc] init];
    });
    return _sharedAccountModel;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self centerGetMessageInfo];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"我的消息";
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:@"清除" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf centerCleanMyMessageManager];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    hasFirstRead = NO;
    self.messageMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.messageTableView){
        self.messageTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.messageTableView.dataSource = self;
        self.messageTableView.delegate = self;
        [self.view addSubview:self.messageTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.messageTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf centerGetMessageInfo];
    }];
    [self.messageTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf centerGetMessageInfo];
    }];
}

#pragma mark - UITableView
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.messageMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    CenterMessageTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[CenterMessageTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cellWithRowOne.transferMessageModel = [self.messageMutableArr objectAtIndex:indexPath.row];
    
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"1");
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [CenterMessageTableViewCell calculationCellHeight:[self.messageMutableArr objectAtIndex:indexPath.row]];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.messageTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == [self.messageMutableArr count] - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([self.messageMutableArr count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }
}

#pragma mark - interface
-(void)centerGetMessageInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchMyMessageWithInfoWithPageNum:self.messageTableView.currentPage block:^(CenterMessageListModel *listModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.messageTableView.isXiaLa){
            [strongSelf.messageMutableArr removeAllObjects];
        }
        if (listModel.content.count){
            [strongSelf.messageMutableArr addObjectsFromArray:listModel.content];
        }
        
        [strongSelf.messageTableView reloadData];

        [strongSelf.messageTableView stopPullToRefresh];
        [strongSelf.messageTableView stopFinishScrollingRefresh];
        
        if(strongSelf.messageMutableArr.count){
            [strongSelf.messageTableView dismissPrompt];
        } else {
            [strongSelf.messageTableView showPrompt:@"当前没有消息数据哦" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
        }

        
        // 全部标记已读
        if (strongSelf->hasFirstRead == NO){
            strongSelf->hasFirstRead = YES;
            [strongSelf centerSignHasRead];
        }
        
    }];
}

#pragma mark - 清除我的消息
-(void)centerCleanMyMessageManager{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] centerCleanMyMessagesWithBlock:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            [strongSelf.messageMutableArr removeAllObjects];
        }
        [strongSelf.messageTableView reloadData];
        if (strongSelf.messageMutableArr.count){
            [strongSelf.messageTableView dismissPrompt];
        } else {
            [strongSelf.messageTableView showPrompt:@"当前没有消息" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
        }
    }];
}

#pragma mark - 全部标记已读
-(void)centerSignHasRead{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] centerMessageReadAllBlock:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            if (strongSelf.messageMutableArr.count){
                for (int i = 0 ;i < strongSelf.messageMutableArr.count;i++){
                    CenterMessageSingleModel *singleModel = [strongSelf.messageMutableArr objectAtIndex:i];
                    singleModel.is_read = YES;
                }
                [strongSelf.messageTableView reloadData];
            }
        }
    }];
}

@end
