//
//  CenterMessageSingleModel.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/30.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "FetchModel.h"

@protocol CenterMessageSingleModel<NSObject>
@end

@interface CenterMessageSingleModel : FetchModel

@property (nonatomic,copy)NSString *_id;
@property (nonatomic,copy)NSString *account_id;
@property (nonatomic,copy)NSString *content;
@property (nonatomic,assign)NSTimeInterval create_time;
@property (nonatomic,copy)NSString *message_id;
@property (nonatomic,copy)NSString *status;
@property (nonatomic,copy)NSString *title;
@property (nonatomic,copy)NSString *type;
@property (nonatomic,copy)NSString *type_code;
@property (nonatomic,assign)BOOL is_read;

@end

@interface CenterMessageListModel : FetchModel

@property (nonatomic,strong)NSArray<CenterMessageSingleModel> *content;

@end
