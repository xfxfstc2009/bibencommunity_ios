//
//  CenterMessageTableViewCell.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/30.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterMessageTableViewCell.h"

@interface CenterMessageTableViewCell()
@property (nonatomic,strong)PDImageView *messageIcon;
@property (nonatomic,strong)UILabel *messageLabel;
@property (nonatomic,strong)PDImageView *nImgView;
@property (nonatomic,strong)UILabel *nLabel;
@property (nonatomic,strong)UILabel *titleLabel;

@end

@implementation CenterMessageTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.messageIcon = [[PDImageView alloc]init];
    self.messageIcon.backgroundColor = [UIColor clearColor];
    [self addSubview:self.messageIcon];
    
    self.messageLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"浅灰"];
    [self addSubview:self.messageLabel];
    
    self.nImgView = [[PDImageView alloc]init];
    self.nImgView.image = [UIImage imageNamed:@"icon_message_new"];
    self.nImgView.backgroundColor = [UIColor clearColor];
    self.nImgView.frame = CGRectMake(0, 0, LCFloat(33), LCFloat(17));
    [self addSubview:self.nImgView];

    self.nLabel = [GWViewTool createLabelFont:@"13" textColor:@"969696"];
    [self addSubview:self.nLabel];
    
    self.titleLabel = [GWViewTool createLabelFont:@"14" textColor:@"2F2F2F"];
    self.titleLabel.numberOfLines = 0;
    [self addSubview:self.titleLabel];
}

-(void)setTransferMessageModel:(CenterMessageSingleModel *)transferMessageModel{
    _transferMessageModel = transferMessageModel;
    // 图标
    self.messageIcon.frame = CGRectMake(LCFloat(11), LCFloat(11), LCFloat(15), LCFloat(15));
    self.messageIcon.image = [UIImage imageNamed:@"icon_message_sounds"];
    //
    self.messageLabel.text = [transferMessageModel.type_code isEqualToString:@"0"]?@"系统消息":@"其他消息";
    CGSize messageSize = [Tool makeSizeWithLabel:self.messageLabel];
    self.messageLabel.frame = CGRectMake(CGRectGetMaxX(self.messageIcon.frame) + LCFloat(6), 0, messageSize.width, messageSize.height);
    self.messageLabel.center_y = self.messageIcon.center_y;
    
    // time
    self.nLabel.text = [NSDate getTimeWithString:transferMessageModel.create_time / 1000.];
    CGSize timeSize = [Tool makeSizeWithLabel:self.nLabel];
    self.nLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - timeSize.width, 0, timeSize.width, timeSize.height);
    self.nLabel.center_y = self.messageLabel.center_y;
    
    self.nImgView.frame = CGRectMake(self.nLabel.orgin_x - LCFloat(14) - LCFloat(33), 0, LCFloat(33), LCFloat(17));
    self.nImgView.center_y = self.nLabel.center_y;
    
    if (transferMessageModel.is_read){
        self.nImgView.hidden = YES;
    } else {
        self.nImgView.hidden = NO;
    }
    
    self.titleLabel.text = transferMessageModel.content;
    CGSize messageInfoSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
    self.titleLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.messageIcon.frame) + LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), messageInfoSize.height);
}

+(CGFloat)calculationCellHeight:(CenterMessageSingleModel *)transferMessageModel{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    cellHeight += LCFloat(20);
    cellHeight += LCFloat(11);
      CGSize messageInfoSize = [transferMessageModel.content sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
    cellHeight += messageInfoSize.height;
    cellHeight += LCFloat(11);
    return cellHeight;
}
@end
