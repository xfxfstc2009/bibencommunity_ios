//
//  CenterMessageRootViewController.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/24.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "AbstractViewController.h"

@interface CenterMessageRootViewController : AbstractViewController

+(instancetype)sharedController;

@end
