//
//  LoginRootViewController.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/22.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "AbstractViewController.h"

@interface LoginRootViewController : AbstractViewController

-(void)loginSuccess:(void(^)(BOOL success))block;


// 登录成功以后dismiss
-(void)actionNavLoginSuccessDismiss;

@end
