//
//  AccountModel.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/25.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "FetchModel.h"
#import "LoginServerModel.h"
#import "ShareSDKManager.h"

@interface AccountModel : FetchModel

+(instancetype)sharedAccountModel;                                                               /**< 单例*/

@property (nonatomic,strong)LoginServerModel *loginServerModel;                                  /**<本地登录的Model*/
@property (nonatomic,strong)NSString *token;
@property (nonatomic,copy)NSString *account_id;
@property (nonatomic,assign)thirdLoginType loginType;                                           /**< 登录类型*/

// 判断是否登录
- (BOOL)hasLoggedIn;

#pragma mark - 自动登录
-(void)autoLoginWithBlock:(void(^)(BOOL isSuccessed))block;
#pragma mark - 自动登录
- (void)authorizeWithCompletionHandler:(void(^)(BOOL successed))handler;
#pragma mark - IM登录
-(void)loginIMManager;

#pragma mark - logout
-(void)logoutManagerBlock:(void(^)())block;

@end
