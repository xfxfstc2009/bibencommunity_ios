//
//  ThirdLoginModel.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/9/7.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "FetchModel.h"

@interface ThirdLoginModel : FetchModel

@property (nonatomic,assign)BOOL isFirst;               /**< 判断是否第一次登录*/

@end
