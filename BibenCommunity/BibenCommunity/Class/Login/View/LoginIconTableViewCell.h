//
//  LoginIconTableViewCell.h
//  BBFinance
//
//  Created by 裴烨烽 on 2018/7/18.
//  Copyright © 2018年 川普投资. All rights reserved.
//

#import "PDBaseTableViewCell.h"

@interface LoginIconTableViewCell : PDBaseTableViewCell

@property (nonatomic,copy)NSString *transferAvatarImg;

@end
