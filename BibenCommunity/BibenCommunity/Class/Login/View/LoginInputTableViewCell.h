//
//  LoginInputTableViewCell.h
//  BBFinance
//
//  Created by 裴烨烽 on 2018/7/10.
//  Copyright © 2018年 川普投资. All rights reserved.
//

#import "PDBaseTableViewCell.h"

typedef NS_ENUM(NSInteger,LoginInputType) {
    LoginInputTypePhone,                    /**< 输入手机号*/
    LoginInputTypePwd,                      /**< 输入密码*/
    LoginInputTypePwdAgain,                 /**< 输入密码*/
    LoginInputTypeSMS,                      /**< 输入验证码*/
    LoginInputTypeNick,                     /**< 输入昵称*/
};

@protocol LoginInputTableViewCellDelegate <NSObject>

-(void)inputTextConfirm:(LoginInputType)type textField:(UITextField *)textField status:(BOOL)status;     /**< 判断当前输入内容是否符合*/

@end

@interface LoginInputTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)UITextField *inputTextField;            /**< 输入框*/
@property (nonatomic,weak)id<LoginInputTableViewCellDelegate> delegate;         /**< 当前代理*/
@property (nonatomic,assign)LoginInputType transferType;                        /**< 传递过来的页面类型*/

-(void)startTimer;          // 开启倒计时
-(void)actionClickWithSMSBtn:(void(^)())block;

@end


