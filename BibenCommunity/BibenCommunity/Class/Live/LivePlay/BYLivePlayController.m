//
//  BYLivePlayController.m
//  BY
//
//  Created by 黄亮 on 2018/8/7.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYLivePlayController.h"
#import "BYLivePlayViewModel.h"

@interface BYLivePlayController ()

@end

@implementation BYLivePlayController

- (Class)getViewModelClass{
    return [BYLivePlayViewModel class];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationTitle = @"视频直播";
    // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
