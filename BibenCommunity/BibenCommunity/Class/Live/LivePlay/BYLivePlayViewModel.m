//
//  BYLivePlayViewModel.m
//  BY
//
//  Created by 黄亮 on 2018/8/7.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYLivePlayViewModel.h"
#import "BYCommonPlayerView.h"

@interface BYLivePlayViewModel()

// ** 直播播放器 */
@property (nonatomic ,strong) BYCommonPlayerView *livePlayerView;

/** 播放器View的父视图 */
@property (nonatomic) UIView *playerFatherView;

@end

@implementation BYLivePlayViewModel

- (void)setContentView{
//    [self.playerView playVideoWithUrl:@"https://pic.ibaotu.com/00/32/58/20d888piCgHw.mp4" title:@"测试的测，测试的试"];
    [self.livePlayerView playLiveWithRtmp:@"rtmp://29266.liveplay.myqcloud.com/live/29266_7001"];
    self.playerFatherView = [[UIView alloc] init];
    self.playerFatherView.backgroundColor = [UIColor blackColor];
    
    [S_V_VIEW addSubview:self.playerFatherView];
    [self.playerFatherView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(IS_iPhoneX ? 44 : 0);
        make.leading.trailing.mas_equalTo(0);
        // 这里宽高比16：9,可自定义宽高比
        make.bottom.mas_equalTo(-30);
    }];
    self.livePlayerView.fatherView = self.playerFatherView;
    
}

- (void)viewControllerPopAction{
    [_livePlayerView resetPlayer];
}


#pragma mark - initMethod
- (BYCommonPlayerView *)livePlayerView {
    if (!_livePlayerView) {
        _livePlayerView = [[BYCommonPlayerView alloc] init];
        _livePlayerView.fatherView = _playerFatherView;
        _livePlayerView.enableFloatWindow = NO;
    }
    return _livePlayerView;
}

@end
