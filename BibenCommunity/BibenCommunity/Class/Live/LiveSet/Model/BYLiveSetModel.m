//
//  BYLiveSetModel.m
//  BY
//
//  Created by Belief on 2018/9/10.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYLiveSetModel.h"

@implementation BYLiveSetModel

- (NSArray *)tableData{
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i < 4; i ++) {
        NSMutableArray *tmpArr = [NSMutableArray array];
        [array addObject:@{@"cell":tmpArr}];
        if (i == 0) {
            for (int j = 0; j < 4; j ++) {
                BYLiveSetCellModel *model = [[BYLiveSetCellModel alloc] init];
                [tmpArr addObject:model];
                model.cellString = @"BYLiveSetCell";
                model.cellHeight = 48;
                switch (j) {
                    case 0:
                        model.title = @"设置头像";
                        model.rightlogoName = [AccountModel sharedAccountModel].loginServerModel.account.head_img;
                        model.showBottomLine = YES;
                        break;
                    case 1:
                        model.title = @"主题名称";
                        model.rightTitle = @"标题";
                        model.showBottomLine = YES;
                        break;
                    case 2:
                        model.title = @"填写主讲人";
                        model.showBottomLine = YES;
                        break;
                    case 3:
                        model.title = @"直播封面图";
                        break;
                    default:
                        break;
                }
            }
        }
        else
        {
            BYLiveSetCellModel *model = [[BYLiveSetCellModel alloc] init];
            [tmpArr addObject:model];
            model.cellString = @"BYLiveSetCell";
            model.cellHeight = 48;
            switch (i) {
                case 1:
                    model.title = @"直播简介";
                    break;
                case 2:
                    model.title = @"直播宣传图";
                    break;
                case 3:
                    model.title = @"推流设置";
                    break;
                default:
                    break;
            }
        }
    }
    return array;
}

@end

@implementation BYLiveSetCellModel

@end
