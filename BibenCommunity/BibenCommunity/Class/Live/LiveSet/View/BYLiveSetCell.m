//
//  BYLiveSetCell.m
//  BY
//
//  Created by Belief on 2018/9/10.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYLiveSetCell.h"
#import "BYLiveSetModel.h"

@implementation BYLiveSetCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = object[indexPath.section];
    BYLiveSetCellModel *model = dic.allValues[0][indexPath.row];
    [self setTitle:model.title];
    [self setRightImage:[UIImage imageNamed:@"common_cell_rightarrow"]];
    [self setRightTitle:model.rightTitle];
    [self showBottomLine:model.showBottomLine];
    if (model.rightlogo) {
        [self setRightLogoImage:model.rightlogo];
    }
    else
    {
        [self setRightLogoImage:[UIImage imageNamed:model.rightlogoName]];
    }
}

@end
