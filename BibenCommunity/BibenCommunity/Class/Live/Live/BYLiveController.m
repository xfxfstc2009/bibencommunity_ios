//
//  BYLiveController.m
//  BibenCommunity
//
//  Created by 随风 on 2018/9/19.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "BYLiveController.h"
#import "BYLiveViewModel.h"

@interface BYLiveController ()

@end

@implementation BYLiveController

- (Class)getViewModelClass{
    return [BYLiveViewModel class];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationTitle = @"直播";
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
