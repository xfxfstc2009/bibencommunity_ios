//
//  BYLiveViewModel.m
//  BibenCommunity
//
//  Created by 随风 on 2018/9/19.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "BYLiveViewModel.h"
// controller
#import "BYILiveRoomController.h"
#import "BYLiveSetController.h"
#import "BYLiveController.h"
#import "LivePPTRootViewController.h"
// view
#import "BYLiveHeaderView.h"
// request
#import "BYLiveHomeRequest.h"
// model
#import "BYCommonModel.h"
#import "BYCommonLiveModel.h"
#import "BYLiveHeaderModel.h"

@interface BYLiveViewModel()
{
    NSInteger _pageNum;
}
@property (nonatomic ,assign) BOOL isHave;
@property (nonatomic ,strong) BYCommonTableView *tableView;
@property (nonatomic ,strong) BYLiveHeaderView *headerView;
@end;

#define S_LIVE_VC ((BYLiveController *)S_VC)
@implementation BYLiveViewModel

- (void)setContentView{
    _pageNum = 0;
    _isHave = S_LIVE_VC.model ? YES : NO;
    [S_V_VIEW addSubview:self.tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 50, 0));
    }];
    
    [self addBottomView];
//    if (![BYCommonLiveModel shareManager]) {
//        [self loadRequestGetLiveRoomInfo];
//    }
//    else
//    {
//        [_headerView reloadData:(BYLiveHeaderModel *)[BYCommonLiveModel shareManager]];
//    }
    if (S_LIVE_VC.model) {
        [self.headerView reloadData:S_LIVE_VC.model];
    }
    [self loadRequestGetLiveList];
}

- (void)refreshData{
    _pageNum = 0;
    [self loadRequestGetLiveList];
}

- (void)loadMoreData{
    [self loadRequestGetLiveList];
}

- (void)editBtnAction{
    BYLiveSetController *liveSetController = [[BYLiveSetController alloc] init];
    liveSetController.model = self.headerView.model;
    [S_V_NC pushViewController:liveSetController animated:YES];
}

- (void)enterLiveBtnAction{
    [self pushControllerByLiveType:self.headerView.model.live_type];
}

- (void)pushControllerByLiveType:(BY_NEWLIVE_TYPE)liveType{
    if (liveType == BY_NEWLIVE_TYPE_ILIVE) {
        //            [[BYIMManager sharedInstance] createAVChatRoomGroupId:[BYCommonLiveModel shareManager].room_id succ:nil fail:nil];
        // 新建成功后暂跳互动直播
        BYLiveConfig *config = [[BYLiveConfig alloc] init];
        config.isHost = YES;
//        config.roomId = self.headerView.model.room_id;
        config.roomId = [AccountModel sharedAccountModel].account_id;
        BYILiveRoomController *ilivewRoomController = [[BYILiveRoomController alloc] initWithConfig:config];
        ilivewRoomController.model = self.headerView.model;
        [S_V_NC pushViewController:ilivewRoomController animated:YES];
    }
    else if (liveType == BY_NEWLIVE_TYPE_AUDIO_PPT ||
             liveType == BY_NEWLIVE_TYPE_AUDIO_LECTURE){
        LivePPTRootViewController *pptRootController = [[LivePPTRootViewController alloc] init];
        pptRootController.transferRoomId = self.headerView.model.live_record_id;
        [S_V_NC pushViewController:pptRootController animated:YES];
    }
}

#pragma mark - request method
- (void)loadRequestGetLiveList{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetLivelist:_pageNum successBlock:^(id object) {
        @strongify(self);
        [self.tableView endRefreshing];
        NSMutableArray *tmpArr = [NSMutableArray array];
        if (self->_pageNum == 0) {
            self->_pageNum ++;
            BYCommonModel *model = [[BYCommonModel alloc] init];
            model.cellString = @"BYLiveRoomHomeTitleCell";
            model.cellHeight = 58;
            model.title = @"其他直播";
            [tmpArr addObject:model];
            if ([object count] >= 1) {
                NSMutableArray *arr = [NSMutableArray arrayWithArray:object];
                if (!self.isHave) {
                    [self.headerView reloadData:object[0]];
                    [arr removeObjectAtIndex:0];
                }
                [tmpArr addObjectsFromArray:object];
            }
            self.tableView.tableData = tmpArr;
            return ;
        }
        self->_pageNum ++;
        [tmpArr addObjectsFromArray:self.tableView.tableData];
        [tmpArr addObjectsFromArray:object];
        self.tableView.tableData = tmpArr;
    } faileBlock:^(NSError *error) {
        [self.tableView endRefreshing];
    }];
}
#pragma mark - initMethod

- (void)addBottomView{
    
    UIView *bottomView = [UIView by_init];
    [bottomView setBackgroundColor:[UIColor whiteColor]];
    [S_V_VIEW addSubview:bottomView];
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(50);
    }];
    
    UIView *lineView = [UIView by_init];
    lineView.backgroundColor = kTextColor_229;
    [bottomView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
    }];
    
    CGFloat spaceW = (kCommonScreenWidth - 132*2)/3;
    
    // 编辑介绍页
    UIButton *editBtn = [UIButton by_buttonWithCustomType];
    [editBtn setBy_attributedTitle:@{@"title":@"编辑介绍页",NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:kTextColor_237} forState:UIControlStateNormal];
    [editBtn addTarget:self action:@selector(editBtnAction) forControlEvents:UIControlEventTouchUpInside];
    editBtn.layer.cornerRadius = 8;
    editBtn.layer.borderWidth = 1;
    editBtn.layer.borderColor = kBgColor_238.CGColor;
    [bottomView addSubview:editBtn];
    [editBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(spaceW);
        make.width.mas_equalTo(132);
        make.height.mas_equalTo(36);
        make.centerY.mas_equalTo(0);
    }];
    
    // 进入直播
    UIButton *enterLiveBtn = [UIButton by_buttonWithCustomType];
    [enterLiveBtn setBackgroundColor:kBgColor_238];
    [enterLiveBtn setBy_attributedTitle:@{@"title":@"进入直播",NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateNormal];
    [enterLiveBtn addTarget:self action:@selector(enterLiveBtnAction) forControlEvents:UIControlEventTouchUpInside];
    enterLiveBtn.layer.cornerRadius = 8;
    [bottomView addSubview:enterLiveBtn];
    [enterLiveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-spaceW);
        make.width.mas_equalTo(132);
        make.height.mas_equalTo(36);
        make.centerY.mas_equalTo(0);
    }];
    
}

- (BYCommonTableView *)tableView{
    if (!_tableView) {
        _tableView = [[BYCommonTableView alloc] init];
        _tableView.tableHeaderView = self.headerView;
        [_tableView addHeaderRefreshTarget:self action:@selector(refreshData)];
        [_tableView addFooterRefreshTarget:self action:@selector(loadMoreData)];
    }
    return _tableView;
}

- (BYLiveHeaderView *)headerView{
    if (!_headerView) {
        _headerView = [[BYLiveHeaderView alloc] initWithFrame:CGRectMake(0, 0, kCommonScreenWidth, 360)];
    }
    return _headerView;
}

@end
