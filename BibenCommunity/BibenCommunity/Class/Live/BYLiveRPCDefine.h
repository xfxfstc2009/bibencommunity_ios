//
//  BYLiveRPCDefine.h
//  BY
//
//  Created by 黄亮 on 2018/7/27.
//  Copyright © 2018年 Belief. All rights reserved.
//

#ifndef BYLiveRPCDefine_h
#define BYLiveRPCDefine_h

/** 获取直播首页列表 */
static NSString * RPC_get_all_live_list = @"get_all_live_list";

/** 获取上传凭证 */
static NSString * RPC_Get_upload_signature = @"get_upload_signature";

/** 上传视频信息成功后同步到服务端 */
static NSString * RPC_insert_vod_record = @"insert_vod_record";

/** 创建在直播间 */
static NSString * RPC_create_live_room = @"create_live_room";

/** 更新直播状态(上报直播状态，结束直播) */
//static NSString * RPC_update_live_record = @"update_live_record";

/** 更新直播状态(上报直播状态，结束直播) */
static NSString * RPC_switch_live = @"switch_live";

/** 获取直播登录的sig */
static NSString * RPC_get_user_sig = @"get_user_sig";

/** 新建直播记录 */
static NSString * RPC_create_live_record = @"create_live_record";

/** 更新直播间信息 */
static NSString * RPC_update_live_room = @"update_live_room";

/** 更新直播信息 */
static NSString * RPC_update_live_record = @"update_live_record";

/** 获取直播间信息 */
static NSString * RPC_get_live_room = @"get_live_room";

/** 获取直播列表 */
static NSString * RPC_get_live_list = @"get_live_list";

/** 获取首页banner列表 */
static NSString * RPC_get_banner_list = @"get_banner_list";

/** 判断是否有直播间 */
static NSString * RPC_whether_has_room = @"whether_has_room";

#endif /* BYLiveRPCDefine_h */
