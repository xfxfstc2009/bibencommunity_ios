//
//  LivePPTAdTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/20.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "LivePPTAdTableViewCell.h"

static char actionReloadImgHeightBlockKey;
@interface LivePPTAdTableViewCell()
@property (nonatomic,strong)PDImageView *adImgView;
@end

@implementation LivePPTAdTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.adImgView = [[PDImageView alloc]init];
    self.adImgView.backgroundColor = [UIColor clearColor];
    self.adImgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, 300);
    self.adImgView.image = [UIImage imageNamed:@"icon_live_up"];
    [self addSubview:self.adImgView];
}

-(void)setTransferAdModel:(LivePPTAdTableViewCellModel *)transferAdModel{
    _transferAdModel = transferAdModel;
    if(!transferAdModel.hasLoad){
        __weak typeof(self)weakSelf = self;
        [self.adImgView uploadMainImageWithURL:transferAdModel.imgUrl placeholder:nil imgType:PDImgTypeOriginal callback:^(UIImage *image) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            
            CGFloat screenWidth = kScreenBounds.size.width - 2 * LCFloat(15);
            CGFloat screenHeight = screenWidth * image.size.height * 1.0 / image.size.width;
            transferAdModel.imgHeight = screenHeight;
            transferAdModel.hasLoad = YES;
            transferAdModel.adImg = image;
            void(^block)() = objc_getAssociatedObject(strongSelf, &actionReloadImgHeightBlockKey);
            if (block){
                block();
            }
        }];
    } else {
        self.adImgView.image = transferAdModel.adImg;
        self.adImgView.frame = CGRectMake(LCFloat(15), 0, kScreenBounds.size.width - 2 * LCFloat(15), transferAdModel.imgHeight);
    }
}

-(CGFloat)calculationCellHeight:(LivePPTAdTableViewCellModel *)model{
    return model.imgHeight;
}

-(void)actionReloadImgHeightBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionReloadImgHeightBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end


@implementation LivePPTAdTableViewCellModel
@end
