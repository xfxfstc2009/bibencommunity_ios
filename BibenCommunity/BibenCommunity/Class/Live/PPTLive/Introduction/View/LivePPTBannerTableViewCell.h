//
//  LivePPTBannerTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/19.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

@interface LivePPTBannerTableViewCell : PDBaseTableViewCell

@property (nonatomic,copy)NSString *transferBanner;

@end
