//
//  LivePPTBannerTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/19.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "LivePPTBannerTableViewCell.h"

@interface LivePPTBannerTableViewCell()
@property (nonatomic,strong)PDImageView *imgView;
@end

@implementation LivePPTBannerTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.imgView = [[PDImageView alloc]init];
    self.imgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, [LivePPTBannerTableViewCell calculationCellHeight]);
    [self addSubview:self.imgView];
}

-(void)setTransferBanner:(NSString *)transferBanner{
    _transferBanner = transferBanner;
    [self.imgView uploadImageWithURL:transferBanner placeholder:nil callback:NULL];
}

+(CGFloat)calculationCellHeight{
    return LCFloat(195);
}

@end
