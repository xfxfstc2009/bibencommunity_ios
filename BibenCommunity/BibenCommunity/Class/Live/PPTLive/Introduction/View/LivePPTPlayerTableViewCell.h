//
//  LivePPTPlayerTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/19.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "PPTLiveMainInfoModel.h"

@interface LivePPTPlayerTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)PPTLiveMainInfoModel *transferMainModel;

+(CGFloat)calculationCellHeight:(PPTLiveMainInfoModel *)transferMainModel;

@end
