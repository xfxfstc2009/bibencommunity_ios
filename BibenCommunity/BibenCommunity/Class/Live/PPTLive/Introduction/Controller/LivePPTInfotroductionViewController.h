//
//  LivePPTInfotroductionViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/14.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "AbstractViewController.h"
#import "PPTLiveMainInfoModel.h"

@interface LivePPTInfotroductionViewController : AbstractViewController

@property (nonatomic,strong)PPTLiveMainInfoModel *transferInfoModel;

@end
