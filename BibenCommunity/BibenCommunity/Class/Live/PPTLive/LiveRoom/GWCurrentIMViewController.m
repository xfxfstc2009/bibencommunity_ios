//
//  GWCurrentIMViewController.m
//  GiganticWhale
//
//  Created by 巨鲸 on 16/3/8.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWCurrentIMViewController.h"
#import "NetworkAdapter.h"
#import "PDScrollView.h"
#import "LiveDetailModel.h"

@interface GWCurrentIMViewController()<UIGestureRecognizerDelegate,YWMessageInputViewDelegate>
@property (nonatomic,strong)UIButton *openDrawerButton;
@property (nonatomic,strong)PDScrollView *mainScrollView;

@end

@implementation GWCurrentIMViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self bottomSetting];
    [self createbanner];
    [self sendRequestToGetLiveClass];
    // 键盘代理
    self.messageInputView.messageInputViewDelegate = self;
}



#pragma mark - bottom
-(void)bottomSetting{
    if (IS_iPhoneX){
        self.messageInputView.frame = CGRectMake(0,self.messageInputView.orgin_y - 34 , kScreenBounds.size.width, self.messageInputView.size_height + 34);
        self.messageInputView.messageInputViewDelegate = self;
        self.tableView.size_height -= 34;
    }
}

- (BOOL)messageInputViewShouldEndEditing:(UIView<IYWMessageInputView> *)inputView{
    if (IS_iPhoneX){
        self.messageInputView.frame = CGRectMake(0,self.messageInputView.orgin_y - 34 , kScreenBounds.size.width, self.messageInputView.size_height + 34);
        self.tableView.size_height += 34;
    }
    self.customTopView = self.mainScrollView;
    return YES;
}


//beginListeningForKeyboard
- (BOOL)messageInputViewShouldBeginEditing:(UIView<IYWMessageInputView> *)inputView{
    self.customTopView = nil;
    return YES;
}


#pragma mark - 创建banner
-(void)createbanner{
    if (!self.mainScrollView){
        self.mainScrollView = [[PDScrollView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width,LCFloat(195))];
        self.mainScrollView.backgroundColor = [UIColor clearColor];
        self.customTopView = self.mainScrollView;
        [self.mainScrollView bannerImgTapManagerWithInfoblock:NULL];
    }
}

-(NSString *)appendUrl:(NSString *)url{
    NSString *transferUrl = @"";
    if (![url hasPrefix:@"http://"]){               // 如果不是http 开头就是我们自己的
        transferUrl = [NSString stringWithFormat:@"%@%@",imageBaseUrl,url];
    } else {
        transferUrl = [NSString stringWithFormat:@"%@",url];
    }
    return transferUrl;
}

#pragma mark - 获取直播课件
-(void)sendRequestToGetLiveClass{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"live_record_id":self.transferRoomId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:@"get_courseware" requestParams:params responseObjectClass:[LiveDetailModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            LiveDetailModel *detail = (LiveDetailModel *)responseObject;
            
            
            NSMutableArray *tempScrollMutableArr = [NSMutableArray array];
            for (int i = 0 ; i < detail.courseware.count;i++){
                NSString *imgStr = [detail.courseware objectAtIndex:i];
                GWScrollViewSingleModel *scrollViewSingleModel = [[GWScrollViewSingleModel alloc]init];
                scrollViewSingleModel.img = [strongSelf appendUrl:imgStr];
                [tempScrollMutableArr addObject:scrollViewSingleModel];
            }
            strongSelf.mainScrollView.transferImArr = tempScrollMutableArr;
        }
    }];
}


@end
