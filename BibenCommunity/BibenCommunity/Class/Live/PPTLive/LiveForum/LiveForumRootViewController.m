//
//  LiveForumRootViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/17.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "LiveForumRootViewController.h"
#import "NetworkAdapter.h"
#import "LiveFourumRootListModel.h"
#import "LiveForumRootSingleTableViewCell.h"
#import "LiveForumRootSingleSubTableViewCell.h"
#import "LiveForumRootInputView.h"
#import "NetworkAdapter+PPTLive.h"
#import "LiveForumItemsTableViewCell.h"
#import "LiveForumDetailViewController.h"

#define InputViewSizeHeight LCFloat(47);
@interface LiveForumRootViewController ()<UITableViewDelegate,UITableViewDataSource>{
    CGRect newKeyboardRect;
    LiveFourumRootListSingleModel *tempSingleModle;
}
@property (nonatomic,strong)UITableView *dianpingTableView;
@property (nonatomic,strong)NSMutableArray *dianpingMutableArr;
@end

@implementation LiveForumRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createInputView];
    [self createTableView];
    [self createNotifi];                // 添加键盘代理
    [self sendRequestToGetInfoWithReReload:NO];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"点评";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.dianpingMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.dianpingTableView){
        self.dianpingTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.dianpingTableView.size_height = self.view.size_height - self.inputView.size_height;
        self.dianpingTableView.dataSource = self;
        self.dianpingTableView.delegate = self;
        [self.view addSubview:self.dianpingTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.dianpingTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfoWithReReload:NO];
    }];
    [self.dianpingTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfoWithReReload:NO];
    }];
    
    [self.view addSubview:self.inputView];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dianpingMutableArr.count;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    LiveFourumRootListSingleModel *singleModel = [self.dianpingMutableArr objectAtIndex:section];
    return singleModel.child.count + 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    LiveFourumRootListSingleModel *singleModel = [self.dianpingMutableArr objectAtIndex:indexPath.section];
    NSInteger rowCount = singleModel.child.count + 2;
    if (indexPath.row == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        LiveForumRootSingleTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[LiveForumRootSingleTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        LiveFourumRootListSingleModel *singleModel = [self.dianpingMutableArr objectAtIndex:indexPath.section];
        cellWithRowOne.transferInfoManager = singleModel;
        return cellWithRowOne;
    } else if (indexPath.row == rowCount - 1){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        LiveForumItemsTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowOne){
            cellWithRowOne = [[LiveForumItemsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        }
        LiveFourumRootListSingleModel *singleModel = [self.dianpingMutableArr objectAtIndex:indexPath.section];
        cellWithRowOne.transferInfoManager = singleModel;
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne itemsReplyManagerWithModel:^(LiveFourumRootListSingleModel *singleModle) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf->tempSingleModle = singleModel;
            strongSelf.inputView.transferListModel = singleModel;
            
            if (![strongSelf.inputView.inputView isFirstResponder]){
                [strongSelf.inputView.inputView becomeFirstResponder];
            }
            strongSelf.inputView.inputView.text = @"";
            strongSelf.inputView.inputView.placeholder = [NSString stringWithFormat:@"回复：%@",singleModel.comment_name];
        }];
        
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        LiveForumRootSingleSubTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[LiveForumRootSingleSubTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        LiveFourumRootListSingleModel *singleModel = [self.dianpingMutableArr objectAtIndex:indexPath.section];
        cellWithRowTwo.transferCount = singleModel.childCount <= 3 ? -1:singleModel.childCount;
        NSInteger index = indexPath.row - 1;
        if (singleModel.child.count > 1){
            if (index == 0){
                cellWithRowTwo.transferSubType = LiveForumRootSingleSubTableViewCellTypeTop;
            } else if (indexPath.row == singleModel.child.count){
                cellWithRowTwo.transferSubType = LiveForumRootSingleSubTableViewCellTypeBottom;
            } else {
                cellWithRowTwo.transferSubType = LiveForumRootSingleSubTableViewCellTypeNormal;
            }
        } else {
            cellWithRowTwo.transferSubType = LiveForumRootSingleSubTableViewCellTypeSingle;
        }
        cellWithRowTwo.transferInfoManager = [singleModel.child objectAtIndex:index];
        return cellWithRowTwo;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    LiveFourumRootListSingleModel *singleModel = [self.dianpingMutableArr objectAtIndex:indexPath.section];
    NSInteger rowCount = singleModel.child.count + 2;
    if (indexPath.row < rowCount - 2){
        LiveForumDetailViewController *controller = [[LiveForumDetailViewController alloc]init];
        controller.transferFourumRootModel = singleModel;
        controller.transferRoomId = self.transferRoomId;
        [self.navigationController pushViewController:controller animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    LiveFourumRootListSingleModel *singleModel = [self.dianpingMutableArr objectAtIndex:indexPath.section];
    NSInteger rowCount = singleModel.child.count + 2;
    if (indexPath.row == 0){
        return [LiveForumRootSingleTableViewCell calculationCellHeightWithModel:singleModel];
    } else if (indexPath.row == rowCount - 1){
        return [LiveForumItemsTableViewCell calculationCellHeight];
    } else {
        NSInteger index = indexPath.row - 1;
        if (singleModel.child.count > 1){
            if (index == 0){
                return [LiveForumRootSingleSubTableViewCell calculationCellHeightWithModel:singleModel type:LiveForumRootSingleSubTableViewCellTypeTop];
            } else if (indexPath.row == singleModel.child.count){
                return [LiveForumRootSingleSubTableViewCell calculationCellHeightWithModel:singleModel type:LiveForumRootSingleSubTableViewCellTypeBottom];
            } else {
                return [LiveForumRootSingleSubTableViewCell calculationCellHeightWithModel:singleModel type:LiveForumRootSingleSubTableViewCellTypeNormal];
            }
        } else {
            return [LiveForumRootSingleSubTableViewCell calculationCellHeightWithModel:singleModel type:LiveForumRootSingleSubTableViewCellTypeSingle];
        }
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor hexChangeFloat:@"E4E4E4"];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return 0;
    } else {
        return .5f;
    }
}

#pragma mark - 创建输入框
-(void)createInputView{
    if (!self.inputView){
        
        CGRect inputFrame = CGRectMake(0, self.viewSizeHeight - [LiveForumRootInputView calculationHeight:@""], kScreenBounds.size.width, [LiveForumRootInputView  calculationHeight:@""]);
        self.inputView = [[LiveForumRootInputView alloc]initWithFrame:inputFrame];
        self.inputView.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
        self.inputView.layer.shadowOpacity = .2f;
        self.inputView.layer.shadowOffset = CGSizeMake(.2f, .2f);
        [self.view addSubview:self.inputView];
    }
    __weak typeof(self)weakSelf = self;
    [self.inputView actionClickWithSendInfoBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToSendInfo];
    }];
    [self.inputView actionTextInputChangeBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.inputView.orgin_y = strongSelf.viewSizeHeight - strongSelf->newKeyboardRect.size.height - [LiveForumRootInputView calculationHeight:strongSelf.inputView.inputView.text] ;
    }];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self.inputView releaseKeyboard];
}

#pragma mark - 键盘通知
-(void)createNotifi{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    
    
    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect newTextViewFrame = self.view.bounds;
    newTextViewFrame.size.height = keyboardTop - self.view.bounds.origin.y;
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    CGRect newTextViewFrame1 = CGRectMake(newTextViewFrame.origin.x, newTextViewFrame.origin.y, newTextViewFrame.size.width, newTextViewFrame.size.height - self.inputView.size_height);
    self.dianpingTableView.frame = newTextViewFrame1;
    self.inputView.orgin_y = self.viewSizeHeight - keyboardRect.size.height - self.inputView.size_height ;
    [UIView commitAnimations];
    newKeyboardRect = keyboardRect;
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    CGRect mainRect = CGRectMake(0, 0, kScreenBounds.size.width,  self.viewSizeHeight - self.inputView.size_height);
    self.dianpingTableView.frame = mainRect;
    self.inputView.orgin_y = self.viewSizeHeight - self.inputView.size_height;
    [UIView commitAnimations];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];
}

-(void)viewDidUnload{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark  - Interface
-(void)sendRequestToGetInfoWithReReload:(BOOL)rereload{
    NSInteger page = rereload ?0:self.dianpingTableView.currentPage;
    NSDictionary *params = @{@"theme_id":self.transferRoomId,@"theme_type":@"0",@"reply_comment_id":@"0",@"page_number":@(page),@"page_size":@"10"};
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:@"page_comment" requestParams:params responseObjectClass:[LiveFourumRootListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            if (strongSelf.dianpingTableView.isXiaLa || rereload){
                [strongSelf.dianpingMutableArr removeAllObjects];
            }
            LiveFourumRootListModel *listModel = (LiveFourumRootListModel *)responseObject;
            [strongSelf.dianpingMutableArr addObjectsFromArray:listModel.content];
            
            for (int i = 0; i <strongSelf.dianpingMutableArr.count;i++){
                LiveFourumRootListSingleModel *singleModel = [strongSelf.dianpingMutableArr objectAtIndex:i];
                if (singleModel.childCount > singleModel.child.count){          // 表示有多余的内容
                    LiveFourumRootListSingleModel *childModel = [[LiveFourumRootListSingleModel alloc]init];
                    childModel.reply_comment_name = @"共同NNNNN条回复";
                    childModel.childCount = singleModel.childCount;
                    NSMutableArray *childRootArr = [NSMutableArray array];
                    [childRootArr addObjectsFromArray:singleModel.child];
                    [childRootArr addObject:childModel];
                    singleModel.child = [childRootArr copy];
                }
            }
          
            [strongSelf.dianpingTableView reloadData];
            
            if (strongSelf.dianpingMutableArr.count){
                [strongSelf.dianpingTableView dismissPrompt];
            } else {
                [strongSelf.dianpingTableView showPrompt:@"当前没有信息" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
            }
            [strongSelf.dianpingTableView stopPullToRefresh];
            [strongSelf.dianpingTableView stopFinishScrollingRefresh];
         }
    }];
}

#pragma mark - 发送信息
-(void)sendRequestToSendInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] liveCreateCommentWiththemeId:self.transferRoomId ThemeInfo:self.inputView.transferListModel content:self.inputView.inputView.text block:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            strongSelf.inputView.transferListModel = nil;
            [strongSelf sendRequestToGetInfoWithReReload:YES];
            [strongSelf.inputView cleanKeyboard];
        }
    }];
}



@end
