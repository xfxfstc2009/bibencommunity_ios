//
//  LiveForumRootSingleSubTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/18.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "LiveFourumRootListModel.h"

typedef NS_ENUM(NSInteger,LiveForumRootSingleSubTableViewCellType) {
    LiveForumRootSingleSubTableViewCellTypeTop,
    LiveForumRootSingleSubTableViewCellTypeNormal,
    LiveForumRootSingleSubTableViewCellTypeBottom,
    LiveForumRootSingleSubTableViewCellTypeSingle,
};

@interface LiveForumRootSingleSubTableViewCell : PDBaseTableViewCell

@property (nonatomic,assign)LiveForumRootSingleSubTableViewCellType transferSubType;
@property (nonatomic,strong)LiveFourumRootListSingleModel *transferInfoManager;
@property (nonatomic,assign)NSInteger transferCount;
+(CGFloat)calculationCellHeightWithModel:(LiveFourumRootListSingleModel *)model type:(LiveForumRootSingleSubTableViewCellType)type;
@end
