//
//  LiveForumRootInputView.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/18.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "LiveForumRootInputView.h"

static char actionClickWithSendInfoBlockKey;
static char actionTextInputChangeBlockKey;

#define kInputBgTopMargin LCFloat(8)
#define kInputBgTopLeftMargin LCFloat(8)
#define kInputMargin (LCFloat(35) - [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"13"]]) / 2.
#define kInputLeftMargin LCFloat(3)

@interface LiveForumRootInputView()
@property (nonatomic,strong)UIButton *sendButton;
@property (nonatomic,strong)PDImageView *bgInputImgView;
@end

@implementation LiveForumRootInputView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if(self){
        self.backgroundColor = [UIColor hexChangeFloat:@"FFFFFF"];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.sendButton.backgroundColor = [UIColor hexChangeFloat:@"424242"];
    [self.sendButton setTitle:@"发送" forState:UIControlStateNormal];
    self.sendButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"15"];
    self.sendButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - LCFloat(63), LCFloat(9), LCFloat(63), LCFloat(32));
    self.sendButton.layer.cornerRadius = self.sendButton.size_height / 2.;
    self.sendButton.clipsToBounds = YES;
    [self addSubview:self.sendButton];
    __weak typeof(self)weakSelf = self;
    [self.sendButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithSendInfoBlockKey);
        if (block){
            block();
        }
    }];
    
    self.bgInputImgView = [[PDImageView alloc]init];
    self.bgInputImgView.backgroundColor = [UIColor hexChangeFloat:@"EEEEEE"];
    self.bgInputImgView.layer.cornerRadius = LCFloat(3);
    self.bgInputImgView.clipsToBounds = YES;
    self.bgInputImgView.userInteractionEnabled = YES;
    [self addSubview:self.bgInputImgView];
    
    self.inputView = [[UITextView alloc]init];
    self.inputView.backgroundColor = [UIColor clearColor];
    self.inputView.font = [UIFont fontWithCustomerSizeName:@"13"];
    self.inputView.userInteractionEnabled = YES;
    [self.bgInputImgView addSubview:self.inputView];

    [self.inputView textViewDidChangeWithBlock:^(NSInteger currentCount) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf calculationTextViewHeight];
    }];
    
    self.bgInputImgView.frame = CGRectMake(LCFloat(15), LCFloat(7), self.sendButton.orgin_x - LCFloat(14) - LCFloat(15), 2 * LCFloat(9) + [NSString contentofHeightWithFont:self.inputView.font]);
    self.inputView.frame = CGRectMake(LCFloat(3), 0, self.bgInputImgView.size_width - 2 * LCFloat(3), self.bgInputImgView.size_height);
    self.inputView.placeholder = @"写评论";
    self.inputView.limitMax = 1000;
}



-(void)setTransferListModel:(LiveFourumRootListSingleModel *)transferListModel{
    _transferListModel = transferListModel;
    
    self.inputView.placeholder = [NSString stringWithFormat:@"回复%@:",transferListModel.reply_comment_name];
}

-(void)actionClickWithSendInfoBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithSendInfoBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionTextInputChangeBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionTextInputChangeBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationHeight:(NSString *)inputText{
    // 1. 计算文字高度
    CGFloat inputWidth = kScreenBounds.size.width - kInputBgTopLeftMargin - kInputLeftMargin - kInputBgTopLeftMargin - LCFloat(63) - kInputBgTopLeftMargin - kInputLeftMargin;
    
    if ([inputText isEqualToString:@""]){
        inputText = @"1";
    }
    
    CGSize fontSize = [inputText sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"13"] constrainedToSize:CGSizeMake(inputWidth, CGFLOAT_MAX)];
    CGFloat MasonrySizeHeight = MIN(3 * [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"13"]], fontSize.height);
    
    CGFloat cellHeight = 0;
    cellHeight += kInputBgTopMargin;
    cellHeight += kInputMargin;
    cellHeight += MAX(MasonrySizeHeight,[NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"13"]]);

    cellHeight += kInputMargin;
    cellHeight += kInputBgTopMargin;
    
    if (IS_iPhoneX){
        cellHeight += LCFloat(20);
    }
    return cellHeight;
}

-(void)calculationTextViewHeight{
    CGFloat inputSizeHeight = [LiveForumRootInputView calculationHeight:self.inputView.text];
    self.size_height = inputSizeHeight;
    self.bgInputImgView.frame = CGRectMake(kInputBgTopLeftMargin, kInputBgTopMargin, self.bgInputImgView.size_width, inputSizeHeight - 2 * kInputBgTopMargin);
    self.inputView.size_height = self.bgInputImgView.size_height;
    
    void(^block)() = objc_getAssociatedObject(self, &actionTextInputChangeBlockKey);
    if (block){
        block();
    }
}


-(void)releaseKeyboard{
    if (self.inputView.isFirstResponder){
        [self.inputView resignFirstResponder];
    }
}

-(void)cleanKeyboard{
    self.inputView.text = @"";
    self.inputView.placeholder = @"写评论";
    [self releaseKeyboard];
}

@end
