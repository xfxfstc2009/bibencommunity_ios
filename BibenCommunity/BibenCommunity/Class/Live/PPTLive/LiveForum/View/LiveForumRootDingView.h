//
//  LiveForumRootDingView.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/18.
//  Copyright © 2018年 币本. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveFourumRootListModel.h"

typedef NS_ENUM(NSInteger,LiveForumRootDingViewType) {
    LiveForumRootDingViewTypeUp,                /**< 顶*/
    LiveForumRootDingViewTypeDown,              /**< 踩*/
    LiveForumRootDingViewTypeReply,             /**< 回复*/
};

@interface LiveForumRootDingView : UIView

@property (nonatomic,assign)LiveForumRootDingViewType transferType;
@property (nonatomic,strong)LiveFourumRootListSingleModel *transferInfoManager;

// 点击回复按钮
-(void)actionButtonWithReplyBlock:(void(^)())block;

+(CGSize)calculationSize;
@end
