//
//  LiveForumRootSingleTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/18.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "LiveForumRootSingleTableViewCell.h"


@interface LiveForumRootSingleTableViewCell()
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *nickNameLabel;
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)UILabel *dymicLabel;
@property (nonatomic,strong)PDImageView *convertView;
@property (nonatomic,strong)UIButton *moreButton;

@end

@implementation LiveForumRootSingleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarImgView];
    
    self.convertView = [[PDImageView alloc]init];
    self.convertView.backgroundColor = [UIColor clearColor];
    self.convertView.image = [UIImage imageNamed:@"icon_live_avatar_convert"];
    self.convertView.frame = CGRectMake(0, 0, LCFloat(67), LCFloat(67));
    [self addSubview:self.convertView];
    
    self.nickNameLabel = [GWViewTool createLabelFont:@"15" textColor:@"4C4C4C"];
    [self addSubview:self.nickNameLabel];
    
    self.timeLabel = [GWViewTool createLabelFont:@"12" textColor:@"7B7B7B"];
    [self addSubview:self.timeLabel];
    
    self.dymicLabel = [GWViewTool createLabelFont:@"13" textColor:@"292929"];
    self.dymicLabel.numberOfLines = 0;
    [self addSubview:self.dymicLabel];
    
    self.moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.moreButton.backgroundColor = [UIColor clearColor];
    self.moreButton.hidden = YES;
    [self.moreButton setImage:[UIImage imageNamed:@"icon_live_more"] forState:UIControlStateNormal];
    [self addSubview:self.moreButton];
}

-(void)setTransferInfoManager:(LiveFourumRootListSingleModel *)transferInfoManager{
    _transferInfoManager = transferInfoManager;
    
    self.avatarImgView.frame = CGRectMake(LCFloat(17), LCFloat(20), LCFloat(37), LCFloat(37));
    [self.avatarImgView uploadImageWithURL:transferInfoManager.comment_pic placeholder:nil callback:NULL];
    
    self.convertView.center = self.avatarImgView.center;
    
    // title
    self.nickNameLabel.text = transferInfoManager.comment_name;
    CGSize nickNameSize = [Tool makeSizeWithLabel:self.nickNameLabel];
    self.nickNameLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(13), LCFloat(20), nickNameSize.width, nickNameSize.height);
    
    // time
    self.timeLabel.text = [NSDate getTimeGap:transferInfoManager.create_time];
    CGSize timeSize = [Tool makeSizeWithLabel:self.timeLabel];
    self.timeLabel.frame = CGRectMake(self.nickNameLabel.orgin_x, CGRectGetMaxY(self.nickNameLabel.frame) + LCFloat(6), timeSize.width, timeSize.height);
    
    // more
    self.moreButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - LCFloat(30), 0, LCFloat(30), LCFloat(30));
    self.moreButton.center_y = self.nickNameLabel.center_y;
    
    self.dymicLabel.text = transferInfoManager.content;
    CGFloat width = kScreenBounds.size.width - LCFloat(67) - LCFloat(16);
    CGSize dymicSize = [transferInfoManager.content sizeWithCalcFont:self.dymicLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    self.dymicLabel.frame = CGRectMake(LCFloat(67), CGRectGetMaxY(self.timeLabel.frame) + LCFloat(18), width, dymicSize.height);
}

+(CGFloat)calculationCellHeightWithModel:(LiveFourumRootListSingleModel *)model{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(20);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"15"]];
    cellHeight += LCFloat(6);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"12"]];
    cellHeight += LCFloat(18);
    CGFloat width = kScreenBounds.size.width - LCFloat(67) - LCFloat(15);
    CGSize contentOfSize = [model.content sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"13"] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    cellHeight += contentOfSize.height;
    cellHeight += LCFloat(15);
    return cellHeight;
}


@end
