//
//  LiveForumRootDingView.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/18.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "LiveForumRootDingView.h"
#import "NetworkAdapter+PPTLive.h"

static char actionButtonWithReplyBlockKey;
@interface LiveForumRootDingView()
@property (nonatomic,strong)PDImageView *actionImgView;
@property (nonatomic,strong)UILabel *actionLabel;
@property (nonatomic,strong)UIButton *actionButton;
@property (nonatomic,strong)UILabel *animationLaebl;
@end

@implementation LiveForumRootDingView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.actionImgView = [[PDImageView alloc]init];
    self.actionImgView.backgroundColor = [UIColor clearColor];
    self.actionImgView.frame = CGRectMake(0, ([LiveForumRootDingView calculationSize].height - LCFloat(14)) / 2., LCFloat(14), LCFloat(14));
    [self addSubview:self.actionImgView];
    
    self.actionLabel = [GWViewTool createLabelFont:@"12" textColor:@"EE4944"];
    [self addSubview:self.actionLabel];
    
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton.backgroundColor = [UIColor clearColor];
    [self addSubview:self.actionButton];
    self.actionButton.frame = self.bounds;
    __weak typeof(self)weakSelf = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.transferType == LiveForumRootDingViewTypeDown || strongSelf.transferType == LiveForumRootDingViewTypeUp){
            [strongSelf actionClickManagerWithInterface];
        } else if(strongSelf.transferType == LiveForumRootDingViewTypeReply){
            void(^block)() = objc_getAssociatedObject(strongSelf, &actionButtonWithReplyBlockKey);
            if (block){
                block();
            }
        }
    }];
    
    self.animationLaebl = [[UILabel alloc]init];
    self.animationLaebl.backgroundColor = [UIColor clearColor];
    self.animationLaebl.font = [UIFont fontWithCustomerSizeName:@"11"];
    self.animationLaebl.hidden = YES;
    [self addSubview:self.animationLaebl];
}

-(void)setTransferType:(LiveForumRootDingViewType)transferType{
    _transferType = transferType;
}

-(void)setTransferInfoManager:(LiveFourumRootListSingleModel *)transferInfoManager{
    _transferInfoManager = transferInfoManager;
    
    if (self.transferType == LiveForumRootDingViewTypeUp){              // 顶
        if (transferInfoManager.isSupport){
            self.actionImgView.image = [UIImage imageNamed:@"icon_live_up_hlt"];
        } else {
            self.actionImgView.image = [UIImage imageNamed:@"icon_live_up"];
        }
        self.actionLabel.textColor = [UIColor hexChangeFloat:@"343434"];
        self.actionLabel.text = [NSString stringWithFormat:@"%li",(long)transferInfoManager.count_support];
    } else if (self.transferType == LiveForumRootDingViewTypeDown){
        if (transferInfoManager.isTread){
            self.actionImgView.image = [UIImage imageNamed:@"icon_live_down_hlt"];
        } else {
            self.actionImgView.image = [UIImage imageNamed:@"icon_live_down"];
        }
        
        self.actionLabel.textColor = [UIColor hexChangeFloat:@"343434"];
        self.actionLabel.text = [NSString stringWithFormat:@"%li",(long)transferInfoManager.count_tread];
    } else {
        self.actionImgView.image = [UIImage imageNamed:@"icon_live_reply"];
        self.actionLabel.textColor = [UIColor hexChangeFloat:@"343434"];
        self.actionLabel.text = @"回复";
    }
    
    NSString *info = @"1234";
    CGSize contentOfSize = [info sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"12"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"12"]])];
    
    self.actionLabel.frame = CGRectMake(CGRectGetMaxX(self.actionImgView.frame) + LCFloat(5), 0, contentOfSize.width, [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"12"]]);
    self.actionLabel.center_y = self.actionImgView.center_y;
    
    if (self.transferType == LiveForumRootDingViewTypeUp){
        self.animationLaebl.textColor = [UIColor hexChangeFloat:@"343434"];
    } else if (self.transferType == LiveForumRootDingViewTypeDown){
        self.animationLaebl.textColor = [UIColor hexChangeFloat:@"343434"];
    } else if (self.transferType == LiveForumRootDingViewTypeReply){
        self.animationLaebl.textColor = [UIColor hexChangeFloat:@"343434"];
    }
}

-(void)animationLabelShow{
    self.animationLaebl.text = @"+1";
    self.animationLaebl.frame = self.actionLabel.frame;
    self.animationLaebl.hidden = NO;
    self.animationLaebl.alpha = 1;
    [UIView animateWithDuration:.5f animations:^{
        self.animationLaebl.orgin_y = 0;
        self.animationLaebl.alpha = 0;
    } completion:^(BOOL finished) {
        self.animationLaebl.hidden = YES;
        self.animationLaebl.alpha = 0;
        if (self.transferType == LiveForumRootDingViewTypeUp){
            self.transferInfoManager.count_support += 1;
            self.actionLabel.text = [NSString stringWithFormat:@"%li",(long)self.transferInfoManager.count_support];
            // 修改图片
            self.transferInfoManager.isSupport = YES;
            if (self.transferInfoManager.isSupport){
                self.actionImgView.image = [UIImage imageNamed:@"icon_live_up_hlt"];
            } else {
                self.actionImgView.image = [UIImage imageNamed:@"icon_live_up"];
            }
        } else if (self.transferType == LiveForumRootDingViewTypeDown){
            self.transferInfoManager.count_tread += 1;
            self.actionLabel.text = [NSString stringWithFormat:@"%li",(long)self.transferInfoManager.count_tread];
            // 修改图片
            self.transferInfoManager.isTread = YES;
            if (self.transferInfoManager.isTread){
                self.actionImgView.image = [UIImage imageNamed:@"icon_live_down_hlt"];
            } else {
                self.actionImgView.image = [UIImage imageNamed:@"icon_live_down"];
            }
        }
    }];
}


+(CGSize)calculationSize{
    CGFloat width = LCFloat(22) + LCFloat(5);
    NSString *info = @"1234";
    CGSize contentOfSize = [info sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"12"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"12"]])];
    width += contentOfSize.width + LCFloat(5);
    
    CGFloat height = LCFloat(41);
    return CGSizeMake(width, height);
}

-(void)actionButtonWithReplyBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionButtonWithReplyBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark - Interface
-(void)actionClickManagerWithInterface{
    __weak typeof(self)weakSelf = self;
    BOOL hasUp = NO;
    if (self.transferType == LiveForumRootDingViewTypeUp){
        hasUp = YES;
    } else {
        hasUp = NO;
    }
    
    [[NetworkAdapter sharedAdapter] liveComment_OperateWithCommentID:self.transferInfoManager._id hasUp:hasUp block:^(BOOL isSuccessed, BOOL isOperate) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            if (!isOperate){
                 [strongSelf animationLabelShow];
            } else {
                if (strongSelf.transferType == LiveForumRootDingViewTypeUp){
                    [StatusBarManager statusBarHidenWithText:@"你已经顶过该条评论"];
                } else {
                    [StatusBarManager statusBarHidenWithText:@"你已经踩过该条评论"];
                }
            }
        }
    }];
}

@end
