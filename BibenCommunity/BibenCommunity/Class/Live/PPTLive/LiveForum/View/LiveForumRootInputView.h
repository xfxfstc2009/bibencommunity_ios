//
//  LiveForumRootInputView.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/18.
//  Copyright © 2018年 币本. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveFourumRootListModel.h"

@interface LiveForumRootInputView : UIView

@property (nonatomic,strong)LiveFourumRootListSingleModel *transferListModel;
@property (nonatomic,strong)UITextView *inputView;

+(CGFloat)calculationHeight:(NSString *)inputText;

-(void)actionClickWithSendInfoBlock:(void(^)())block;                   //  点击发送按钮
-(void)actionTextInputChangeBlock:(void(^)())block;

-(void)releaseKeyboard;
-(void)cleanKeyboard;
@end
