//
//  LiveForumItemsTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/20.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "LiveForumItemsTableViewCell.h"
#import "LiveForumRootDingView.h"

static char itemsReplyManagerWithModelKey;
@interface LiveForumItemsTableViewCell()
@property (nonatomic,strong)LiveForumRootDingView *replyView;
@property (nonatomic,strong)LiveForumRootDingView *dingView;
@property (nonatomic,strong)LiveForumRootDingView *downView;
@end

@implementation LiveForumItemsTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - - createView
-(void)createView{
    self.dingView = [[LiveForumRootDingView alloc]initWithFrame:CGRectMake(0, 0, [LiveForumRootDingView calculationSize].width, [LiveForumRootDingView calculationSize].height)];
    self.dingView.transferType = LiveForumRootDingViewTypeUp;
    [self addSubview:self.dingView];
    
    self.downView = [[LiveForumRootDingView alloc]initWithFrame:CGRectMake(kScreenBounds.size.width - LCFloat(16) - [LiveForumRootDingView calculationSize].width, 0, [LiveForumRootDingView calculationSize].width, [LiveForumRootDingView calculationSize].height)];
    self.downView.transferType = LiveForumRootDingViewTypeDown;
    [self addSubview:self.downView];
    
    // 回复
    self.replyView = [[LiveForumRootDingView alloc]initWithFrame:CGRectMake(0, 0, [LiveForumRootDingView calculationSize].width, [LiveForumRootDingView calculationSize].height)];
    self.replyView.transferType = LiveForumRootDingViewTypeReply;
    [self addSubview:self.replyView];
    
    __weak typeof(self)weakSelf = self;
    [self.replyView actionButtonWithReplyBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(LiveFourumRootListSingleModel *singleModle) = objc_getAssociatedObject(strongSelf, &itemsReplyManagerWithModelKey);
        if (block){
            block(strongSelf.transferInfoManager);
        }
    }];
}

-(void)itemsReplyManagerWithModel:(void(^)(LiveFourumRootListSingleModel *singleModle))block{
    objc_setAssociatedObject(self, &itemsReplyManagerWithModelKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)setTransferInfoManager:(LiveFourumRootListSingleModel *)transferInfoManager{
    _transferInfoManager = transferInfoManager;

    self.downView.transferInfoManager = transferInfoManager;
    self.dingView.transferInfoManager = transferInfoManager;
    self.replyView.transferInfoManager = transferInfoManager;
    
    self.downView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - self.downView.size_height, 0 , [LiveForumRootDingView calculationSize].width, [LiveForumRootDingView calculationSize].height);
    self.dingView.frame = CGRectMake(self.downView.orgin_x - self.dingView.size_width, self.downView.orgin_y, self.downView.size_width, self.downView.size_height);
    self.replyView.frame = CGRectMake(self.dingView.orgin_x - self.replyView.size_width, self.downView.orgin_y, self.replyView.size_width, self.replyView.size_height);
}

+(CGFloat)calculationCellHeight{
    return [LiveForumRootDingView calculationSize].height;
}

@end
