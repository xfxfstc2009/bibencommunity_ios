//
//  LiveDetailModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/27.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "FetchModel.h"

@interface LiveDetailModel : FetchModel

@property (nonatomic,copy)NSString *account_id;
@property (nonatomic,copy)NSString *attention_user_id;
@property (nonatomic,copy)NSString *begin_time;
@property (nonatomic,copy)NSString *datetime;
@property (nonatomic,copy)NSString *invite_account_id;
@property (nonatomic,copy)NSString *live_cover_url;
@property (nonatomic,copy)NSString *live_record_id;
@property (nonatomic,copy)NSString *live_type;
@property (nonatomic,copy)NSString *live_title;
@property (nonatomic,copy)NSString *room_id;
@property (nonatomic,copy)NSString *share_detail;
@property (nonatomic,copy)NSString *share_img;
@property (nonatomic,copy)NSString *share_url;
@property (nonatomic,copy)NSString *status;
@property (nonatomic,copy)NSString *user_id;
@property (nonatomic,copy)NSString *watch_times;
@property (nonatomic,strong)NSArray *courseware;                /**< 课件*/

@end
