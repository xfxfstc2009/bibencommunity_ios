//
//  BYLiveHostController.m
//  BibenCommunity
//
//  Created by 黄亮 on 2018/9/17.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "BYLiveHostController.h"
#import "BYLiveHostViewModel.h"

@interface BYLiveHostController ()

@end

@implementation BYLiveHostController

- (Class)getViewModelClass{
    return [BYLiveHostViewModel class];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationTitle = @"直播主题";
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
