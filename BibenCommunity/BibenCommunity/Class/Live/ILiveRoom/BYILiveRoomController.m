//
//  BYILiveRoomController.m
//  BY
//
//  Created by 黄亮 on 2018/8/14.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYILiveRoomController.h"
#import "BYILiveRoomViewModel.h"

#define ViewModel ((BYILiveRoomViewModel *)self.viewModel)
@interface BYILiveRoomController ()

@property (nonatomic ,strong) BYLiveConfig *config;

@end

@implementation BYILiveRoomController

- (Class)getViewModelClass{
    return [BYILiveRoomViewModel class];
}

- (id)initWithConfig:(BYLiveConfig *)config{
    self = [super init];
    if (self) {
        _config = config;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewDidLoad {
    ViewModel.config = self.config;
    [super viewDidLoad];
    // 关闭手势返回
    self.popGestureRecognizerEnale = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
