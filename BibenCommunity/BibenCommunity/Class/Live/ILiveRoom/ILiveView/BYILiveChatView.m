//
//  BYILiveChatView.m
//  BY
//
//  Created by 黄亮 on 2018/8/30.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYILiveChatView.h"
#import "BYIMDefine.h"
#import "BYILiveChatViewCellModel.h"

@interface BYILiveChatView()<BYCommonTableViewDelegate>

@property (nonatomic ,strong) BYCommonTableView *tableView;

@end;

@implementation BYILiveChatView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setContentView];
        [self addMessageListener];
    }
    return self;
}

- (void)addMessageListener{
    // 设置消息回调
    @weakify(self);
    [[BYIMManager sharedInstance] setMessageListener:^(NSArray *msgs) {
        @strongify(self);
        [self receiveMessage:msgs];
    }];
}

#pragma mark - MessageListener
- (void)receiveMessage:(NSArray *)msgs{
    NSMutableArray *tableData = [NSMutableArray arrayWithArray:self.tableView.tableData];
    NSArray *msgArr = [BYILiveChatViewCellModel transMessageArr:msgs];
    msgArr = [[msgArr reverseObjectEnumerator] allObjects];
    [tableData addObjectsFromArray:msgArr];
    self.tableView.tableData = tableData;
    if (self.tableView.tableData.count) {
        NSIndexPath *indexPath=[NSIndexPath indexPathForRow:self.tableView.tableData.count-1 inSection:0];
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];

    }
}

#pragma mark - BYCommonTableViewDelegate

#pragma mark - configUI Method
- (void)setContentView{
    [self addSubview:self.tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    @weakify(self);
    [[BYIMManager sharedInstance] getMessage:20 succ:^(NSArray *msgs) {
        @strongify(self);
        [self receiveMessage:msgs];
    } fail:^(int code) {
        
    }];
}

- (BYCommonTableView *)tableView{
    if (!_tableView) {
        _tableView = [[BYCommonTableView alloc] init];
        _tableView.group_delegate = self;
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.showsVerticalScrollIndicator = NO;
    }
    return _tableView;
}
@end
