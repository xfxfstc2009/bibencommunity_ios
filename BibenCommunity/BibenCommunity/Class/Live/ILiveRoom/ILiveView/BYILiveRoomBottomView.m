//
//  BYILiveRoomBottomView.m
//  BY
//
//  Created by 黄亮 on 2018/8/14.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYILiveRoomBottomView.h"
#import "BYILiveRoomToolView.h"
#import "BYKeyBoardTextView.h"

@interface BYILiveRoomBottomView()

/** 是否为主播 */
@property (nonatomic ,assign) BOOL isHost;

/** 父视图 */
@property (nonatomic ,weak) UIView *fathureView;

/** 发布直播 */
@property (nonatomic ,strong) UIButton *openLiveBtn;

/** 聊天按钮 */
@property (nonatomic ,strong) UIButton *chatBtn;

/** 工具按钮 */
@property (nonatomic ,strong) UIButton *toolBtn;

/** 工具视图 */
@property (nonatomic ,strong) BYILiveRoomToolView *toolView;

@property (nonatomic ,strong) BYKeyBoardTextView *keyBoardTextView;

@end

@implementation BYILiveRoomBottomView

- (instancetype)initWithFathureView:(UIView *)fathureView isHost:(BOOL)isHost
{
    self = [super init];
    if (self) {
        self.fathureView = fathureView;
        self.isHost = isHost;
//        if (isHost)
//            [self setContentView];
//        else
            [self setContentViewNotHost];
    }
    return self;
}

+ (BYILiveRoomBottomView *)initWithIsHost:(BOOL)isHost inView:(UIView *)view{
    BYILiveRoomBottomView *bottomView = [[BYILiveRoomBottomView alloc] initWithFathureView:view isHost:isHost];
    return bottomView;
}

- (void)openLiveBtnAction{
    if (_openLiveBtnHandle)
        _openLiveBtnHandle();
}

- (void)chatBtnAction{
    [self.keyBoardTextView showAnimation];
}

- (void)toolBtnAction{
    [self.toolView showAnimation];
}

- (void)chatAction{
    [self.keyBoardTextView showAnimation];
}

- (void)likeBtnAction{
    
}

- (void)moreBtnAction{
    if (_moreBtnHandle) {
        _moreBtnHandle();
    }
}

#pragma mark - setUI Method

- (void)setContentView{
    // 开始直播
    _openLiveBtn = [UIButton by_buttonWithCustomType];
    [_openLiveBtn setBy_attributedTitle:@{@"title":@"开始直播",
                                             NSFontAttributeName:[UIFont systemFontOfSize:14],
                                             NSForegroundColorAttributeName:[UIColor whiteColor]
                                             } forState:UIControlStateNormal];
    [_openLiveBtn addTarget:self action:@selector(openLiveBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [_openLiveBtn setBackgroundColor:kColorRGBValue(0xed4a45)];
    [_openLiveBtn layerCornerRadius:17 size:CGSizeMake(120, 34)];
    [self addSubview:_openLiveBtn];
    [_openLiveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.centerX.mas_equalTo(0);
        make.width.mas_equalTo(120);
        make.height.mas_equalTo(34);
    }];
    
    // 聊天按钮
    _chatBtn = [UIButton by_buttonWithCustomType];
    [_chatBtn setBy_imageName:@"ilive_chat" forState:UIControlStateNormal];
    [_chatBtn addTarget:self action:@selector(chatBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_chatBtn];
    @weakify(self);
    [_chatBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.width.mas_equalTo(self.chatBtn.imageView.image.size.width);
        make.height.mas_equalTo(self.chatBtn.imageView.image.size.height);
        make.centerY.equalTo(self.openLiveBtn.mas_centerY).with.offset(0);
        make.left.mas_equalTo(15);
    }];
    
    // 工具按钮(美颜，摄像机，闪光灯..)
    _toolBtn = [UIButton by_buttonWithCustomType];
    [_toolBtn setBy_imageName:@"ilive_tool" forState:UIControlStateNormal];
    [_toolBtn addTarget:self action:@selector(toolBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_toolBtn];
    [_toolBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.chatBtn.mas_right).with.offset(8);
        make.centerY.equalTo(self.chatBtn.mas_centerY).with.offset(0);
        make.width.mas_equalTo(self.toolBtn.imageView.image.size.width);
        make.height.mas_equalTo(self.toolBtn.imageView.image.size.height);
    }];
   
    _keyBoardTextView = [BYKeyBoardTextView keyBoardTextViewWithFathureView:_fathureView];
}

- (void)setContentViewNotHost{
    // 跟主播聊点什么
    UIImageView *talkImg = [[UIImageView alloc] init];
    talkImg.userInteractionEnabled = YES;
    [talkImg setImage:[[UIImage imageNamed:@"iliveroom_talkField"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 190, 0, 30)]];
    [self addSubview:talkImg];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chatAction)];
    [talkImg addGestureRecognizer:tapGestureRecognizer];
    [talkImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.bottom.mas_equalTo(-7);
        make.right.mas_equalTo(-63);
        make.height.mas_equalTo(35);
    }];
    
    // 更多操作
    UIButton *moreBtn = [UIButton by_buttonWithCustomType];
    [moreBtn setBy_imageName:@"iliveroom_more" forState:UIControlStateNormal];
    [moreBtn addTarget:self action:@selector(moreBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:moreBtn];
    [moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(33);
        make.centerY.equalTo(talkImg.mas_centerY).with.offset(0);
        make.right.mas_equalTo(-15);
    }];
    
    _keyBoardTextView = [BYKeyBoardTextView keyBoardTextViewWithFathureView:_fathureView];
    
}

- (BYILiveRoomToolView *)toolView{
    if (!_toolView) {
        _toolView = [[BYILiveRoomToolView alloc] initInView:_fathureView];
    }
    return _toolView;
}

@end
