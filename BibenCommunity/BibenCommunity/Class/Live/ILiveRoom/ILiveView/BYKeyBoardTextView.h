//
//  BYKeyBoardTextView.h
//  BY
//
//  Created by 黄亮 on 2018/8/20.
//  Copyright © 2018年 BY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BYKeyBoardTextView : UIControl


/** 初始化BYKeyBoardTextView */
+ (instancetype)keyBoardTextViewWithFathureView:(UIView *)fathureView;


- (void)showAnimation;
@end
