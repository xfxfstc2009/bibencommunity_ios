//
//  BYILiveVODSubController.m
//  BibenCommunity
//
//  Created by 随风 on 2018/9/26.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "BYILiveVODSubController.h"

#import "BYIMDefine.h"
#import "BYLiveConfig.h"
// View
#import "BYILiveChatView.h"
#import "BYILiveRoomBottomView.h"
#import "BYILiveOpeartionView.h"
#import "BYCommonPlayerView.h"


@interface BYILiveVODSubController ()<SuperPlayerDelegate>

/** 聊天室展示 */
@property (nonatomic ,strong) BYILiveChatView *chatView;
/** 底部footerView */
//@property (nonatomic ,strong) BYILiveRoomBottomView *bottomView;
/** 主播、用户操作台 */
@property (nonatomic ,strong) BYILiveOpeartionView *opeartionView;

@property (nonatomic ,strong) BYToastView *endToastView;

/** 播放器View的父视图*/
@property (nonatomic) UIView *playerFatherView;
/** 是否播放默认宣传视频 */
@property (nonatomic, assign) BOOL isPlayDefaultVideo;
/** 播放器 */
@property (nonatomic ,strong) BYCommonPlayerView *playerView;


@end

@implementation BYILiveVODSubController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self configIM];
    // Do any additional setup after loading the view.
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if (![SuperPlayerWindow sharedInstance].isShowing) {
        [self.playerView resetPlayer];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configIM{
    [[BYIMManager sharedInstance] setConversationGroupId:_config.roomId];
}

#pragma mark - SuperPlayerDelegate
- (void)onPlayerBackAction {
    // player加到控制器上，只有一个player时候
    // 状态条的方向旋转的方向,来判断当前屏幕的方向
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    // 是竖屏时候响应关
    if (orientation == UIInterfaceOrientationPortrait && SuperPlayerGlobleConfigShared.enableFloatWindow &&
        (self.playerView.state == StatePlaying) && _isPlayDefaultVideo) {
        [SuperPlayerWindowShared setSuperPlayer:self.playerView];
        [SuperPlayerWindowShared show];
        SuperPlayerWindowShared.backController = self;
    } else {
        [self.playerView resetPlayer];  //非常重要
    }
//    [self.superController.navigationController popViewControllerAnimated:YES];
}

#pragma mark - buttonAction Method
// 退出
- (void)backBtnAction{
    _endToastView = [BYToastView toastViewPresentLoading];
    TILLiveManager *manager = [TILLiveManager getInstance];
    @weakify(self);
    [manager quitRoom:^{
        @strongify(self);
        [self.endToastView dissmissToastView];
#ifdef DEBUG
        showToastView(@"退出房间成功", kCommonWindow);
#endif
        [self.superController.navigationController popToRootViewControllerAnimated:YES];
    } failed:^(NSString *module, int errId, NSString *errMsg) {
        @strongify(self);
        [self.endToastView dissmissToastView];
        showToastView(@"退出房间失败", self.view);
    }];
}

- (void)configUI{
    
    self.isPlayDefaultVideo = NO;
    self.playerFatherView = [[UIView alloc] init];
    self.playerFatherView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:self.playerFatherView];
    [self.playerFatherView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(kiPhoneX_Mas_top(0), 0, kiPhoneX_Mas_buttom(0), 0));
    }];
    self.playerView.fatherView = self.playerFatherView;
    
    [_playerView playVideoWithUrl:_config.video_url title:@""];
    
//    self.bottomView = [BYILiveRoomBottomView initWithIsHost:_config.isHost inView:self.view];
    @weakify(self);
//    _bottomView.moreBtnHandle = ^{
//        @strongify(self);
//        [self backBtnAction];
//    };
//    [self.view addSubview:self.bottomView];
//    [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
//        @strongify(self);
//        if (self.config.isVOD) {
//            make.bottom.mas_equalTo(kiPhoneX_Mas_buttom(45));
//        }else{
//            make.bottom.mas_equalTo(kiPhoneX_Mas_buttom(5));
//        }
//        make.height.mas_equalTo(54);
//        make.left.right.mas_equalTo(0);
//    }];

    
    self.chatView = [BYILiveChatView by_init];
    [self.view addSubview:self.chatView];
    [_chatView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(0);
        if (self.config.isHost) {
            make.bottom.mas_equalTo(kiPhoneX_Mas_buttom(30));
        }
        else {
            make.bottom.mas_equalTo(kiPhoneX_Mas_buttom(60));
        }
        make.right.mas_equalTo(-80);
        make.height.mas_equalTo(200);
    }];
    
    self.opeartionView = [[BYILiveOpeartionView alloc] initWithIsHost:_config.isHost target:self];
    _opeartionView.didExitLiveBtnHandle = ^{
        @strongify(self);
        [self backBtnAction];
    };
    [self.view addSubview:_opeartionView];
    [_opeartionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.width.mas_equalTo(69);
        make.height.mas_equalTo(130);
        make.bottom.mas_equalTo(kiPhoneX_Mas_buttom(130));
    }];
}

- (BYCommonPlayerView *)playerView {
    if (!_playerView) {
        _playerView = [[BYCommonPlayerView alloc] init];
        _playerView.fatherView = _playerFatherView;
        _playerView.enableFloatWindow = NO;
        // 设置代理
        _playerView.delegate = self;
    }
    return _playerView;
}

@end
