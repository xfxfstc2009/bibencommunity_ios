//
//  BYILiveRoomSubController.h
//  BibenCommunity
//
//  Created by 随风 on 2018/9/22.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "BYCommonViewController.h"
#import "BYLiveConfig.h"

@class BYCommonLiveModel;
@interface BYILiveRoomSubController : BYCommonViewController

/** 配置 */
@property (nonatomic ,strong) BYLiveConfig *config;
@property (nonatomic ,strong) BYCommonLiveModel *model;
/** 父Controller */
@property (nonatomic ,weak) UIViewController *superController;

- (void)quitRoom;
@end
