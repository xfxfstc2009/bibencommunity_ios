//
//  BYILiveRoomSubController.m
//  BibenCommunity
//
//  Created by 随风 on 2018/9/22.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "BYILiveRoomSubController.h"
#import <ILiveSDK/ILiveCoreHeader.h>
#import <TILLiveSDK/TILLiveSDK.h>
#import "BYIMDefine.h"

// Controller
#import "BYILiveEndController.h"

// View
#import "BYILiveChatView.h"
#import "BYILiveRoomBottomView.h"
#import "BYILiveOpeartionView.h"

// model、request
#import "NSDate+BYExtension.h"
#import "BYCommonLiveModel.h"
#import "BYLiveHomeRequest.h"


@interface BYILiveRoomSubController ()<QAVRemoteVideoDelegate,ILVLiveAVListener,ILVLiveIMListener,QAVLocalVideoDelegate>

@property (nonatomic ,strong) UIImageView *roomBgImgView;

/** 聊天室展示 */
@property (nonatomic ,strong) BYILiveChatView *chatView;

/** 心跳计时 */
@property (nonatomic ,strong) NSTimer *roomTimer;

/** 推流的id */
@property (nonatomic ,assign) UInt64 channelID;

/** 底部footerView */
@property (nonatomic ,strong) BYILiveRoomBottomView *bottomView;

@property (nonatomic ,strong) BYToastView *toastView;
@property (nonatomic ,strong) BYToastView *endToastView;
@property (nonatomic ,strong) dispatch_source_t timer;
@property (nonatomic ,strong) UILabel *timerLab;
@property (nonatomic ,strong) NSTimer *logTimer;
@property (nonatomic ,strong) UITextView *textView;
/** 主播、用户操作台 */
@property (nonatomic ,strong) BYILiveOpeartionView *opeartionView;

@end

@implementation BYILiveRoomSubController

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self configIM];
    [self configUI];
    [self configRoom];
    if (_config.isHost) { // 开播时间校验
        [self verifyLiveTime];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)configRoom{
    TILLiveManager *manager = [TILLiveManager getInstance];
    [manager setAVListener:self];
    [manager setIMListener:self];
    //如果要使用美颜，必须设置本地视频代理
    [[ILiveRoomManager getInstance] setLocalVideoDelegate:self];
    [[ILiveRoomManager getInstance] setRemoteVideoDelegate:self];
    if (!_config.isHost) {
        [self joinRoom];
    }
    else{
    }
    
}
- (void)configIM{
    [[BYIMManager sharedInstance] setConversationGroupId:_model.live_record_id];
    [[BYIMManager sharedInstance] joinAVChatRoomGroupId:_model.live_record_id succ:^{
        NSLog(@"2222231");
    } fail:^{
        NSLog(@"2222251");
    }];
}

// 开播时间与当前时间比对
- (void)verifyLiveTime{
    NSDate *date = [NSDate by_dateFromString:self.model.begin_time dateformatter:K_D_F];
    NSDate *nowDate = [NSDate by_date];
    NSComparisonResult result = [date compare:nowDate];
    if (result == NSOrderedAscending ||
        result == NSOrderedSame) {
        [self creatRoom];
    }
    else // 未到开播时间，倒计时自动开播
    {
        [self timerCountDown];
    }
}

// 倒计时
- (void)timerCountDown{
    NSDate *endDate = [NSDate by_dateFromString:self.model.begin_time dateformatter:K_D_F];
    NSInteger timeDif = [NSDate by_startTime:[NSDate by_date] endTime:endDate];
    __weak __typeof(self) weakSelf = self;
    
    if (_timer == nil) {
        __block NSInteger timeout = timeDif; // 倒计时时间
        
        if (timeout!=0) {
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
            dispatch_source_set_timer(self.timer, dispatch_walltime(NULL, 0), 1.0*NSEC_PER_SEC,  0); //每秒执行
            dispatch_source_set_event_handler(self.timer, ^{
                if(timeout <= 0){ //  当倒计时结束时做需要的操作: 关闭 活动到期不能提交
                    dispatch_source_cancel(self.timer);
                    self.timer = nil;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self creatRoom];
                        weakSelf.timerLab.hidden = YES;
                    });
                } else { // 倒计时重新计算 时/分/秒
                    NSInteger days = (int)(timeout/(3600*24));
                    NSInteger hours = (int)((timeout-days*24*3600)/3600);
                    NSInteger minute = (int)(timeout-days*24*3600-hours*3600)/60;
                    NSInteger second = timeout - days*24*3600 - hours*3600 - minute*60;
                    NSString *strTime = [NSString stringWithFormat:@"开播倒计时 %02ld : %02ld : %02ld", hours, minute, second];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (days == 0) {
                            weakSelf.timerLab.text = strTime;
                        } else {
                            weakSelf.timerLab.text = [NSString stringWithFormat:@"开播倒计时  %ld天 %02ld : %02ld : %02ld", days, hours, minute, second];
                        }
                    });
                    timeout--; // 递减 倒计时-1(总时间以秒来计算)
                }
            });
            dispatch_resume(self.timer);
        }
    }
}

#pragma mark - 心跳(房间保活)

// 开始发送心跳
- (void)startLiveTimer{
    if (_roomTimer) {
        [_roomTimer invalidate];
        _roomTimer = nil;
    }
    _roomTimer = [NSTimer scheduledTimerWithTimeInterval:kRoomTimerDuration target:self selector:@selector(postHeartBeat:) userInfo:nil repeats:YES];
}

#pragma mark - customMethod

// 获取直播测试信息
- (void)setLogInfo{
    NSString *string = [self.config onLogTimer];
    NSLog(@"%@",string);
    dispatch_async(dispatch_get_main_queue(), ^{
        self.textView.text = string;
    });
}

// 进入直播房间
- (void)joinRoom{
    TILLiveRoomOption *roomOption = [TILLiveRoomOption defaultGuestLiveOption];
    roomOption.avOption.autoHdAudio = NO;
    roomOption.controlRole = @"Guest";
    TILLiveManager *manager = [TILLiveManager getInstance];
    [manager setAVRootView:self.view];
    [manager addAVRenderView:self.view.bounds forIdentifier:_config.roomId srcType:QAVVIDEO_SRC_TYPE_CAMERA];
    @weakify(self);
    [manager joinRoom:[_config.roomId intValue] option:roomOption succ:^{
        @strongify(self);
        self.roomBgImgView.hidden = YES;
//        [self showAlertView:nil message:@"进入房间成功"];
    } failed:^(NSString *module, int errId, NSString *errMsg) {
#if DEBUG
//        @strongify(self);
        if ([module isEqualToString:@"IMSDK"]) return ; // 屏蔽IM错误信息
        NSString *errinfo = [NSString stringWithFormat:@"join room fail.module=%@,errid=%d,errmsg=%@",module,errId,errMsg];
        PDLog(@"%@",errinfo);
//        [self showAlertView:@"进入房间失败" message:errinfo];
#endif
    }];
}

// 创建互动直播间
- (void)creatRoom{
    _toastView = [BYToastView toastViewPresentLoading];
    TILLiveRoomOption *option = [TILLiveRoomOption defaultHostLiveOption];
    option.avOption.cameraPos = CameraPosBack;
    option.controlRole = _config.liveQuality;
    TILLiveManager *manager = [TILLiveManager getInstance];
    //设置渲染承载的视图
    [manager setAVRootView:self.view];
    //添加渲染视图，userid：画面所属者id type:相机/屏幕共享
    [manager addAVRenderView:self.view.bounds forIdentifier:[AccountModel sharedAccountModel].account_id srcType:QAVVIDEO_SRC_TYPE_CAMERA];
    @weakify(self);
    [manager createRoom:[_config.roomId intValue] option:option succ:^{
        @strongify(self);
        [self.toastView dissmissToastView];
        [BYLiveConfig setBeauty:8];
        [BYLiveConfig setWhite:8];
        [self startPushStream];
        NSLog(@"创建房间成功");
    } failed:^(NSString *moudle, int errId, NSString *errMsg) {
        @strongify(self);
        [self.toastView dissmissToastView];
        showToastView(@"创建房间失败", self.view);
    }];
}

- (void)quitRoom{
    [self backBtnAction];
}

#pragma mark - buttonAction Method
// 退出
- (void)backBtnAction{
    // 角色为非主播退出时
    if (!_config.isHost) {
        _endToastView = [BYToastView toastViewPresentLoading];
        TILLiveManager *manager = [TILLiveManager getInstance];
        @weakify(self);
        [manager quitRoom:^{
            @strongify(self);
            [self.endToastView dissmissToastView];
#ifdef DEBUG
            showToastView(@"退出房间成功", kCommonWindow);
#endif
            [self.superController.navigationController popToRootViewControllerAnimated:YES];
        } failed:^(NSString *module, int errId, NSString *errMsg) {
            @strongify(self);
            [self.endToastView dissmissToastView];
            showToastView(@"退出房间失败", self.view);
        }];
        return;
    }
    
    // 角色为主播退出时
    if ([[ILiveRoomManager getInstance] getRoomId] <= 0) {
        [self.superController.navigationController popViewControllerAnimated:YES];
        return;
    }
    _endToastView = [BYToastView toastViewPresentLoading];
    // 获取当前聊天室人数
    [[TIMGroupManager sharedInstance] GetGroupMembers:_model.live_record_id succ:^(NSArray *members) {
        self.model.watchNum = members.count;
    } fail:^(int code, NSString *msg) {
        NSLog(@"");
    }];
    // 停止推流
    @weakify(self);
    [self stopPushStream:^{
        @strongify(self);
//        @weakify(self);
//        [manager quitRoom:^{
//            @strongify(self);
        [self.endToastView dissmissToastView];
        [self loadRequestUpdateLiveStatus:BY_LIVE_STATUS_END];
        BYILiveEndController *liveEndController = [[BYILiveEndController alloc] init];
        liveEndController.model = self.model;
        [self.superController.navigationController pushViewController:liveEndController animated:YES];
            //            [S_V_NC popViewControllerAnimated:YES];
//        } failed:^(NSString *module, int errId, NSString *errMsg) {
//            @strongify(self);
//            [self.endToastView dissmissToastView];
//            showToastView(@"退出房间失败", self.view);
//        }];
    }];
}


// 切换摄像头
- (void)swichCameraBtnAction{
    // 切换摄像头
    [BYLiveConfig changeCameraPos];
}

// 查看直播信息log
- (void)infoBtnAction:(UIButton *)sender{
    if (!sender.selected) {
        self.textView = [[UITextView alloc] init];
        self.textView.editable = NO;
        [self.textView setFont:[UIFont systemFontOfSize:17]];
        [self.textView setTextColor:kColorRGBValue(0xed4a45)];
        [self.textView setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:self.textView];
        [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(kCommonScreenWidth);
            make.centerX.bottom.mas_equalTo(0);
            make.top.mas_equalTo(50);
        }];
        
        self.logTimer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(setLogInfo) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:self.logTimer forMode:NSDefaultRunLoopMode];
    }
    else
    {
        [self.textView removeFromSuperview];
        self.textView = nil;
        if (self.logTimer) {
            [self.logTimer invalidate];
            self.logTimer = nil;
        }
    }
    sender.selected = !sender.selected;
}

// 开始直播
- (void)openLiveAction{
    // 主播创建房间
    [self creatRoom];
}

#pragma mark - requestMethod
// 心跳包请求
- (void)postHeartBeat:(NSTimer *)timer{
    
}

// 更新直播间状态
- (void)loadRequestUpdateLiveStatus:(BY_LIVE_STATUS)status{
    [[BYLiveHomeRequest alloc] loadRequestUpdateLiveStatus:status sig:[ILiveLoginManager getInstance].getSig live_record_id:self.model.live_record_id successBlock:^(id object) {
        NSLog(@"更新直播间状态成功");
    } faileBlock:^(NSError *error) {
        NSLog(@"更新直播间状态失败");
    }];
}

#pragma mark - 推流
// 开始推流
- (void)startPushStream{
    
    @weakify(self);
    [[ILiveRoomManager getInstance] startPushStream:[BYLiveConfig getHostPushOption] succ:^(id selfPtr) {
        @strongify(self);
        [self.toastView dissmissToastView];
        AVStreamerResp *resp = (AVStreamerResp *)selfPtr;
        AVLiveUrl *url = nil;
        if (resp && resp.urls && resp.urls.count > 0)
        {
            url = resp.urls[0];
        }
        if (url) {
            self.channelID = resp.channelID;
            [self loadRequestUpdateLiveStatus:BY_LIVE_STATUS_LIVING];
//            [self showAlertView:@"推流成功" message:url.playUrl];
#pragma mark - 重置播放起始时间
            self.model.begin_time = [NSDate by_stringFromDate:[NSDate by_date] dateformatter:K_D_F];
            self.roomBgImgView.hidden = YES;
            NSLog(@"推流地址：%@",url.playUrl);
        }
    } failed:^(NSString *module, int errId, NSString *errMsg) {
        NSString *errinfo = [NSString stringWithFormat:@"push stream fail.module=%@,errid=%d,errmsg=%@",module,errId,errMsg];
        [self showAlertView:@"推流失败" message:errinfo];
    }];
}

// 停止推流
- (void)stopPushStream:(void(^)(void))block{
    @weakify(self);
    [[ILiveRoomManager getInstance] stopPushStreams:@[@(self.channelID)] succ:^{
        block ? block() : nil ;
    } failed:^(NSString *module, int errId, NSString *errMsg) {
        @strongify(self);
        NSString *errinfo = [NSString stringWithFormat:@"stop push stream fail.module=%@,errid=%d,errmsg=%@",module,errId,errMsg];
        [self.endToastView dissmissToastView];
        [self showAlertView:@"停止推流失败" message:errinfo];
    }];
}

#pragma mark - ILVLiveIMListener

- (void)onTextMessage:(ILVLiveTextMessage *)msg{
    NSLog(@"%s",__func__);
}
- (void)onCustomMessage:(ILVLiveCustomMessage *)msg{
    NSLog(@"%s",__func__);
}
- (void)onOtherMessage:(TIMMessage *)msg{
    NSLog(@"%s",__func__);
}

#pragma mark - QAVLocalVideoDelegate

- (void)OnLocalVideoPreview:(QAVVideoFrame *)frameData{
    //仅仅是为了打log
    NSString *key = frameData.identifier;
    if (key.length == 0)
    {
        key = [[ILiveLoginManager getInstance] getLoginId];
    }
    if (!key.length) return;
    QAVFrameDesc *desc = [[QAVFrameDesc alloc] init];
    desc.width = frameData.frameDesc.width;
    desc.height = frameData.frameDesc.height;
    [_config.resolutionDic setObject:desc forKey:key];
}
- (void)OnLocalVideoPreProcess:(QAVVideoFrame *)frameData{
    
}
- (void)OnLocalVideoRawSampleBuf:(CMSampleBufferRef)buf result:(CMSampleBufferRef *)ret{
    
}

#pragma mark - QAVRemoteVideoDelegate
- (void)OnVideoPreview:(QAVVideoFrame *)frameData{
    NSString *key = frameData.identifier;
    QAVFrameDesc *desc = [[QAVFrameDesc alloc] init];
    desc.width = frameData.frameDesc.width;
    desc.height = frameData.frameDesc.height;
    [_config.resolutionDic setObject:desc forKey:key];
    
}
#pragma mark - ILVLiveAVListener
- (void)onUserUpdateInfo:(ILVLiveAVEvent)event users:(NSArray *)users{
    if (event == ILVLIVE_AVEVENT_CAMERA_OFF && [users indexOfObject:_config.roomId] != NSNotFound) {
//        showToastView(@"主播已经退出", self.view);
        [self showAlertView:@"主播已经退出" message:nil];
    }
    
}
- (void)onFirstFrameRecved:(int)width height:(int)height identifier:(NSString *)identifier srcType:(avVideoSrcType)srcType{
    
}
- (void)onRoomDisconnect:(int)reason{
    showToastView(@"失去连接", self.view);
}

#pragma mark - initMethod/configUI

// 初始化ui
- (void)configUI{
    
    UIImageView *roomBgImgView = [[UIImageView alloc] init];
    [roomBgImgView by_setImageName:@"room_bg"];
    [self.view addSubview:roomBgImgView];
    _roomBgImgView = roomBgImgView;
    [roomBgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    if (!_config.isHost) {
        [self.view addSubview:self.bottomView];
        @weakify(self);
        [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.height.mas_equalTo(54);
            make.left.right.mas_equalTo(0);
            if (self.config.isVOD) {
                make.bottom.mas_equalTo(kiPhoneX_Mas_buttom(45));
            }else{
                make.bottom.mas_equalTo(kiPhoneX_Mas_buttom(5));
            }
        }];
    }
    //    UIButton *infoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [infoBtn setTitle:@"直播信息" forState:UIControlStateNormal];
    //    [infoBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    //    [infoBtn addTarget:self action:@selector(infoBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    //    infoBtn.selected = NO;
    //    [self.view addSubview:infoBtn];
    //    [infoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.top.mas_equalTo(kiPhoneX_Mas_top(10));
    //        make.height.mas_equalTo(40);
    //        make.width.mas_equalTo(100);
    //        make.centerX.mas_equalTo(0);
    //    }];
    
    [self.view addSubview:self.chatView];
    @weakify(self);
    [_chatView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(0);
        if (self.config.isHost) {
            make.bottom.mas_equalTo(kiPhoneX_Mas_buttom(30));
        }
        else {
            make.bottom.equalTo(self.bottomView.mas_top).with.offset(-20);
        }
        make.right.mas_equalTo(-80);
        make.height.mas_equalTo(250);
    }];
    
    _timerLab = [UILabel by_init];
    _timerLab.font = [UIFont systemFontOfSize:18];
    _timerLab.textColor = [UIColor redColor];
    _timerLab.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:_timerLab];
    [_timerLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kCommonScreenWidth);
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(100);
        make.height.mas_equalTo(25);
    }];
    
    self.opeartionView = [[BYILiveOpeartionView alloc] initWithIsHost:_config.isHost target:self];
    _opeartionView.didExitLiveBtnHandle = ^{
        @strongify(self);
        [self backBtnAction];
    };
    [self.view addSubview:_opeartionView];
    [_opeartionView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        if (self.config.isHost) {
            make.width.mas_equalTo(33);
            make.height.mas_equalTo(170);
            make.top.right.mas_equalTo(0);
        }
        else
        {
            make.right.mas_equalTo(0);
            make.width.mas_equalTo(69);
            make.height.mas_equalTo(130);
            make.bottom.mas_equalTo(kiPhoneX_Mas_buttom(130));
        }
    }];
}

- (BYILiveRoomBottomView *)bottomView{
    if (!_bottomView) {
        _bottomView = [BYILiveRoomBottomView initWithIsHost:_config.isHost inView:self.view];
        @weakify(self);
        _bottomView.openLiveBtnHandle = ^{
            @strongify(self);
            [self openLiveAction];
        };
        _bottomView.moreBtnHandle = ^{
            @strongify(self);
            [self backBtnAction];
        };
    }
    return _bottomView;
}

- (BYILiveChatView *)chatView{
    if (!_chatView) {
        _chatView = [BYILiveChatView by_init];
    }
    return _chatView;
}

@end
