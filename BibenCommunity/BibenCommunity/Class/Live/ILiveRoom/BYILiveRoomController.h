//
//  BYILiveRoomController.h
//  BY
//
//  Created by 黄亮 on 2018/8/14.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCommonViewController.h"

@class BYLiveConfig,BYCommonLiveModel;
@interface BYILiveRoomController : BYCommonViewController

@property (nonatomic ,readonly ,strong) BYLiveConfig *config;
@property (nonatomic ,strong) BYCommonLiveModel *model;

- (id)initWithConfig:(BYLiveConfig *)config;

@end
