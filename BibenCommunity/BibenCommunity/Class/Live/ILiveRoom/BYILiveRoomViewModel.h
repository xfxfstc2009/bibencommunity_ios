//
//  BYILiveRoomViewModel.h
//  BY
//
//  Created by 黄亮 on 2018/8/14.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCommonViewModel.h"
#import "BYLiveConfig.h"

@interface BYILiveRoomViewModel : BYCommonViewModel

@property (nonatomic ,strong) BYLiveConfig *config;

@end
