//
//  BYILiveRoomViewModel.m
//  BY
//
//  Created by 黄亮 on 2018/8/14.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYILiveRoomViewModel.h"
#import "BYILiveRoomSegmentView.h"
#import "BYILiveRoomSubController.h"
#import "BYILiveVODSubController.h"
#import "BYILiveRoomController.h"
#import "LiveForumRootViewController.h"
#import "BYILiveIntroView.h"

#import "BYCommonLiveModel.h"

#define S_ILIVE_VC ((BYILiveRoomController *)S_VC)
@interface BYILiveRoomViewModel()<BYILiveRoomSegmentViewDelegate>

/** 分类选择器 */
@property (nonatomic ,strong) BYILiveRoomSegmentView *segmentView;
/** 直播显示的view */
@property (nonatomic ,strong) BYILiveRoomSubController *liveSubController;
/** 回房显示的view */
@property (nonatomic ,strong) BYILiveVODSubController *liveVODSubController;
/** 简介 */
@property (nonatomic ,strong) BYILiveIntroView *introView;
/** 点评 */
@property (nonatomic ,strong) LiveForumRootViewController *forumController;

@end

@implementation BYILiveRoomViewModel

- (void)setContentView{
    
    // 简介
    self.introView = [[BYILiveIntroView alloc] init];
    self.introView.orginY = kNavigationHeight;
    [self.introView reloadData:S_ILIVE_VC.model];
    
    // 分类选择器
    self.segmentView = [[BYILiveRoomSegmentView alloc] initWithIsHost:_config.isHost titles:@"简介",@"直播",@"点评", nil];
    self.segmentView.defultSelIndex = 1;
    self.segmentView.delegate = self;
    @weakify(self);
    self.segmentView.didBackActionHandle = ^{
        @strongify(self);
        [S_V_NC popViewControllerAnimated:YES];
        [self.liveSubController quitRoom];
    };
    self.segmentView.didShareActionHandle = ^{
        @strongify(self);
        showToastView(@"分享", S_V_VIEW);
    };
    [S_V_VIEW addSubview:self.segmentView];
    [self.segmentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
 
}

#pragma mark - BYILiveRoomSegmentViewDelegate
- (UIView *)by_segmentViewLoadSubView:(NSInteger)index{
    switch (index) {
        case 0:
            return self.introView; // 简介
            break;
        case 1:
            if (_config.isVOD) // 判断是否为点播
                return self.liveVODSubController.view; // 加载点播界面
            else
                return self.liveSubController.view; // 加载直播界面
            break;
        default:
            return self.forumController.view; // 点评
            break;
    }
}

- (void)by_segmentViewDidScrollToIndex:(NSInteger)index{
    
}

- (BYILiveRoomSubController *)liveSubController{
    if (!_liveSubController) {
        // 直播界面
        self.liveSubController = [[BYILiveRoomSubController alloc] init];
        self.liveSubController.superController = S_VC;
        self.liveSubController.config = self.config;
        self.liveSubController.model = S_ILIVE_VC.model;
        CGFloat height = kCommonScreenHeight - kiPhoneX_Mas_top(0) - (-kiPhoneX_Mas_buttom(0));
        self.liveSubController.view.frame = CGRectMake(0, 0, kCommonScreenWidth, height);
//        [S_VC addChildViewController:self.liveSubController];
    }
    return _liveSubController;
}

- (BYILiveVODSubController *)liveVODSubController{
    if (!_liveVODSubController) {
        self.liveVODSubController = [[BYILiveVODSubController alloc] init];
        self.liveVODSubController.superController = S_VC;
        self.liveVODSubController.model = S_ILIVE_VC.model;
        self.liveVODSubController.config = self.config;
    }
    return _liveVODSubController;
}

- (LiveForumRootViewController *)forumController{
    if (!_forumController) {
        _forumController = [[LiveForumRootViewController alloc] init];
        _forumController.transferRoomId = S_ILIVE_VC.model.live_record_id;
        CGFloat height = kCommonScreenHeight - kNavigationHeight - (-kiPhoneX_Mas_buttom(0));
        _forumController.viewSizeHeight = height;
        _forumController.view.frame = CGRectMake(0, kNavigationHeight, kCommonScreenWidth, height);
    }
    return _forumController;
}
@end
