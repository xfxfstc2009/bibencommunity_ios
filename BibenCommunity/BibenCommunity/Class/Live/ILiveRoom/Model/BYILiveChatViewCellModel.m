//
//  BYILiveChatViewCellModel.m
//  BY
//
//  Created by 黄亮 on 2018/9/13.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYILiveChatViewCellModel.h"
#import <ImSDK/TIMMessage.h>
#import <YYLabel.h>
#import <NSAttributedString+YYText.h>
#import <ImSDK/TIMFriendshipManager.h>

@implementation BYILiveChatViewCellModel

+ (NSArray *)transMessageArr:(NSArray *)arr{
    NSMutableArray *tableData = [NSMutableArray array];
    for (TIMMessage *msg in arr) {
        if ([(TIMTextElem *)[msg getElem:0] isMemberOfClass:[TIMGroupSystemElem class]]) break;
        if ([(TIMTextElem *)[msg getElem:0] isMemberOfClass:[TIMProfileSystemElem class]]) break;
        BYILiveChatViewCellModel *model = [[BYILiveChatViewCellModel alloc] init];
        TIMTextElem *elem = (TIMTextElem *)[msg getElem:0];
        model.cellString = @"BYILivewChatViewTextCell";
        model.userName = msg.sender;
        if (msg.isSelf) {
            model.userName = [AccountModel sharedAccountModel].loginServerModel.user.nickname;
            model.userlogoUrl = [AccountModel sharedAccountModel].loginServerModel.account.head_img;
        }
        else
        {
            TIMUserProfile *user = [msg GetSenderProfile];
            model.userName = user.nickname;
            model.userlogoUrl = user.faceURL;
        }
//        NSDictionary *dic =[NSJSONSerialization JSONObjectWithData:msg.customData options:NSJSONReadingMutableLeaves error:nil];
//        model.userlogoUrl = dic[@"user_logo"];
//        model.userName = dic[@"user_name"];
        model.message = elem.text;
        NSString *message = [NSString stringWithFormat:@"%@ : %@",model.userName,model.message];
        NSMutableAttributedString *messageAttributed = [[NSMutableAttributedString alloc]initWithString:message];
        messageAttributed.yy_color = [UIColor whiteColor];
        messageAttributed.yy_lineSpacing = 4;
        messageAttributed.yy_font = [UIFont systemFontOfSize:12];
        YYTextContainer *textContainer = [YYTextContainer new];
        textContainer.size = CGSizeMake(kCommonScreenWidth - 150, MAXFLOAT);
        YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:messageAttributed];
        model.cellHeight = layout.textBoundingSize.height + 8 + 10;
        model.messageSize = CGSizeMake(layout.textBoundingSize.width, layout.textBoundingSize.height);
        [tableData addObject:model];
    }
    return tableData;
}

@end
