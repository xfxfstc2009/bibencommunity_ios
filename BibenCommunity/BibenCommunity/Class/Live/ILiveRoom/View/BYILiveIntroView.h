//
//  BYILiveIntroView.h
//  BY
//
//  Created by 黄亮 on 2018/9/15.
//  Copyright © 2018年 BY. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BYCommonLiveModel;
@interface BYILiveIntroView : UIView

/** 初始显示Y值 */
@property (nonatomic ,assign) CGFloat orginY;

- (void)reloadData:(BYCommonLiveModel *)model;

- (void)showAnimation;
- (void)hiddenAnimation;

@end
