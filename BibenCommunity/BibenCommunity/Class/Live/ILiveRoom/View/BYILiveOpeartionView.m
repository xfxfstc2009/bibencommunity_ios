//
//  BYILiveOpeartionView.m
//  BY
//
//  Created by 黄亮 on 2018/9/13.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYILiveOpeartionView.h"
#import "BYLiveConfig.h"

static NSInteger baseTag = 0x422;
@interface BYILiveOpeartionView()

/** 当前是否为主播 */
@property (nonatomic ,assign) BOOL isHost;

@property (nonatomic ,weak) UIViewController *target;

@end

@implementation BYILiveOpeartionView

- (instancetype)initWithIsHost:(BOOL)isHost target:(UIViewController *)target{
    self = [super init];
    if (self) {
        self.isHost = isHost;
        self.target = target;
        [self setContentView];
    }
    return self;
}

- (void)setContentView{
    if (_isHost)
        [self configHostView];
    else
        [self configGuestView];
    
}

// 配置主播操作界面
- (void)configHostView{
    NSArray *imageNames = @[@"ilive_exit",@"ilive_share",@"ilive_switch_camera"];
    for (int i = 0; i < 3; i ++) {
        UIButton *button = [UIButton by_buttonWithCustomType];
        [button setBy_imageName:imageNames[i] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(hostButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -15/2, 0, 0);
        [self addSubview:button];
        button.tag = baseTag + i;
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(0);
            make.top.mas_equalTo(kiPhoneX_Mas_top(20) + 43*i);
            make.height.mas_equalTo(43);
        }];
    }
}

// 配置用户操作界面
- (void)configGuestView{
    NSArray *imageNames = @[@"iliveroom_reward",@"iliveroom_top",@"iliveroom_ trample"];
    for (int i = 0; i < 3; i ++) {
        UIButton *button = [UIButton by_buttonWithCustomType];
        [button setBy_imageName:imageNames[i] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(guestButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        button.tag = baseTag + i;
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(i == 1 ? 0 : 22);
            make.top.mas_equalTo(48*i);
            make.width.height.mas_equalTo(32);
        }];
    }

}

- (void)hostButtonAction:(UIButton *)sender{
    switch (sender.tag - baseTag) {
        case 0:
            [self showExitSheetView];
            break;
        case 1:
            showToastView(@"点击分享", kCommonWindow);
            break;
        default: // 切换摄像头
            [BYLiveConfig changeCameraPos];
            break;
    }
}

- (void)guestButtonAction:(UIButton *)sender{
    switch (sender.tag - baseTag) {
        case 0:
            showToastView(@"点击赏", kCommonWindow);
            break;
        case 1:
            showToastView(@"点击顶", kCommonWindow);
            break;
        default:
            showToastView(@"点击踩", kCommonWindow);
            break;
    }
}

// 结束直播弹框
- (void)showExitSheetView{
    UIAlertController *sheetController = [UIAlertController alertControllerWithTitle:@"确定要结束直播吗？" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    @weakify(self);
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self);
        if (self.didExitLiveBtnHandle) self.didExitLiveBtnHandle();
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [sheetController addAction:confirmAction];
    [sheetController addAction:cancelAction];
    [self.target presentViewController:sheetController animated:YES completion:nil];
}

@end
