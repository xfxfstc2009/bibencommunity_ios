//
//  BYILiveOpeartionView.h
//  BY
//
//  Created by 黄亮 on 2018/9/13.
//  Copyright © 2018年 BY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BYILiveOpeartionView : UIView

/** 退出直播按钮回调 */
@property (nonatomic ,copy) void(^didExitLiveBtnHandle)(void);


/** 初始化判断是否为主播 */
- (instancetype)initWithIsHost:(BOOL)isHost target:(UIViewController *)target;

@end
