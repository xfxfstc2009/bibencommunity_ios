//
//  BYILiveIntroView.m
//  BY
//
//  Created by 黄亮 on 2018/9/15.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYILiveIntroView.h"
#import <YYLabel.h>
#import "BYCommonLiveModel.h"

@interface BYILiveIntroView()

@property (nonatomic ,strong) UIScrollView *scrollView;

/** 直播封面图 */
@property (nonatomic ,strong) PDImageView *room_coverImgView;
/** 直播标题 */
@property (nonatomic ,strong) UILabel *titleLab;
/** 用户头像 */
@property (nonatomic ,strong) PDImageView *logoImgView;
/** 昵称 */
@property (nonatomic ,strong) UILabel *userNameLab;
/** 开始时间 */
@property (nonatomic ,strong) UILabel *beginTimeLab;
/** 直播宣传图 */
@property (nonatomic ,strong) PDImageView *room_inrtoImgView;
/** 直播简介 */
@property (nonatomic ,strong) YYLabel *room_introLab;

@end

@implementation BYILiveIntroView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        [self setContentView];
    }
    return self;
}

- (void)setOrginY:(CGFloat)orginY{
    _orginY = orginY;
    @weakify(self);
    [_scrollView mas_updateConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.edges.mas_equalTo(UIEdgeInsetsMake(self.orginY, 0, 0, 0));
    }];
}

- (void)reloadData:(BYCommonLiveModel *)model{
    [_room_coverImgView uploadImageWithURL:model.live_cover_url placeholder:nil callback:nil];
    _titleLab.text = model.live_title;
    [_titleLab sizeToFit];
    @weakify(self);
//    [_logoImgView uploadMainImageWithURL:model.head_img placeholder:nil imgType:PDImgTypeDrawRound callback:nil];
    [self.logoImgView uploadMainImageWithURL:model.head_img placeholder:nil imgType:PDImgTypeOriginal callback:^(UIImage *image) {
        @strongify(self);
        self.logoImgView.image = [image addCornerRadius:11 size:CGSizeMake(22, 22)];
    }];
    _userNameLab.text = model.nickname;
    [_userNameLab sizeToFit];
    _beginTimeLab.text = model.begin_time;
    _room_introLab.text = model.live_intro;
    [_room_introLab sizeToFit];
    [_room_inrtoImgView uploadImageWithURL:model.ad_img placeholder:nil callback:^(UIImage *image) {
        @strongify(self);
        if (image) {
            CGFloat height = (kCommonScreenWidth - 30)*image.size.height/image.size.width;
            dispatch_async(dispatch_get_main_queue(), ^{
                @weakify(self);
                [self.room_inrtoImgView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.height.mas_equalTo(height);
                }];
                [self.scrollView mas_updateConstraints:^(MASConstraintMaker *make) {
                    @strongify(self);
                    make.bottom.mas_equalTo(self.room_inrtoImgView.mas_bottom).offset(20).priorityLow();
                    make.bottom.mas_greaterThanOrEqualTo(self);
                }];
            });
        }
    }];
}

- (void)showAnimation{
    [self.superview updateConstraintsIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
        }];
        [self.superview layoutIfNeeded];
    }];
}

- (void)hiddenAnimation{
    [self.superview updateConstraintsIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kCommonScreenWidth);
        }];
        [self.superview layoutIfNeeded];
    }];
}

- (void)setContentView{
    
    self.scrollView = [UIScrollView by_init];
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.showsHorizontalScrollIndicator = NO;
    [self addSubview:_scrollView];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    PDImageView *room_coverImgView = [[PDImageView alloc] init];
    room_coverImgView.contentMode = UIViewContentModeScaleAspectFill;
    room_coverImgView.clipsToBounds = YES;
//    [room_coverImgView uploadImageWithURL:model.live_cover_url placeholder:nil callback:nil];
    [_scrollView addSubview:room_coverImgView];
    _room_coverImgView = room_coverImgView;
    [room_coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(kCommonScreenWidth);
        make.centerX.mas_equalTo(0);
        make.height.mas_equalTo(194);
    }];
    
    // 标题
    UILabel *titleLab = [UILabel by_init];
    [titleLab setBy_font:15];
    titleLab.textColor = kColorRGBValue(0x4c4b4b);
    titleLab.numberOfLines = 0;
//    titleLab.text = model.live_title;
    [_scrollView addSubview:titleLab];
    _titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.top.equalTo(room_coverImgView.mas_bottom).with.offset(15);
        make.right.mas_equalTo(-kCellRightSpace);
        make.height.mas_greaterThanOrEqualTo(15);
    }];
    
    // 头像
    PDImageView *logoImgView = [[PDImageView alloc] init];
    logoImgView.contentMode = UIViewContentModeScaleAspectFit;
    logoImgView.clipsToBounds = YES;
//    [logoImgView uploadMainImageWithURL:model.head_img placeholder:nil imgType:PDImgTypeDrawRound callback:nil];
//    [logoImgView sd_setImageWithURL:[NSURL URLWithString:model.head_img] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
//        logoImgView.image = [image addCornerRadius:11 size:CGSizeMake(22, 22)];
//    }];
    [_scrollView addSubview:logoImgView];
    _logoImgView = logoImgView;
    [logoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.top.equalTo(titleLab.mas_bottom).with.offset(14);
        make.width.height.mas_equalTo(22);
    }];
    
    UIView *logoViewBg = [UIView by_init];
    [logoImgView setBackgroundColor:[UIColor whiteColor]];
    logoImgView.layer.cornerRadius = 13;
    [_scrollView addSubview:logoViewBg];
    [_scrollView insertSubview:logoViewBg aboveSubview:logoImgView];
    [logoViewBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(logoImgView.mas_centerX);
        make.centerY.mas_equalTo(logoViewBg.mas_centerY);
        make.width.height.mas_equalTo(26);
    }];
    
    // 用户名
    UILabel *userNameLab = [UILabel by_init];
    [userNameLab setBy_font:13];
    userNameLab.textColor = kTextColor_135;
//    userNameLab.text = model.nickname;
    [_scrollView addSubview:userNameLab];
    _userNameLab = userNameLab;
    [userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(logoImgView.mas_right).with.offset(5);
        make.centerY.equalTo(logoImgView.mas_centerY).with.offset(0);
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_equalTo(12);
    }];
    
    // 时间图标
    UIImageView *timeImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"common_time_icon"]];
    [_scrollView addSubview:timeImgView];
    [timeImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(userNameLab.mas_right).with.offset(30);
        make.centerY.equalTo(userNameLab.mas_centerY).with.offset(0);
        make.width.mas_equalTo(timeImgView.image.size.width);
        make.height.mas_equalTo(timeImgView.image.size.height);
    }];
    
    // 时间
    UILabel *timeLab = [[UILabel alloc] init];
    timeLab.font = [UIFont systemFontOfSize:13];
    timeLab.textColor = kTextColor_135;
//    timeLab.text = model.begin_time;
    [_scrollView addSubview:timeLab];
    _beginTimeLab = timeLab;
    [timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(timeImgView.mas_right).with.offset(5);
        make.centerY.equalTo(timeImgView.mas_centerY).with.offset(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(18);
    }];
    
    UIView *spaceView = [UIView by_init];
    spaceView.backgroundColor = kBgColor_248;
    [_scrollView addSubview:spaceView];
    [spaceView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kCommonScreenWidth);
        make.centerX.mas_equalTo(0);
        make.top.equalTo(logoImgView.mas_bottom).with.offset(13);
        make.height.mas_equalTo(8);
    }];

    // 直播简介
    UILabel *room_intro = [UILabel by_init];
    [room_intro setBy_font:15];
    room_intro.textColor = kColorRGBValue(0x202020);
    room_intro.text = @"直播介绍";
    [_scrollView addSubview:room_intro];
    [room_intro mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.top.equalTo(spaceView.mas_bottom).with.offset(30);
        make.width.mas_equalTo(62);
        make.height.mas_equalTo(stringGetHeight(room_intro.text, room_intro.font.pointSize));
    }];
    
    // 简介
    YYLabel *room_introLab = [YYLabel by_init];
    room_introLab.font = [UIFont systemFontOfSize:14];
    room_introLab.textColor = kTextColor_111;
    room_introLab.numberOfLines = 0;
//    room_introLab.text = model.live_intro;
    [_scrollView addSubview:room_introLab];
    _room_introLab = room_introLab;
    [room_introLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.right.mas_equalTo(kCellRightSpace);
        make.top.equalTo(room_intro.mas_bottom).with.offset(20);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    
    PDImageView *room_inrtoImgView = [PDImageView by_init];
    [_scrollView addSubview:room_inrtoImgView];
    _room_inrtoImgView = room_inrtoImgView;
    [room_inrtoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kCommonScreenWidth - 30);
        make.centerX.mas_equalTo(0);
        make.top.equalTo(room_introLab.mas_bottom).with.offset(30);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
}
@end
