//
//  BYVideoRoomController.m
//  BibenCommunity
//
//  Created by 随风 on 2018/9/21.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "BYVideoRoomController.h"
#import "BYVideoRoomViewModel.h"
#import "BYCommonLiveModel.h"

@interface BYVideoRoomController ()

@end

@implementation BYVideoRoomController

- (Class)getViewModelClass{
    return [BYVideoRoomViewModel class];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.barMainTitle = [NSString stringWithFormat:@"%@直播间",nullToEmpty(self.introModel.nickname)];
    self.barSubTitle = @"正在直播·20万人次";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
