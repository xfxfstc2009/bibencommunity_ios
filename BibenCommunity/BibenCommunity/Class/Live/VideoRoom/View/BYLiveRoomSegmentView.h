//
//  BYLiveRoomSegmentView.h
//  BY
//
//  Created by 黄亮 on 2018/9/3.
//  Copyright © 2018年 BY. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BYLiveRoomSegmentViewDelegate <NSObject>

/**
 根据index自定义添加View

 @param index index
 @return return value description
 */
- (UIView *)by_segmentViewLoadSubView:(NSInteger)index;

/**
 当前滚动到的Index

 @param index index
 */
- (void)by_segmentViewDidScrollToIndex:(NSInteger)index;

@end

@class BYPPTRoomViewModel;
@interface BYLiveRoomSegmentView : UIView

/** 初始化选择的index (默认1) */
@property (nonatomic ,assign) NSInteger defultSelIndex;
@property (nonatomic ,weak) id<BYLiveRoomSegmentViewDelegate>delegate;


@property (nonatomic ,weak) BYPPTRoomViewModel *viewModel;

/**
 
 */
@property (nonatomic ,copy) void (^didLoadSubView)(NSInteger index,UIView *subView);

- (instancetype)initWithTitles:(NSString *)titles, ... NS_REQUIRES_NIL_TERMINATION;

@end
