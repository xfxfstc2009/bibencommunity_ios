//
//  BYVideoPlaySubController.h
//  BibenCommunity
//
//  Created by 随风 on 2018/9/21.
//  Copyright © 2018年 币本. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BYCommonPlayerView.h"

@interface BYVideoPlaySubController : UIViewController

@property (nonatomic ,copy) NSString *videourl;
@property (nonatomic ,weak) UIViewController *superController;

/** 播放器 */
@property (nonatomic ,strong) BYCommonPlayerView *playerView;

@end
