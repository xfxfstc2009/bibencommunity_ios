//
//  BYCreatLiveRoomTitleCell.m
//  BY
//
//  Created by 黄亮 on 2018/8/30.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCreatLiveRoomTitleCell.h"
#import "BYCreatLiveRoomCellModel.h"

@implementation BYCreatLiveRoomTitleCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = kBgColor_248;
        self.textLabel.font = [UIFont systemFontOfSize:13];
        self.textLabel.textColor = kColorRGBValue(0x4c4c4c);
        self.textLabel.frame = CGRectMake(15, 0, kCommonScreenWidth, 14);
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYCreatLiveRoomCellModel *model = object[indexPath.row];
    self.textLabel.text = model.title;
}

@end
