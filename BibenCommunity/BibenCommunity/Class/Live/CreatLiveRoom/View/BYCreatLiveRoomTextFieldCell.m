//
//  BYCreatLiveRoomTextFieldCell.m
//  BY
//
//  Created by 黄亮 on 2018/8/30.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCreatLiveRoomTextFieldCell.h"
#import "BYCreatLiveRoomCellModel.h"

@interface BYCreatLiveRoomTextFieldCell()<UITextFieldDelegate>

@property (nonatomic ,strong) BYCreatLiveRoomCellModel *model;

@end

@implementation BYCreatLiveRoomTextFieldCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.textField.delegate = self;
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    self.indexPath = indexPath;
    if (indexPath.row == 1) {
        self.textField.keyboardType = UIKeyboardTypeDefault;
    }
    else
    {
        self.textField.keyboardType = UIKeyboardTypeASCIICapable;
    }
    BYCreatLiveRoomCellModel *model = object[indexPath.row];
    [self setPlaceholder:model.placeholder];
    [self setTextFieldText:model.title];

}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *rangString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self sendActionName:@"textField_change" param:@{@"text":nullToEmpty(rangString)} indexPath:self.indexPath];
    return YES;
}


@end
