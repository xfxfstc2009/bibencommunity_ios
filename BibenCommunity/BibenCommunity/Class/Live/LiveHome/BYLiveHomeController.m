//
//  BYLiveHomeController.m
//  BY
//
//  Created by 黄亮 on 2018/7/25.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYLiveHomeController.h"
#import "BYLiveHomeViewModel.h"
#import "BYCreatLiveRoomController.h"
#import "BYLiveDefine.h"
#import "BYLiveHomeRequest.h"
#import "BYIMManager.h"

@interface BYLiveHomeController ()

@end

@implementation BYLiveHomeController

- (Class)getViewModelClass{
    return [BYLiveHomeViewModel class];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.barMainTitle = self.tabBarController.selectedIndex == 0 ? @"首页" : @"分类";
    if (self.tabBarController.selectedIndex == 0) {
        [self loginSuccessedNotif:^(BOOL isSuccessed) {
            if (!isSuccessed) return ;
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:kiSHaveLiveRoomKey] isEqual:@YES]) return;
            // 校验直播间
            [self verifyLiveRoom];
        }];
    }    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];

//    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kiSHaveLiveRoom([AccountModel sharedAccountModel].account_id)] isEqual:@NO]) {
//        self.navigationItem.rightBarButtonItem = nil;
//    }
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

// 设置直播间状态
- (void)verifyLiveRoom{
    [[BYLiveHomeRequest alloc] loadReuqestIsHaveLiveRoomSuccessBlock:^(id object) {
        [[NSUserDefaults standardUserDefaults] setObject:@([object boolValue]) forKey:kiSHaveLiveRoomKey];
    } faileBlock:^(NSError *error) {

    }];
}

- (void)rightBarButtonItemAction{
    BYCreatLiveRoomController *creatLiveRoomController = [[BYCreatLiveRoomController alloc] init];
    [self authorizePush:creatLiveRoomController animation:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
