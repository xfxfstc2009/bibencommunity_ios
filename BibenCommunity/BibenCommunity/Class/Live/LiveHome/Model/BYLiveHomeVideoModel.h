//
//  BYLiveHomeVideoModel.h
//  BY
//
//  Created by 黄亮 on 2018/7/26.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYCommonLiveModel.h"
#import "BYLiveDefine.h"r

@interface BYLiveHomeVideoModel : BYCommonLiveModel

@property (nonatomic ,copy) NSString *video_url;

@property (nonatomic ,assign) BOOL appointStatus;

- (NSArray *)getRespondData:(NSDictionary *)data type:(BY_LIVE_HOME_LIST_TYPE)type;

@end
