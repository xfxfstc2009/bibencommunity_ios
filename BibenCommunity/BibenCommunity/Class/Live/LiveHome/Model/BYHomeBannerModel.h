//
//  BYHomeBannerModel.h
//  BibenCommunity
//
//  Created by 随风 on 2018/9/28.
//  Copyright © 2018 币本. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BYHomeBannerModel : NSObject

/** 图片地址 */
@property (nonatomic ,copy) NSString *content;
@property (nonatomic ,copy) NSString *banner_id;
@property (nonatomic ,copy) NSString *sort_id;

+ (NSArray *)getBannerData:(NSArray *)respondArr;

@end
