

//
//  BYHomeBannerModel.m
//  BibenCommunity
//
//  Created by 随风 on 2018/9/28.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYHomeBannerModel.h"

@implementation BYHomeBannerModel

+ (NSArray *)getBannerData:(NSArray *)respondArr{
    __block NSMutableArray *data = [NSMutableArray array];
    [respondArr enumerateObjectsUsingBlock:^(NSDictionary *dic, NSUInteger idx, BOOL * _Nonnull stop) {
        BYHomeBannerModel *model = [BYHomeBannerModel mj_objectWithKeyValues:dic];
        [data addObject:model];
    }];
    return data;
}

@end
