//
//  BYLiveHomeVideoCell.m
//  BY
//
//  Created by 黄亮 on 2018/7/26.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYLiveHomeVideoCell.h"
#import "BYLiveHomeVideoModel.h"

@interface BYLiveHomeVideoCell()

/** 封面图 */
@property (nonatomic ,strong) PDImageView *coverImgView;
/** 头像 */
@property (nonatomic ,strong) PDImageView *userlogoImgView;
/** 昵称 */
@property (nonatomic ,strong) UILabel *userNameLab;
/** 组织 */
@property (nonatomic ,strong) UILabel *organLab;
/** 标题 */
@property (nonatomic ,strong) UILabel *titleLab;

@property (nonatomic ,strong) UIView *timerView;
/** 时间 */
@property (nonatomic ,strong) UILabel *timeLab;

@property (nonatomic ,strong) UIView *watchNumView;
/** 观看人数 */
@property (nonatomic ,strong) UILabel *watchNumLab;
/** 直播状态 */
@property (nonatomic ,strong) UIView *liveStatus;
/** 直播状态 */
@property (nonatomic ,strong) UILabel *liveStatusLab;
/** 预约按钮 */
@property (nonatomic ,strong) UIButton *appointBtn;

@end

@implementation BYLiveHomeVideoCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = object[indexPath.section];
    self.indexPath = indexPath;
    BYLiveHomeVideoModel *model = dic.allValues[0][indexPath.row];
    @weakify(self);
    [_coverImgView uploadImageWithURL:model.live_cover_url placeholder:nil callback:^(UIImage *image) {
        @strongify(self);
        self.coverImgView.image = [image addCornerRadius:5 size:CGSizeMake(132, 97)];
    }];
    _titleLab.attributedText = [[NSAttributedString alloc] initWithString:nullToEmpty(model.live_title) attributes:[self getTitleAttributes]];
    _timeLab.text = model.begin_time;
    [_userlogoImgView uploadMainImageWithURL:model.head_img placeholder:nil imgType:PDImgTypeDrawRound callback:nil];
    _userNameLab.text = model.nickname;
    [self reloadLiveStatus:model.status];
    _organLab.text = model.organ_type == BY_LIVE_ORGAN_TYPE_PERSON ? @"个人" : (model.organ_type == BY_LIVE_ORGAN_TYPE_LECTURER ? @"讲师" : @"机构");
    _appointBtn.selected = model.appointStatus;
    
    _appointBtn.hidden = model.status == BY_LIVE_STATUS_SOON ? NO : YES;
    _watchNumView.hidden = model.status == BY_LIVE_STATUS_LIVING ? NO : YES;
}

- (void)appointBtnAction:(UIButton *)sender{
    [self sendActionName:@"appointAction" param:@{@"btn":sender} indexPath:self.indexPath];
}

- (void)reloadLiveStatus:(BY_LIVE_STATUS)status{
    switch (status) {
        case BY_LIVE_STATUS_LIVING:
            _liveStatus.backgroundColor = kColorRGBValue(0xed4a45);
            _liveStatusLab.text = @"正在直播";
            break;
        case BY_LIVE_STATUS_SOON:
            _liveStatus.backgroundColor = kColorRGBValue(0x00a43e);
            _liveStatusLab.text = @"直播预告";
            break;
        case BY_LIVE_STATUS_END:
            _liveStatus.backgroundColor = kColorRGBValue(0xed9c45);
            _liveStatusLab.text = @"直播回放";
            break;
        default:
            break;
    }
}

- (NSDictionary *)getTitleAttributes{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    //首行缩进
    paragraphStyle.firstLineHeadIndent = 33;
    //富文本样式
    NSDictionary *attributeDic = @{
                                   NSFontAttributeName : [UIFont systemFontOfSize:13],
                                   NSParagraphStyleAttributeName : paragraphStyle,
                                   NSForegroundColorAttributeName : kTextColor_71
                                   };
    return attributeDic;
}

- (void)setContentView{
    // 封面图
    PDImageView *coverImgView = [[PDImageView alloc] init];
    [self.contentView addSubview:coverImgView];
    _coverImgView = coverImgView;
    [coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(132);
        make.height.mas_equalTo(97);
    }];
    
    UIView *maskView= [UIView by_init];
    [maskView setBackgroundColor:kColorRGB(47, 47, 47, 0.6)];
    [maskView layerCornerRadius:5 byRoundingCorners:UIRectCornerTopLeft size:CGSizeMake(60, 23)];
    [self.contentView addSubview:maskView];
    [maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(23);
    }];
    
    // 直播状态
    UIView *liveStatus = [UIView by_init];
    liveStatus.layer.cornerRadius = 2;
    [maskView addSubview:liveStatus];
    _liveStatus = liveStatus;
    [liveStatus mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(4);
        make.left.mas_equalTo(5);
        make.centerY.mas_equalTo(0);
    }];
    
    // 直播状态文案
    UILabel *liveStatusLab = [UILabel by_init];
    liveStatusLab.text = @"直播回放";
    [liveStatusLab setBy_font:11];
    [liveStatusLab setTextColor:[UIColor whiteColor]];
    [maskView addSubview:liveStatusLab];
    _liveStatusLab = liveStatusLab;
    [liveStatusLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(liveStatus.mas_right).with.offset(3);
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(stringGetWidth(liveStatusLab.text, liveStatusLab.font.pointSize));
        make.height.mas_equalTo(stringGetHeight(liveStatusLab.text, liveStatusLab.font.pointSize));
    }];
    
    // 组织
    UILabel *organLab = [UILabel by_init];
    [organLab setBy_font:10];
    organLab.textColor = [UIColor whiteColor];
    organLab.textAlignment = NSTextAlignmentCenter;
    organLab.backgroundColor = kTextColor_68;
    [organLab layerCornerRadius:3 size:CGSizeMake(30, 16)];
    [self.contentView addSubview:organLab];
    _organLab = organLab;
    [organLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(coverImgView.mas_right).with.offset(10);
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(30);
        make.height.mas_equalTo(16);
    }];
    
    // 标题
    UILabel *titleLab = [[UILabel alloc] init];
    titleLab.font = [UIFont systemFontOfSize:14];
    titleLab.textColor = kColorRGBValue(0x2c2c2c);
    titleLab.numberOfLines = 2;
    [self.contentView addSubview:titleLab];
    _titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(coverImgView.mas_right).with.offset(10);
        make.top.mas_equalTo(0);
        make.right.mas_equalTo(-15);
        make.height.mas_greaterThanOrEqualTo(14);
    }];
    
    UIView *timerView = [self getTimerView];
    _timerView = timerView;
    [self.contentView addSubview:timerView];
    [timerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(coverImgView.mas_right).with.offset(10);
        make.top.equalTo(titleLab.mas_bottom).with.offset(10);
        make.right.mas_equalTo(-50);
        make.height.mas_equalTo(20);
    }];
    
    // 头像
    PDImageView *userlogoImgView = [PDImageView by_init];
    [self.contentView addSubview:userlogoImgView];
    _userlogoImgView = userlogoImgView;
    [userlogoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(22);
        make.bottom.equalTo(coverImgView.mas_bottom).with.offset(-2);
        make.left.equalTo(coverImgView.mas_right).with.offset(10);
    }];
    
    UIImageView *userlogoImgBgView = [UIImageView by_init];
    [userlogoImgBgView by_setImageName:@"livehome_userlogo_bg"];
    [self.contentView addSubview:userlogoImgBgView];
    [self.contentView insertSubview:userlogoImgBgView belowSubview:userlogoImgView];
    [userlogoImgBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(userlogoImgBgView.image.size.width);
        make.height.mas_equalTo(userlogoImgBgView.image.size.height);
        make.centerX.mas_equalTo(userlogoImgView.mas_centerX);
        make.centerY.mas_equalTo(userlogoImgView.mas_centerY);
    }];
    
    // 昵称
    UILabel *userNameLab = [UILabel by_init];
    [userNameLab setBy_font:12];
    userNameLab.textColor = kTextColor_77;
    _userNameLab = userNameLab;
    [self.contentView addSubview:userNameLab];
    [userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(userlogoImgView.mas_right).with.offset(7);
        make.centerY.mas_equalTo(userlogoImgView.mas_centerY);
        make.right.mas_equalTo(-70);
        make.height.mas_equalTo(14);
    }];
    
    // 直播观看人数view
    UIView *watchNumView = [self getWatchNumView];
    _watchNumView = watchNumView;
    watchNumView.layer.opacity = 0.0;
    [self.contentView addSubview:watchNumView];
    [watchNumView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(13);
        make.centerY.mas_equalTo(userNameLab.mas_centerY);
        make.width.mas_equalTo(60);
    }];
    
    // 预约按钮
    UIButton *appointBtn = [UIButton by_buttonWithCustomType];
    [appointBtn setBy_imageName:@"livehome_btn_appoint" forState:UIControlStateNormal];
    [appointBtn setBy_imageName:@"livehome_btn_isappoint" forState:UIControlStateSelected];
    [appointBtn addTarget:self action:@selector(appointBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:appointBtn];
    _appointBtn = appointBtn;
    [appointBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(52);
        make.height.mas_equalTo(23);
        make.right.mas_equalTo(-kCellRightSpace);
        make.bottom.mas_equalTo(coverImgView.mas_bottom);
    }];
}

- (UIView *)getTimerView{
    UIView *timerView = [UIView by_init];
    // 时间图标
    UIImageView *timeImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"common_time_icon"]];
    [timerView addSubview:timeImgView];
    [timeImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(timeImgView.image.size.width);
        make.height.mas_equalTo(timeImgView.image.size.height);
    }];
    
    // 时间
    UILabel *timeLab = [[UILabel alloc] init];
    timeLab.font = [UIFont systemFontOfSize:12];
    timeLab.textColor = kTextColor_154;
    [timerView addSubview:timeLab];
    _timeLab = timeLab;
    [timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(timeImgView.mas_right).with.offset(5);
        make.centerY.equalTo(timeImgView.mas_centerY).with.offset(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(14);
    }];
    return timerView;
}

- (UIView *)getWatchNumView{
    UIView *watchNumView = [UIView by_init];
    UIImageView *watchNumImgView = [UIImageView by_init];
    [watchNumImgView by_setImageName:@"livehome_watchnum"];
    [watchNumView addSubview:watchNumImgView];
    [watchNumImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(0);
        make.width.mas_equalTo(watchNumImgView.image.size.width);
        make.height.mas_equalTo(watchNumImgView.image.size.height);
    }];
    
    // 直播人数
    UILabel *watchNumLab = [UILabel by_init];
    [watchNumLab setBy_font:12];
    watchNumLab.textColor = kTextColor_154;
    [watchNumView addSubview:watchNumLab];
    [watchNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(watchNumImgView.mas_right).with.offset(5);
        make.centerY.mas_equalTo(watchNumLab.mas_centerY);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(11);
    }];
    return watchNumView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
