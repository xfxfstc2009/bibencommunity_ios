//
//  BYLiveHomeSortHeaderView.m
//  BibenCommunity
//
//  Created by 随风 on 2018/9/21.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "BYLiveHomeSortHeaderView.h"

@interface BYLiveHomeSortHeaderView()

@property (nonatomic ,strong) UILabel *sortLab;
@property (nonatomic ,assign) BY_SORT_TYPE sort_type;

@end

@implementation BYLiveHomeSortHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _sort_type = BY_SORT_TYPE_TIME;
        [self setContentView];
    }
    return self;
}

- (void)setContentView{
    UIImageView *sortImgView = [UIImageView by_init];
    [sortImgView by_setImageName:@"livehome_sort"];
    [self addSubview:sortImgView];
    [sortImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.top.mas_equalTo(3);
        make.width.mas_equalTo(sortImgView.image.size.width);
        make.height.mas_equalTo(sortImgView.image.size.height);
    }];
    
    UILabel *sortLab = [UILabel by_init];
    [sortLab setBy_font:13];
    sortLab.textColor = kTextColor_139;
    sortLab.text = @"最新";
    [self addSubview:sortLab];
    _sortLab = sortLab;
    [sortLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(28);
        make.height.mas_equalTo(15);
        make.left.equalTo(sortImgView.mas_right).with.offset(10);
        make.centerY.mas_equalTo(sortImgView.mas_centerY);
    }];
    
    UIButton *button = [UIButton by_buttonWithCustomType];
    [button addTarget:self action:@selector(buttonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.mas_equalTo(0);
        make.right.equalTo(sortLab.mas_right).with.offset(20);
    }];
}

- (void)buttonAction{
    if (_didSelectSortBtnHandle) {
        _didSelectSortBtnHandle();
    }
}

- (void)reloadSortStatus{
    switch (_sort_type) {
        case BY_SORT_TYPE_HOT:
            _sortLab.text = @"最新";
            _sort_type = BY_SORT_TYPE_TIME;
            break;
        default:
            _sortLab.text = @"最热";
            _sort_type = BY_SORT_TYPE_HOT;
            break;
    }
}

@end
