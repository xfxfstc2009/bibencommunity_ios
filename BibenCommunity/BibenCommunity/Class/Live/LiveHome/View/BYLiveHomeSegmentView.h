//
//  BYLiveHomeSegmentView.h
//  BibenCommunity
//
//  Created by 随风 on 2018/9/18.
//  Copyright © 2018年 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BYLiveHomeSegmentViewDelegate <NSObject>

/** 根据index自定义添加View */
- (UIView *)by_segmentViewLoadSubView:(NSInteger)index;

/** 当前滚动到的Index */
- (void)by_segmentViewDidScrollToIndex:(NSInteger)index;

@end

@class BYLiveHomeViewModel;
@interface BYLiveHomeSegmentView : UIView

/** 初始化选择的index (默认0) */
@property (nonatomic ,assign) NSInteger defultSelIndex;
@property (nonatomic ,weak) id<BYLiveHomeSegmentViewDelegate>delegate;

@property (nonatomic ,weak) BYLiveHomeViewModel *viewModel;

/** 点击分类回调 */
@property (nonatomic ,copy) void (^didLoadSubView)(NSInteger index,UIView *subView);

- (instancetype)initWithTitles:(NSString *)titles, ... NS_REQUIRES_NIL_TERMINATION;

@end
