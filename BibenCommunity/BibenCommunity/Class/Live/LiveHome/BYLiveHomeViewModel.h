//
//  BYLiveHomeViewModel.h
//  BY
//
//  Created by 黄亮 on 2018/7/26.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYCommonViewModel.h"

@interface BYLiveHomeViewModel : BYCommonViewModel


#pragma mark - ares
+(void)liveLoginManager:(NSString *)userId pwd:(NSString *)sig;                // 直播登录


@end
