//
//  BYLiveHomeViewModel.m
//  BY
//
//  Created by 黄亮 on 2018/7/26.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYLiveHomeViewModel.h"
#import "BYLiveHomeHeaderView.h"
#import "BYLiveHomeSegmentView.h"
#import "BYLiveHomeSortHeaderView.h"
#import "BYLiveHomeModel.h"
#import "BYLiveHomeRequest.h"
#import "BYCommonLiveModel.h"
#import "NSDate+BYExtension.h"
#import "BYIMManager.h"

#import "BYVideoRoomController.h"
#import "BYILiveRoomController.h"
#import "LivePPTRootViewController.h"

#define S_DM ((BYLiveHomeModel *)self.dataModel)
@interface BYLiveHomeViewModel ()<BYCommonTableViewDelegate,BYLiveHomeSegmentViewDelegate>
{
    BY_SORT_TYPE _sort_type;
}
// 记录各个类型加载的页码
@property (nonatomic ,assign) NSInteger hotPageNum;
@property (nonatomic ,assign) NSInteger audioPageNum;
@property (nonatomic ,assign) NSInteger iLivePageNum;
@property (nonatomic ,assign) NSInteger followPageNum;

@property (nonatomic ,assign) NSInteger currentIndex;

/** 轮播 */
@property (nonatomic ,strong) BYLiveHomeHeaderView *headerView;
/** 分类器 */
@property (nonatomic ,strong) BYLiveHomeSegmentView *segmentView;
/** 音频排序按钮 */
@property (nonatomic ,strong) BYLiveHomeSortHeaderView *audioHeaderView;
/** 视频排序按钮 */
@property (nonatomic ,strong) BYLiveHomeSortHeaderView *videoHeaderView;
@end

static NSInteger baseTag = 0x731;
@implementation BYLiveHomeViewModel

#pragma mark - setUI

- (Class)getDataModelClass{
    return [BYLiveHomeModel class];
}

- (void)viewWillAppear{
    [_headerView startTimer];
}

- (void)viewWillDisappear{
    [_headerView destoryTimer];
}

- (void)setContentView{
    
    _hotPageNum    = 0;
    _audioPageNum  = 0;
    _iLivePageNum  = 0;
    _followPageNum = 0;
    
    [S_V_VIEW addSubview:self.headerView];
    [_headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(0);
        make.height.mas_equalTo(195);
    }];
    
    [S_V_VIEW addSubview:self.segmentView];
    [_segmentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(195, 0, 0, 0));
    }];

//    [self sendRequestToGetInfo];
//    [self loadRequestWithIndex:0];
    [self loadRequestGetBanner];
    
}

-(void)sendRequestToGetInfo{
    // @"13696686633" @"123456"
    // @"13688885555" @"123456"
    [[BYIMManager sharedInstance] loginILive:nil fail:nil];
}

#pragma mark - customMethod
// 数据刷新
- (void)refreshData{
    switch (self.currentIndex) {
        case 0:
            self.hotPageNum     = 0;
            break;
        case 1:
            self.audioPageNum   = 0;
            break;
        case 2:
            self.iLivePageNum   = 0;
            break;
        default:
            self.followPageNum  = 0;
            break;
    }
    if (self.currentIndex == 0) {
        [self loadRequestGetBanner];
    }
    [self loadRequestWithIndex:self.currentIndex isCurSort:NO];
}

// 数据加载更多
- (void)loadMoreData{
//    [_tableView endRefreshing];
    [self loadRequestWithIndex:self.currentIndex isCurSort:NO];
}

- (void)headerViewTitleSelectIndex:(NSInteger)index{
    
}

#pragma mark - requestMethod
- (void)loadRequestWithIndex:(NSInteger)index pageNum:(NSInteger)pageNum sortType:(BY_SORT_TYPE)sortType isCurSort:(BOOL)isCurSort{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestLiveHome:index pageNum:pageNum sortType:sortType successBlock:^(id object) {
        @strongify(self);
        BYCommonTableView *tableView = (BYCommonTableView *)[self.segmentView viewWithTag:baseTag + index];
        switch (index) {
            case 0:
                self.hotPageNum++;
                break;
            case 1:
                self.audioPageNum++;
                break;
            case 2:
                self.iLivePageNum++;
                break;
            default:
                self.followPageNum++;
                break;
        }
        tableView.tableData = object;
        [tableView endRefreshing];
        if (index == 1 && isCurSort) {
            [self.audioHeaderView reloadSortStatus];
        }
        else if (index == 2 && isCurSort){
            [self.videoHeaderView reloadSortStatus];
        }
    } faileBlock:^(NSError *error) {
        @strongify(self);
        BYCommonTableView *tableView = (BYCommonTableView *)[self.segmentView viewWithTag:baseTag + index];
        [tableView endRefreshing];
    }];
}

- (void)loadRequestWithIndex:(NSInteger)index isCurSort:(BOOL)isCurSort{
    NSInteger pageNum = 0;
    BY_SORT_TYPE sortType = 0;
    switch (index) {
        case 0:
            pageNum = _hotPageNum;
            break;
        case 1:
            pageNum = _audioPageNum;
            if (isCurSort) { // 是否切换排序方式
                sortType = _audioHeaderView.sort_type == BY_SORT_TYPE_HOT ? BY_SORT_TYPE_TIME : BY_SORT_TYPE_HOT;
                break;
            }
            sortType = _audioHeaderView.sort_type;
            break;
        case 2:
            pageNum = _iLivePageNum;
            if (isCurSort) { // 是否切换排序方式
                sortType = _videoHeaderView.sort_type == BY_SORT_TYPE_HOT ? BY_SORT_TYPE_TIME : BY_SORT_TYPE_HOT;
                break;
            }
            sortType = _videoHeaderView.sort_type;
            break;
        default:
            pageNum = _followPageNum;
            break;
    }
    [self loadRequestWithIndex:index pageNum:pageNum sortType:sortType isCurSort:isCurSort];
}

- (void)loadRequestAttentionLive{
    
}

- (void)loadRequestGetBanner{
    [[BYLiveHomeRequest alloc] loadRequestGetHomeBannerSuccessBlock:^(id object) {
        self.headerView.advertData = object;
        [self.headerView reloadData];
    } faileBlock:^(NSError *error) {
        PDLog(@"%@",error);
    }];
}

#pragma mark - BYLiveHomeSegmentViewDelegate
- (UIView *)by_segmentViewLoadSubView:(NSInteger)index{
    BYCommonTableView *tableView = [[BYCommonTableView alloc] init];
    tableView.group_delegate = self;
    tableView.tag = baseTag + index;
    [tableView addHeaderRefreshTarget:self action:@selector(refreshData)];
    if (tableView != 0) {
        [tableView addFooterRefreshTarget:self action:@selector(loadMoreData)];
    }
    // 添加排序按钮
    if (index == 1) {
        tableView.tableHeaderView = self.audioHeaderView;
    }
    else if (index == 2){
        tableView.tableHeaderView = self.videoHeaderView;
    }
    return tableView;
}

- (void)by_segmentViewDidScrollToIndex:(NSInteger)index{
    _currentIndex = index;
    BYCommonTableView *tableView = (BYCommonTableView *)[S_V_VIEW viewWithTag:baseTag + index];
    if (!tableView.tableData.count) { // 当前分类数据为空时发起请求
        [self loadRequestWithIndex:index isCurSort:NO];
    }
}

#pragma mark - BYCommonTableViewDelegate
- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    NSDictionary *dic = tableView.tableData[indexPath.section];
    BYLiveHomeVideoModel *model = dic.allValues[0][indexPath.row];
    [[TIMGroupManager sharedInstance] GetGroupInfo:@[nullToEmpty(model.room_id)] succ:^(NSArray *arr) {
        NSLog(@"%@",arr);
    } fail:^(int code, NSString *msg) {
        NSLog(@"%i-%@",code,msg);
    }];
        switch (model.live_type) {
            case BY_NEWLIVE_TYPE_ILIVE:
            {
                [BYCommonLiveModel shareManager].begin_time = [NSDate by_stringFromDate:[NSDate by_date] dateformatter:K_D_F];
                BYLiveConfig *config = [[BYLiveConfig alloc] init];
                config.isHost = NO;
                switch (model.status) {
                    case BY_LIVE_STATUS_END:
                    {
                        config.isVOD = YES;
                        config.video_url = model.video_url;
                    }
                        break;
                    case BY_LIVE_STATUS_WAITING:
                        return;
                        break;
                    default:
                        break;
                }
                config.roomId = model.room_id;
                BYILiveRoomController *ilivewRoomController = [[BYILiveRoomController alloc] initWithConfig:config];
                ilivewRoomController.model = model;
                [S_VC authorizePush:ilivewRoomController animation:YES];
            }
                break;
            case BY_NEWLIVE_TYPE_UPLOAD_VIDEO:
            {
                BYVideoRoomController *videoRoomController = [[BYVideoRoomController alloc] init];
                videoRoomController.introModel = model;
                [S_VC authorizePush:videoRoomController animation:YES];
            }
                break;
            case BY_NEWLIVE_TYPE_AUDIO_PPT:
            case BY_NEWLIVE_TYPE_AUDIO_LECTURE:
            {
                LivePPTRootViewController *pptRootViewController = [[LivePPTRootViewController alloc] init];
                pptRootViewController.transferRoomId = model.live_record_id;
                [S_VC authorizePush:pptRootViewController animation:YES];
            }
                break;
            default:
                break;
        }
}

- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = tableView.tableData[indexPath.section];
    BYLiveHomeVideoModel *model = dic.allValues[0][indexPath.row];
    if ([actionName isEqualToString:@"appointAction"]) { // 关注
        [self loadRequestAttentionLive];
    }
}

#pragma make - initMethod
- (BYLiveHomeHeaderView *)headerView{
    if (!_headerView) {
        _headerView = [[BYLiveHomeHeaderView alloc] initWithFrame:CGRectMake(0, 0, kCommonScreenWidth, 195)];
        @weakify(self);
        _headerView.didSelectHeaderTitle = ^(NSInteger index) {
            @strongify(self);
            [self headerViewTitleSelectIndex:index];
        };
    }
    return _headerView;
}

- (BYLiveHomeSegmentView *)segmentView{
    if (!_segmentView) {
        _segmentView = [[BYLiveHomeSegmentView alloc] initWithTitles:@"热门推荐",@"音频直播",@"视频直播",@"我关注的", nil];
        _segmentView.delegate = self;
    }
    return _segmentView;
}

- (BYLiveHomeSortHeaderView *)audioHeaderView{
    if (!_audioHeaderView) {
        _audioHeaderView = [[BYLiveHomeSortHeaderView alloc] init];
        _audioHeaderView.frame = CGRectMake(0, 0, kCommonScreenWidth, 36);
        
        @weakify(self);
        _audioHeaderView.didSelectSortBtnHandle = ^{
            @strongify(self);
            [self loadRequestWithIndex:self.currentIndex isCurSort:YES];
        };
    }
    return _audioHeaderView;
}

- (BYLiveHomeSortHeaderView *)videoHeaderView{
    if (!_videoHeaderView) {
        _videoHeaderView = [[BYLiveHomeSortHeaderView alloc] init];
        _videoHeaderView.frame = CGRectMake(0, 0, kCommonScreenWidth, 36);
        @weakify(self);
        _videoHeaderView.didSelectSortBtnHandle = ^{
            @strongify(self);
            [self loadRequestWithIndex:self.currentIndex isCurSort:YES];
        };
    }
    return _videoHeaderView;
}



#pragma mark - ares
+(void)liveLoginManager:(NSString *)userId pwd:(NSString *)sig{                // 直播登录
    __weak typeof(self)weakSelf = self;
    [[ILiveLoginManager getInstance] iLiveLogin:userId sig:sig succ:^{
        if (!weakSelf){
            return ;
        }
        [StatusBarManager statusBarHidenWithText:@"腾讯云 登录成功"];
    } failed:^(NSString *module, int errId, NSString *errMsg) {
        [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"腾讯云 登录失败%@",errMsg]];
    }];
}

@end
