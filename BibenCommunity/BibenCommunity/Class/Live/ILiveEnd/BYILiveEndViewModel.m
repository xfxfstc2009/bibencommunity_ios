//
//  BYILiveEndViewModel.m
//  BY
//
//  Created by 黄亮 on 2018/9/15.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYILiveEndViewModel.h"
#import "BYILiveEndController.h"
#import "BYCommonLiveModel.h"
#import "NSDate+BYExtension.h"
#import <ImSDK/TIMGroupManager.h>

#define S_END_VC ((BYILiveEndController *)S_VC)
@interface BYILiveEndViewModel()

@property (nonatomic ,strong) UILabel *watchNumLab;

@end

@implementation BYILiveEndViewModel

- (void)backBtnAction{
    [S_V_NC popToRootViewControllerAnimated:YES];
}

- (void)setContentView{
    
    S_VC.navigationController.navigationBar.hidden = YES;
    S_VC.popGestureRecognizerEnale = NO;
    
    UIImageView *roomBgImgView = [[UIImageView alloc] init];
    [roomBgImgView by_setImageName:@"room_bg"];
    [S_V_VIEW addSubview:roomBgImgView];
    [roomBgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    UIView *maskView = [[UIView alloc] init];
    maskView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.7];
    [S_V_VIEW addSubview:maskView];
    [maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    BYCommonLiveModel *model = S_END_VC.model;
    PDImageView *logoImgView = [[PDImageView alloc] init];
    logoImgView.clipsToBounds = YES;
    [logoImgView uploadMainImageWithURL:model.head_img placeholder:nil imgType:PDImgTypeDrawRound callback:nil];
    [S_V_VIEW addSubview:logoImgView];
    [logoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(66);
        make.top.mas_equalTo(26 + kNavigationHeight);
        make.centerX.mas_equalTo(0);
    }];
    
    UIImageView *logoBg = [UIImageView by_init];
    [logoBg by_setImageName:@"ilive_end_logo_bg"];
    [S_V_VIEW addSubview:logoBg];
    [S_V_VIEW insertSubview:logoBg belowSubview:logoImgView];
    [logoBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(logoImgView.mas_centerX);
        make.centerY.mas_equalTo(logoImgView.mas_centerY);
        make.width.mas_equalTo(logoBg.image.size.width);
        make.height.mas_equalTo(logoBg.image.size.height);
    }];
    
    
    UILabel *userNameLab = [UILabel by_init];
    [userNameLab setBy_font:17];
    userNameLab.textColor = [UIColor whiteColor];
    userNameLab.text = model.nickname;
    [S_V_VIEW addSubview:userNameLab];
    [userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(model.nickname, 17));
        make.height.mas_equalTo(stringGetHeight(model.nickname, 17));
        make.centerX.mas_equalTo(0);
        make.top.equalTo(logoImgView.mas_bottom).with.offset(25);
    }];
    
    UILabel *idLab = [UILabel by_init];
    [idLab setBy_font:11];
    idLab.textColor = [UIColor colorWithWhite:1 alpha:0.7];
    idLab.text = [NSString stringWithFormat:@"ID:%@",model.user_id];
    [S_V_VIEW addSubview:idLab];
    [idLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(idLab.text, 11));
        make.height.mas_equalTo(stringGetHeight(idLab.text, 11));
        make.centerX.mas_equalTo(0);
        make.top.equalTo(userNameLab.mas_bottom).with.offset(8);
    }];
    
    UILabel *liveEndLab = [UILabel by_init];
    [liveEndLab setBy_font:23];
    liveEndLab.textColor = [UIColor whiteColor];
    liveEndLab.text = @"直播结束";
    [S_V_VIEW addSubview:liveEndLab];
    [liveEndLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(liveEndLab.text, 23));
        make.height.mas_equalTo(stringGetHeight(liveEndLab.text, 23));
        make.centerX.mas_equalTo(0);
        make.top.equalTo(idLab.mas_bottom).with.offset(55);
    }];
    
    NSDate *beginDate = [NSDate by_dateFromString:model.begin_time dateformatter:K_D_F];
    NSInteger timeDif = [NSDate by_startTime:beginDate endTime:[NSDate by_date]];
    NSInteger days = (int)(timeDif/(3600*24));
    NSInteger hours = (int)((timeDif-days*24*3600)/3600);
    NSInteger minute = (int)(timeDif-days*24*3600-hours*3600)/60;
    NSInteger second = timeDif - days*24*3600 - hours*3600 - minute*60;
    UILabel *liveTimeLab = [UILabel by_init];
    [liveTimeLab setBy_font:13];
    liveTimeLab.textColor = [UIColor colorWithWhite:1 alpha:0.9];
    liveTimeLab.text = [NSString stringWithFormat:@"直播时长 ：%02ld : %02ld : %02ld", hours, minute, second];;
    [S_V_VIEW addSubview:liveTimeLab];
    [liveTimeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(liveTimeLab.text, 13));
        make.height.mas_equalTo(stringGetHeight(liveTimeLab.text, 13));
        make.centerX.mas_equalTo(0);
        make.top.equalTo(liveEndLab.mas_bottom).with.offset(25);
    }];
    
    UILabel *watchNumLab = [UILabel by_init];
    [watchNumLab setBy_font:13];
    watchNumLab.textColor = [UIColor colorWithWhite:1 alpha:0.9];
    watchNumLab.text = [NSString stringWithFormat:@"观看人数 ：%d人次",(int)model.watchNum];
    [S_V_VIEW addSubview:watchNumLab];
    [watchNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(liveTimeLab.mas_bottom).with.offset(8);
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(stringGetWidth(watchNumLab.text, 13));
        make.height.mas_equalTo(stringGetHeight(watchNumLab.text, 13));
    }];
    
    UIButton *backBtn = [UIButton  by_buttonWithCustomType];
    [backBtn setBackgroundColor:kTextColor_238];
    [backBtn setBy_attributedTitle:@{@"title":@"返回",NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [backBtn layerCornerRadius:10 size:CGSizeMake(150, 40)];
    [S_V_VIEW addSubview:backBtn];
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(150);
        make.centerX.mas_equalTo(0);
        make.bottom.mas_equalTo(-65);
        make.height.mas_equalTo(46);
    }];
}

@end
