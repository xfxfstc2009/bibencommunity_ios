//
//  BYUploadVodViewModel.m
//  BY
//
//  Created by 黄亮 on 2018/8/2.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYUploadVodViewModel.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <Photos/Photos.h>
#import "TXUGCPublish.h"

#import "BYLiveHomeRequest.h"

@interface BYUploadVodViewModel()<UINavigationControllerDelegate,UIImagePickerControllerDelegate,TXVideoPublishListener>
{
    TXUGCPublish   *_videoPublish;
    TXPublishParam   *_videoPublishParams;

}
@property (nonatomic ,strong) NSString *uploadTempFilePath;

// ** 上传凭证
@property (nonatomic ,strong) NSString *signature;

@end

@implementation BYUploadVodViewModel

- (void)setContentView{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"相册" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    [S_V_VIEW addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(50);
        make.top.mas_equalTo(100);
        make.centerX.mas_equalTo(0);
    }];
    
    UIButton *uploadBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [uploadBtn setTitle:@"上传" forState:UIControlStateNormal];
    [uploadBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [uploadBtn addTarget:self action:@selector(uploadBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [S_V_VIEW addSubview:uploadBtn];
    [uploadBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(50);
        make.top.mas_equalTo(200);
        make.centerX.mas_equalTo(0);
    }];
    
    [self loadGetVodSignature];
}

- (void)btnAction:(UIButton *)sender{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    NSString *requiredMediaType1 = ( NSString *)kUTTypeMovie;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    NSArray *arrMediaTypes=[NSArray arrayWithObjects:requiredMediaType1,nil];
    [imagePickerController setMediaTypes: arrMediaTypes];
    imagePickerController.delegate = self;
    [self.viewController presentViewController:imagePickerController animated:YES completion:nil];
}

- (void)uploadBtnAction:(UIButton *)sender{
    if (!self.uploadTempFilePath) {
        NSLog(@"没有选择文件！！！");
        return;
    }
    if (!_signature.length) {
        NSLog(@"上传凭证获取失败");
        return;
    }
    
    if(_videoPublish == nil) {
        _videoPublish = [[TXUGCPublish alloc] initWithUserID:@"6666"];
        _videoPublish.delegate = self;
    }
    
    TXPublishParam *videoPublishParams = [[TXPublishParam alloc] init];
    videoPublishParams.signature  = _signature;
    videoPublishParams.coverPath = nil;
    videoPublishParams.videoPath  = self.uploadTempFilePath;
    [_videoPublish publishVideo:videoPublishParams];
}

#pragma mark - requestMethod

- (void)loadGetVodSignature{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadGetUploadVodSignatureSuccessBlock:^(id object) {
        @strongify(self);
        self.signature = nullToEmpty(object);
    } faileBlock:^(NSError *error) {
        
    }];
}

- (void)uploadVodInfoWithTitle:(NSString *)title
                      videoUrl:(NSString *)videoUrl
                       videoId:(NSString *)videoId{
    [[BYLiveHomeRequest alloc] uploadVodInfoWithUserId:6666
                                                 title:title
                                              videoUrl:videoUrl
                                               videoId:videoId
                                              coverUrl:@""
                                          successBlock:^(id object) {
                                              NSLog(@"更新成功");
                                          } faileBlock:^(NSError *error) {
                                              NSLog(@"更新失败");
                                          }];
}

#pragma mark - delegate

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    NSURL *sourceURL = [info objectForKey:UIImagePickerControllerMediaURL];
    NSURL *newVideoUrl ; //一般.mp4
    NSString* tempPath = [self TempFilePathWithExtension:@"mp4"];
    newVideoUrl = [NSURL fileURLWithPath:tempPath];
    self.uploadTempFilePath = tempPath;
    //self.imagePreviewView.image = image;
    [picker dismissViewControllerAnimated:NO completion:^{
        
    }];
    [self convertVideoQuailtyWithInputURL:sourceURL outputURL:newVideoUrl completeHandler:nil];
}

- (void) convertVideoQuailtyWithInputURL:(NSURL*)inputURL
                               outputURL:(NSURL*)outputURL
                         completeHandler:(void (^)(AVAssetExportSession*))handler
{
    AVURLAsset *avAsset = [AVURLAsset URLAssetWithURL:inputURL options:nil];
    
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:avAsset presetName:AVAssetExportPresetMediumQuality];
    //  NSLog(resultPath);
    exportSession.outputURL = outputURL;
    exportSession.outputFileType = AVFileTypeMPEG4;
    exportSession.shouldOptimizeForNetworkUse= YES;
    [exportSession exportAsynchronouslyWithCompletionHandler:^(void)
     {
         switch (exportSession.status) {
             case AVAssetExportSessionStatusCancelled:
                 NSLog(@"AVAssetExportSessionStatusCancelled");
                 break;
             case AVAssetExportSessionStatusUnknown:
                 NSLog(@"AVAssetExportSessionStatusUnknown");
                 break;
             case AVAssetExportSessionStatusWaiting:
                 NSLog(@"AVAssetExportSessionStatusWaiting");
                 break;
             case AVAssetExportSessionStatusExporting:
                 NSLog(@"AVAssetExportSessionStatusExporting");
                 break;
             case AVAssetExportSessionStatusCompleted:
                 NSLog(@"AVAssetExportSessionStatusCompleted");
                 self.uploadTempFilePath = [outputURL path];
                 break;
             case AVAssetExportSessionStatusFailed:
                 NSLog(@"AVAssetExportSessionStatusFailed");
                 break;
         }
     }];
}

-(NSString *) TempFilePathWithExtension:(NSString*) extension{
    NSString* fileName = [NSUUID UUID].UUIDString;
    NSString* path = NSTemporaryDirectory();
    path = [path stringByAppendingPathComponent:fileName];
    path = [path stringByAppendingPathExtension:extension];
    return path;
}

#pragma mark - TXVideoPublishListener
-(void) onPublishProgress:(NSInteger)uploadBytes totalBytes: (NSInteger)totalBytes
{
//    self.progressView.progress = (float)uploadBytes/totalBytes;
    NSLog(@"onPublishProgress [%li/%li]", (long)uploadBytes, (long)totalBytes);
}

-(void) onPublishComplete:(TXPublishResult*)result
{
    NSString *string = [NSString stringWithFormat:@"上传完成，错误码[%li]，信息[%@]", (long)result.retCode, result.retCode == 0? result.videoURL: result.descMsg];
    [self uploadVodInfoWithTitle:@"测试视频" videoUrl:result.videoURL videoId:result.videoId];
    NSLog(@"onPublishComplete [%d/%@]", result.retCode, result.retCode == 0? result.videoURL: result.descMsg);
}


@end
