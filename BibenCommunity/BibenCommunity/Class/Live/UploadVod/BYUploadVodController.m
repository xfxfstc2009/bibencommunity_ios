//
//  BYUploadVodController.m
//  BY
//
//  Created by 黄亮 on 2018/8/2.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYUploadVodController.h"
#import "BYUploadVodViewModel.h"

@interface BYUploadVodController ()

@end

@implementation BYUploadVodController

- (Class)getViewModelClass{
    return [BYUploadVodViewModel class];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationTitle = @"视频上传";
}

- (void)didMoveToParentViewController:(UIViewController *)parent{
    NSLog(@"%@",parent);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
