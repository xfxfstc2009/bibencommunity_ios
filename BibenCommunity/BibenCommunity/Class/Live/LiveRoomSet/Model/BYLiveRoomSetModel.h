//
//  BYLiveRoomSetModel.h
//  BY
//
//  Created by 黄亮 on 2018/9/10.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCommonModel.h"

@interface BYLiveRoomSetModel : BYCommonModel

@property (nonatomic ,strong) NSArray *tableData;

@end

@interface BYLiveRoomSetCellModel : BYCommonModel

@property (nonatomic ,strong) NSString *rightTitle;

@property (nonatomic ,assign) BOOL showBottomLine;


@end

