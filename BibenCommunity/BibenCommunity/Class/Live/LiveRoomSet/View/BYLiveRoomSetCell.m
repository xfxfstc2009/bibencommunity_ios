//
//  BYLiveRoomSetCell.m
//  BY
//
//  Created by 黄亮 on 2018/9/10.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYLiveRoomSetCell.h"
#import "BYLiveRoomSetModel.h"

@implementation BYLiveRoomSetCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = object[indexPath.section];
    BYLiveRoomSetCellModel *model = dic.allValues[0][indexPath.row];
    [self setTitle:model.title];
    [self setRightImage:[UIImage imageNamed:@"common_cell_rightarrow"]];
    [self setRightTitle:model.rightTitle];
    [self showBottomLine:model.showBottomLine];
}

@end
