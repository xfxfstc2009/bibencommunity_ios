//
//  BYLiveEditBgImgController.m
//  BY
//
//  Created by 黄亮 on 2018/9/11.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYLiveEditBgImgController.h"
#import "BYLiveEditBgImgViewModel.h"

@interface BYLiveEditBgImgController ()

@end

@implementation BYLiveEditBgImgController

- (Class)getViewModelClass{
    return [BYLiveEditBgImgViewModel class];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    switch (_edit_type) {
        case EDIT_IMG_TYPE_LIVEROOM:
            self.navigationTitle = @"直播间设置";
            break;
        default:
            self.navigationTitle = @"直播设置";

            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
