//
//  BYLiveEditBgImgViewModel.m
//  BY
//
//  Created by 黄亮 on 2018/9/11.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYLiveEditBgImgViewModel.h"
#import "BYLiveEditBgImgController.h"
#import "NSDate+BYExtension.h"
#import "BYLiveHomeRequest.h"

@interface BYLiveEditBgImgViewModel()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    CGFloat _imageW;
    CGFloat _imageH;
    UIImage *_selectImage;
    NSString *_updateKey;
}
/** contentView */
@property (nonatomic ,strong) UIScrollView *scrollView;

@property (nonatomic ,strong) UIImageView *imageView;
@property (nonatomic ,strong) NSArray *defultImgs;
@end

@implementation BYLiveEditBgImgViewModel


#pragma mark - action Method

- (void)addImageAction{
    [self openPhotoLibrary];
}

- (void)delBtnAction{
    _selectImage = nil;
    [_imageView removeFromSuperview];
}

// 图片上传
- (void)rightBarButtonItemAction{
    if (!_selectImage) {
        showToastView(@"请先选择上传图片", S_V_VIEW);
        return;
    }
    OSSFileModel *model = [OSSFileModel new];
    model.objcImage = _selectImage;
    model.objcName  = [NSString stringWithFormat:@"%@-%@",[NSDate by_stringFromDate:[NSDate by_date] dateformatter:@"yyyy-MM-dd-HH:mm:ss"],_updateKey];
    @weakify(self);
    BYToastView *toastView = [BYToastView toastViewPresentLoading];
    [[OSSManager sharedUploadManager] uploadImageManagerWithImageList:[@[model] copy] withUrlBlock:^(NSArray *imgUrlArr) {
        @strongify(self);
        if (((BYLiveEditBgImgController *)S_VC).edit_type == EDIT_IMG_TYPE_PUBLICITY) {
            [self loadRequestUpdateLive:@{self->_updateKey:imgUrlArr[0]} toastView:toastView];
        }
        else
        {
            [self loadRequestUpdateLiveRoom:@{self->_updateKey:imgUrlArr[0]} toastView:toastView];
        }
    }];
}

#pragma mark - request method

// 更新直播间信息
- (void)loadRequestUpdateLiveRoom:(NSDictionary *)param toastView:(BYToastView *)toastView{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadReuqestUpdateLiveRoom:param successBlock:^(id object) {
        @strongify(self);
        [toastView dissmissToastView];
        showToastView(@"上传成功", S_V_VIEW);
    } faileBlock:^(NSError *error) {
        @strongify(self);
        [toastView dissmissToastView];
        showToastView(@"上传失败", S_V_VIEW);
    }];
}

// 更新直播信息
- (void)loadRequestUpdateLive:(NSDictionary *)param toastView:(BYToastView *)toastView{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestUpdateLiveSet:((BYLiveEditBgImgController *)S_VC).live_record_id param:param successBlock:^(id object) {
        @strongify(self);
        [toastView dissmissToastView];
        showToastView(@"修改成功", S_V_VIEW);
    } faileBlock:^(NSError *error) {
        [toastView dissmissToastView];
    }];
}

// 打开相册
- (void)openPhotoLibrary{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.navigationBar.translucent=NO;
        [self.viewController presentViewController:imagePicker animated:YES completion:nil];
        return;
    }
}

// 图片尺寸校验
- (BOOL)verifyImgSize:(UIImage *)image{
    switch (((BYLiveEditBgImgController *)S_VC).edit_type) {
        case EDIT_IMG_TYPE_LIVE:
        {
            if (image.size.width < image.size.height) {
                showToastView(@"图片尺寸超出范围", S_V_VIEW);
                return NO;
            }
            return YES;
        }
            break;
        default:
        {
            if (image.size.width > image.size.height) {
                showToastView(@"图片尺寸超出范围", S_V_VIEW);
                return NO;
            }
            return YES;
        }
            break;
    }
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{

    UIImage *image = info[UIImagePickerControllerOriginalImage];
//    // 校验图片尺寸
//    if (![self verifyImgSize:image]) {
//        [picker dismissViewControllerAnimated:YES completion:nil];
//        return;
//    }
    // 图片尺寸限制
    UIImage *newImage = [image resizeImageToMaxWidthOrHeight:_imageW > _imageH ? _imageW : _imageH];
    // 图片大小限制
    NSData *data = [newImage compressWithMaxLength:1024*1024*2];
    UIImage *tmpImg  = [UIImage imageWithData:data];
    _imageView.image = [tmpImg addCornerRadius:8 size:tmpImg.size];
    _selectImage = tmpImg;
    if (!_imageView.superview) {
        [_scrollView addSubview:_imageView];
        [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(0);
            make.width.mas_equalTo(kCommonScreenWidth - 30);
            make.height.mas_equalTo((kCommonScreenWidth-30)*tmpImg.size.height/tmpImg.size.width);
            make.top.mas_equalTo(10);
        }];
    }
    [_imageView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo((kCommonScreenWidth-30)*tmpImg.size.height/tmpImg.size.width);
    }];
    @weakify(self);
    [_scrollView mas_updateConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.bottom.mas_equalTo(self.imageView.mas_bottom).offset(20).priorityLow();
        make.bottom.mas_greaterThanOrEqualTo(S_V_VIEW);
    }];
    
    //如果是相机拍照，则保存到相册
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
    {
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - initMethod/configSubView

- (void)setContentView{
    [self addRightBarButtonItem];
    
    [S_V_VIEW addSubview:self.scrollView];
    [_scrollView setContentSize:CGSizeMake(kCommonScreenWidth, kCommonScreenHeight)];
    [_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    [self addHeaderView];
    
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.userInteractionEnabled = YES;
    _imageView = imageView;
    
    UIButton *delBtn = [UIButton by_buttonWithCustomType];
    [delBtn setBy_imageName:@"liveedit_del" forState:UIControlStateNormal];
    [delBtn addTarget:self action:@selector(delBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:delBtn];
    [delBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.mas_equalTo(0);
        make.width.height.mas_equalTo(40);
    }];
}

- (void)addHeaderView{
    CGFloat height = 0;
    CGFloat top = 0;
    NSString *messageStr = @"";
    switch (((BYLiveEditBgImgController *)S_VC).edit_type) {
        case EDIT_IMG_TYPE_LIVEROOM:
            height = 171;
            top = 30;
            _imageW = 750;
            _imageH = 370;
            messageStr = @"JPG、BMP、JPEG最大2M，370*750）";
            _updateKey = @"cover_url";
            break;
        case EDIT_IMG_TYPE_LIVE:
            height = 153;
            top = 24;
            _imageW = 360;
            _imageH = 240;
            messageStr = @"（JPG、BMP、JPEG最大2M，360*240）";
            _updateKey = @"live_cover_url";
            break;
        default:
            height = 230;
            top = 60;
            _imageW = 750;
            _imageH = 370;
            messageStr = @"（JPG、BMP、JPEG最大2M，370*750）";
            _updateKey = @"ad_img";
            break;
    }
    UIView *bgView = [UIView by_init];
    bgView.backgroundColor = kBgColor_237;
    bgView.layer.cornerRadius = 8;
    //    [bgView layerCornerRadius:8 size:CGSizeMake(kCommonScreenWidth-30, height)];
    [_scrollView addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kCommonScreenWidth - 30);
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(10);
        make.height.mas_equalTo(height);
    }];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addImageAction)];
    [bgView addGestureRecognizer:tapGestureRecognizer];
    
    UIImageView *addimgView = [[UIImageView alloc] init];
    [addimgView by_setImageName:@"liveedit_bgimg_addimg"];
    [bgView addSubview:addimgView];
    [addimgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(top);
        make.width.mas_equalTo(addimgView.image.size.width);
        make.height.mas_equalTo(addimgView.image.size.height);
        make.centerX.mas_equalTo(0);
    }];
    
    UILabel *titleLab = [UILabel by_init];
    [titleLab setBy_font:13];
    titleLab.textColor = kTextColor_60;
    titleLab.text = @"从相册上传背景";
    [bgView addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.equalTo(addimgView.mas_bottom).with.offset(17);
        make.width.mas_equalTo(stringGetWidth(titleLab.text, titleLab.font.pointSize));
        make.height.mas_equalTo(stringGetHeight(titleLab.text, titleLab.font.pointSize));
    }];
    
    UILabel *messageLab = [UILabel by_init];
    [messageLab setBy_font:12];
    messageLab.textColor = kTextColor_117;
    messageLab.text = messageStr;
    [bgView addSubview:messageLab];
    [messageLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.equalTo(titleLab.mas_bottom).with.offset(5);
        make.width.mas_equalTo(stringGetWidth(messageLab.text, messageLab.font.pointSize));
        make.height.mas_equalTo(stringGetHeight(messageLab.text, messageLab.font.pointSize));
    }];
}

- (void)addRightBarButtonItem{
    UIBarButtonItem *rightBarButtonItem = [UIBarButtonItem initWithTitle:@"提交" target:self action:@selector(rightBarButtonItemAction) type:BYBarButtonItemTypeRight];
    S_VC.navigationItem.rightBarButtonItem = rightBarButtonItem;
}

- (void)addDefultImgBg{
    for (int i = 0; i < 2; i ++) {
        for (int j = 0; j < 3; j ++) {
            UIImageView *imageView = [[UIImageView alloc] init];
        }
    }
}

- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] init];
    }
    return _scrollView;
}

@end
