//
//  BYLiveRoomHomeController.m
//  BY
//
//  Created by 黄亮 on 2018/9/12.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYLiveRoomHomeController.h"
#import "BYLiveRoomHomeViewModel.h"

@interface BYLiveRoomHomeController ()

@end

@implementation BYLiveRoomHomeController

- (Class)getViewModelClass{
    return [BYLiveRoomHomeViewModel class];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.popGestureRecognizerEnale = NO;
    self.navigationTitle = @"直播间";
}

@end
