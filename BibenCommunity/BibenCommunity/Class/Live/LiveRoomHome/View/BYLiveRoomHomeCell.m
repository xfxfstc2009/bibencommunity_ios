//
//  BYLiveRoomHomeCell.m
//  BY
//
//  Created by 黄亮 on 2018/9/12.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYLiveRoomHomeCell.h"
#import "BYCommonLiveModel.h"

@interface BYLiveRoomHomeCell()

@property (nonatomic ,strong) PDImageView *coverImgView;
@property (nonatomic ,strong) UILabel *titleLab;
@property (nonatomic ,strong) UILabel *timeLab;
@property (nonatomic ,strong) UIView *liveStatus;
@property (nonatomic ,strong) UILabel *liveStatusLab;

@end;

@implementation BYLiveRoomHomeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    self.indexPath = indexPath;
    BYCommonLiveModel *model = object[indexPath.row];
    @weakify(self);
    [_coverImgView uploadImageWithURL:model.live_cover_url placeholder:nil callback:^(UIImage *image) {
        @strongify(self);
        self.coverImgView.image = [image addCornerRadius:5 size:CGSizeMake(132, 97)];

    }];
    _titleLab.text = model.live_title;
    _timeLab.text = model.begin_time;
    [self reloadLiveStatus:model.status];
}

- (void)enterBtnAction{
    [self sendActionName:@"enterAction" param:nil indexPath:self.indexPath];
}

- (void)reloadLiveStatus:(BY_LIVE_STATUS)status{
    switch (status) {
        case BY_LIVE_STATUS_LIVING:
            _liveStatus.backgroundColor = kColorRGBValue(0xed4a45);
            _liveStatusLab.text = @"正在直播";
            break;
        case BY_LIVE_STATUS_SOON:
            _liveStatus.backgroundColor = kColorRGBValue(0x00a43e);
            _liveStatusLab.text = @"直播预告";
            break;
        case BY_LIVE_STATUS_END:
            _liveStatus.backgroundColor = kColorRGBValue(0xed9c45);
            _liveStatusLab.text = @"直播回放";
            break;
        default:
            break;
    }
}

- (void)setContentView{
    // 封面图
    PDImageView *coverImgView = [[PDImageView alloc] init];
    [self.contentView addSubview:coverImgView];
    _coverImgView = coverImgView;
    [coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(132);
        make.height.mas_equalTo(97);
    }];
    
    UIView *maskView= [UIView by_init];
    [maskView setBackgroundColor:kColorRGB(47, 47, 47, 0.6)];
    [maskView layerCornerRadius:5 byRoundingCorners:UIRectCornerTopLeft size:CGSizeMake(60, 23)];
    [self.contentView addSubview:maskView];
    [maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(kCellLeftSpace);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(23);
    }];
    
    // 直播状态
    UIView *liveStatus = [UIView by_init];
    liveStatus.layer.cornerRadius = 2;
    [maskView addSubview:liveStatus];
    _liveStatus = liveStatus;
    [liveStatus mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(4);
        make.left.mas_equalTo(5);
        make.centerY.mas_equalTo(0);
    }];
    
    // 直播状态文案
    UILabel *liveStatusLab = [UILabel by_init];
    liveStatusLab.text = @"直播回放";
    [liveStatusLab setBy_font:11];
    [liveStatusLab setTextColor:[UIColor whiteColor]];
    [maskView addSubview:liveStatusLab];
    _liveStatusLab = liveStatusLab;
    [liveStatusLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(liveStatus.mas_right).with.offset(3);
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(stringGetWidth(liveStatusLab.text, liveStatusLab.font.pointSize));
        make.height.mas_equalTo(stringGetHeight(liveStatusLab.text, liveStatusLab.font.pointSize));
    }];
    
    
    // 标题
    UILabel *titleLab = [[UILabel alloc] init];
    titleLab.font = [UIFont systemFontOfSize:14];
    titleLab.textColor = kColorRGBValue(0x2c2c2c);
    titleLab.numberOfLines = 2;
    [self.contentView addSubview:titleLab];
    _titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(coverImgView.mas_right).with.offset(10);
        make.top.mas_equalTo(2);
        make.right.mas_equalTo(-15);
        make.height.mas_greaterThanOrEqualTo(14);
    }];
    
    // 时间图标
    UIImageView *timeImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"common_time_icon"]];
    [self.contentView addSubview:timeImgView];
    [timeImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(coverImgView.mas_right).with.offset(10);
        make.bottom.equalTo(coverImgView.mas_bottom).with.offset(-5);
        make.width.mas_equalTo(timeImgView.image.size.width);
        make.height.mas_equalTo(timeImgView.image.size.height);
    }];
    
    // 时间
    UILabel *timeLab = [[UILabel alloc] init];
    timeLab.font = [UIFont systemFontOfSize:12];
    timeLab.textColor = kTextColor_154;
    [self.contentView addSubview:timeLab];
    _timeLab = timeLab;
    [timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(timeImgView.mas_right).with.offset(5);
        make.centerY.equalTo(timeImgView.mas_centerY).with.offset(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(18);
    }];
    
    // 进入按钮
    UIButton *enterBtn = [UIButton by_buttonWithCustomType];
    [enterBtn setBackgroundColor:kBgColor_238];
    [enterBtn setBy_attributedTitle:@{@"title":@"进入",NSFontAttributeName:[UIFont systemFontOfSize:11],NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateNormal];
    [enterBtn addTarget:self action:@selector(enterBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [enterBtn layerCornerRadius:4 size:CGSizeMake(52, 23)];
    [self.contentView addSubview:enterBtn];
    [enterBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(52);
        make.height.mas_equalTo(23);
        make.right.mas_equalTo(-kCellRightSpace);
        make.bottom.mas_equalTo(coverImgView.mas_bottom);
    }];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
