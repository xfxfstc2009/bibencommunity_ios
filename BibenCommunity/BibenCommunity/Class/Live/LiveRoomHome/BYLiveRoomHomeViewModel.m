//
//  BYLiveRoomHomeViewModel.m
//  BY
//
//  Created by 黄亮 on 2018/9/12.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYLiveRoomHomeViewModel.h"
// Controller
#import "BYNewLiveController.h"
#import "BYLiveRoomSetController.h"
#import "BYLiveHostController.h"
#import "BYLiveController.h"
// View
#import "BYLiveRoomHomeHeaderView.h"

#import "BYCommonModel.h"
#import "BYCommonLiveModel.h"
// Request
#import "BYLiveHomeRequest.h"


@interface BYLiveRoomHomeViewModel()<BYCommonTableViewDelegate>

@property (nonatomic ,strong) BYCommonTableView *tableView;
@property (nonatomic ,strong) BYLiveRoomHomeHeaderView *headerView;
@property (nonatomic ,assign) NSInteger pageNum;
@end

@implementation BYLiveRoomHomeViewModel

- (void)setContentView{
    _pageNum = 0;
    [S_V_VIEW addSubview:self.tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    [self loadRequestGetLiveRoomInfo];
    [self loadRequestGetLiveList];
}

- (void)loadMoreData{
    [self loadRequestGetLiveList];
}

#pragma mark - buttonAction
// 新建直播
- (void)didNewLiveBtnAction{
    BYNewLiveController *newLiveController = [[BYNewLiveController alloc] init];
    [S_V_NC pushViewController:newLiveController animated:YES];
}
// 直播间设置
- (void)didRoomSetBtnAction{
    BYLiveRoomSetController *liveRoomSetController = [[BYLiveRoomSetController alloc] init];
    [S_V_NC pushViewController:liveRoomSetController animated:YES];
}

#pragma mark - request
- (void)loadRequestGetLiveRoomInfo{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetLiveRoomInfoSuccessBlock:^(id object) {
        @strongify(self);
        [self.headerView reloadData:(BYLiveRoomHomeHeaderModel *)object];
    } faileBlock:^(NSError *error) {
        
    }];
}

- (void)loadRequestGetLiveList{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetLivelist:_pageNum successBlock:^(id object) {
        @strongify(self);
        [self.tableView endRefreshing];
        NSMutableArray *tmpArr = [NSMutableArray array];
        if (self->_pageNum == 0) {
            self.pageNum ++;
            BYCommonModel *model = [[BYCommonModel alloc] init];
            model.cellString = @"BYLiveRoomHomeTitleCell";
            model.cellHeight = 58;
            model.title = @"直播列表";
            [tmpArr addObject:model];
            [tmpArr addObjectsFromArray:object];
            self.tableView.tableData = tmpArr;
            return ;
        }
        self.pageNum ++;
        [tmpArr addObjectsFromArray:self.tableView.tableData];
        [tmpArr addObjectsFromArray:object];
        self.tableView.tableData = tmpArr;
    } faileBlock:^(NSError *error) {
        [self.tableView endRefreshing];
    }];
}
#pragma mark - BYCommonTableViewDelegate
- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) return;
    BYCommonLiveModel *model = tableView.tableData[indexPath.row];
    BYLiveController *livecontroller = [[BYLiveController alloc] init];
    livecontroller.model = model;
    [S_V_NC pushViewController:livecontroller animated:YES];
}

#pragma mark - initMethod

- (BYCommonTableView *)tableView{
    if (!_tableView) {
        _tableView = [[BYCommonTableView alloc] init];
        _tableView.group_delegate = self;
        _tableView.tableHeaderView = self.headerView;
        [_tableView addFooterRefreshTarget:self action:@selector(loadMoreData)];
    }
    return _tableView;
}

- (BYLiveRoomHomeHeaderView *)headerView{
    if (!_headerView) {
        _headerView = [[BYLiveRoomHomeHeaderView alloc] initWithFrame:CGRectMake(0, 0, kCommonScreenWidth, 385)];
        @weakify(self);
        _headerView.didNewLiveBtnHandle = ^{
            @strongify(self);
            [self didNewLiveBtnAction];
        };
        _headerView.didRoomSetBtnHandle = ^{
            @strongify(self);
            [self didRoomSetBtnAction];
        };
    }
    return _headerView;
}

@end
