//
//  BYLiveDefine.h
//  BY
//
//  Created by 黄亮 on 2018/8/31.
//  Copyright © 2018年 BY. All rights reserved.
//

#ifndef BYLiveDefine_h
#define BYLiveDefine_h

typedef NS_ENUM(NSInteger, BY_LIVE_ORGAN_TYPE) {
    BY_LIVE_ORGAN_TYPE_PERSON = 0, // 个人
    BY_LIVE_ORGAN_TYPE_LECTURER, // 讲师
    BY_LIVE_ORGAN_TYPE_ORGAN // 机构
};

typedef NS_ENUM(NSInteger, BY_LIVE_TYPE) {
    BY_LIVE_TYPE_AUDIO, // 语音讲座形式
    BY_LIVE_TYPE_PPT, // PPT幻灯片形式
    BY_LIVE_TYPE_VIDEO // 视频形式
};

typedef NS_ENUM(NSInteger, BY_LIVE_STATUS) {
    BY_LIVE_STATUS_WAITING = 0, // 申请直播
    BY_LIVE_STATUS_SOON, // 预约直播
    BY_LIVE_STATUS_LIVING, // 正在直播
    BY_LIVE_STATUS_END // 结束直播
};

// 新建直播时的选择直播形式
typedef NS_ENUM(NSInteger, BY_NEWLIVE_TYPE) {
    BY_NEWLIVE_TYPE_AUDIO_LECTURE = 0, // 音频讲座形式
    BY_NEWLIVE_TYPE_AUDIO_PPT, // 音频PPT形式
    BY_NEWLIVE_TYPE_ILIVE, // 个人互动直播形式
    BY_NEWLIVE_TYPE_VIDEO, // 视频直播形式
    BY_NEWLIVE_TYPE_RECORD_SCREEN, // 录屏直播形式
    BY_NEWLIVE_TYPE_UPLOAD_VIDEO, // 音视频录播形式
};

// 首页列表分类
typedef NS_ENUM(NSInteger, BY_LIVE_HOME_LIST_TYPE) {
    BY_LIVE_HOME_LIST_TYPE_HOT = 0, // 热门推荐
    BY_LIVE_HOME_LIST_TYPE_AUDIO, // 音频类
    BY_LIVE_HOME_LIST_TYPE_ILIVE, // 视频类
    BY_LIVE_HOME_LIST_TYPE_FOLLOW // 我的关注
};

// 排序类型
typedef NS_ENUM(NSInteger, BY_SORT_TYPE) {
    BY_SORT_TYPE_HOT = 0, // 热度
    BY_SORT_TYPE_TIME, // 时间
    BY_SORT_TYPE_HOT_TIME // 热度与时间
};

// 操作指南
static const NSString *opeartionGuide = @"1.听不到声音请往下翻，点击语音即可播放，并确认手机没有静音；\n2.点击右侧操作菜单一键回到顶部或底部；\n3.课程文件长期保留，无限回听；\n4.遇到卡顿和或加载不出，点返回，重新进入即可。";

#define K_S_D_F @"yyyy-MM-dd HH:mm" // show dateformatter 展示的时间格式

#define kiSHaveLiveRoomKey [NSString stringWithFormat:@"%@-kiSHaveLiveRoom",[AccountModel sharedAccountModel].account_id]

#endif /* BYLiveDefine_h */
