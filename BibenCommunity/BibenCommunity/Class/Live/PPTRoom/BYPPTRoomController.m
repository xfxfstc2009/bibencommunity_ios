//
//  BYPPTRoomController.m
//  BY
//
//  Created by 黄亮 on 2018/8/21.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYPPTRoomController.h"
#import "BYPPTRoomViewModel.h"
#import "BYLiveHomeVideoModel.h"

#import "BYALIMManager.h"

@interface BYPPTRoomController ()

@end

@implementation BYPPTRoomController

- (Class)getViewModelClass{
    return [BYPPTRoomViewModel class];
}

- (instancetype)initWithModel:(BYLiveHomeVideoModel *)model{
    self = [super init];
    if (self) {
        ((BYPPTRoomViewModel *)self.viewModel).model = model;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.popGestureRecognizerEnale = NO;
    @weakify(self);
    [[BYALIMManager shareInstance] loginWithUserId:@"123545" password:@"ajiknklasd!#$#" suc:^{
        @strongify(self);
        showToastView(@"ALIM登录成功", self.view);
    } fail:^(NSError *error) {
        @strongify(self);
        showToastView(@"ALIM登录失败", self.view);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
