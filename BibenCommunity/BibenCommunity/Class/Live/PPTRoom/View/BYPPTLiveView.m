//
//  BYPPTLiveView.m
//  BY
//
//  Created by 黄亮 on 2018/9/4.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYPPTLiveView.h"
#import "BYPPTLiveHeaderView.h"

@interface BYPPTLiveView()

@property (nonatomic ,strong) BYPPTLiveHeaderView *headerView;

@end

@implementation BYPPTLiveView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentView{
    [self addSubview:self.headerView];
    [_headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(0);
        make.height.mas_equalTo(195);
    }];
}

#pragma mark - initMethod
- (BYPPTLiveHeaderView *)headerView{
    if (!_headerView) {
        _headerView = [[BYPPTLiveHeaderView alloc] init];
    }
    return _headerView;
}
@end
