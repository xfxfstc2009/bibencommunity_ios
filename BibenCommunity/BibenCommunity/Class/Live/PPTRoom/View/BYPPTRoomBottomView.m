//
//  BYPPTRoomBottomView.m
//  BY
//
//  Created by 黄亮 on 2018/8/21.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYPPTRoomBottomView.h"
#import "BYIMDefine.h"
 
@interface BYPPTRoomBottomView()<UITextFieldDelegate>
{
    // 输入的text
    NSString *_inputText;
}

// ** 输入源
@property (nonatomic ,strong) UITextField  *textField;

// ** 消息发送
@property (nonatomic ,strong) UIButton     *sendBtn;

// ** 遮罩层
@property (nonatomic ,strong) UIControl       *maskView;

@end

static CGFloat sendBtnW = 50.0f;
@implementation BYPPTRoomBottomView

- (void)dealloc
{
    [[BYIMManager sharedInstance] removeMessageListener];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setContentView];
        [self addNSNotification];
    }
    return self;
}

- (void)addNSNotification{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
    
    // 设置消息回调
    [[BYIMManager sharedInstance] setMessageListener:^(NSArray *msgs) {
        
    }];
}

- (void)voiceBtnAction{
    
}

- (void)sendBtnAction{
    if (!_inputText.length) {
        [self importMessage];
    }
    @weakify(self);
    [[BYIMManager sharedInstance] sendTextMessage:_inputText succ:^{
        @strongify(self);
        [self clearInputText];
        showToastView(@"消息发送成功", self.superview);
    } fail:^(int code) {
//        @strongify(self);
//        showToastView(@"消息发送失败", self.superview);
    }];
}

- (void)endExit{
    [_textField resignFirstResponder];
}

// 清除输入内容
- (void)clearInputText{
    self.textField.text = @"";
    _inputText = nil;
}

- (void)importMessage{
    
    [[BYIMManager sharedInstance] getLocalMessage:20 succ:^{
        
    } fail:^(int code) {
        
    }];
}

// 修改控件mas
- (void)layoutSubviewMas{
    [self setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.2 animations:^{
        @weakify(self);
        [self.textField mas_updateConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.right.mas_equalTo(self.textField.isFirstResponder ? -(sendBtnW + 15) : -15);
        }];
        [self.sendBtn mas_updateConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.right.mas_equalTo(self.textField.isFirstResponder ? - 10 : sendBtnW);
        }];
        [self layoutIfNeeded];
    }];
}

// 开始编辑时动画
- (void)layoutKeyBoardWillShowMas:(CGFloat)keyBoardH{
    self.maskView.frame = self.superview.bounds;
    [self.superview addSubview:self.maskView];
    [self.superview insertSubview:self.maskView belowSubview:self];
    [self.superview setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.2 animations:^{
        if (self.mas_bottom || self.mas_top || self.mas_left || self.mas_right) {
            [self mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(-keyBoardH);
            }];
            [self.superview layoutIfNeeded];
        }
        else
        {
            CGRect frame = self.frame;
            frame.origin.y = self.superview.bounds.size.height - keyBoardH - BV_DF_H;
            self.frame = frame;
        }
    }];
}

// 结束编辑时动画
- (void)layoutKeyBoardWillHiddenMas{
    [self.fathureView setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.2 animations:^{
        if (self.mas_bottom || self.mas_top || self.mas_left || self.mas_right) {
            [self mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(0);
            }];
            [self.superview layoutIfNeeded];
        }
        else
        {
            CGRect frame = self.frame;
            frame.origin.y = self.superview.bounds.size.height - BV_DF_H;
            self.frame = frame;
        }
    }completion:^(BOOL finished) {
        [self.maskView removeFromSuperview];
    }];
}

#pragma mark - NSNotification
- (void)keyBoardWillShow:(NSNotification *)notic{
    NSDictionary *userInfo = notic.userInfo;
    NSValue *value = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyBoardRect = [value CGRectValue];
    CGFloat keyBoardH = keyBoardRect.size.height;
    [self layoutKeyBoardWillShowMas:keyBoardH];
}

- (void)keyBoardWillHidden:(NSNotification *)notic{
    [self layoutKeyBoardWillHiddenMas];
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self layoutSubviewMas];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self layoutSubviewMas];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    _inputText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    return YES;
}

#pragma make - configSubView Method

- (void)setContentView{
    
    UIButton *voiceBtn = [UIButton by_buttonWithCustomType];
    [voiceBtn setBackgroundColor:kRedColor];
    [voiceBtn setTitle:@"语音" forState:UIControlStateNormal];
    [voiceBtn addTarget:self action:@selector(voiceBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:voiceBtn];
    [voiceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(5);
        make.bottom.mas_equalTo(-5);
        make.width.mas_equalTo(40);
    }];
    
    UITextField *textField = [[UITextField alloc] init];
    textField.borderStyle = UITextBorderStyleLine;
    textField.delegate = self;
    [self addSubview:textField];
    _textField = textField;
    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(voiceBtn.mas_right).with.offset(10);
        make.right.mas_equalTo(-15);
        make.top.mas_equalTo(5);
        make.bottom.mas_equalTo(-5);
    }];
    
    UIButton *sendBtn = [UIButton by_buttonWithCustomType];
    [sendBtn setBackgroundColor:kRedColor];
    [sendBtn setTitle:@"send" forState:UIControlStateNormal];
    [sendBtn addTarget:self action:@selector(sendBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sendBtn];
    _sendBtn = sendBtn;
    [sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(sendBtnW);
        make.top.mas_equalTo(5);
        make.bottom.mas_equalTo(-5);
        make.width.mas_equalTo(sendBtnW);
    }];
    
    self.maskView = [UIControl by_init];
    [self.maskView addTarget:self action:@selector(endExit) forControlEvents:UIControlEventTouchUpInside];
}


@end
