//
//  BYPPTRoomBottomView.h
//  BY
//
//  Created by 黄亮 on 2018/8/21.
//  Copyright © 2018年 BY. All rights reserved.
//

#import <UIKit/UIKit.h>

// buttomViewDefultHieght
static CGFloat BV_DF_H = 50;

@interface BYPPTRoomBottomView : UIView

// ** 传入superView
@property (nonatomic ,weak) UIView *fathureView;

- (instancetype)init;

@end
