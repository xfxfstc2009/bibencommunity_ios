//
//  BYPPTLiveHeaderView.m
//  BY
//
//  Created by 黄亮 on 2018/9/4.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYPPTLiveHeaderView.h"
#import "iCarousel.h"

@interface BYPPTLiveHeaderView()<iCarouselDelegate,iCarouselDataSource>

@property (nonatomic ,strong) iCarousel *pptImgsView;

@property (nonatomic ,strong) NSArray *dataArr;

@property (nonatomic ,strong) UIPageControl *pageControl;

@end

@implementation BYPPTLiveHeaderView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentView{
    self.dataArr = @[[UIColor yellowColor],[UIColor redColor],[UIColor grayColor]];
    _pptImgs = _dataArr;
    [self addSubview:self.pptImgsView];
    [_pptImgsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    [self addSubview:self.pageControl];
    [_pageControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.mas_equalTo(0);
        make.height.mas_equalTo(30);
    }];
}

- (void)reloadData{
    [_pptImgsView reloadData];
}
#pragma mark - UIPageControl Action
- (void)chagePageControlValue:(UIPageControl *)sender{
    
}

#pragma mark - iCarouselDelegate/DataScoure
- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel{
    return _pptImgs.count;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(nullable UIView *)view{
    if (!view) {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kCommonScreenWidth, CGRectGetHeight(carousel.frame))];
        [view setBackgroundColor:_pptImgs[index]];
    }
    return view;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value{
    switch (option) {
            // 设置滚动循环
        case iCarouselOptionWrap:
            return YES;
            break;
        default:
            break;
    }
    return value;
}

#pragma mark - initMethod

- (iCarousel *)pptImgsView{
    if (!_pptImgsView) {
        _pptImgsView = [[iCarousel alloc] init];
        _pptImgsView.delegate = self;
        _pptImgsView.dataSource = self;
        _pptImgsView.pagingEnabled = YES;
    }
    return _pptImgsView;
}

- (UIPageControl *)pageControl{
    if (!_pageControl) {
        _pageControl = [[UIPageControl alloc] init];
        _pageControl.numberOfPages = _pptImgs.count;
        _pageControl.currentPage = 0;
        _pageControl.currentPageIndicatorTintColor = kTextColor_238;
        _pageControl.pageIndicatorTintColor = [UIColor whiteColor];
        [_pageControl addTarget:self action:@selector(chagePageControlValue:) forControlEvents:UIControlEventValueChanged];
    }
    return _pageControl;
}
@end
