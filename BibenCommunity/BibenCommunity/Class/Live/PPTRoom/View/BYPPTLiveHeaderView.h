//
//  BYPPTLiveHeaderView.h
//  BY
//
//  Created by 黄亮 on 2018/9/4.
//  Copyright © 2018年 BY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BYPPTLiveHeaderView : UIView


/**
 ppt课件图片组
 */
@property (nonatomic ,strong) NSArray *pptImgs;


- (void)reloadData;
@end
