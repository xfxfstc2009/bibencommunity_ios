//
//  BYPPTRoomViewModel.h
//  BY
//
//  Created by 黄亮 on 2018/8/21.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCommonViewModel.h"

@class BYLiveHomeVideoModel;
@interface BYPPTRoomViewModel : BYCommonViewModel


/**
 直播间信息model
 */
@property (nonatomic ,strong) BYLiveHomeVideoModel *model;

@end
