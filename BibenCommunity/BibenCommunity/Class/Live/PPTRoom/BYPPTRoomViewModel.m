//
//  BYPPTRoomViewModel.m
//  BY
//
//  Created by 黄亮 on 2018/8/21.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYPPTRoomViewModel.h"
#import "BYIMDefine.h"

// childview
#import "BYLiveRoomSegmentView.h"
#import "BYPPTRoomBottomView.h"
#import "BYPPTLiveView.h"
#import "BYIMChatViewController.h"


#import "BYLiveHomeVideoModel.h"

@interface BYPPTRoomViewModel()<BYLiveRoomSegmentViewDelegate>

@property (nonatomic ,strong) BYCommonTableView *tableView;

/** 分类选择 */
@property (nonatomic ,strong) BYLiveRoomSegmentView *segmentView;

/** 底部输入控件 */
@property (nonatomic ,strong) BYPPTRoomBottomView *bottomView;

/** 聊天内容 */
@property (nonatomic ,strong) BYIMChatViewController *imChatView;

/** 直播展示 */
@property (nonatomic ,strong) BYPPTLiveView *liveView;

@end

@implementation BYPPTRoomViewModel

- (void)viewControllerPopAction{
    _bottomView = nil;
//    [[BYIMManager sharedInstance] loginOut:nil];
}

- (void)setContentView{
    
    [S_V_VIEW addSubview:self.segmentView];
    [self.segmentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
//    // 设置群聊会话
//    [[BYIMManager sharedInstance] setConversationGroupId:self.model.livehome_groupId];
//

}
#pragma mark - BYLiveRoomSegmentViewDelegate

- (UIView *)by_segmentViewLoadSubView:(NSInteger)index{
    if (index == 1) {
        return self.liveView;
    }
    UIView *view = [UIView by_init];
    view.backgroundColor = index == 0 ? [UIColor yellowColor] : (index == 1 ? [UIColor whiteColor] : [UIColor blackColor]);
    return view;
}

- (void)by_segmentViewDidScrollToIndex:(NSInteger)index{
    
}

#pragma mark - initMethod

- (BYLiveRoomSegmentView *)segmentView{
    if (!_segmentView) {
        _segmentView = [[BYLiveRoomSegmentView alloc] initWithTitles:@"简介",@"直播",@"点评", nil];
        _segmentView.delegate = self;
        _segmentView.viewModel = self;
    }
    return _segmentView;
}

- (BYPPTLiveView *)liveView{
    if (!_liveView) {
        _liveView = [BYPPTLiveView by_init];
    }
    return _liveView;
}

//- (BYIMChatViewController *)imChatView{
//    if
//}


@end
