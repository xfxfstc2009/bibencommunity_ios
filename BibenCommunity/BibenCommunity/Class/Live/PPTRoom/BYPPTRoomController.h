//
//  BYPPTRoomController.h
//  BY
//
//  Created by 黄亮 on 2018/8/21.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCommonViewController.h"

@class BYLiveHomeVideoModel;
@interface BYPPTRoomController : BYCommonViewController


- (instancetype)initWithModel:(BYLiveHomeVideoModel *)model;

@end
