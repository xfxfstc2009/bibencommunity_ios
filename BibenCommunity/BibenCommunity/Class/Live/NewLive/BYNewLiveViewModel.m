//
//  BYNewLiveViewModel.m
//  BY
//
//  Created by 黄亮 on 2018/9/7.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYNewLiveViewModel.h"
#import "BYILiveRoomController.h"
#import "BYLiveController.h"

// view
#import "BYNewLiveFooterView.h"
#import "BYSelectTimePickerView.h"

// model
#import "BYNewLiveModel.h"
#import "BYNewLiveCellModel.h"
#import "BYCommonLiveModel.h"

// request
#import "BYLiveHomeRequest.h"
#import "BYIMManager.h"
#import "AliChatManager.h"


#define S_DM ((BYNewLiveModel *)self.dataModel)
@interface BYNewLiveViewModel()<BYCommonTableViewDelegate>

@property (nonatomic ,strong) BYCommonTableView *tableView;

@property (nonatomic ,strong) BYNewLiveFooterView *footerView;

/** 时间选择器 */
@property (nonatomic ,strong) BYSelectTimePickerView *timePickerView;

@end

@implementation BYNewLiveViewModel

- (Class)getDataModelClass{
    return [BYNewLiveModel class];
}

- (void)setContentView{
    [S_V_VIEW addSubview:self.tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    // 设置默认不存的直播形式
    S_DM.liveType = -1;
}

#pragma mark - buttonAction
- (void)confirmAction{
    if (!S_DM.liveTheme.length) {
        [S_VC showToastView:@"请填写直播主题"];
        return;
    }
    if (!S_DM.beginTime.length) {
        [S_VC showToastView:@"请选择开始时间"];
        return;
    }
    if ([@(S_DM.liveType) integerValue] == -1) {
        [S_VC showToastView:@"请选择直播形式"];
        return;
    }
    // 发起新建直播请求
    [self loadRequestNewLive];
}

#pragma mark - BYCommonTableViewDelegate
- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    if ([actionName isEqualToString:@"textField_change"]) {
        S_DM.liveTheme = param[@"text"];
    }
}

- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 3) {
        [self.timePickerView showAnimation];
    }
}

#pragma mark - requestMethod
- (void)loadRequestNewLive{
    if (S_DM.liveType == BY_NEWLIVE_TYPE_VIDEO ||
        S_DM.liveType == BY_NEWLIVE_TYPE_RECORD_SCREEN ||
        S_DM.liveType == BY_NEWLIVE_TYPE_UPLOAD_VIDEO) {
        showToastView(@"暂不支持此类型直播创建", S_V_VIEW);
        return;
    }
    BYToastView *toastView = [BYToastView toastViewPresentLoading];
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestNewLive:S_DM.liveTheme beginTime:S_DM.beginTime liveType:S_DM.liveType successBlock:^(id object) {
        @strongify(self);
     
        if (S_DM.liveType == BY_NEWLIVE_TYPE_ILIVE) {
            [self creatIMGroup:object[@"live_record_id"]]; // 创建互动直播群聊id
        }
        else if (S_DM.liveType == BY_NEWLIVE_TYPE_AUDIO_PPT ||
                 S_DM.liveType == BY_NEWLIVE_TYPE_AUDIO_LECTURE ){
            [self creatAliGroup:object[@"live_record_id"]]; // 创建视频音频直播群聊id
        }
        [BYCommonLiveModel shareManager].begin_time = S_DM.beginTime;
        [BYCommonLiveModel shareManager].live_title = S_DM.liveTheme;
        [BYCommonLiveModel shareManager].live_type  = S_DM.liveType;
        BYLiveController *liveController = [[BYLiveController alloc] init];
        [S_V_NC pushViewController:liveController animated:YES];
        [toastView dissmissToastView];
    } faileBlock:^(NSError *error) {
        [toastView dissmissToastView];
    }];
}

// 创建互动直播群聊id
- (void)creatIMGroup:(NSString *)groupId{
    PDLog(@"互动直播聊天室创建groupId:%@",groupId);
    @weakify(self);
    [[BYIMManager sharedInstance] createAVChatRoomGroupId:groupId succ:^{
        @strongify(self);
        PDLog(@"互动直播群聊id创建成功");
        [self updateGroupId:groupId recordId:groupId];
    } fail:nil];
}

// 创建视频音频直播群聊id
- (void)creatAliGroup:(NSString *)record_id{
    @weakify(self);
    [[AliChatManager sharedInstance] createTribeWithSuccessBlock:^(NSString *tribeNumber) {
        @strongify(self);
        PDLog(@"视频音频直播群聊id创建成功");
        [self updateGroupId:tribeNumber recordId:record_id];
    }];
}

- (void)updateGroupId:(NSString *)group_id recordId:(NSString *)recordId{
    [[BYLiveHomeRequest alloc] loadRequestUpdateLiveSet:recordId param:@{@"group_id":group_id} successBlock:^(id object) {
        PDLog(@"同步群聊id成功");
    } faileBlock:nil];
}

#pragma mark - initMethod

- (BYCommonTableView *)tableView{
    if (!_tableView) {
        _tableView = [[BYCommonTableView alloc] init];
        _tableView.group_delegate = self;
        _tableView.tableFooterView = self.footerView;
        _tableView.tableData = S_DM.tableData;
    }
    return _tableView;
}

- (BYNewLiveFooterView *)footerView{
    if (!_footerView) {
        _footerView = [[BYNewLiveFooterView alloc] initWithFrame:CGRectMake(0, 0, kCommonScreenWidth, 497)];
        @weakify(self);
        _footerView.didSelectNewLiveType = ^(BY_NEWLIVE_TYPE type) {
            @strongify(self);
            S_DM.liveType = type;
        };
        _footerView.didConfirmHandle = ^{
            @strongify(self);
            [self confirmAction];
        };
    }
    return _footerView;
}

- (BYSelectTimePickerView *)timePickerView{
    if (!_timePickerView) {
        _timePickerView = [[BYSelectTimePickerView alloc] initWithFathureView:kCommonWindow];
        @weakify(self);
        _timePickerView.didSelectTimeString = ^(NSString *data,NSString *showTimeData) {
            @strongify(self);
            S_DM.beginTime = data;
            BYNewLiveCellModel *model = self.tableView.tableData[3];
            model.textField_text = showTimeData;
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:3 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        };
    }
    return _timePickerView;
}
@end
