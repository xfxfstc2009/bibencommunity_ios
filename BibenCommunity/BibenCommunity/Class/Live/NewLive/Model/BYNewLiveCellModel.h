//
//  BYNewLiveCellModel.h
//  BY
//
//  Created by 黄亮 on 2018/9/7.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCommonModel.h"

@interface BYNewLiveCellModel : BYCommonModel

/**
 按提示
 */
@property (nonatomic ,strong) NSString *placeholder;

/**
 arrowImage
 */
@property (nonatomic ,strong) NSString *rightImageName;

/**
 textField.text
 */
@property (nonatomic ,strong) NSString *textField_text;

/**
 textField.enable
 */
@property (nonatomic ,assign) BOOL textField_enable;
@end
