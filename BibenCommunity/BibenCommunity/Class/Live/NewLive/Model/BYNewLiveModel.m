//
//  BYNewLiveModel.m
//  BY
//
//  Created by 黄亮 on 2018/9/7.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYNewLiveModel.h"
#import "BYNewLiveCellModel.h"

@implementation BYNewLiveModel

- (NSArray *)tableData{
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i < 5; i ++) {
        BYNewLiveCellModel *model = [[BYNewLiveCellModel alloc] init];
        [array addObject:model];
        model.cellString = @"BYNewLiveCell";
        model.cellHeight = 40;
        switch (i) {
            case 0:
                model.title = @"直播主题";
                model.textField_enable = YES;
                model.cellHeight = 40;
                break;
            case 1:
                model.placeholder = @"请输入主题名称...";
                model.cellHeight = 48;
                break;
            case 2:
                model.title = @"开始时间";
                model.cellHeight = 40;
                model.rightImageName = @"common_cell_rightarrow";
                break;
            case 3:
                model.placeholder = @"请选择时间";
                model.textField_enable = NO;
                model.cellHeight = 48;
                break;
            case 4:
                model.title = @"直播形式";
                model.cellHeight = 40;
                break;
            default:
                break;
        }
    }
    return array;
}

@end
