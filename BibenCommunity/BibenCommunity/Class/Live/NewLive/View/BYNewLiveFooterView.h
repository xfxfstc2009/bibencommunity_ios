//
//  BYNewLiveFooterView.h
//  BY
//
//  Created by 黄亮 on 2018/9/7.
//  Copyright © 2018年 BY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BYLiveDefine.h"

@interface BYNewLiveFooterView : UIView

/** 回调选择的直播形式 */
@property (nonatomic ,copy) void(^didSelectNewLiveType)(BY_NEWLIVE_TYPE type);

/** 提交事件回调 */
@property (nonatomic ,copy) void(^didConfirmHandle)(void);

@end
