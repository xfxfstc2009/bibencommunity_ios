//
//  BYNewLiveFooterView.m
//  BY
//
//  Created by 黄亮 on 2018/9/7.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYNewLiveFooterView.h"

@interface BYNewLiveFooterView()

/** 存储选择按钮 */
@property (nonatomic ,strong) NSMutableArray *buttons;

@property (nonatomic ,strong) UIView *infoView;

@property (nonatomic ,strong) UIImageView *infoImageView;

@end

static CGFloat singleViewH = 62.f;
static NSInteger baseTag = 0x494;
@implementation BYNewLiveFooterView

- (instancetype)init
{
    return [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _buttons = [NSMutableArray array];
        [self setBackgroundColor:kBgColor_248];
        [self setContentView];

    }
    return self;
}

- (void)setContentView{
    NSArray *titles = @[@"音频讲座形式",@"音频幻灯片形式",@"个人视频直播形式",@"视频直播形式",@"录屏直播形式",@"音视频录播形式"];
    NSArray *subTitles = @[@"适用于图片较少的分享",@"适用于PPT课件的分享",@"适用于个人手机摄像的分享",@"适用于沙龙、课程有关摄像设备的分享",@"适用于共享电脑屏幕实时讲解的分享",@"适用于上传已制作完成音视频的分享"];
    for (int i = 0 ; i < 6; i ++) {
        UIView *singleView = [self getSingleTypeView:titles[i] subTitle:subTitles[i] index:i];
        [self addSubview:singleView];
        CGFloat topY = 0;
        switch (i) {
            case 0:
                topY = 0;
                break;
            case 1:
                topY = 62;
                break;
            case 2:
                topY = 137;
                break;
            case 3:
                topY = 199;
                break;
            case 4:
                topY = 273;
                break;
            case 5:
                topY = 349;
                break;
            default:
                break;
        }
        [singleView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(kCommonScreenWidth);
            make.height.mas_equalTo(singleViewH);
            make.centerX.mas_equalTo(0);
            make.top.mas_equalTo(topY);
        }];
    }
    
    UIButton *confimBtn = [UIButton  by_buttonWithCustomType];
    [confimBtn setBackgroundColor:kTextColor_238];
    [confimBtn setBy_attributedTitle:@{@"title":@"提交",NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateNormal];
    [confimBtn addTarget:self action:@selector(confimBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [confimBtn layerCornerRadius:10 size:CGSizeMake(kCommonScreenWidth - 30, 46)];
    [self addSubview:confimBtn];
    [confimBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kCommonScreenWidth - 30);
        make.centerX.mas_equalTo(0);
        make.bottom.mas_equalTo(-20);
        make.height.mas_equalTo(46);
    }];
    
}

- (void)selBtnAction:(UIButton *)sender{
    for (UIButton *btn in _buttons) {
        btn.selected = btn == sender ? YES : NO;
    }
    BY_NEWLIVE_TYPE type = sender.tag - baseTag;
    if (_didSelectNewLiveType) {
        _didSelectNewLiveType(type);
    }
}

- (void)infoBtnAction:(UIButton *)sender{
    BY_NEWLIVE_TYPE type = sender.tag - 2*baseTag;
    [self showInfoView:type];
}

- (void)confimBtnAction:(UIButton *)sender{
    if (_didConfirmHandle) {
        _didConfirmHandle();
    }
}

// 获取单个样式view
- (UIView *)getSingleTypeView:(NSString *)title subTitle:(NSString *)subTitle index:(NSInteger)index{
    UIView *view = [UIView by_init];
    [view setBackgroundColor:[UIColor whiteColor]];
    UIButton *button = [UIButton by_buttonWithCustomType];
    [button setBy_imageName:@"creatroom_unselect" forState:UIControlStateNormal];
    [button setBy_imageName:@"creatroom_select" forState:UIControlStateSelected];
    [button addTarget:self action:@selector(selBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:button];
    button.tag = baseTag + index;
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(9);
        make.top.mas_equalTo(12);
        make.width.height.mas_equalTo(33);
    }];
    [_buttons addObject:button];
    
    UILabel *titleLab = [UILabel by_init];
    [titleLab setBy_font:14];
    [titleLab setTextColor:kTextColor_77];
    titleLab.text = title;
    [view addSubview:titleLab];
    CGFloat titleW = stringGetWidth(title, titleLab.font.pointSize);
    CGFloat titleH = stringGetHeight(title, titleLab.font.pointSize);
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(40);
        make.top.mas_equalTo(15);
        make.width.mas_equalTo(titleW);
        make.height.mas_equalTo(titleH);
    }];
    
    UILabel *subTitleLab = [UILabel by_init];
    [subTitleLab setBy_font:13];
    [subTitleLab setTextColor:kTextColor_168];
    subTitleLab.text = subTitle;
    [view addSubview:subTitleLab];
    CGFloat subTitleW = stringGetWidth(subTitle, subTitleLab.font.pointSize);
    CGFloat subTitleH = stringGetHeight(subTitle, subTitleLab.font.pointSize);
    [subTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLab.mas_bottom).with.offset(3);
        make.left.mas_equalTo(40);
        make.width.mas_equalTo(subTitleW);
        make.height.mas_equalTo(subTitleH);
    }];
    
    UIButton *infoBtn = [UIButton by_buttonWithCustomType];
    [infoBtn setBy_imageName:@"newlive_info" forState:UIControlStateNormal];
    [infoBtn addTarget:self action:@selector(infoBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:infoBtn];
    infoBtn.tag = 2*baseTag + index;
    [infoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(21);
        make.right.mas_equalTo(-kCellRightSpace);
        make.centerY.mas_equalTo(0);
    }];
    
    return view;
}

- (void)showInfoView:(BY_NEWLIVE_TYPE)type{
    [kCommonWindow addSubview:self.infoView];
    NSString *imageName = @"";
    switch (type) {
        case BY_NEWLIVE_TYPE_AUDIO_LECTURE:
            imageName = @"newlive_audio_show";
            break;
        case BY_NEWLIVE_TYPE_AUDIO_PPT:
            imageName = @"newlive_ppt_show";
            break;
        case BY_NEWLIVE_TYPE_ILIVE:
            imageName = @"newlive_ilive_show";
            break;
        case BY_NEWLIVE_TYPE_VIDEO:
            imageName = @"newlive_video_show";
            break;
        case BY_NEWLIVE_TYPE_RECORD_SCREEN:
            imageName = @"newlive_record_screen_show";
            break;
        case BY_NEWLIVE_TYPE_UPLOAD_VIDEO:
            imageName = @"newlive_upload_video_show";
            break;
        default:
            break;
    }
    [self updateConstraintsIfNeeded];
    [UIView animateWithDuration:0.1 animations:^{
        @weakify(self);
        [self.infoImageView setImage:[UIImage imageNamed:imageName]];
        self.infoView.alpha = 1.0;
        @strongify(self);
        [self.infoImageView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(self.infoImageView.image.size.width);
            make.height.mas_equalTo(self.infoImageView.image.size.height);
        }];
        [self layoutIfNeeded];
    }];
}

- (void)hiddenInfoViewAnimation{
    [UIView animateWithDuration:0.1 animations:^{
        self.infoView.alpha = 0;
    }completion:^(BOOL finished) {
        [self.infoView removeFromSuperview];
    }];
}

- (UIView *)infoView{
    if (!_infoView) {
        _infoView = [UIView by_init];
        _infoView.alpha = 0.0;
        _infoView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.47];
        _infoView.frame = CGRectMake(0, 0, kCommonScreenWidth, kCommonScreenHeight);
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenInfoViewAnimation)];
        [_infoView addGestureRecognizer:tapGestureRecognizer];
        
        UIImageView *imageView = [[UIImageView alloc] init];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [_infoView addSubview:imageView];
        _infoImageView = imageView;
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.centerY.mas_equalTo(0);
            make.width.height.mas_greaterThanOrEqualTo(0);
        }];
        
    }
    return _infoView;
}

@end
