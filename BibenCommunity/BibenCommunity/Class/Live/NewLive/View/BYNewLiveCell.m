//
//  BYNewLiveCell.m
//  BY
//
//  Created by 黄亮 on 2018/9/7.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYNewLiveCell.h"
#import "BYNewLiveCellModel.h"

@interface BYNewLiveCell ()<UITextFieldDelegate>

@property (nonatomic ,strong) BYNewLiveCellModel *model;

@end

@implementation BYNewLiveCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYNewLiveCellModel *model = object[indexPath.row];
    _model = model;
    self.indexPath = indexPath;
    switch (indexPath.row) {
        case 1:
        case 3:
        {
            [self setPlaceholder:model.placeholder];
            [self setTextFieldText:model.textField_text];
            self.textField.enabled = indexPath.row == 1 ? YES : NO;
            [self setRightImage:indexPath.row == 1 ? nil : [UIImage imageNamed:@"common_cell_rightarrow"]];
            [self.contentView setBackgroundColor:[UIColor whiteColor]];
            self.textField.delegate = self;
        }
            break;
        case 0:
        case 2:
        case 4:
            [self setTitle:model.title];
            [self.contentView setBackgroundColor:kBgColor_248];
            break;
        default:
            break;
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *rangString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (rangString.length > 30) { // 当直播主题输入30字符以上时禁止继续输入
        return NO;
    }
    _model.textField_text = rangString;
    [self sendActionName:@"textField_change" param:@{@"text":nullToEmpty(rangString)} indexPath:self.indexPath];
    return YES;
}

@end
