//
//  BYNewLiveController.m
//  BY
//
//  Created by 黄亮 on 2018/9/7.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYNewLiveController.h"
#import "BYNewLiveViewModel.h"

@interface BYNewLiveController ()

@end

@implementation BYNewLiveController

- (Class)getViewModelClass{
    return [BYNewLiveViewModel class];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationTitle = @"新建直播";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
