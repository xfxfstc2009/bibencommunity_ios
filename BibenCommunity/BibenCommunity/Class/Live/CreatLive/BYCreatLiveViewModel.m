//
//  BYCreatLiveViewModel.m
//  BY
//
//  Created by 黄亮 on 2018/8/13.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCreatLiveViewModel.h"
#import <AVFoundation/AVFoundation.h>
#import "BYAVMediaUtil.h"
#import "BYLiveHomeRequest.h"

#import "BYILiveRoomController.h"

@interface BYCreatLiveViewModel()<UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic ,strong) UIImageView *imageView;

@property (nonatomic ,strong) UITextField *textField;

@property (nonatomic ,strong) UIButton *qualityBtn;

@property (nonatomic ,strong) BYLiveConfig *config;

@end

@implementation BYCreatLiveViewModel


- (void)setContentView{
    self.config = [[BYLiveConfig alloc] init];
    
    // 封面图
    UIImageView *imageView = [[UIImageView alloc] init];
    [S_V_VIEW addSubview:imageView];
    _imageView = imageView;
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(0);
        make.height.mas_equalTo(230);
    }];
    
    // 选取图片
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:@"设置一张封面图" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonAction) forControlEvents:UIControlEventTouchUpInside];
    [S_V_VIEW addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(button.titleLabel.text, button.titleLabel.font.pointSize));
        make.height.mas_equalTo(stringGetHeight(button.titleLabel.text, button.titleLabel.font.pointSize));
        make.centerY.equalTo(imageView.mas_centerY).with.offset(0);
        make.centerX.equalTo(imageView.mas_centerX).with.offset(0);
    }];
    
    UITextField *textField = [[UITextField alloc] init];
    textField.delegate = self;
    textField.placeholder = @"请输入直播标题";
    [S_V_VIEW addSubview:textField];
    _textField = textField;
    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imageView.mas_bottom).with.offset(0);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(20);
    }];
    
    UIButton *qualityBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [qualityBtn setTitle:@"选择直播清晰度" forState:UIControlStateNormal];
    [qualityBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [qualityBtn addTarget:self action:@selector(qualityBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [S_V_VIEW addSubview:qualityBtn];
    _qualityBtn = qualityBtn;
    [qualityBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(100);
        make.top.equalTo(textField.mas_bottom).with.offset(30);
        make.centerX.mas_equalTo(0);
    }];
    
    UIButton *confirmBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [confirmBtn setTitle:@"发布直播" forState:UIControlStateNormal];
    [confirmBtn setBackgroundColor:kColorRGBValue(0xed4a45)];
    [confirmBtn addTarget:self action:@selector(confirmBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [S_V_VIEW addSubview:confirmBtn];
    [confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(50);
    }];
}

- (void)buttonAction{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"选择封面图" message:nil preferredStyle:0];
    @weakify(self);
    UIAlertAction *cancleAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self);
        [self openCamera];
        
    }];
    UIAlertAction *action2= [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self);
        [self openPhotoLibrary];
    }];
    [alertController addAction:cancleAction];
    [alertController addAction:action1];
    [alertController addAction:action2];
    [self.viewController presentViewController:alertController animated:YES completion:nil];
}

- (void)qualityBtnAction{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"选择直播清晰度" message:nil preferredStyle:0];
    @weakify(self);
    UIAlertAction *cancleAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    UIAlertAction *acrAction = [UIAlertAction actionWithTitle:@"超清" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self);
        self.config.liveQuality = kRole_HostUSD;
        [self.qualityBtn setTitle:@"超清" forState:UIControlStateNormal];

    }];
    UIAlertAction *hdAction = [UIAlertAction actionWithTitle:@"高清" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self);
        self.config.liveQuality = kRole_HostHD;
        [self.qualityBtn setTitle:@"高清" forState:UIControlStateNormal];
    }];
    UIAlertAction *sdAction = [UIAlertAction actionWithTitle:@"标清" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self);
        self.config.liveQuality = kRole_HostSD;
        [self.qualityBtn setTitle:@"标清" forState:UIControlStateNormal];
    }];

    [alertController addAction:cancleAction];
    [alertController addAction:acrAction];
    [alertController addAction:hdAction];
    [alertController addAction:sdAction];
    [self.viewController presentViewController:alertController animated:YES completion:nil];
}

- (void)confirmBtnAction{
    if ([BYAVMediaUtil verifyAVAuthorization:BYAVMedioTypeVideo || BYAVMedioTypeAudio]) {
        // 开始获取房间号
        [self loadRequestCreatLive];
    }
}

// 打开相机
- (void)openCamera{
    // 打开系统相机拍照
    // 相机权限判断
    if(![BYAVMediaUtil verifyAVAuthorization:BYAVMedioTypeVideo]) return;

    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *cameraIPC = [[UIImagePickerController alloc] init];
        cameraIPC.delegate = self;
        cameraIPC.allowsEditing = YES;
        cameraIPC.navigationBar.translucent=NO;
        cameraIPC.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self.viewController presentViewController:cameraIPC animated:YES completion:nil];
        return;
    }
}

// 打开相册
- (void)openPhotoLibrary{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        imagePicker.navigationBar.translucent=NO;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self.viewController presentViewController:imagePicker animated:YES completion:nil];
        return;
    }
}

#pragma mark - requestMethod
// 获取直播房间号
- (void)loadRequestCreatLive{
    [[BYLiveHomeRequest alloc] loadRequestCreatLiveWithUserId:K_C_U_M.user_id
                                                     coverUrl:@"https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=3763883972,772097188&fm=200&gp=0.jpg"
                                                        title:_textField.text
                                                      message:@""
                                                    beginTime:@""
                                                    organType:BY_LIVE_ORGAN_TYPE_PERSON
                                                     liveType:BY_LIVE_TYPE_VIDEO
                                                 successBlock:^(id object) {
                                                     
                                                 } faileBlock:^(NSError *error) {
                                                     
                                                 }];
    self.config.isHost = YES;
    self.config.roomId = stringFormatInteger(K_C_U_M.user_id);
    BYILiveRoomController *liveRoomController = [[BYILiveRoomController alloc] initWithConfig:self.config];
    [S_V_NC pushViewController:liveRoomController animated:YES];
}


#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return YES;
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerEditedImage];
    UIImage *cutImage = [image cutImageSize:CGSizeMake(kCommonScreenWidth, 230)];
    _imageView.image = cutImage;
    
    //如果是相机拍照，则保存到相册
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
    {
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}
@end
