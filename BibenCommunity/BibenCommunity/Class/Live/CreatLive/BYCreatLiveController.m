//
//  BYCreatLiveController.m
//  BY
//
//  Created by 黄亮 on 2018/8/13.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCreatLiveController.h"
#import "BYCreatLiveViewModel.h"
#import <ILiveSDK/ILiveLoginManager.h>
@interface BYCreatLiveController ()

@end

@implementation BYCreatLiveController

- (Class)getViewModelClass{
    return [BYCreatLiveViewModel class];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationTitle = @"创建直播间";
    
    UIBarButtonItem *rightBarButtonItem = [UIBarButtonItem initWithTitle:@"提交" target:self action:@selector(rightBarButtonItemAction) type:BYBarButtonItemTypeRight];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    // Do any additional setup after loading the view.
}

- (void)rightBarButtonItemAction{
//    @weakify(self);
//    [[ILiveLoginManager getInstance] iLiveLogout:^{
//        @strongify(self);
//        [self dismissViewControllerAnimated:YES completion:nil];
//    } failed:^(NSString *module, int errId, NSString *errMsg) {
//        showToastView(@"互动直播退出失败！", kCommonWindow);
//    }];
    [self dismissViewControllerAnimated:YES completion:nil];

}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
