//
//  BYLiveHomeRequest.m
//  BY
//
//  Created by 黄亮 on 2018/7/27.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYLiveHomeRequest.h"
#import "BYLiveRPCDefine.h"

#import "BYLiveHomeVideoModel.h"
#import "BYCommonLiveModel.h"
#import "BYHomeBannerModel.h"
@implementation BYLiveHomeRequest

- (void)loadRequestLiveHome:(BY_LIVE_HOME_LIST_TYPE)listType
                    pageNum:(NSInteger)pageNum
                   sortType:(BY_SORT_TYPE)sortType
               successBlock:(void(^)(id object))successBlock
                 faileBlock:(void(^)(NSError *error))faileBlock{
    NSString *sortValue = sortType == BY_SORT_TYPE_HOT ? @"watch_times" : (sortType == BY_SORT_TYPE_TIME ? @"begin_time" : @"");
    NSDictionary *param = @{@"list_type":@(listType),@"page_number":[NSNumber numberWithInteger:pageNum],@"page_size":@(10),@"sort_param":sortValue};
    [BYRequestManager ansyRequestWithURLString:RPC_get_all_live_list parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        NSArray *data = [[BYLiveHomeVideoModel alloc] getRespondData:result type:listType];
        if (successBlock) successBlock(data);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadGetUploadVodSignatureSuccessBlock:(void(^)(id object))successBlock
                                   faileBlock:(void(^)(NSError *error))faileBlock{
    [BYRequestManager ansyRequestWithURLString:RPC_Get_upload_signature parameters:nil operationType:BYOperationTypePost successBlock:^(id result) {
        NSString *sign = result[@"data"][@"sign"];
        if (successBlock) successBlock(sign);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)uploadVodInfoWithUserId:(NSInteger)userId
                          title:(NSString *)title
                       videoUrl:(NSString *)videoUrl
                        videoId:(NSString *)videoId
                       coverUrl:(NSString *)coverUrl
                   successBlock:(void(^)(id object))successBlock
                     faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"user_id":[NSNumber numberWithInteger:userId],
                            @"video_url":nullToEmpty(videoUrl),
                            @"file_id":nullToEmpty(videoId),
                            @"cover_Url":nullToEmpty(coverUrl),
                            @"title":nullToEmpty(title)
                            };
    [BYRequestManager ansyRequestWithURLString:RPC_insert_vod_record parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestCreatLiveWithUserId:(NSInteger)userId
                              coverUrl:(NSString *)coverUrl
                                 title:(NSString *)title
                               message:(NSString *)message
                             beginTime:(NSString *)beginTime
                             organType:(BY_LIVE_ORGAN_TYPE)organType
                              liveType:(BY_LIVE_TYPE)liveType
                          successBlock:(void(^)(id object))successBlock
                            faileBlock:(void(^)(NSError *error))faileBlock{
    NSString *type = liveType == BY_LIVE_TYPE_AUDIO ? @"audio" : (liveType == BY_LIVE_TYPE_PPT ? @"ppt" : @"video");
    NSDictionary *param = @{@"user_id":[NSNumber numberWithInteger:userId],
                            @"coverUrl":nullToEmpty(coverUrl),
                            @"title":nullToEmpty(title),
                            @"subtitle":nullToEmpty(message),
                            @"beginTime":nullToEmpty(beginTime),
                            @"organType":[NSNumber numberWithInteger:organType],
                            @"typeType":type,
                            @"status":@"WAITING"
                            };
    [BYRequestManager ansyRequestWithURLString:RPC_create_live_room parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result[@"data"][@"roomId"]);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestUpdateLiveStatus:(BY_LIVE_STATUS)status
                                sig:(NSString *)sig
                     live_record_id:(NSString *)live_record_id
                       successBlock:(void(^)(id object))successBlock
                         faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"sig":nullToEmpty(sig),
                            @"status":@(status),
                            @"live_record_id":nullToEmpty(live_record_id)
                            };
    [BYRequestManager ansyRequestWithURLString:RPC_switch_live parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestGetILiveLoginSigSuccessBlock:(void(^)(id object))successBlock
                                     faileBlock:(void(^)(NSError *error))faileBlock{
    [BYRequestManager ansyRequestWithURLString:RPC_get_user_sig parameters:nil operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result[@"userSig"]);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

// --------------------------

- (void)loadRequestCreatLiveRoom:(NSString *)roomTitle
                          weChat:(NSString *)weChat
                       organType:(BY_LIVE_ORGAN_TYPE)organType
                    successBlock:(void(^)(id object))successBlock
                      faileBlock:(void(^)(NSError *error))faileBlock{
    NSString *defultRoomBg = [NSString stringWithFormat:@"defult_liveroom_bg_%02d",arc4random()%6];
    NSDictionary *param = @{@"room_title":roomTitle,
                            @"wechat":weChat,
                            @"organ_type":@(organType),
                            @"cover_url":defultRoomBg};
    [BYRequestManager ansyRequestWithURLString:RPC_create_live_room parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result[@"roomId"]);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestNewLive:(NSString *)title
                 beginTime:(NSString *)beginTime
                  liveType:(BY_NEWLIVE_TYPE)liveType
              successBlock:(void(^)(id object))successBlock
                faileBlock:(void(^)(NSError *error))faileBlock{
    NSString *defultRoomBg = [NSString stringWithFormat:@"defult_live_bg_%02d",arc4random()%6];
    NSDictionary *param = @{@"live_title":nullToEmpty(title),
                            @"begin_time":nullToEmpty(beginTime),
                            @"live_type":@(liveType),
                            @"live_cover_url":defultRoomBg
                            };
    [BYRequestManager ansyRequestWithURLString:RPC_create_live_record parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadReuqestUpdateLiveRoom:(NSDictionary *)param
                     successBlock:(void(^)(id object))successBlock
                       faileBlock:(void(^)(NSError *error))faileBlock{
    NSArray *allKeys = @[@"room_title",@"room_intro",@"cover_url",@"organ_type",@"live_title",@"live_intro",@"speaker",@"speaker_head_img",@"ad_img",@"begin_time",@"live_cover_url"];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [allKeys enumerateObjectsUsingBlock:^(NSString *key, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([param.allKeys indexOfObject:key] != NSNotFound) {
            [params setObject:param[key] forKey:key];
        }
        else
        {
            [params setObject:key forKey:key];
        }
    }];
    [BYRequestManager ansyRequestWithURLString:RPC_update_live_room parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

// 更新直播信息
- (void)loadRequestUpdateLiveSet:(NSString*)liveId
                           param:(NSDictionary *)param
                    successBlock:(void(^)(id object))successBlock
                      faileBlock:(void(^)(NSError *error))faileBlock{
    NSArray *allKeys = @[@"live_title",@"live_intro",@"speaker",@"speaker_head_img",@"ad_img",@"begin_time",@"live_cover_url"];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:nullToEmpty(liveId) forKey:@"live_record_id"];
    [allKeys enumerateObjectsUsingBlock:^(NSString *key, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([param.allKeys indexOfObject:key] != NSNotFound) {
            [params setObject:param[key] forKey:key];
        }
        else
        {
            [params setObject:key forKey:key];
        }
    }];
    [param.allKeys enumerateObjectsUsingBlock:^(NSString *key, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([allKeys indexOfObject:key] == NSNotFound) {
            [params setObject:param[key] forKey:key];
        }
    }];
    [BYRequestManager ansyRequestWithURLString:RPC_update_live_record parameters:params operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}


- (void)loadRequestGetLiveRoomInfoSuccessBlock:(void(^)(id object))successBlock
                                    faileBlock:(void(^)(NSError *error))faileBlock{
    [BYRequestManager ansyRequestWithURLString:RPC_get_live_room parameters:nil operationType:BYOperationTypePost successBlock:^(id result) {
        BYCommonLiveModel *liveModel = [BYCommonLiveModel shareManager];
        id keyValues = [result mj_JSONObject];
        liveModel = [liveModel mj_setKeyValues:keyValues];
        liveModel.room_id = liveModel.user_id;
        if (successBlock) successBlock(liveModel);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestGetLivelist:(NSInteger)pageNum
                  successBlock:(void(^)(id object))successBlock
                    faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"page_number":@(pageNum),@"page_size":@(10)};
    [BYRequestManager ansyRequestWithURLString:RPC_get_live_list parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        NSMutableArray *tableData = [NSMutableArray array];
        if ([result[@"content"] count]) {
            NSArray *tmpArr = (NSArray *)result[@"content"];
            [tmpArr enumerateObjectsUsingBlock:^(NSDictionary *dic, NSUInteger idx, BOOL * _Nonnull stop) {
                BYCommonLiveModel *liveModel = [BYCommonLiveModel mj_objectWithKeyValues:dic];
                liveModel.cellString = @"BYLiveRoomHomeCell";
                liveModel.cellHeight = 118;
                [tableData addObject:liveModel];
            }];
        }
        if (successBlock) successBlock(tableData);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestGetHomeBannerSuccessBlock:(void(^)(id object))successBlock
                                  faileBlock:(void(^)(NSError *error))faileBlock{
    [BYRequestManager ansyRequestWithURLString:RPC_get_banner_list parameters:nil operationType:BYOperationTypePost successBlock:^(id result) {
        NSArray *data = [BYHomeBannerModel getBannerData:result];
        if (successBlock) successBlock(data);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadReuqestIsHaveLiveRoomSuccessBlock:(void(^)(id object))successBlock
                                   faileBlock:(void(^)(NSError *error))faileBlock{
    [BYRequestManager ansyRequestWithURLString:RPC_whether_has_room parameters:nil operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

@end
