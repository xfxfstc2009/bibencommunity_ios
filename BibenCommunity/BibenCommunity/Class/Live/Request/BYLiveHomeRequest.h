//
//  BYLiveHomeRequest.h
//  BY
//
//  Created by 黄亮 on 2018/7/27.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYRequestManager.h"
#import "BYLiveConfig.h"
#import "BYLiveDefine.h"

@interface BYLiveHomeRequest : BYRequestManager

/**
 获取直播首页数据

 @param listType 分类列表
 @param pageNum 页码
 @param sortType 排序
 */
- (void)loadRequestLiveHome:(BY_LIVE_HOME_LIST_TYPE)listType
                    pageNum:(NSInteger)pageNum
                   sortType:(BY_SORT_TYPE)sortType
               successBlock:(void(^)(id object))successBlock
                 faileBlock:(void(^)(NSError *error))faileBlock;


/**
 获取视频上传凭证

 @param successBlock successBlock description
 @param faileBlock faileBlock description
 */
- (void)loadGetUploadVodSignatureSuccessBlock:(void(^)(id object))successBlock
                                   faileBlock:(void(^)(NSError *error))faileBlock;


/**
 上传视频成功后同步到服务端


 @param userId 用户id
 @param title 视频标标题
 @param videoUrl 视频连接
 @param videoId 视频id（腾讯云返回的filedId）
 @param coverUrl 封面图url
 @param successBlock successBlock description
 @param faileBlock faileBlock description
 */
- (void)uploadVodInfoWithUserId:(NSInteger)userId
                          title:(NSString *)title
                       videoUrl:(NSString *)videoUrl
                        videoId:(NSString *)videoId
                       coverUrl:(NSString *)coverUrl
                   successBlock:(void(^)(id object))successBlock
                     faileBlock:(void(^)(NSError *error))faileBlock;


/**
 直播创建

 @param userId 用户id
 @param coverUrl 封面图
 @param title 标题
 @param message 内容
 @param beginTime 开始时间
 @param organType 组织形式
 @param liveType 直播形式
 @param successBlock successBlock description
 @param faileBlock faileBlock description
 */
- (void)loadRequestCreatLiveWithUserId:(NSInteger)userId
                              coverUrl:(NSString *)coverUrl
                                 title:(NSString *)title
                               message:(NSString *)message
                             beginTime:(NSString *)beginTime
                             organType:(BY_LIVE_ORGAN_TYPE)organType
                              liveType:(BY_LIVE_TYPE)liveType
                          successBlock:(void(^)(id object))successBlock
                            faileBlock:(void(^)(NSError *error))faileBlock;


/**
 更新直播状态

 @param sig 直播登录凭证
 @param status 直播状态
 @param successBlock successBlock description
 @param faileBlock faileBlock description
 */
- (void)loadRequestUpdateLiveStatus:(BY_LIVE_STATUS)status
                                sig:(NSString *)sig
                     live_record_id:(NSString *)live_record_id
                       successBlock:(void(^)(id object))successBlock
                         faileBlock:(void(^)(NSError *error))faileBlock;


/** 获取直播登录的sig凭证 */
- (void)loadRequestGetILiveLoginSigSuccessBlock:(void(^)(id object))successBlock
                                     faileBlock:(void(^)(NSError *error))faileBlock;
;
#pragma mark - new

/**
 创建直播间
 
 @param roomTitle 直播间主题
 @param weChat 联系微信
 @param organType 身份
 @param successBlock successBlock description
 @param faileBlock faileBlock description
 */
- (void)loadRequestCreatLiveRoom:(NSString *)roomTitle
                          weChat:(NSString *)weChat
                       organType:(BY_LIVE_ORGAN_TYPE)organType
                    successBlock:(void(^)(id object))successBlock
                      faileBlock:(void(^)(NSError *error))faileBlock;

/**
 新建直播

 @param title 直播主题
 @param beginTime 开始时间
 @param liveType 直播形式
 @param successBlock successBlock description
 @param faileBlock faileBlock description
 */
- (void)loadRequestNewLive:(NSString *)title
                 beginTime:(NSString *)beginTime
                  liveType:(BY_NEWLIVE_TYPE)liveType
              successBlock:(void(^)(id object))successBlock
                faileBlock:(void(^)(NSError *error))faileBlock;

/** 更新直播间信息 */
- (void)loadReuqestUpdateLiveRoom:(NSDictionary *)param
                     successBlock:(void(^)(id object))successBlock
                       faileBlock:(void(^)(NSError *error))faileBlock;

/** 更新直播信息 */
- (void)loadRequestUpdateLiveSet:(NSString*)liveId
                           param:(NSDictionary *)param
                    successBlock:(void(^)(id object))successBlock
                      faileBlock:(void(^)(NSError *error))faileBlock;

/** 获取直播间信息 */
- (void)loadRequestGetLiveRoomInfoSuccessBlock:(void(^)(id object))successBlock
                                    faileBlock:(void(^)(NSError *error))faileBlock;

/** 获取个人直播记录 */
- (void)loadRequestGetLivelist:(NSInteger)pageNum
                  successBlock:(void(^)(id object))successBlock
                    faileBlock:(void(^)(NSError *error))faileBlock;

/** 获取首页banner数据 */
- (void)loadRequestGetHomeBannerSuccessBlock:(void(^)(id object))successBlock
                                  faileBlock:(void(^)(NSError *error))faileBlock;

/** 判断是否有直播间 */
- (void)loadReuqestIsHaveLiveRoomSuccessBlock:(void(^)(id object))successBlock
                                   faileBlock:(void(^)(NSError *error))faileBlock;

@end
