//
//  AliChatManager.h
//  SmartMin
//
//  Created by 裴烨烽 on 16/1/25.
//  Copyright © 2016年 SmartMin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WXOpenIMSDKFMWK/YWFMWK.h>
#import <WXOUIModule/YWUIFMWK.h>
#import <WXOUIModule/YWIndicator.h>
#import <objc/runtime.h>
#import <WXOpenIMSDKFMWK/YWTribeSystemConversation.h>
#import "GWCurrentIMViewController.h"

@protocol AliChatManagerDelegate <NSObject>
-(void)openProfileWithPersonId:(NSString *)personId;
@optional
-(void)listenNewMessageManagerWithMessage:(id<IYWMessage>)message;
@end

@interface AliChatManager : NSObject

+ (instancetype)sharedInstance;
@property (nonatomic,weak)id<AliChatManagerDelegate> messageDelegate;

@property (strong, nonatomic, readwrite) YWIMKit *ywIMKit;
@property (nonatomic, assign) YWIMConnectionStatus lastConnectionStatus;

#pragma mark - 1.Launch初始化页面进行调用
- (void)callThisInDidFinishLaunching;

#pragma mark - 2.1 登录注册
-(void)loginWithUserName:(NSString *)userName password:(NSString *)password preloginedBlock:(void(^)())aPreloginedBlock successBlock:(void(^)())aSuccessBlock failedBlock:(void (^)(NSError *))aFailedBlock ;

#pragma mark - 2.2 登出
-(void)aliLogoutWithBlock:(void (^)())logoutBlock;


#pragma mark 打开单聊页面
-(void)openConversationWithPerson:(YWPerson *)person controllerClass:(Class)controllerClass callBack:(void(^)(YWConversationViewController *conversationController))block;
- (void)exampleOpenConversationViewControllerWithPerson:(YWPerson *)aPerson fromNavigationController:(UINavigationController *)aNavigationController callBcak:(void (^)(YWConversationViewController *conversationController))block;

#pragma mark - 创建群聊页面
-(void)createTribeWithSuccessBlock:(void(^)(NSString *tribeNumber))block;
#pragma mark - 加入群
-(void)joinTribeWithNumber:(NSString *)tribeNumber fromNav:(UINavigationController *)nav controller:(Class)controller callBcak:(void (^)(YWConversationViewController *conversationController))block;

// 删除某个回话
-(void)deleteConversationWithId:(NSString *)conversationId block:(void(^)(NSError *error))block;

@end

