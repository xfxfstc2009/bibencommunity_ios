//
//  MainLinkConstants.h
//  BitbenLibrary
//
//  Created by 裴烨烽 on 2018/8/21.
//  Copyright © 2018年 币本. All rights reserved.
//

#ifndef MainLinkConstants_h
#define MainLinkConstants_h

#import <UIKit/UIKit.h>
#import <objc/runtime.h>
#import <Lottie/Lottie.h>
#import "Constants.h"
#import "URLConstance.h"                                // 【网络配置】
#import "PDWebViewController.h"

// 【Main】
#import "SBJSON.h"
#import "PDImageView.h"
#import "GWViewTool.h"
#import "CategoryStaticConstans.h"
#import "ViewConstance.h"
#import "Tool.h"

// Root
#import "BYTabbarViewController.h"

#import "AccountModel.h"
#import "ShareSDKManager.h"
#import "LoginRootViewController.h"
#import "GWAssetsLibraryViewController.h"
#import "OSSManager.h"
#import "BYAlertView.h"
#import "PDSelectionListTitleView.h"

#import "AliChatManager.h"

#import "BYLiveHomeController.h"                // 首页
#import "CenterRootViewController.h"            // 个人中心
#import "CenterMessageRootViewController.h"     // 消息
#import "BYSchoolController.h"                  // 内容
#import "LivePPTRootViewController.h"


// 【app配置】
#define User_Choose_Game @"User_Choose_Game"
#define TestNet_Log @"testNet_Log"

#endif /* MainLinkConstants_h */

