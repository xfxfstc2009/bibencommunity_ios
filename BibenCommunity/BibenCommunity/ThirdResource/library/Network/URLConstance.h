//
//  URLConstance.h
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/10/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#ifndef URLConstance_h
#define URLConstance_h

// 【接口环境】
#ifdef DEBUG        // 测试
//#define JAVA_Host @"47.97.211.170"
//#define JAVA_Port @"9500"
//#define JAVA_Host @"192.168.31.61"
//#define JAVA_Port @"9500"
//#define JAVA_Host @"192.168.31.62"
//#define JAVA_Port @"8080"
#define JAVA_Host @"47.97.211.170"
#define JAVA_Port @"9500"
#else               // 线上
#define JAVA_Host @"47.97.211.170"
#define JAVA_Port @"9500"
#endif

// 【配置】
static NSString *const ErrorDomain = @"ErrorDomain";
static BOOL NetLogAlert = YES;                                                  /**< 网络输出*/
static NSInteger TimeoutInterval = 15;                                          /**< 短链接失效时间*/

// 【长链接】
static BOOL isHeartbeatPacket = NO;                                            /**< 是否心跳包*/
static BOOL isSocketLogAlert = NO;                                             /**< 是否socket*/

#ifdef DEBUG
#define PDSocketDebug
#endif

#ifdef PDSocketDebug
#define PDSocketLog(format, ...) NSLog(format, ## __VA_ARGS__)
#else
#define PDSocketLog(format, ...)
#endif

// 【login】
static NSString *login_register = @"register";                                 // 1.注册
static NSString *login_login = @"login";                                       // 1.登录
static NSString *login_getAccount = @"get_account_by_id";                      // 根据账户ID获取账户信息
static NSString *login_getAccountByPhone = @"get_account_by_phone";            // 根据账户手机获取账户信息
static NSString *login_updatePwd = @"update_password_by_phone";                // 修改密码
static NSString *login_updatePhone = @"update_phone";                          // 修改手机号
static NSString *login_SMS = @"send_register_code";                            // 注册 发送验证码
static NSString *login_send_password_code = @"send_password_code";
static NSString *login_validateSMS = @"validate_sms_code";                     // 验证验证码
static NSString *login_updateAvatar = @"update_head_img";                      // 修改头像
static NSString *login_getUserInfo = @"get_user_and_account";                  // 获取当前账户信息
static NSString *login_updateAccountInfo = @"update_user_and_account";         // 修改信息
static NSString *login_other_login = @"other_login";                           // 三方登录


// 【center】
static NSString *center_root = @"get_user_count";                                // 获取用户个人中心数据统计等相关信息
static NSString *center_attendance = @"attendance";                              // 签到领BBT
static NSString *center_get_finance = @"get_finance";                            // 获取注册奖励
static NSString *center_invitation = @"get_my_invitation";                       // 获取我的邀请
static NSString *center_invitation_history = @"page_invitation_history";         // 我的邀请历史
static NSString *center_page_attention = @"page_attention";                      // 获取我关注的用户
static NSString *center_page_fans = @"page_fans";                                // 获取我的粉丝
static NSString *center_create_attention = @"create_attention";                  // 关注
static NSString *center_create_cancelAttention = @"delete_attention";            // 取消关注
static NSString *center_page_finance_history = @"page_finance_history";          // 获取我的资金流水历史
static NSString *center_page_message = @"page_message";                          // 我的消息
static NSString *center_cleanMessage = @"delete_all_message";                    // 清空我的消息
static NSString *center_read = @"update_message_read";                           // 我的消息已读
static NSString *center_get_invitation_url = @"get_invitation_url";             // 分享

// 【Live】
static NSString *live_ppt_main_info = @"get_live_room";                         // 获取直播间信息
static NSString *live_ppt_create_comment = @"create_comment";                   // 添加评论
static NSString *live_ppt_comment_operate = @"create_comment_operate";          // 顶踩赞;
#endif /* URLConstance_h */

