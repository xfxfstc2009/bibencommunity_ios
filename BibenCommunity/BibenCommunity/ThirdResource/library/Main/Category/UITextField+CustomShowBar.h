//
//  UITextField+CustomShowBar.h
//  RegenerationPlan
//
//  Created by SmartMin on 15/11/12.
//  Copyright © 2015年 baimifan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (CustomShowBar)<UITextFieldDelegate>

-(void)showBarCallback:(void(^)(UITextField *textField,NSInteger buttonIndex,UIButton *button))callBack;

@property (nonatomic,strong)UITextField *inputTextField;

@end
