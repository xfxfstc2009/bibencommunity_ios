//
//  UICollectionViewCell+PDSeparatorLine.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger,CollectionSeparatorType)
{
    CollectionSeparatorTypeHead    =   1,
    CollectionSeparatorTypeMiddle      ,
    CollectionSeparatorTypeBottom      ,
    CollectionSeparatorTypeSingle      ,
};

@interface UICollectionViewCell (PDSeparatorLine)

- (void)addSeparatorLineWithType:(CollectionSeparatorType)separatorType;

@end
