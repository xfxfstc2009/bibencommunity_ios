//
//  NSArray+SMart.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/18.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (SMart)

- (NSString *)mmh_JSONString;

@end
