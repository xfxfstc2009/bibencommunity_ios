//
//  UINavigationController+PDBackGesture.h
//  PandaKing
//
//  Created by GiganticWhale on 16/10/16.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (PDBackGesture)

@property (assign,nonatomic) BOOL enableBackGesture;

@end
