//
//  PDNavigationController.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/6/6.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDNavigationController.h"

@interface PDNavigationController()<UINavigationControllerDelegate,UINavigationBarDelegate>

@end

@implementation PDNavigationController

-(instancetype)initWithRootViewController:(UIViewController *)rootViewController{
    self = [super initWithRootViewController:rootViewController];
    if (self){
        [self.navigationBar setBarStyle:UIBarStyleBlack];
        self.navigationBar.tintColor = [UIColor blackColor];
    }
    return self;
}

//-(void)pushViewController:(nonnull UIViewController *)controller animated:(BOOL)animation{
//    [[PDMainTabbarViewController sharedController] setBarHidden:YES animated:YES];
//    [self pushViewController:controller animated:YES];
//}

//-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
//    return [self.topViewController shouldAutorotateToInterfaceOrientation:toInterfaceOrientation];
//}
//
//// 6.0 之后系统调用方法
//-(BOOL)shouldAutorotate{
////    系统会调用跟视图的旋转控制方法，所以我们将跟视图将控制条件交给顶层视图（顶层视图即我们需要控制的视图）
////    系统调用该方法
//    return self.topViewController.shouldAutorotate;
//}
//
//-(UIInterfaceOrientationMask)supportedInterfaceOrientations {
//    return self.topViewController.supportedInterfaceOrientations;
//}


@end
