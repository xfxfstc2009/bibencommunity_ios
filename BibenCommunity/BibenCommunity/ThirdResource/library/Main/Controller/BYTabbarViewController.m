//
//  BYTabbarViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/25.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "BYTabbarViewController.h"
#import "BYNavigationController.h"
#import "PDGradientNavBar.h"

@interface BYTabbarViewController ()<ZYTabBarDelegate>

@end

@implementation BYTabbarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createCustomerTabBar];
    [self pageSetting];
    
    [self.customerTabBar addCenterBtn];
    self.customerTabBar.plusBtn.frame = CGRectMake((kScreenBounds.size.width - self.customerTabBar.plusBtn.size_width) / 2., - LCFloat(10), self.customerTabBar.plusBtn.size_width, self.customerTabBar.plusBtn.size_height);
    
    [[UITabBar appearance]  addSubview:self.customerTabBar.plusBtn];
}

+(instancetype)sharedController{
    static BYTabbarViewController *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[BYTabbarViewController alloc] init];
    });
    return _sharedAccountModel;
}


#pragma mark page
-(void)pageSetting{
    BYNavigationController *navigationController = [[BYNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
    [[navigationController navigationBar] setTranslucent:NO];
    
    UIColor *color1 = [UIColor colorWithCustomerName:@"白"];
    UIColor *color6 = [UIColor colorWithCustomerName:@"白"];
    NSArray *navBarArr = [NSArray arrayWithObjects:(id)color6.CGColor,(id)color1.CGColor,nil];
    
    
    UIImage *unselectedImage = [UIImage imageNamed:@"icon_tabbar_home_nor"];
    UIImage *selectedImage = [UIImage imageNamed:@"icon_tabbar_home_hlt"];
    
    // 1. 【首页】
    BYLiveHomeController *homeViewController = [[BYLiveHomeController alloc]init];
    BYNavigationController *pandaNav = [[BYNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
    [pandaNav setViewControllers:@[homeViewController]];
    pandaNav.navigationBar.layer.shadowColor = [[UIColor colorWithCustomerName:@"浅灰"] CGColor];
    pandaNav.navigationBar.layer.shadowOpacity = 2.0;
    pandaNav.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    
    homeViewController.tabBarItem.tag = 1;
    homeViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"推荐" image:[unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    // 2. 【分类】
    BYLiveHomeController *productViewController = [[BYLiveHomeController alloc]init];
    BYNavigationController *productNav = [[BYNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
    [productNav setViewControllers:@[productViewController]];
    
    productNav.navigationBar.layer.shadowColor = [[UIColor colorWithCustomerName:@"浅灰"] CGColor];
    productNav.navigationBar.layer.shadowOpacity = 2.0;
    productNav.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);

    productViewController.tabBarItem.tag = 2;
    unselectedImage = [UIImage imageNamed:@"icon_tabbar_class_nor"];
    selectedImage = [UIImage imageNamed:@"icon_tabbar_class_hlt"];
    productViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"分类" image:[unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    // 3. 【发布】
    BYSchoolController *lotteryRootViewController = [[BYSchoolController alloc]init];
    BYNavigationController *lotteryRootViewNav = [[BYNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
    [lotteryRootViewNav setViewControllers:@[lotteryRootViewController]];
    
    lotteryRootViewNav.navigationBar.layer.shadowColor = [[UIColor colorWithCustomerName:@"浅灰"] CGColor];
    lotteryRootViewNav.navigationBar.layer.shadowOpacity = 2.0;
    lotteryRootViewNav.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    
    lotteryRootViewController.tabBarItem.tag = 3;
    unselectedImage = [UIImage imageNamed:@"icon_tabbar_release"];
    selectedImage = [UIImage imageNamed:@"icon_tabbar_release"];
    lotteryRootViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"发布" image:[unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    
    // 3.【消息】
    CenterMessageRootViewController *findViewController = [CenterMessageRootViewController sharedController];
    BYNavigationController *findNav = [[BYNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
    [findNav setViewControllers:@[findViewController]];
    
    findNav.navigationBar.layer.shadowColor = [[UIColor colorWithCustomerName:@"浅灰"] CGColor];
    findNav.navigationBar.layer.shadowOpacity = 2.0;
    findNav.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    
    findViewController.tabBarItem.tag = 4;
    unselectedImage = [UIImage imageNamed:@"icon_tabbar_message_nor"];
    selectedImage = [UIImage imageNamed:@"icon_tabbar_message_hlt"];
    findViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"消息" image:[unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    
    // 4.【个人中心】
    CenterRootViewController *centerViewController = [CenterRootViewController sharedController];
    BYNavigationController *centerNav = [[BYNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
    [centerNav setViewControllers:@[centerViewController]];
    
    centerNav.navigationBar.layer.shadowColor = [[UIColor colorWithCustomerName:@"浅灰"] CGColor];
    centerNav.navigationBar.layer.shadowOpacity = 2.0;
    centerNav.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    
    centerViewController.tabBarItem.tag = 5;
    unselectedImage = [UIImage imageNamed:@"icon_tabbar_center_nor"];
    selectedImage = [UIImage imageNamed:@"icon_tabbar_center_hlt"];
    centerViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"我的" image:[unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    self.viewControllers = @[pandaNav,productNav,findNav,centerNav];
    
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor hexChangeFloat:@"EE4C47"],NSFontAttributeName : [UIFont fontWithCustomerSizeName:@"12"]} forState:UIControlStateSelected];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor hexChangeFloat:@"353535"],NSFontAttributeName : [UIFont fontWithCustomerSizeName:@"12"]} forState:UIControlStateNormal];
    
    UITabBar *tabBar = self.tabBar;
    
    centerNav.popGestureRecognizer = YES;
    pandaNav.popGestureRecognizer = YES;
    productNav.popGestureRecognizer = YES;
    findNav.popGestureRecognizer = YES;

    
    
    
    [[PDGradientNavBar appearance] setBarTintGradientColors:navBarArr];
    // 设置背景颜色
    
    if ([tabBar respondsToSelector:@selector(setBarTintColor:)]){
        [tabBar setBarTintColor:[UIColor clearColor]];
    }else{
        for (UIView *view in tabBar.subviews) {
            if ([NSStringFromClass([view class]) hasSuffix:@"TabBarBackgroundView"]) {
                [view removeFromSuperview];
                break;
            }
        }
    }

    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width,self.tabBarHeight)];
    backView.backgroundColor = BACKGROUND_VIEW_COLOR;
    [self.tabBar insertSubview:backView atIndex:0];
    self.tabBar.opaque = NO;
}

-(void)createCustomerTabBar{
    self.customerTabBar = [[ZYTabBar alloc]init];;
    self.customerTabBar.tabbarDelegate = self;
    self.customerTabBar.backgroundColor = [UIColor redColor];
    self.customerTabBar.tintColor = [UIColor whiteColor];
    self.customerTabBar.pathButtonArray = nil;
    self.customerTabBar.basicDuration = 0.5;
    self.customerTabBar.allowSubItemRotation = YES;
    self.customerTabBar.bloomRadius = 100;
    self.customerTabBar.allowCenterButtonRotation = NO;
    self.customerTabBar.bloomAngel = 100;
    self.customerTabBar.allowSubItemRotation = NO;
    [self setValue:self.customerTabBar forKeyPath:@"tabBar"];
}


-(CGFloat)tabBarHeight{
    if (IS_iPhoneX){
        return  49 + 34;
    } else {
        return  49 ;
    }
}
-(CGFloat)navBarHeight{
if (IS_iPhoneX){
        return  44 + 44;
    } else {
        return  20 + 44;
    }
}

-(CGFloat)statusHeight{
    if (IS_iPhoneX){
        return  44 ;
    } else {
        return  20 ;
    }
}

-(void)showMessage{
    self.selectedIndex = TabbarTypeMessage;
}

-(void)showCenter{
    self.selectedIndex = TabbarTypeCenter;
}

-(void)showTabbar{
    NSArray *views = self.tabBarController.view.subviews;
    UIView *contentView = [views objectAtIndex:0];
    contentView.size_height -= self.tabBarHeight;
    self.tabBarController.tabBar.hidden = NO;
}

-(void)hiddenTabbar{
//    NSArray *views = [BYTabbarViewController sharedController].tabBar.view.subviews;
    UIView *contentView = [BYTabbarViewController sharedController].customerTabBar;
    contentView.size_height += self.tabBarHeight;
    self.tabBarController.tabBar.hidden = YES;
}

+(void)directToProduct{
    
}

-(void)centerItemBtnClick{
    
}
@end
