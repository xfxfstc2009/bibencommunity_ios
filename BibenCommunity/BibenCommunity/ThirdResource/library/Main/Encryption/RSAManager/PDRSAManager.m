//
//  PDRSAManager.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDRSAManager.h"
#import "GTMBase64.h"
#import "RSA.h"

@implementation PDRSAManager


+(NSString *)encryptStringWithString:(NSString *)code{
    NSString *publicKey = [GTMBase64 decodeBase64String:kBase64Key];
    NSString *encWithPubKey;
    encWithPubKey = [RSA encryptString:code publicKey:publicKey];
    return  encWithPubKey;
}


// 解密码
+(NSString *)decryptPrivateStringString:(NSString *)smart{
    // 1. 解密
    NSString *publicKey = [GTMBase64 decodeBase64String:privateKey1];
    NSString *encWithPubKey;
    encWithPubKey = [RSA decryptString:smart privateKey:publicKey];
    return  encWithPubKey;
}

@end
