//
//  MD5Manager.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/6/7.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MD5Manager : NSObject

+(NSString *) md5: (NSString *) inPutText;

@end
