//
//  GWSwitchTableViewCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/20.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDBaseTableViewCell.h"

@interface GWSwitchTableViewCell : PDBaseTableViewCell

@property (nonatomic,copy)NSString *transferInfo;
@property (nonatomic,strong)UISwitch *normalSwitch;
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,assign)BOOL isOn;
+(CGFloat)calculationCellHeight;

-(void)actionClickWIthSwitch:(void(^)(BOOL switchStatus))block;

@end
