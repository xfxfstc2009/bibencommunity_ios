//
//  PDImageView.h
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/10/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIImageView+WebCache.h>

typedef NS_ENUM(NSInteger ,PDImgType) {
    PDImgTypeNormal,                        /**<方框裁切*/
    PDImgTypeOriginal,                      /**<原图*/
    PDImgTypeRound,                         /**<圆形裁切 */
    PDImgTypeDrawRound,                     /** 圆角绘制 */
};

#define imageBaseUrl @"http://img.bitben.net/"

@interface PDImageView : UIImageView

// 更新图片
-(void)uploadImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock;
// 更新头像方法
-(void)uploadImageWithRoundURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock;
// 更新图片主方法
-(void)uploadMainImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder imgType:(PDImgType)type callback:(void(^)(UIImage *image))callbackBlock;

// 清除图片缓存
+(void)cleanImgCacheWithBlock:(void(^)())block;
// 当前图片数量
+(NSString *)diskCount;

+(NSString *)appendingImgUrl:(NSString *)url;
@end
