//
//  PDImageView.m
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/10/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "PDImageView.h"
#import "PDImageView+DefultBg.h"

@implementation PDImageView

// 更新图片
-(void)uploadImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock{
     [self uploadMainImageWithURL:urlString placeholder:placeholder imgType:PDImgTypeNormal callback:callbackBlock];
}

-(void)uploadImageOriginalWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock{
    [self uploadMainImageWithURL:urlString placeholder:placeholder imgType:PDImgTypeOriginal callback:callbackBlock];
}

// 更新用户头像图片
-(void)uploadImageWithRoundURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock{
    [self uploadMainImageWithURL:urlString placeholder:placeholder imgType:PDImgTypeRound callback:callbackBlock];
}

#pragma mark - 更新图片 主方法
-(void)uploadMainImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder imgType:(PDImgType)type callback:(void(^)(UIImage *image))callbackBlock{
    if ((![urlString isKindOfClass:[NSString class]]) ||(!urlString.length)){
        return;
    }
    // 判断是否为本地默认图
    if ([self verifyIsLocalDefultBg:urlString]) {
        self.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.jpg",urlString]];
        if (callbackBlock){
            callbackBlock(self.image);
        }
        return;
    }
    // 1. 判断placeholder
    if (placeholder == nil){
        placeholder = [UIImage imageNamed:@"icon_main_logo"];
    }
    
    // 2. 判断url
    NSString *mainImgUrl = @"";
    if ([urlString hasPrefix:@"http://"] || [urlString hasPrefix:@"https://"]){
        mainImgUrl = urlString;
    } else {
        if (type == PDImgTypeOriginal){
            mainImgUrl = [NSString stringWithFormat:@"%@%@",imageBaseUrl,urlString];
        } else {
            mainImgUrl = [NSString stringWithFormat:@"%@%@?imageView2/1/w/%li/h/%li/q/75|imageslim",imageBaseUrl,urlString,(long)self.size_width,(long)self.size_height];
        }
    }

    NSURL *imgUrl =  [Tool transformationUrl:mainImgUrl];
    
    // 3. 进行加载图片
    @weakify(self);
    [self sd_setImageWithURL:imgUrl placeholderImage:placeholder completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        @strongify(self);
        if (image){
            if (type == PDImgTypeNormal){
                self.image = image;
            } else if (type == PDImgTypeRound){
                self.image = [Tool imageByComposingImage:image withMaskImage:[UIImage imageNamed:@"round.png"]];
            }else if (type == PDImgTypeDrawRound){
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.image = [image addCornerRadius:image.size.width/2 size:image.size];
                });
            }
        }
        if (callbackBlock){
            callbackBlock(self.image);
        }
    }];
}

#pragma mark - 清除缓存
+(void)cleanImgCacheWithBlock:(void(^)())block{
    [[SDWebImageManager sharedManager].imageCache clearMemory];
    __weak typeof(self)weakSelf = self;
    [[SDWebImageManager sharedManager].imageCache clearDiskOnCompletion:^{
        if (!weakSelf){
            return ;
        }
        if (block){
            block();
        }
    }];
}

+(NSString *)diskCount{
    float tmpSize = [[SDImageCache sharedImageCache] getSize];
    CGFloat tempSizeWithM = tmpSize / 1024. / 1024.;
    
    NSString *clearCacheName = tempSizeWithM >= 1 ? [NSString stringWithFormat:@"%.2fM",tempSizeWithM] : [NSString stringWithFormat:@"%.2fK",tempSizeWithM * 1024];
    return clearCacheName;
}

+(CGFloat)diskCountFloat{
    float tmpSize = [[SDImageCache sharedImageCache] getSize];
    CGFloat tempSizeWithM = tmpSize / 1024. / 1024.;
    
    return tempSizeWithM;
}

+(NSString *)appendingImgUrl:(NSString *)url{
   NSString *mainImgUrl = [NSString stringWithFormat:@"%@%@",imageBaseUrl,url];
    return mainImgUrl;
}

@end
