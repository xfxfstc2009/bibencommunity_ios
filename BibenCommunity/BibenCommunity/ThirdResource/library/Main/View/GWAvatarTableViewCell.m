//
//  GWAvatarTableViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/19.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWAvatarTableViewCell.h"

@interface GWAvatarTableViewCell()
@property (nonatomic,strong)PDImageView *arrowImg;


@end

@implementation GWAvatarTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    self.fixedLabel.text = @"头像";
    [self addSubview:self.fixedLabel];

    self.avatar = [[PDImageView alloc]init];
    self.avatar.backgroundColor = [UIColor clearColor];
    self.avatar.layer.cornerRadius = LCFloat(5);
    self.avatar.clipsToBounds = YES;
    [self addSubview:self.avatar];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    // 1. title
    CGSize titleSize = [self.fixedLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.fixedLabel.font])];
    self.fixedLabel.frame = CGRectMake(LCFloat(11), 0, titleSize.width, transferCellHeight);

    self.avatar.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - (transferCellHeight - 2 * LCFloat(7)), LCFloat(7), (transferCellHeight - 2 * LCFloat(7)), (transferCellHeight - 2 * LCFloat(7)));
}


-(void)setTransferAvatarUrl:(NSString *)transferAvatarUrl{
    _transferAvatarUrl = transferAvatarUrl;

    [self.avatar uploadImageWithURL:transferAvatarUrl placeholder:nil callback:NULL];
}

-(void)setTransferAvatar:(UIImage *)transferAvatar{
    _transferAvatar = transferAvatar;
    self.avatar.image = transferAvatar;
}

+(CGFloat)calculationCellHeight{
    return LCFloat(80);
}


@end
