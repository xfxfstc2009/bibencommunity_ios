//
//  OSSFileModel.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/9/6.
//  Copyright © 2018年 币本. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol OSSFileModel<NSObject>

@end

@interface OSSFileModel : NSObject

@property (nonatomic,copy)NSString *objcName;
@property (nonatomic,strong)UIImage *objcImage;

@end
