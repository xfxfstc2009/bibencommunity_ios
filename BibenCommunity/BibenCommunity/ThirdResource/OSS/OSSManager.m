//
//  OSSManager.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/9/4.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "OSSManager.h"
#import "NetworkAdapter.h"
#import <QiniuSDK.h>

@implementation OSSManager

+ (OSSManager *)sharedUploadManager{
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}


#pragma mark - 上传图片方法
-(void)uploadImageManagerWithImageList:(NSArray<OSSFileModel> *)imgList withUrlBlock:(void(^)(NSArray *imgUrlArr))block{
    __weak typeof(self)weakSelf = self;
    NSMutableArray *imgUrlMutableArr = [NSMutableArray array];
    [self sendRequestToGetUploadTokenManagerWithBlock:^(NSString *token) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (token.length){
            for (int i = 0 ; i < imgList.count;i++){
                OSSFileModel *fileModel = [imgList objectAtIndex:i];
                [strongSelf uploadFileManager:fileModel token:token blockArr:^(NSString *imgUrl) {
                    [imgUrlMutableArr addObject:imgUrl];
                    if (i == imgList.count - 1){
                        if (block){
                            block(imgUrlMutableArr);
                        }
                    }
                }];
            }
        }
    }];
}

#pragma mark - 1. 获取当前的上传权限
-(void)sendRequestToGetUploadTokenManagerWithBlock:(void(^)(NSString *token))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:@"get_upload_token" requestParams:nil responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            NSString *token = [responseObject objectForKey:@"token"];
            if (token.length && block){
                block(token);
            }
        } else {
            if (block){
                block(@"");
            }
        }
    }];
}

-(void)uploadFileManager:(OSSFileModel *)fileModel token:(NSString *)token blockArr:(void(^)(NSString *imgUrl))block{
    
    QNUploadManager *upManager = [[QNUploadManager alloc] init];
    // 1. 获取当前的图片对象
    UIImage *image = fileModel.objcImage;
    NSData *objcData = UIImageJPEGRepresentation(image,.9f);
    
    NSString *ossMainObjcName = fileModel.objcName;
    if (!ossMainObjcName.length){
        ossMainObjcName = [NSString stringWithFormat:@"%@-%@",@"other",[NSDate getCurrentTimeWithFileName]];
    }
    
    // 2. 上传图片信息
 
    [upManager putData:objcData key:ossMainObjcName token:token complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
        if (key.length && block){
            block(key);
        }
    } option:NULL];
}


//
//
//
//-(void)uploadImg{
////    [[NetworkAdapter sharedAdapter] fetchWithPath:@"" requestParams:nil responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
////
//
//    [self uploadImagerWithToken:@""];
////    }];
//
//
//}
//
//-(void)uploadImagerWithToken:(NSString *)token{
////     http://pe3yqz025.bkt.clouddn.com/hello
//    token = @"yeHypBgOtmxYwpWTqkS8U0tsS081R7qeKrHbQP5y:E8cUWIyruqNDHgPlknkAXjaY_PE=:eyJzY29wZSI6ImltYWdlIiwiZGVhZGxpbmUiOjE1MzYwNDg2MTd9";
//
//
//
//
//
//
//
//    [upManager putData:objcData key:@"hello2" token:token  complete: ^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
//                  NSLog(@"%@", info);
//                  NSLog(@"%@", resp);
//
//
//              } option:nil];
//
//    [upManager putFile:@"using" key:@"hello2" token:token complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
//
//    } option:NULL];
//}

@end
