//
//  BYALIMManager.h
//  BY
//  阿里百川sdk
//  Created by 黄亮 on 2018/8/29.
//  Copyright © 2018年 BY. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WXOpenIMSDKFMWK/YWFMWK.h>
#import <WXOUIModule/YWUIFMWK.h>

@interface BYALIMManager : NSObject

/// SDK中的接口分为全局接口和非全局接口，非全局接口需要通过YWIMKit或其成员YWIMCore调用，所以这里需要持有这个对象
@property (strong, nonatomic, readwrite) YWIMKit *ywIMKit;


+ (instancetype)shareInstance;

- (void)initSDK;

- (void)loginWithUserId:(NSString *)userId password:(NSString *)password suc:(void(^)(void))suc fail:(void(^)(NSError *error))fail;

@end
