//
//  BYIMManager+SendMsg.m
//  BY
//
//  Created by 黄亮 on 2018/8/24.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYIMManager+SendMsg.h"

@interface BYIMManager()<TIMMessageListener,TIMMessageReceiptListener>

// ** 群聊会话
@property (nonatomic ,strong) TIMConversation *groupConversation;

// ** 单聊会话
@property (nonatomic ,strong) TIMConversation *c2cConversation;

// ** 会话类型
@property (nonatomic ,assign) BYIMConversationType conversationType;

@property (nonatomic ,copy) void (^messageHandle)(NSArray *msgs);

@end

static const char groupCon;
static const char c2cCon;
static const char conType;
static const char msgHandle;
@implementation BYIMManager (SendMsg)

#pragma mark - initMethod

- (TIMConversation *)groupConversation{
    TIMConversation *conversation = objc_getAssociatedObject(self, &groupCon);
    return conversation;
}

- (void)setGroupConversation:(TIMConversation *)groupConversation{
    objc_setAssociatedObject(self, &groupCon, groupConversation, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    self.conversationType = BYIM_CVT_TYPE_GROUP;
}

- (TIMConversation *)c2cConversation{
    TIMConversation *conversation = objc_getAssociatedObject(self, &c2cCon);
    return conversation;
}

- (void)setC2cConversation:(TIMConversation *)c2cConversation{
    objc_setAssociatedObject(self, &c2cCon, c2cConversation, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    self.conversationType = BYIM_CVT_TYPE_C2C;
}

- (BYIMConversationType)conversationType{
    return [objc_getAssociatedObject(self, &conType) integerValue];
}

- (void)setConversationType:(BYIMConversationType)conversationType{
    objc_setAssociatedObject(self, &conType, @(conversationType), OBJC_ASSOCIATION_ASSIGN);
}

- (void)setMessageHandle:(void (^)(NSArray *))messageHandle{
    objc_setAssociatedObject(self, &msgHandle, messageHandle, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void (^)(NSArray *))messageHandle{
    return objc_getAssociatedObject(self, &msgHandle);
}

#pragma mark - Listener

// 设置消息监听
- (void)setMessageListener:(void (^)(NSArray *))msgs{
    self.messageHandle = msgs;
    [[TIMManager sharedInstance] setMessageListener:self];
    [[TIMManager sharedInstance] setMessageReceiptListener:self];
}

// 移除消息监听
- (void)removeMessageListener{
    [[TIMManager sharedInstance] removeMessageListener:self];
}

// 接收消息回调
- (void)onNewMessage:(NSArray *)msgs{
    if (self.messageHandle) {
        self.messageHandle(msgs);
    }
    for (TIMMessage *msg in msgs) {
        if ([(TIMTextElem *)[msg getElem:0] isMemberOfClass:[TIMGroupSystemElem class]]) break;
        if ([(TIMTextElem *)[msg getElem:0] isMemberOfClass:[TIMProfileSystemElem class]]) break;
        [msg convertToImportedMsg];
//        TIMTextElem *elem = (TIMTextElem *)[msg getElem:0];
//        dispatch_semaphore_t semaphore = dispatch_semaphore_create(1);
//        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [BYToastView tostViewPresentInView:kCommonWindow title:elem.text duration:1.0 complete:^{
//                dispatch_semaphore_signal(semaphore);
//            }];
//        });
    }
}

- (void) onRecvMessageReceipts:(NSArray*)receipts{
    NSLog(@"%s",__func__);
    NSLog(@"%@",receipts);
}


#pragma mark - customMethod

// 获取消息会话
- (void)setConversationGroupId:(NSString *)groupId{
    self.groupConversation = [[TIMManager sharedInstance] getConversation:2 receiver:groupId];
}

- (void)setConversationUserId:(NSString *)userId{
    self.c2cConversation = [[TIMManager sharedInstance] getConversation:1 receiver:userId];
}

- (void)sendTextMessage:(NSString *)text succ:(sucCallback)succ fail:(failCodeCallback)fail{
    if (!text.length) return;
    TIMTextElem *textElem = [[TIMTextElem alloc] init];
    [textElem setText:text];
    TIMMessage *msg = [[TIMMessage alloc] init];
    [msg addElem:textElem];
    [msg setTime:[NSDate date].timeIntervalSince1970];
    [msg setSender:[AccountModel sharedAccountModel].account_id];
//    NSString *url = nullToEmpty([AccountModel sharedAccountModel].loginServerModel.account.head_img);
//    NSString *userName = nullToEmpty([AccountModel sharedAccountModel].loginServerModel.user.nickname);
//    NSData *customData = [NSJSONSerialization dataWithJSONObject:@{@"user_name":userName,@"user_logo":url} options:NSJSONWritingPrettyPrinted error:nil];
//    BOOL isOk =  [msg setCustomData:customData];
    TIMConversation *conversation = self.conversationType == BYIM_CVT_TYPE_GROUP ? self.groupConversation : self.c2cConversation;
    [conversation sendMessage:msg succ:^{
//        NSLog(@"消息发送成功");
        // 消息导入本地
        [msg convertToImportedMsg];
        if (self.messageHandle) {
            self.messageHandle(@[msg]);
        }
        if (succ) succ();
    } fail:^(int code, NSString *msg) {
        if (fail) fail(code);
        showToastView(kError_Room(@"文本消息发送", code, msg), kCommonWindow);
    }];
}

- (void)sendImageMessage:(NSString *)imagePath succ:(sucCallback)succ fail:(failCodeCallback)fail{
    
}

- (void)getLocalMessage:(int)count succ:(sucCallback)succ fail:(failCodeCallback)fail{
    TIMConversation *conversation = self.conversationType == BYIM_CVT_TYPE_GROUP ? self.groupConversation : self.c2cConversation;
    [conversation getLocalMessage:count last:nil succ:^(NSArray *msgs) {
        NSLog(@"%@",msgs);
        if (succ) succ();
    } fail:^(int code, NSString *msg) {
        if (fail) fail(code);
    }];
}

- (void)getMessage:(int)count succ:(sucMsgsCallback)succ fail:(failCodeCallback)fail{
    TIMConversation *conversation = self.conversationType == BYIM_CVT_TYPE_GROUP ? self.groupConversation : self.c2cConversation;
    [conversation getMessage:count last:nil succ:^(NSArray *msgs) {
//        if (self.messageHandle) self.messageHandle(msgs);
        if (succ) succ(msgs);
    } fail:^(int code, NSString *msg) {
        
    }];
}

- (void)getRoamMessage{
    
}


@end
