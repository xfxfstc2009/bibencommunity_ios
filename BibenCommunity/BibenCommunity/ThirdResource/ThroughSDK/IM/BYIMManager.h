//
//  BYIMManager.h
//  BY
//
//  Created by 黄亮 on 2018/8/22.
//  Copyright © 2018年 BY. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ImSDK/TIMCallback.h>
#import <ImSDK/ImSDK.h>
#import "BYIMCommon.h"

typedef void (^sucCallback) (void);
typedef void (^failCallback) (void);
typedef void (^failCodeCallback) (int code);

typedef void (^sucMsgsCallback) (NSArray *msgs);

@interface BYIMManager : NSObject


/**
 获取管理器实例

 @return BYIMManager
 */
+ (BYIMManager *)sharedInstance;


/**
 初始化TIM SDK
 */
- (void)initSdk;

/** 互动直播登录 */
- (void)loginILive:(sucCallback)suc  fail:(failCallback)fail;

/** 互动直播登出 */
- (void)loginOutILive:(sucCallback)suc;

/**
 TIM登录调用

 @param cb  回调
 */
- (void)login:(sucCallback)cb;

/**
 TIM登出调用

 @param cb 回调
 */
- (void)loginOut:(sucCallback)cb;

/**
 创建聊天室

 @param groupId 聊天室id
 @param succ 成功回调
 @param fail 失败回调
 */
- (void)createAVChatRoomGroupId:(NSString *)groupId succ:(sucCallback)succ fail:(failCallback)fail;

/**
 加入聊天室

 @param groupId 聊天室id
 @param succ succ description
 @param fail fail description
 */
- (void)joinAVChatRoomGroupId:(NSString *)groupId succ:(sucCallback)succ fail:(failCallback)fail;

/**
 修改聊天室成员的角色

 @param userId 用户id
 @param role 角色
 @param succ succ description
 @param fail fail description
 */
- (void)modifyGroupMemberInfoSetRole:(NSString *)userId role:(BYIMGroupMemberRole)role succ:(sucCallback)succ fail:(failCallback)fail;

- (void)setNickName:(NSString *)nick;
- (void)setHeadImg:(NSString *)headImg;

@end
