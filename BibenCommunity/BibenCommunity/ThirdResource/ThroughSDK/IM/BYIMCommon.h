//
//  BYIMCommon.h
//  BY
//
//  Created by 黄亮 on 2018/8/24.
//  Copyright © 2018年 BY. All rights reserved.
//

#ifndef BYIMCommon_h
#define BYIMCommon_h

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger ,BYIMGroupMemberRole) {
    BYTIM_GROUP_MEMBER_ROLE_MEMBER  = 200, // 普通成员
    BYTIM_GROUP_MEMBER_ROLE_ADMIN   = 300 // 管理员
};

typedef NS_ENUM(NSInteger ,BYIMConversationType) {
    BYIM_CVT_TYPE_C2C, // 单聊
    BYIM_CVT_TYPE_GROUP // 群聊
};

// ** 获取聊天室登录sig
static NSString * RPC_get_chat_sig = @"get_chat_sig";

#define connFailed @"网络连接失败"
#define connlost   @"网络连接断开"
#define connecting @"网络连接中"

#define kJoinMeg    @"进入直播间"
#define kGroupType  @"AVChatRoom"

#define kError_Room(t,c,m) [NSString stringWithFormat:@"TIM%@失败\n errorCode:%i errMsg:%@",t,c,m]

#endif /* BYIMCommon_h */
