//
//  BYIMManager.m
//  BY
//
//  Created by 黄亮 on 2018/8/22.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYIMManager.h"
#import "BYToastView.h"
#import "BYRequestManager.h"
#import <sqlite3.h>
#import "BYIMManager+SendMsg.h"
#import "BYLiveRPCDefine.h"
#import <ILiveSDK/ILiveLoginManager.h>

@interface BYIMManager()<NSCopying,TIMConnListener,TIMCallback>
{
    sucCallback _callBack;
}
@property (nonatomic ,strong) BYToastView *connectingToast;

@end

@implementation BYIMManager

static BYIMManager *_manager;
+ (BYIMManager *)sharedInstance{
    return [[self alloc] init];
}

+ (instancetype)allocWithZone:(struct _NSZone *)zone{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!_manager) {
            _manager = [super allocWithZone:zone];
        }
    });
    return _manager;
}

- (id)copyWithZone:(NSZone *)zone{
    return _manager;
}

- (void)initSdk{
    TIMManager *manager = [TIMManager sharedInstance];
    //    NSString *logPath = [NSSearchPathForDirectoriesInDomains(NSTemporaryDirectory(), NSLocalDomainMask, YES) lastObject]
#if DEBUG
    [manager setLogLevel:2];
#else
    [manager disableCrashReport];
#endif

    [manager setConnListener:self];
//    [manager initSdk:kTXTIMAppid accountType:kTXTIMAccountType];
    // 获取数据库文件的路径
    sqlite3 *db;
    NSString *doc = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *fileName = [doc stringByAppendingPathComponent:@"tim.sqlite"];
    const char *cfileName = fileName.UTF8String;
    // 数据库不存在则创建
    sqlite3_open(cfileName, &db);
    // 设置消息本地存储数据库
    BOOL isReadyMessageDB = [manager setDBPath:fileName];
    if (!isReadyMessageDB)
        NSLog(@"\n设置用户自定义消息数据库路径成功");
    else
        NSLog(@"\n设置用户自定义消息数据库路径失败");
}

/** 互动直播登录 */
- (void)loginILive:(sucCallback)suc  fail:(failCallback)fail{
    [BYRequestManager ansyRequestWithURLString:RPC_get_user_sig parameters:nil operationType:BYOperationTypePost successBlock:^(id result) {
        [[ILiveLoginManager getInstance] iLiveLogin:[AccountModel sharedAccountModel].account_id sig:result[@"userSig"] succ:^{
#if DEBUG
            showToastView(@"互动直播登录成功", kCommonWindow);
#endif
            if (suc) suc();
        } failed:^(NSString *module, int errId, NSString *errMsg) {
            showToastView(kError_Room(@"互动直播登录", errId, errMsg), kCommonWindow);
        }];
    } failureBlock:^(NSError *error) {
        showToastView(@"获取互动直播Sig失败", kCommonWindow);
        if (fail) fail();
    }];
}

/** 互动直播登出 */
- (void)loginOutILive:(sucCallback)suc{
    [[ILiveLoginManager getInstance] iLiveLogout:^{
        if (suc) suc();
    } failed:^(NSString *module, int errId, NSString *errMsg) {
        showToastView(kError_Room(@"互动直播登出", errId, errMsg), kCommonWindow);
    }];
}

// 获取登录sig
- (void)getSigRequest:(void(^)(NSString *sig))suc{
    NSDictionary *param = @{@"user_id":stringFormatInteger([BYCommonUserModel shareObject].user_id)};
    [BYRequestManager ansyRequestWithURLString:RPC_get_chat_sig
                                    parameters:param operationType:BYOperationTypePost
                                  successBlock:^(id result) {
                                      NSString *sig = result[@"data"][@"chatSig"];
                                      NSAssert(sig, @"sig为空");
                                      if (suc) suc(sig);
                                  } failureBlock:^(NSError *error) {
                                      NSLog(@"获取聊天室sig失败");
                                  }];
}

- (void)login:(sucCallback)cb{
    _callBack = cb;
    if ([[TIMManager sharedInstance] getLoginStatus] == TIM_STATUS_LOGINED) {
        _callBack();
        return;
    }
    @weakify(self);
    [self getSigRequest:^(NSString *sig) {
        @strongify(self);
        TIMLoginParam *param = [[TIMLoginParam alloc] init];
        param.accountType = kTXTIMAccountType;
        param.identifier  = stringFormatInteger(K_C_U_M.user_id);
        param.userSig     = sig;
        param.sdkAppId    = kTXTIMAppid;
        param.appidAt3rd  = stringFormatInteger(kTXTIMAppid);
        [[TIMManager sharedInstance] login:param cb:self];
    }];
}

- (void)loginOut:(sucCallback)cb{
    [[TIMManager sharedInstance] logout:^{
        if (cb) cb();
    } fail:^(int code, NSString *msg) {
        showToastView(kError_Room(@"登出", code, msg), kCommonWindow);
    }];
}

- (void)createAVChatRoomGroupId:(NSString *)groupId succ:(sucCallback)succ fail:(failCallback)fail{
    if ([[TIMManager sharedInstance] getLoginStatus] == 3) {
        [[BYIMManager sharedInstance] loginILive:^{
            [[BYIMManager sharedInstance] createAVChatRoomGroupId:groupId succ:succ fail:fail];
        } fail:^{
            
        }];
    }
    NSString *groupName = [NSString stringWithFormat:@"%@-%@",[AccountModel sharedAccountModel].account_id,groupId];
    TIMCreateGroupInfo *groupInfo = [[TIMCreateGroupInfo alloc] init];
    groupInfo.group = groupId;
    groupInfo.groupName = groupName;
    groupInfo.groupType = kGroupType;
    groupInfo.setAddOpt = false;
    groupInfo.addOpt = TIM_GROUP_ADD_ANY;
    [[TIMGroupManager sharedInstance] CreateGroup:groupInfo succ:^(NSString *groupId) {
        if (succ) succ();
//        [self modifyGroupMemberVisible:groupId];
    } fail:^(int code, NSString *msg) {
//        @strongify(self);
//        if (code == 10025) { // 聊天室被自己创建过则直接进入
//            [self joinAVChatRoomGroupId:groupId succ:^{
//                if (succ) succ();
//            } fail:nil];
//            return ;
//        }
        if (fail) fail();
        showToastView(kError_Room(@"创建聊天室", code, msg), kCommonWindow);
    }];
}

- (void)modifyGroupMemberVisible:(NSString *)groupId{
    [[TIMGroupManager sharedInstance] ModifyGroupMemberVisible:groupId visible:NO succ:^{
        
    } fail:^(int code, NSString *msg) {
        showToastView(kError_Room(@"聊天室设置不可被搜索", code, msg), kCommonWindow);
    }];
}

- (void)joinAVChatRoomGroupId:(NSString *)groupId succ:(sucCallback)succ fail:(failCallback)fail{
    if ([[TIMManager sharedInstance] getLoginStatus] == 3) {
        [[BYIMManager sharedInstance] loginILive:^{
            [[BYIMManager sharedInstance] joinAVChatRoomGroupId:groupId succ:succ fail:fail];
        } fail:^{
            
        }];
    }
    [[TIMGroupManager sharedInstance] JoinGroup:groupId msg:kJoinMeg succ:^{
        if (succ) succ();
    } fail:^(int code, NSString *msg) {
        showToastView(kError_Room(@"进入聊天室", code, msg), kCommonWindow);
        if (fail) fail();
    }];
}

- (void)modifyGroupMemberInfoSetRole:(NSString *)userId role:(BYIMGroupMemberRole)role succ:(sucCallback)succ fail:(failCallback)fail{
    TIMGroupMemberRole memberRole = (int)role;
    [[TIMGroupManager sharedInstance] ModifyGroupMemberInfoSetRole:K_C_U_M.user_roomId user:userId role:memberRole succ:^{
        if (succ) succ();
    } fail:^(int code, NSString *msg) {
        if (fail) fail();
        showToastView(kError_Room(@"修改成员角色", code, msg), kCommonWindow);
    }];
}

- (void)setNickName:(NSString *)nick{
    if (!nick.length) return;
    int success = [[TIMFriendshipManager sharedInstance] SetNickname:nick succ:nil fail:nil];
    NSLog(@"%d",success);
}

- (void)setHeadImg:(NSString *)headImg{
    if (!headImg.length) return;
    int success = [[TIMFriendshipManager sharedInstance] SetFaceURL:headImg succ:nil fail:nil];
    NSLog(@"%d",success);
}

#pragma mark - TIMCallback(TIM登录回调)
- (void)onSucc{
    if (_callBack) _callBack();
    showToastView(@"TIM登录成功", kCommonWindow);
}

- (void)onErr:(int)errCode errMsg:(NSString *)errMsg{
    showToastView(kError_Room(@"登录", errCode, errMsg), kCommonWindow);
}

#pragma mark - TIMConnListener(网络监听)

- (void)onConnSucc{
    [_connectingToast dissmissToastView];
}

- (void)onConnFailed:(int)code err:(NSString *)err{
    [BYToastView toastViewPresentInWindowTitle:connFailed duration:2.0 toastViewType:kToastViewTypeNormal complete:nil];
}

- (void)onDisconnect:(int)code err:(NSString *)err{
    [BYToastView toastViewPresentInWindowTitle:connlost duration:2.0 toastViewType:kToastViewTypeNormal complete:nil];
}

- (void)onConnecting{
    _connectingToast = [BYToastView tostViewPresentInView:kCommonWindow title:connecting duration:MAXFLOAT complete:nil];
}
@end
