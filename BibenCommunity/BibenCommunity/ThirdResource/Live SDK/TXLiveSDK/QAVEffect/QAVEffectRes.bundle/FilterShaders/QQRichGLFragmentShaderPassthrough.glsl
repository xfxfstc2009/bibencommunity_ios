//
//  QQRichGLFragmentShaderPassthrough.glsl
//  QQMSFContact
//
//  Created by hodxiang on 16/8/24.
//
//

varying highp vec2 textureCoordinate;
uniform sampler2D inputImageTexture;
void main() {
    gl_FragColor = texture2D(inputImageTexture, textureCoordinate);
}
