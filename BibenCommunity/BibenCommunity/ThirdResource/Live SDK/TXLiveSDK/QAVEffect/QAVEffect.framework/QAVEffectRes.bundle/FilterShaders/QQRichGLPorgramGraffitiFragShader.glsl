precision highp float;
varying vec2 textureCoordinate;
uniform sampler2D inputImageTexture;
varying float needTransform;
varying vec4  outColor;

void main(void)
{
    if (needTransform > 0.5) {
        gl_FragColor = outColor;
    }
    else {
        vec4 texColor = texture2D(inputImageTexture, textureCoordinate);
        gl_FragColor = texColor;
    }
}
