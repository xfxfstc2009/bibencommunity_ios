//
//  QQRichGLVertexShaderMesh.glsl
//  QQMSFContact
//
//  Created by hodxiang on 16/7/13.
//
//

precision highp float;
attribute vec4  position;
attribute vec2  inputTextureCoordinate;
varying vec2    textureCoordinate;
uniform float   screenRatio;
//struct MeshDistortionType {
//    int     type;
//    float   strength;
//    vec2    point;
//    float   radius;
//    int     direction;
//    float   faceDegree;
//    float   faceRatio;
//};
//uniform MeshDistortionType items[30];
//
//vec4 distortedPosition(vec4 currentPosition) {
//    vec4 newPosition = currentPosition;
//    if (newPosition.x == -1.0 || newPosition.y == -1.0 || newPosition.x == 1.0 || newPosition.y == 1.0) {
//        return newPosition;
//    }
//    for (int i = 0; i < 30; i++) {
//        MeshDistortionType item = items[i];
//        if (item.type <= 0) {
//            return newPosition;
//        }
//        vec2 centerPoint = vec2(item.point.x, item.point.y * screenRatio);
//        vec2 ratioTransTargetPoint = vec2(newPosition.x, newPosition.y * screenRatio);
//        float dist = distance(ratioTransTargetPoint, centerPoint);
//        if (dist < item.radius) {
//            float distRatio = dist / item.radius;
//            float dx = centerPoint.x - ratioTransTargetPoint.x;
//            float dy = (centerPoint.y - ratioTransTargetPoint.y) / screenRatio;
//            if (item.type == 1) {
//                float weight = 1.5 * (1.0 - sin(distRatio * 3.1415 * 0.5)) * item.strength;
//                newPosition.x -= dx * weight;
//                newPosition.y -= dy * weight;
//            } else if (item.type == 2) {
//                float weight = cos(3.1415 * 0.5 * distRatio) * item.strength;
//                newPosition.x += dx * weight;
//                newPosition.y += dy * weight;
//            } else if (item.type == 3) {
//                float weight = (cos(3.1415 * 0.5 * distRatio)) * item.radius * 0.5 / item.faceRatio * item.strength;
//                vec2 vector = vec2(item.faceRatio, item.faceRatio / screenRatio);
//                if (item.direction == 1) {
//                    vector.x *= -weight; vector.y = 0.0;
//                } else if (item.direction == 2) {
//                    vector.x = 0.0; vector.y *= -weight;
//                } else if (item.direction == 3) {
//                    vector.x *= weight; vector.y = 0.0;
//                } else if (item.direction == 4) {
//                    vector.x = 0.0; vector.y *= weight;
//                } else if (item.direction == 5) {
//                    vector.x *= -weight; vector.y *= -weight;
//                } else if (item.direction == 6) {
//                    vector.x *= weight; vector.y *= -weight;
//                } else if (item.direction == 7) {
//                    vector.x *= -weight; vector.y *= weight;
//                } else if (item.direction == 8) {
//                    vector.x *= weight; vector.y *= weight;
//                } else {
//                    vector.x = 0.0; vector.y = 0.0;
//                }
//                newPosition.x += vector.x * cos(item.faceDegree) - vector.y * sin(item.faceDegree);
//                newPosition.y += (vector.y * cos(item.faceDegree) + vector.x * sin(item.faceDegree)) / screenRatio;
//            }
//        }
//    }
//    return newPosition;
//}

//  hodxiang
uniform ivec2   type_direction_s[30];
uniform vec4    strength_radius_faceDegree_faceRatio_s[30];
uniform vec2    point_s[30];

vec4 distortedPosition(vec4 currentPosition) {
    vec4 newPosition = currentPosition;
    if (newPosition.x == -1.0 || newPosition.y == -1.0 || newPosition.x == 1.0 || newPosition.y == 1.0) {
        return newPosition;
    }
    for (int i = 0; i < 30; i++) {
        int     item_type        = type_direction_s[i].x;
        float   item_strength    = strength_radius_faceDegree_faceRatio_s[i].x;
        vec2    item_point       = point_s[i];
        float   item_radius      = strength_radius_faceDegree_faceRatio_s[i].y;
        int     item_direction   = type_direction_s[i].y;
        float   item_faceDegree  = strength_radius_faceDegree_faceRatio_s[i].z;
        float   item_faceRatio   = strength_radius_faceDegree_faceRatio_s[i].w;

        if (item_type <= 0) {
            return newPosition;
        }
        vec2 centerPoint = vec2(item_point.x, item_point.y * screenRatio);
        vec2 ratioTransTargetPoint = vec2(newPosition.x, newPosition.y * screenRatio);
        float dist = distance(ratioTransTargetPoint, centerPoint);
        if (dist < item_radius) {
            float distRatio = dist / item_radius;
            float dx = centerPoint.x - ratioTransTargetPoint.x;
            float dy = (centerPoint.y - ratioTransTargetPoint.y) / screenRatio;
            if (item_type == 1) {
                float weight = 1.5 * (1.0 - sin(distRatio * 3.1415 * 0.5)) * item_strength;
                newPosition.x -= dx * weight;
                newPosition.y -= dy * weight;
            } else if (item_type == 2) {
                float weight = cos(3.1415 * 0.5 * distRatio) * item_strength;
                newPosition.x += dx * weight;
                newPosition.y += dy * weight;
            } else if (item_type == 3) {
                float weight = (cos(3.1415 * 0.5 * distRatio)) * item_radius * 0.5 / item_faceRatio * item_strength;
                vec2 vector = vec2(item_faceRatio, item_faceRatio / screenRatio);
                if (item_direction == 1) {
                    vector.x *= -weight; vector.y = 0.0;
                } else if (item_direction == 2) {
                    vector.x = 0.0; vector.y *= -weight;
                } else if (item_direction == 3) {
                    vector.x *= weight; vector.y = 0.0;
                } else if (item_direction == 4) {
                    vector.x = 0.0; vector.y *= weight;
                } else if (item_direction == 5) {
                    vector.x *= -weight; vector.y *= -weight;
                } else if (item_direction == 6) {
                    vector.x *= weight; vector.y *= -weight;
                } else if (item_direction == 7) {
                    vector.x *= -weight; vector.y *= weight;
                } else if (item_direction == 8) {
                    vector.x *= weight; vector.y *= weight;
                } else {
                    vector.x = 0.0; vector.y = 0.0;
                }
                newPosition.x += vector.x * cos(item_faceDegree) - vector.y * sin(item_faceDegree);
                newPosition.y += (vector.y * cos(item_faceDegree) + vector.x * sin(item_faceDegree)) / screenRatio;
            }
        }
    }
    return newPosition;
}

void main() {
    textureCoordinate = inputTextureCoordinate.xy;
    gl_Position = distortedPosition(position);
}

