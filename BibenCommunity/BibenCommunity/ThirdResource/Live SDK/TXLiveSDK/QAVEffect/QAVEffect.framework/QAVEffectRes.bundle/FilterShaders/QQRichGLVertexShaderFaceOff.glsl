//
//  QQRichGLVertexShaderFaceOff.glsl
//  QQMSFContact
//
//  Created by hodxiang on 16/9/10.
//
//

attribute vec4 position;
attribute vec4 inputTextureCoordinate;
varying vec2 textureCoordinate;

attribute vec4 inputGrayTextureCoordinate;
varying vec2 grayTextureCoordinate;

uniform int inputEnableAlphaFromGray;


void main(void) {
    gl_Position = position;
    textureCoordinate      = inputTextureCoordinate.xy;
    if (inputEnableAlphaFromGray==1) {
        grayTextureCoordinate  = inputGrayTextureCoordinate.xy;
    }

}
