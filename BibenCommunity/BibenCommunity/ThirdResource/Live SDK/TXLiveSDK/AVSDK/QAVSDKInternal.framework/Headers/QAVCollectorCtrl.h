
#ifndef QAVCollectorCtrl_h
#define QAVCollectorCtrl_h

#import <AVFoundation/AVFoundation.h>
#import <Foundation/Foundation.h>

@protocol QAVAudioSuttersDelegate;
@protocol QAVVideoSuttersDelegate;
@protocol QAVEventSuttersDelegate;
/*!
 @discussion    音频帧
 */
@interface QAVAudioStutterFrame : NSObject
/*!
 @abstract      帧长度
 */
@property(assign,nonatomic) int frameLength;
/*!
 @abstract      时间戳
 @discussion    当前音频帧的时间戳
 */
@property(assign,nonatomic) UInt64 timeStamp;

@end
/*!
 @discussion    视频帧
 */
@interface QAVVideoStutterFrame : NSObject
/*!
 @abstract      帧长度
 */
@property(assign,nonatomic) int frameLength;
/*!
 @abstract      是否为大画面
 @discussion    把320×240及以上大小的画面认为是大画面；反之，认为是小画面
 */
@property(assign,nonatomic) BOOL isSmallVideo;
/*!
 @abstract      是否为辅路视频
 @discussion    辅路包括屏幕分享、播片，摄像头视频为主路
 */
@property(assign,nonatomic) BOOL isSubChannel;
/*!
 @abstract      时间戳

 */
@property(assign,nonatomic) UInt64 timeStamp;

@end

typedef enum emMainEventMsgType {
    MAIN_EVENT_MSG_TYPE_SPEAR,
    MAIN_EVENT_MSG_TYPE_ROOM,
    MAIN_EVENT_MSG_TYPE_CODEC,
}MainEventMsgType;

typedef enum emSubEventMsgType {
    SUB_EVENT_TYPE_SPEAR_SUCC,
    SUB_EVENT_TYPE_SPEAR_FAIL,
    
    SUB_EVENT_TYPE_ROMM_SUCC = 0x100,
    SUB_EVENT_TYPE_ROMM_INFO_CHANGE,
    
    SUB_EVENT_TYPE_CODEC_HW_DEC = 0x200,
    SUB_EVENT_TYPE_CODEC_SOFT_DEC,
    SUB_EVENT_TYPE_CODEC_HW_ENC,
    SUB_EVENT_TYPE_CODEC_SOFT_ENC,
}SubEventMsgType;
/*!
 @discussion    事件帧
 */
@interface QAVEventStutterFrame : NSObject
/*!
 @abstract      主事件
 */
@property(assign,nonatomic) UInt64 mainEvent;
/*!
 @abstract      字事件
 */
@property(assign,nonatomic) UInt64 subEvent;
/*!
 @abstract      事件数据
 */
@property(assign,nonatomic) NSObject *eventData;

@end

/*!
 @discussion    房间事件
 */
@interface QAVEventRoomStutterFrame : NSObject
/*!
 @abstract      roomid
 */
@property(assign,nonatomic) UInt64 roomid;
/*!
 @abstract
 */
@property(assign,nonatomic) int connectionType;

@end

/*!
 @discussion    编解码事件
 */
@interface QAVEventCodecStutterFrame : NSObject
/*!
 @abstract      retCode
 */
@property(assign,nonatomic) int retCode;
/*!
 @abstract
 */
@property(assign,nonatomic) NSString *retDesc;

@end

/*!
 @discussion    Role切换事件
 */
@interface QAVEventRoleStutterFrame : NSObject
/*!
 @abstract      roomid
 */
@property(assign,nonatomic) UInt64 roomid;
/*!
 @abstract
 */
@property(assign,nonatomic) NSString *role;
/*!
 @abstract      width
 */
@property(assign,nonatomic) int width;
/*!
 @abstract      height
 */
@property(assign,nonatomic) int height;

@end
/*!
 @discussion    音视频帧收集器的封装类
 */
@interface QAVCollectorCtrl : NSObject
/*!
 @abstract      打开/关闭视频卡顿收集
 
 @param         bEnable         是否打开
 @param         delegate        卡顿收集器委托对象
 @discussion    视频采集/编码/发送的两帧间隔
 */
- (void)enableVideoStuttersCollector:(BOOL)enable delegate:(id<QAVVideoSuttersDelegate>)delegate;

/*!
 @abstract      打开/关闭音频卡顿收集

 @param         bEnable         是否打开
 @param         delegate        卡顿收集器委托对象
 @discussion    主播200ms内发送的音频数据和时间戳；（业务可以根据需要判断200ms发送的数据量是否正常）
 */
- (void)enableAudioStuttersCollector:(BOOL)enable delegate:(id<QAVAudioSuttersDelegate>)delegate;
/*!
 @abstract      打开/关闭事件上抛
 
 @param         bEnable         是否打开
 @param         delegate        事件收集器委托对象
 @discussion    发送平台部关注事件
 */
- (void)enableAVEventStuttersCollector:(BOOL)enable delegate:(id<QAVEventSuttersDelegate>)delegate;
@end

/*!
 @discussion    视频帧收集器委托协议。业务侧需要实现它来处理卡顿器异步操作返回的结果。
 */
@protocol QAVVideoSuttersDelegate <NSObject>
/*!
 @abstract      音频卡顿数据的回调。
 @param         msgList         是一个QAVVideoStutterFrame的数组，包含了2s内的视频卡顿数据
 @discussion    SDK通过此回调通知APP视频卡顿数据。app层可以精确的计算出每个帧画面的间隔时间。
 */
- (void)onVideoStuttes:(NSArray *)msgList;
@end
/*!
 @discussion    音频收集器委托协议。业务侧需要实现它来处理卡顿器异步操作返回的结果。
 */
@protocol QAVAudioSuttersDelegate <NSObject>
/*!
 @abstract      音频卡顿数据的回调。
 @param         msgList         是一个QAVVideoStutterFrame的数组，包含了2s内的音频卡顿数据
 @discussion    SDK通过此回调通知APP音频卡顿数据。app层可以精确的计算出每200ms的卡顿情况
 */
- (void)onAudioStuttes:(NSArray *)msgList;
@end
/*!
 @discussion    平台部事件收集器委托协议。
 */
@protocol QAVEventSuttersDelegate <NSObject>
/*!
 @abstract      音频卡顿数据的回调。
 @param         msg
 @discussion    SDK通过此回调通知APP音频卡顿数据。app层可以精确的计算出每200ms的卡顿情况
 */
- (void)onAVEventStuttes:(QAVEventStutterFrame *)msg;
@end
#endif
