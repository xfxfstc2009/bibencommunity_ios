//
//  QAVExtenfForiLive.h
//  QAVSDK
//
//  Created by lokiyu on 2017/8/14.
//  Copyright © 2017年 loki. All rights reserved.
//

#pragma once

#import <Foundation/Foundation.h>
#import "QAVExtend.h"


/**
 @brief
 */
@interface QAVRoomIntServerInfoForiLive: NSObject{
}

@property(assign, nonatomic) UInt32 ip;    ///<
@property(assign,nonatomic) UInt16 port;    ///<
@property(assign, nonatomic) BOOL tcp;  ///<

@end


/**
 @brief
 */
@interface QAVMultiRoomExtendForiLive : QAVMultiRoomExtend{
    
}

+(QAVMultiRoomExtendForiLive*)CreateQAVMultiRoomExtendForiLive;


/**
 @brief
 */
-(NSArray*)getIntServerInfoList;

@end

