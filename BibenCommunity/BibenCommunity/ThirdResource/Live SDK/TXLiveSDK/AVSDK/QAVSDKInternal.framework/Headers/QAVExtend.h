//
//  QAVAudioCtrl.h
//  QAVSDK
//
//  Created by zhuojuntian on 16/1/20.
//  Copyright (c) 2016年 zhuojuntian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QAVSDK/QAVContext.h"
#import "QAVSDK/QAVRoomMulti.h"
#import "QAVSDK/QAVError.h"


/**
 @brief 
 */
@interface QAVExtend : NSObject

@end

/**
 @brief
 */
@interface QAVContextExtend : QAVExtend{
    
}

@property (retain,nonatomic)QAVContext* context;  ///<

@end

/**
 @brief
 */
@interface QAVMultiRoomExtend : QAVExtend{

}

@property (retain,nonatomic)QAVRoomMulti* room;  ///<

@end

/**
 @brief
 */
@interface QAVEndpointExtend : QAVExtend{
    
}

@property (retain,nonatomic)QAVEndpoint* endpoint;  ///<

@end
