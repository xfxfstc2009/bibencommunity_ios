//
//  ILiveEventListener.h
//  ILiveSDK
//
//  Created by ericxwli on 2018/8/1.
//  Copyright © 2018年 AlexiChen. All rights reserved.
//

#ifndef ILiveEventListener_h
#define ILiveEventListener_h

#import <Foundation/Foundation.h>
#import <QAVSDK/QAVVideoCtrl.h>

@protocol ILiveEventListener <NSObject>
@optional

/***********登录事件回调***********/

/**
 *  登录成功事件
 *
 *  param  userId  用户id
 */
- (void)onLoginSuccess:(NSString *)userId;

/**
 *  登录失败事件
 *
 *  param  userId  用户id
 *  param  module  错误模块名
 *  param  errCode 错误码
 *  param  errMsg  错误描述
 */
- (void)onLoginFailed:(NSString *)userId module:(NSString *)module errCode:(int)errCode errMsg:(NSString *)errMsg;

/**
 *  登出成功事件
 *
 *  param  userId  用户id
 */
- (void)onLogoutSuccess:(NSString *)userId;

/**
 *  登出成功事件
 *
 *  param  userId  用户id
 *  param  module  错误模块名
 *  param  errCode 错误码
 *  param  errMsg  错误描述
 */
- (void)onLogoutFailed:(NSString *)userId module:(NSString *)module errCode:(int)errCode errMsg:(NSString *)errMsg;

/**
 *  账号下线事件
 *
 *  param  userId  用户id
 *  param  module  错误模块名
 *  param  errCode 错误码
 *  param  errMsg  错误描述
 */
- (void)onForceOffline:(NSString *)userId module:(NSString *)module errCode:(int)errCode errMsg:(NSString *)errMsg;

/**
 *  创建房间成功事件
 *
 *  param  roomId  房间号
 *  param  groupId 群组id
 */
- (void)onCreateRoomSuccess:(int)roomId groupId:(NSString *)groupId;

/**
 *  创建房间失败事件
 *
 *  param  roomId  房间号
 *  param  module  错误模块名
 *  param  errCode 错误码
 *  param  errMsg  错误描述
 */
- (void)onCreateRoomFailed:(int)roomId module:(NSString *)module errCode:(int)errCode errMsg:(NSString *)errMsg;

/**
 *  加入房间成功事件
 *
 *  param  roomId  房间号
 *  param  groupId 群组id
 */
- (void)onJoinRoomSuccess:(int)roomId groupId:(NSString *)groupId;

/**
 *  加入房间失败事件
 *
 *  param  roomId  房间号
 *  param  module  错误模块名
 *  param  errCode 错误码
 *  param  errMsg  错误描述
 */
- (void)onJoinRoomFailed:(int)roomId module:(NSString *)module errCode:(int)errCode errMsg:(NSString *)errMsg;

/**
 *  退出房间成功事件
 *
 *  param  roomId  房间号
 *  param  groupId 群组id
 */
- (void)onQuitRoomSuccess:(int)roomId groupId:(NSString *)groupId;

/**
 *  退出房间失败事件
 *
 *  param  roomId  房间号
 *  param  module  错误模块名
 *  param  errCode 错误码
 *  param  errMsg  错误描述
 */
- (void)onQuitRoomFailed:(int)roomId module:(NSString *)module errCode:(int)errCode errMsg:(NSString *)errMsg;

/**
 *  房间断开连接事件
 *
 *  param  roomId  房间号
 *  param  module  错误模块名
 *  param  errCode 错误码
 *  param  errMsg  错误描述
 */
- (void)onRoomDisconnected:(int)roomId module:(NSString *)module errCode:(int)errCode errMsg:(NSString *)errMsg;

/**
 *  聊天群组解散事件
 *
 *  param  roomId   房间号
 *  param  groupId  IM群组id
 */
- (void)onGroupDisband:(int)roomId groupId:(NSString *)groupId;

/**
 *  成员进入房间状态事件回调
 *
 *  param  roomId  房间号
 *  param  groupId 群组id
 *  param  userId  成员id
 */
- (void)onRoomMemberIn:(int)roomId groupId:(NSString *)groupid userId:(NSString *)userId;

/**
 *  成员退出房间状态事件回调
 *
 *  param  roomId  房间号
 *  param  groupId 群组id
 *  param  userId  成员id
 */
- (void)onRoomMemberOut:(int)roomId groupId:(NSString *)groupId userId:(NSString *)userId;

/**
 *  相机变更成功事件回调
 *
 *  param  cameraId  相机设备id  0:前置相机 1:后置相机
 *  param  enable    YES/打开 NO/关闭
 */
- (void)onCameraUpdate:(cameraPos)cameraId enable:(BOOL)enable;

/**
 *  相机变更失败事件回调
 *
 *  param  module  错误模块名
 *  param  errCode 错误码
 *  param  errMsg  错误描述
 */
- (void)onCameraFailed:(NSString *)module errCode:(int)errCode errMsg:(NSString *)errMsg;

/**
 *  视频上行开始事件回调
 *
 *  param  roomId  房间号
 *  param  videoType 视频类型 1:摄像头 2:屏幕 3:文件
 *  param  userId  用户id
 */
- (void)onRoomHasVideo:(int)roomId videoType:(int)type userId:(NSString *)userId;

/**
 *  视频上行结束事件回调
 *
 *  param  roomId  房间号
 *  param  videoType 视频类型 1:摄像头 2:屏幕 3:文件
 *  param  userId  用户id
 */
- (void)onRoomNoVideo:(int)roomId videoType:(int)type userId:(NSString *)userId;

/**
 *  音频上行开始事件回调
 *
 *  param  roomId  房间号
 *  param  userId  用户id
 */
- (void)onRoomHasAudio:(int)roomId userId:(NSString *)userId;

/**
 *  音频上行结束事件回调
 *
 *  param  roomId  房间号
 *  param  userId  用户id
 */
- (void)onRoomNoAudio:(int)roomId userId:(NSString *)userId;

/**
 *  视频首帧到达事件回调
 *
 *  param  errCode 视频类型 1:摄像头 2:屏幕 3:文件
 *  param  userId  用户id
 */
- (void)onRecvVideoEvent:(int)videoType userId:(NSString *)userId;

@end
#endif /* ILiveEventListener_h */
