//
//  SuperPlayerView+Private.h
//  TXLiteAVDemo
//
//  Created by annidyfeng on 2018/7/9.
//  Copyright © 2018年 Tencent. All rights reserved.
//

#ifndef SuperPlayerView_Private_h
#define SuperPlayerView_Private_h
#import "SuperPlayer.h"
#import "CFDanmakuView.h"
#import "SuperPlayerControlViewDelegate.h"
#import "TimeShiftHelper.h"

// 枚举值，包含水平移动方向和垂直移动方向
typedef NS_ENUM(NSInteger, PanDirection){
    PanDirectionHorizontalMoved, // 横向移动
    PanDirectionVerticalMoved    // 纵向移动
};



@interface SuperPlayerView () <UIGestureRecognizerDelegate,UIAlertViewDelegate,
TXVodPlayListener, TXLivePlayListener, CFDanmakuDelegate, SuperPlayerControlViewDelegate>

/** 滑杆 */
@property (nonatomic, strong) UISlider               *volumeViewSlider;
/** 用来保存快进的总时长 */
@property (nonatomic, assign) CGFloat                sumTime;
/** 定义一个实例变量，保存枚举值 */
@property (nonatomic, assign) PanDirection           panDirection;
/** 是否为全屏 */
@property (nonatomic, assign) BOOL                   isFullScreen;
/** 是否锁定屏幕方向 */
@property (nonatomic, assign) BOOL                   isLocked;
/** 是否在调节音量*/
@property (nonatomic, assign) BOOL                   isVolume;
/** 是否被用户暂停 */
@property (nonatomic, assign) BOOL                   isPauseByUser;
/** slider上次的值 */
@property (nonatomic, assign) CGFloat                sliderLastValue;
/** 是否再次设置URL播放视频 */
@property (nonatomic, assign) BOOL                   repeatToPlay;
/** 播放完了*/
@property (nonatomic, assign) BOOL                   playDidEnd;
/** 进入后台*/
@property (nonatomic, assign) BOOL                   didEnterBackground;
/** 单击 */
@property (nonatomic, strong) UITapGestureRecognizer *singleTap;
/** 双击 */
@property (nonatomic, strong) UITapGestureRecognizer *doubleTap;
/** 亮度view */
@property (nonatomic, strong) SuperPlayerSharedView  *brightnessView;

@property (nonatomic, strong) UIButton               *lockTipsBtn;


@property (nonatomic, strong) SuperPlayerControlView *controlView;
@property (nonatomic, strong) SuperPlayerModel       *playerModel;
@property (nonatomic, strong) SuperPlayerSubModel    *playerSubModel;
@property (nonatomic, assign) float                  seekTime;
@property (nonatomic, strong) NSString               *videoURL;

@property (nonatomic, strong) NSArray<SuperPlayerSubModel *> *resolutionArray;

// add for txvodplayer
@property BOOL  isLoaded;
@property (nonatomic, strong) CFDanmakuView *danmakuView;
@property (nonatomic, strong) NSDate *danmakuStartTime;

@property NSURLSessionDataTask *getInfoHttpTask;


@property (nonatomic) TimeShiftHelper *timeShiftHelper;
@property NSInteger   liveTimeCounter;
@property dispatch_source_t liveTimer;
@property (nonatomic) BOOL  isShiftPlayback;
@property NSInteger shiftStartTime;

/** 是否是直播流 */
@property BOOL isLive;

/** 腾讯点播播放器 */
@property (nonatomic, strong) TXVodPlayer                *vodPlayer;
/** 腾讯直播播放器 */
@property (nonatomic, strong) TXLivePlayer               *livePlayer;

@property NSDate *reportTime;

@end



#endif /* SuperPlayerView_Private_h */
