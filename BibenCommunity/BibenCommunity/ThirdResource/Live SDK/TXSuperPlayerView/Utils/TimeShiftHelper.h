//
//  TimeShiftHelper.h
//  TXLiteAVDemo
//
//  Created by annidyfeng on 2018/7/5.
//  Copyright © 2018年 Tencent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimeShiftHelper : NSObject

@property NSString *liveUrl;

@property NSDate   *startTime;

- (instancetype)initWithUrl:(NSString *)liveUrl;

- (void)getDuration:(void(^)(NSInteger timestamp))block;

- (NSString *)seekUrl:(NSInteger)seekTo;

@end
