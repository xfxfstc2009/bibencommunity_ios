//
//  TimeShiftHelper.m
//  TXLiteAVDemo
//
//  Created by annidyfeng on 2018/7/5.
//  Copyright © 2018年 Tencent. All rights reserved.
//

#import "TimeShiftHelper.h"
#import "SuperPlayer.h"
#import "UIView+MMLayout.h"

static NSDateFormatter *outputFormatter;

@implementation TimeShiftHelper {
    NSString *_fileId;
    NSString *_stream;
}

- (instancetype)initWithUrl:(NSString *)liveUrl
{
    self = [super init];
    
    _liveUrl = liveUrl;
    
//    _liveUrl = @"http://5815.liveplay.myqcloud.com/live/5815_62fe94d692ab11e791eae435c87f075e.flv?xxx=yyy";
    
    _startTime = [NSDate date];
    
    if (![self parseUrl])
        return nil;
    
    return self;
}

- (BOOL)parseUrl {
    
    NSURL *tmpUrl = [NSURL URLWithString:_liveUrl];
    if (!tmpUrl)
        return NO;
    
    _fileId = [[tmpUrl host] componentsSeparatedByString:@"."].firstObject;
    
    NSString *path = [tmpUrl path];
    if (![path hasPrefix:@"/live/"]) {
        return NO;
    }
    
    _stream = [path substringFromIndex:6];
    if ([_stream hasSuffix:@".flv"]) {
        _stream = [_stream substringToIndex:_stream.length-4];
    } else if ([_stream hasSuffix:@".m3u8"]) {
        _stream = [_stream substringToIndex:_stream.length-5];
    }
    
    if (!outputFormatter) {
        outputFormatter = [[NSDateFormatter alloc] init];
        [outputFormatter setDateFormat:@"yyyyMMddHHmmss"];
    }
    
    return TRUE;
}

- (void)getDuration:(void(^)(NSInteger timestamp))block
{
    NSString *url = [NSString stringWithFormat:@"http://playtimeshift.live.myqcloud.com/%@/%@/timeshift.m3u8?delay=90&appid=%ld&txKbps=0", _fileId, _stream, SuperPlayerGlobleConfigShared.appId];
    
    AFURLSessionManager *sessionManager = [[AFURLSessionManager alloc] init];
    AFHTTPResponseSerializer *responseSerializer = [AFHTTPResponseSerializer serializer];
    responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/vnd.apple.mpegurl", nil];
    
    sessionManager.responseSerializer = responseSerializer;
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    @m_weakify(self);
    NSURLSessionDataTask *dataTask = [sessionManager dataTaskWithRequest:request completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        @m_strongify(self);
        NSData *responseData = (NSData *)responseObject;
        NSString *texts = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        for (NSString *line in [texts componentsSeparatedByString:@"\n"]) {
            if ([line hasPrefix:@"#EXT-TX-TS-START-TIME:"]) {
                NSInteger timeVal = [[line substringFromIndex:@"#EXT-TX-TS-START-TIME:".length] integerValue];
                self.startTime = [NSDate dateWithTimeIntervalSince1970:timeVal];
                block(-[self.startTime timeIntervalSinceNow]);
                break;
            }
        }
        
        NSLog(@"%@ %@", responseObject, error);
    }];

    
    //开始下载
    [dataTask resume];
    [sessionManager invalidateSessionCancelingTasks:NO];
}

- (NSString *)seekUrl:(NSInteger)seekTo;
{
    NSDate *seekDate = [self.startTime dateByAddingTimeInterval:seekTo];
    NSString *seek = [outputFormatter stringFromDate:seekDate];
    
    NSString *url = [NSString stringWithFormat:@"http://playtimeshift.live.myqcloud.com/%@/%@/timeshift.m3u8?starttime=%@&appid=%ld&txKbps=0", _fileId, _stream, seek, SuperPlayerGlobleConfigShared.appId];
    
    return url;
}
@end
