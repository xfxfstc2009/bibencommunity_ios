//
//  LiveConstance.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/13.
//  Copyright © 2018年 币本. All rights reserved.
//

#ifndef LiveConstance_h
#define LiveConstance_h

// ** 互动直播
#define kTXLiveAppid 1400120680
#define kTXLiveAccountType 33297

// ** 云通信
#define kTXTIMAppid 1400130565
#define kTXTIMAccountType @"36053"

// ** 打开log打印
#define kIsOpenLog 0


#define RTCV_ClASS NSClassFromString(@"RTContainerController")
#define RTCNC_CLASS NSClassFromString(@"RTContainerNavigationController")

#endif /* LiveConstance_h */
