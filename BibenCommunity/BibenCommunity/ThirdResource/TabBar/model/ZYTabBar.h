//
//  ZYTabBar.h
//  自定义tabbarDemo
//
//  Created by tarena on 16/7/1.
//  Copyright © 2016年 张永强. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZYPathButton.h"

static CGFloat const bfPaperTabBar_tapCircleDiameterMedium = 200.f;
static CGFloat const bfPaperTabBar_tapCircleDiameterSmall = bfPaperTabBar_tapCircleDiameterMedium / 2.f;
static CGFloat const bfPaperTabBar_tapCircleDiameterLarge = bfPaperTabBar_tapCircleDiameterMedium * 1.8f;
static CGFloat const bfPaperTabBar_tapCircleDiameterDefault = -1.f;


@class ZYTabBar;
@protocol ZYTabBarDelegate <NSObject>
@required
-(void)centerItemBtnClick;
@end

@interface ZYTabBar : UITabBar
/** 点中button代理属性 */
@property (nonatomic , weak)id<ZYTabBarDelegate> tabbarDelegate;

@property (nonatomic , strong)ZYPathButton *plusBtn;                            /**< 按钮*/
/** 所有的弹出按钮 */
@property (nonatomic , strong)NSArray<ZYPathItemButton *> *pathButtonArray;

/** 弹出动画时间*/
@property (assign, nonatomic) NSTimeInterval basicDuration;
/** 设置弹出时是否旋转   */
@property (assign, nonatomic) BOOL allowSubItemRotation;

/**  设置底部弹出的半径，默认是105 */
@property (assign, nonatomic) CGFloat bloomRadius;

/**  设置散开的角度 */
@property (assign, nonatomic) CGFloat bloomAngel;

/**  设置中间的按钮是否旋转 */
@property (assign, nonatomic) BOOL allowCenterButtonRotation;


#pragma mark - 点击水波纹
@property (nonatomic) BOOL usesSmartColor;
@property UIColor *tapCircleColor;
@property UIColor *backgroundFadeColor;
@property BOOL rippleFromTapLocation;
@property CGFloat tapCircleDiameter;
@property UIColor *underlineColor;
@property CGFloat underlineThickness;
@property (nonatomic) BOOL showUnderline;
@property BOOL showTapCircleAndBackgroundFade;

-(void)addCenterBtn;                    /**< 添加按钮*/
-(void)centerShow;
@end
