//
//  ZYPathButton.h
//  ZYPathButton
//
//  Created by tang dixi on 30/7/14.
//  Copyright (c) 2014 Tangdxi. All rights reserved.
//

#import "ZYPathItemButton.h"

//@import UIKit;
//@import QuartzCore;
//@import AudioToolbox;
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <AudioToolbox/AudioToolbox.h>

@class ZYPathButton;

typedef NS_ENUM(NSUInteger, kZYPathButtonBloomDirection) {
    kZYPathButtonBloomDirectionTop = 1,
    kZYPathButtonBloomDirectionTopLeft = 2,
    kZYPathButtonBloomDirectionLeft = 3,
    kZYPathButtonBloomDirectionBottomLeft = 4,
    kZYPathButtonBloomDirectionBottom = 5,
    kZYPathButtonBloomDirectionBottomRight = 6,
    kZYPathButtonBloomDirectionRight = 7,
    kZYPathButtonBloomDirectionTopRight = 8,
};

@protocol ZYPathButtonDelegate <NSObject>


@optional

- (void)willPresentZYPathButtonItems:(ZYPathButton *)ZYPathButton;
- (void)didPresentZYPathButtonItems:(ZYPathButton *)ZYPathButton;
- (void)willDismissZYPathButtonItems:(ZYPathButton *)ZYPathButton;
- (void)didDismissZYPathButtonItems:(ZYPathButton *)ZYPathButton;
- (void)pathCenterButtonFoldDelegateManager;
- (void)pathCenterButtonBloomDelegateManager;
@end

@interface ZYPathButton : UIView <UIGestureRecognizerDelegate>
@property (weak, nonatomic) id<ZYPathButtonDelegate> delegate;
@property (assign, nonatomic) NSTimeInterval basicDuration;
@property (assign, nonatomic) BOOL allowSubItemRotation;
@property (assign, nonatomic) CGFloat bloomRadius;
@property (assign, nonatomic) CGFloat bloomAngel;
@property (assign, nonatomic) CGPoint ZYButtonCenter;
@property (assign, nonatomic) BOOL allowSounds;
@property (copy, nonatomic) NSString *bloomSoundPath;
@property (copy, nonatomic) NSString *foldSoundPath;
@property (copy, nonatomic) NSString *itemSoundPath;
@property (assign, nonatomic) BOOL allowCenterButtonRotation;
@property (strong, nonatomic) UIColor *bottomViewColor;
@property (assign, nonatomic) kZYPathButtonBloomDirection bloomDirection;
- (instancetype)initWithCenterImage:(UIImage *)centerImage highlightedImage:(UIImage *)centerHighlightedImage;
- (instancetype)initWithButtonFrame:(CGRect)centerButtonFrame centerImage:(UIImage *)centerImage highlightedImage:(UIImage *)centerHighlightedImage;
- (void)addPathItems:(NSArray *)pathItemButtons;

- (void)pathCenterButtonFold;
- (void)pathCenterButtonBloom;
@end
