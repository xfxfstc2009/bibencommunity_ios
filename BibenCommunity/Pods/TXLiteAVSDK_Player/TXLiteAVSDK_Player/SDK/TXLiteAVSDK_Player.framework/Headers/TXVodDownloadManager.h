//
//  TXVodDownloadManager.h
//  TXLiteAVSDK
//
//  Created by annidyfeng on 2017/12/25.
//  Copyright © 2017年 Tencent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TXPlayerAuthParams.h"

/**
 * 下载视频的清晰度
 */
typedef NS_ENUM(NSInteger, TXVodQuality) {
    TXVodQualityOD = 0, ///原画
    TXVodQualityFLU, ///流畅
    TXVodQualitySD, ///标清
    TXVodQualityHD, ///高清
    TXVodQualityFHD, ///全高清
    TXVodQuality2K, ///2K
    TXVodQuality4K, ///4K
};

/**
 * 下载错误码
 */
typedef NS_ENUM(NSInteger, TXDownloadError) {
    TXDownloadSuccess   = 0,
    TXDownloadAuthFaild = -5001,    //fileid鉴权失败
    TXDownloadNoFile    = -5003,    //无此清晰度文件
    TXDownloadFormatError = -5004,  //格式不支持
    TXDownloadDisconnet = -5005,    //网络断开
    TXDownloadHlsKeyError = -5006,  //获取HLS解密key失败
    TXDownloadPathError = -5007,    //下载目录访问失败
};

/**
 * 下载源，通过fileid方式下载
 */
@interface TXVodDownloadDataSource : NSObject
@property TXPlayerAuthParams *auth; // fileid信息
@property TXVodQuality quality; // 下载清晰度，默认原画
@property NSString *token;  // 如地址有加密，请填写token
@end

@interface TXVodDownloadMediaInfo : NSObject
@property TXVodDownloadDataSource *dataSource; // fileid下载对象（可选）
@property NSString *url;    // 下载地址
@property (nonatomic) int duration; //时长，ms
@property (nonatomic) int size;  // 文件总大小, byte
@property (nonatomic) int downloadSize;  // 已下载大小, byte
@property float progress;   // 进度
@property NSString *playPath;   // 播放路径，可传给TXVodPlayer播放
@end

/**
 * 下载回调
 */
@protocol TXVodDownloadDelegate <NSObject>
- (void)onDownloadStart:(TXVodDownloadMediaInfo *)mediaInfo;
- (void)onDownloadProgress:(TXVodDownloadMediaInfo *)mediaInfo;
- (void)onDownloadStop:(TXVodDownloadMediaInfo *)mediaInfo;
- (void)onDownloadFinish:(TXVodDownloadMediaInfo *)mediaInfo;
- (void)onDownloadError:(TXVodDownloadMediaInfo *)mediaInfo errorCode:(TXDownloadError)code errorMsg:(NSString *)msg;
@end


@interface TXVodDownloadManager : NSObject

/**
 * 下载任务回调
 */
@property (weak) id<TXVodDownloadDelegate> delegate;

/**
 * 全局单例接口
 */
+ (TXVodDownloadManager *)shareInstance;

/**
 * 设置下载文件的根目录.
 * @param path 目录地址，如不存在，将自动创建
 *
 *  @warn 开始下载前必须设置，否者不能下载
 */
- (void)setDownloadPath:(NSString *)path;

/**
 * 下载文件
 *  @param source 下载源。
 *  @return 成功返回下载对象，否者nil
 *
 *  @waring 目前只支持hls下载
 */
- (TXVodDownloadMediaInfo *)startDownload:(TXVodDownloadDataSource *)source;

/**
 * 下载文件
 *  @param url 下载地址
 *  @return 成功返回下载对象，否者nil
 *
 *  @waring 目前只支持hls下载，不支持master playlist
 */
- (TXVodDownloadMediaInfo *)startDownloadUrl:(NSString *)url;

/**
 * 停止下载
 *  @param media 停止下载对象
 */
- (void)stopDownload:(TXVodDownloadMediaInfo *)media;

/**
 * 删除下载产生的文件
 *
 *  @return 文件正在下载将无法删除，返回NO
 */
- (BOOL)deleteDownloadFile:(NSString *)playPath;

@end
